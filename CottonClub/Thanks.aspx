﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Thanks.aspx.cs" Inherits="CottonClub.Thanks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Gracias</title>
	    <link href="admin/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	</head>
	<body class="thanks">
		
		<div class="jumbotron vertical-center">
			<div class="container" id="thanks">
				<div class="col-sm-6 col-sm-offset-3 panel panel-default">
					<div class="panel-body text-center">
						<asp:Image ID="imgLogo" runat="server" ImageUrl="" Width="180px" Height="120px"/>
						<form id="form1" runat="server" method="post" class="form-thanks">
							<h1>GRACIAS</h1>
							<hr/>
							<p>EN BREVE TE LLEGARÁ<br/><small>un correo para tu pase al evento junto con las ligas de descarga de las aplicaciones oficiales.</small></p>
							<p><small>En caso de que no encuentres el correo, por favor revisa en tu bandeja de spam.</small></p>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>
