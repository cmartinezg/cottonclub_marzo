USE [master]
GO
/****** Object:  Database [CottonClub]    Script Date: 18/05/2017 07:50:22 a.m. ******/
CREATE DATABASE [CottonClub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CottonClub', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\CottonClub.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CottonClub_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\CottonClub_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CottonClub] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CottonClub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CottonClub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CottonClub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CottonClub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CottonClub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CottonClub] SET ARITHABORT OFF 
GO
ALTER DATABASE [CottonClub] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [CottonClub] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CottonClub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CottonClub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CottonClub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CottonClub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CottonClub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CottonClub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CottonClub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CottonClub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CottonClub] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CottonClub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CottonClub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CottonClub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CottonClub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CottonClub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CottonClub] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CottonClub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CottonClub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CottonClub] SET  MULTI_USER 
GO
ALTER DATABASE [CottonClub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CottonClub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CottonClub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CottonClub] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CottonClub]
GO
/****** Object:  StoredProcedure [dbo].[uso_InsertEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[uso_InsertEvent] 
(
@Name as nvarchar(100), 
@Location as text, 
@WelcomeEmail as text, 
@Logo as nvarchar(100), 
@Date as datetime, 
@CalculatorDisplayTime as time(7), 
@UserModify as nvarchar(50)
) 
as 
Insert into Events 
(
Name, Location, WelcomeEmail, Logo, Date, CalculatorDisplayTime, UserModify, DatetimeModify, Active
) values
(
@Name , 
@Location , 
@WelcomeEmail , 
@Logo,
@Date,
@CalculatorDisplayTime,
@UserModify, 
GETDATE(), 
1
)

GO
/****** Object:  StoredProcedure [dbo].[usp_checkifuserexists]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_checkifuserexists] (@username as nvarchar(100), @Id as int)
AS
SELECT Id, Username, Password, FirstName, LastName, CorreoElectronico, UserModify 
FROM Users WHERE CorreoElectronico=@username
AND Id <> @Id



GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteCotizaciones]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DeleteCotizaciones] (@Id as int)
as
Delete from Calc_Historico where IdCotizacion=@Id
Delete from Calc_Historico_Productos where IdCotizacion=@Id
Delete from Calc_Historico_Semillas where IdCotizacion=@Id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DeleteDiary](@id as int)
as
Delete from Diary where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DeleteEvent](@id as int)
as
Delete from Events where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteGamificacion]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DeleteGamificacion] (@id as int)
as
Delete from Gamification where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteIncidenciaProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteIncidenciaProducto]
(
@Id AS INT
)
AS

DELETE Calc_Incidencias 
WHERE Id = @Id
DELETE Calc_Incidencias_Productos
WHERE IdIncidencia = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DeleteInformative](@id as int)
as
Delete from [Informative] where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeletePorcentaje]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeletePorcentaje]
(
@Id AS INT
)
AS

DELETE Calc_Productos_Porcentaje 
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteProducto]
(
@Id AS INT
)
AS

DELETE Calc_Productos 
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteRegalo]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteRegalo]
(
@Id AS INT
)
AS

DELETE Calc_Regalos 
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteUser]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_DeleteUser](@id as int)
as
Delete from Users where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteVariedad]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteVariedad]
(
@Id AS INT
)
AS

DELETE Calc_Variedades 
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCompanions]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InsertCompanions]
(
@PIN as nvarchar(7),
@Nombre1 as nvarchar(100),
@Edad1 as int,
@Nombre2 as nvarchar(100),
@Edad2 as int,
@Nombre3 as nvarchar(100),
@Edad3 as int,
@Nombre4 as nvarchar(100),
@Edad4 as int,
@Telefono as nvarchar(50),
@Ocupacion as nvarchar(100),
@Estado as nvarchar(50),
@Municipio as nvarchar(100),
@Distribuidor as int
)
as
Declare 
@Registrado as int,
@IdUsuario as bigint,
@Contador as int=0

Select @Registrado=isnull(PreRegister,0), @IdUsuario=Id from [Users] where PIN=@PIN

if(@Registrado=0)
	begin
		if(@Nombre1<>'')
			begin
				insert into [Companions] (IdUser, IdEvento, Companion, Age) 
				values
				(@IdUsuario,(Select top 1 Id from Events where Active=1),@Nombre1,@Edad1)
				set @Contador=@Contador+1
			end
		if(@Nombre2<>'')
			begin
				insert into [Companions] (IdUser, IdEvento, Companion, Age) 
				values
				(@IdUsuario,(Select top 1 Id from Events where Active=1),@Nombre2,@Edad2)
				set @Contador=@Contador+1
			end
		if(@Nombre3<>'')
			begin
				insert into [Companions] (IdUser, IdEvento, Companion, Age) 
				values
				(@IdUsuario,(Select top 1 Id from Events where Active=1),@Nombre3,@Edad3)
				set @Contador=@Contador+1
			end
		if(@Nombre4<>'')
			begin
				insert into [Companions] (IdUser, IdEvento, Companion, Age) 
				values
				(@IdUsuario,(Select top 1 Id from Events where Active=1),@Nombre4,@Edad4)
				set @Contador=@Contador+1
			end
	update 
	[Users] 
	set 
	Companions=@Contador, 
	PreRegister=1,
	Telefono=@Telefono,
	Ocupacion=@Ocupacion,
	Estado=@Estado,
	Municipio=@Municipio,
	IdDistribuidor=@Distribuidor
	where PIN=@PIN
	end

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCotizacion]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InsertCotizacion]
(
@token as nvarchar(100),
@cotizacion as nvarchar(256),
@total as decimal (18,2),
@id as int output
)
AS
INSERT INTO Calc_Historico
(
IdUsuario,
Cotizacion,
IdEvento,
FechaHr,
Total
) 
VALUES 
(
(SELECT IdUser FROM Tokens WHERE Token = @token),
@cotizacion,
(SELECT Id FROM Events WHERE Active = 1),
GETDATE(),
@total
)

SELECT @id = @@IDENTITY

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCotizacionProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InsertCotizacionProducto]
(
@idCotizacion as int,
@idProducto as int,
@cantidad as bigint,
@total as decimal(18,2)
)
AS
INSERT INTO Calc_Historico_Productos
(
IdCotizacion,
IdProducto,
Cantidad,
Total
) 
VALUES 
(
@idCotizacion,
@idProducto,
@cantidad,
@total
)

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCotizacionSemilla]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InsertCotizacionSemilla]
(
@idCotizacion as int,
@idSemilla as int,
@cantidad as bigint,
@total as decimal(18,2)
)
AS
INSERT INTO Calc_Historico_Semillas
(
IdCotizacion,
IdSemilla,
Cantidad,
Total
) 
VALUES 
(
@idCotizacion,
@idSemilla,
@cantidad,
@total
)


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertDiary]
(
@Name AS NVARCHAR(100), 
@Lastname AS NVARCHAR(100), 
@ConferenceDescription AS TEXT, 
@ConferenceTime AS TIME(7), 
@InformationLecturer AS TEXT, 
@PhotoLecturer AS NVARCHAR(100), 
@AttachedImageLecturer AS NVARCHAR(100), 
@Presentation AS NVARCHAR(100),
@IdEvent AS BIGINT,
@ConferenceTimeEnd AS TIME(7),
@ConferenceDate AS DATE,
@Ubication AS NVARCHAR(250) = '',
@Detail AS BIT = 0,
@Active as int = 1
)
AS
INSERT INTO Diary 
(
Name, 
Lastname, 
ConferenceDescription, 
ConferenceTime, 
InformationLecturer, 
PhotoLecturer, 
AttachedImageLecturer, 
Presentation,
IdEvent,
ConferenceTimeEnd,
ConferenceDate,
Ubication,
Detail,
Active
) 
VALUES 
(
@Name, 
@Lastname, 
@ConferenceDescription, 
@ConferenceTime, 
@InformationLecturer, 
@PhotoLecturer, 
@AttachedImageLecturer, 
@Presentation,
@IdEvent,
@ConferenceTimeEnd,
@ConferenceDate,
@Ubication,
@Detail,
@Active
)


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertEvent] 
(
@Name AS NVARCHAR(100), 
@Location AS TEXT, 
@WelcomeEmail AS TEXT, 
@Logo AS NVARCHAR(100), 
@Date AS DATETIME, 
@CalculatorDisplayTime AS TIME(7), 
@UserModify AS NVARCHAR(50),
@Address AS TEXT,
@Mapa AS NVARCHAR(100),
@Latitude AS NVARCHAR(100),
@Length AS NVARCHAR(100),
@Gamification AS BIT = 0,
@Calculator AS BIT = 0,
@Survey AS BIT = 0,
@GamificationTime AS TIME(7), 
@SurveyTime AS TIME(7) 
) 
AS 
INSERT INTO Events 
(
Name, Location, WelcomeEmail, Logo, Date, CalculatorDisplayTime, UserModify, DatetimeModify, Active, Address,Mapa,Latitude,Length, Gamification, Calculator, Survey,GamificationTime,SurveyTime
) VALUES
(
@Name , 
@Location , 
@WelcomeEmail , 
@Logo,
@Date,
@CalculatorDisplayTime,
@UserModify, 
GETDATE(), 
1,
@Address,
@Mapa,
@Latitude,
@Length,
@Gamification,
@Calculator,
@Survey,
@GamificationTime,
@SurveyTime
)


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InsertGamification]
(
@Pregunta as text, 
@Respuesta1 as text, 
@Respuesta2 as text, 
@Respuesta3 as text, 
@RespuestaCorrecta as int,
@IdEvento as bigint, 
@UsuarioModificacion as nvarchar(100),
@Pregunta2 as text, 
@Respuesta2_1 as text, 
@Respuesta2_2 as text, 
@Respuesta2_3 as text, 
@RespuestaCorrecta2 as int,
@Pregunta3 as text, 
@Respuesta3_1 as text, 
@Respuesta3_2 as text, 
@Respuesta3_3 as text, 
@RespuestaCorrecta3 as int,
@Puntos as decimal(18,0), 
@PuntosFallo as decimal(18,0),
@PuntosFallo3er as decimal(18,0) 
)
as
insert into [Gamification] 
(
Pregunta, 
Respuesta1, 
Respuesta2, 
Respuesta3, 
IdEvento, 
FechaCreacion,
FechaModificacion,
UsuarioModificacion,
Activo,
RespuestaCorrecta,
Pregunta2, 
Respuesta2_1, 
Respuesta2_2, 
Respuesta2_3, 
RespuestaCorrecta2,
Pregunta3, 
Respuesta3_1, 
Respuesta3_2, 
Respuesta3_3, 
RespuestaCorrecta3,
Puntos,
PuntosFallo,
GUID,
PuntosFallo3er
) 
values
(
@Pregunta, 
@Respuesta1, 
@Respuesta2, 
@Respuesta3, 
@IdEvento, 
GETDATE(),
GETDATE(),
@UsuarioModificacion,
1,
@RespuestaCorrecta,
@Pregunta2, 
@Respuesta2_1, 
@Respuesta2_2, 
@Respuesta2_3, 
@RespuestaCorrecta2,
@Pregunta3, 
@Respuesta3_1, 
@Respuesta3_2, 
@Respuesta3_3, 
@RespuestaCorrecta3,
@Puntos, 
@PuntosFallo,
replace(newid(),'-',''),
@PuntosFallo3er
)



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertIncidenciaProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertIncidenciaProducto]
(
@Nombre AS NVARCHAR(50),
@Productos AS VARCHAR(8000)
)
AS
DECLARE @IdIncidencia AS INT
INSERT INTO Calc_Incidencias (Nombre)
VALUES(@Nombre)
SELECT @IdIncidencia = @@IDENTITY 

DELETE FROM Calc_Incidencias_Productos
WHERE IdIncidencia = @IdIncidencia

INSERT INTO Calc_Incidencias_Productos
(
IdIncidencia,
IdProducto
) 
SELECT @IdIncidencia, splitdata from dbo.fnSplitString(@Productos,',')

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InsertInformative]
(
@Title as text, 
@Description as text, 
@Image as nvarchar(100), 
@UserModify as nvarchar(100), 
@Active as int, 
@Event as bigint,
@URL as text
)
as
insert into Informative 
(
Title, 
Description, 
Image, 
UserModify, 
DatetimeModify, 
Active, 
Event,
URL
) 
values 
(
@Title, 
@Description, 
@Image, 
@UserModify, 
GETDATE(), 
@Active, 
@Event,
@URL
)

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertPorcentaje]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InsertPorcentaje]
(
@Cantidad AS INT,
@Paquetes AS INT,
@Porcentaje AS DECIMAL (18,2),
@PorcentajeSemilla AS DECIMAL (18,2),
@MontoProducto AS DECIMAL (18,2)
)
AS
INSERT INTO Calc_Productos_Porcentaje
(
Cantidad,
Paquetes,
Porcentaje,
PorcentajeSemilla,
MontoProducto
) 
VALUES 
(
@Cantidad,
@Paquetes,
@Porcentaje,
@PorcentajeSemilla,
@MontoProducto
)



GO
/****** Object:  StoredProcedure [dbo].[usp_InsertProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertProducto]
(
@Nombre AS NVARCHAR(50),
@PrecioRegular AS DECIMAL(18,2),
@PrecioPreventa AS DECIMAL(18,2),
@Has AS DECIMAL(18,2),
@CantPorPaquete AS INT
)
AS
INSERT INTO Calc_Productos
(
Nombre,
PrecioRegular,
PrecioPreventa,
Has,
CantPorPaquete
) 
VALUES 
(
@Nombre,
@PrecioRegular,
@PrecioPreventa,
@Has,
@CantPorPaquete
)


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertRegalo]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertRegalo]
(
@Nombre AS NVARCHAR(50), 
@Cantidad AS INT,
@Sacos AS INT,
@Precio AS DECIMAL(18,2)
)
AS
INSERT INTO Calc_Regalos
(
Nombre,
Cantidad,
Sacos,
Precio
) 
VALUES 
(
@Nombre,
@Cantidad,
@Sacos,
@Precio
)


GO
/****** Object:  StoredProcedure [dbo].[usp_InsertUser]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InsertUser] 
(
@Username as nvarchar(50)
,@Password as nvarchar(50)
,@FirstName as nvarchar(100)
,@LastName as nvarchar(100)
,@CorreoElectronico as nvarchar(100)
,@Administrador as int
,@Photo as nvarchar(100)
,@Telefono as nvarchar(50)
,@Ocupacion as nvarchar(100)
,@Estado as nvarchar(50)
,@Municipio as nvarchar(100)
,@Distribuidor as int
)
as

 ---- Create the variables for the random number generation
DECLARE @Random nvarchar(7);
Select @Random=substring(replace(newid(),'-',''),1,7)

Insert into Users (Username, Password, FirstName, LastName, CorreoElectronico, UserModify, DateTimeCreate, DateTimeModify, Active, Administrador,Photo, PIN, Telefono, Ocupacion, Estado, Municipio, IdDistribuidor) 
values
(
@Username,
@Password,
@FirstName,
@LastName,
@CorreoElectronico,
'admin',
GETDATE(),
GETDATE(),
1,
@Administrador
,@Photo
,@Random
,@Telefono
,@Ocupacion
,@Estado
,@Municipio
,@Distribuidor
)

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertVariedad]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InsertVariedad]
(
@Nombre AS NVARCHAR(50),
@PrecioRegular AS DECIMAL(18,2),
@PrecioPreventa AS DECIMAL(18,2),
@Sacos AS INT,
@SemillasPorSaco AS INT,
@Has AS DECIMAL(18,2)
)
AS
INSERT INTO Calc_Variedades
(
Nombre,
PrecioRegular,
PrecioPreventa,
Sacos,
SemillasPorSacos,
HasPorSaco
) 
VALUES 
(
@Nombre,
@PrecioRegular,
@PrecioPreventa,
@Sacos,
@SemillasPorSaco,
@Has
)


GO
/****** Object:  StoredProcedure [dbo].[usp_PublicRanking]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_PublicRanking] (@IdEvent as bigint)
as
declare 
@baseURL as nvarchar(100)

Select @baseURL=baseURL from Config

Select top 10  
IDENTITY(INT, 1, 1) AS RankPos,
Concat(u.FirstName,' ',u.LastName) as Fullname, 
CONCAT(@baseURL,'photos/',Photo) as Photo,
sum(s.Points) as Score 
INTO #ScoreTemp
from [Score] as s inner join [Users] as u on s.IdUser=u.Id 
where s.IdEvent in (Select Id from Events where Active=1)
group by Concat(u.FirstName,' ',u.LastName), Photo
order by sum(s.Points) desc

select * from  #ScoreTemp
drop table #ScoreTemp

GO
/****** Object:  StoredProcedure [dbo].[usp_Ranking]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Ranking] (@IdEvent as bigint)
as

Select 
IDENTITY(INT, 1, 1) AS RankPos,
Concat(P.FirstName,' ',P.LastName) as Fullname, 
CONCAT('http://cottonclub.motti.mx/admin/photos/',P.Photo) as Photo,
sum(P.Points) as Score 
INTO #ScoreTemp
from 
(
select Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
(
SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
FROM Users u LEFT JOIN 
Score as s on u.Id=s.IdUser and IdEvent =@IdEvent and IdAnswer=0 and IdQuestion=0
GROUP BY u.ID, u.FirstName, u.LastName, Photo
union all
SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
FROM Users u LEFT JOIN 
answers as a on u.Id=a.IdUsuario
where a.IdUsuario=u.Id and a.IdEvento =@IdEvent
GROUP BY u.ID, u.FirstName, u.LastName, Photo
) Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
) P
group by Concat(P.FirstName,' ',P.LastName), P.Photo
order by sum(P.Points) desc

select * from  #ScoreTemp
drop table #ScoreTemp

GO
/****** Object:  StoredProcedure [dbo].[usp_ReportDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportDiary] (@IdEvent as bigint)
as
Select cast( d.ConferenceDescription as nvarchar(1000)) as Agenda,count(s.Respuesta6) as Votos 
from dbo.Diary as d 
left join dbo.Survey as s on d.Id=s.Respuesta6 and d.IdEvent=s.IdEvento
where d.IdEvent=@IdEvent 
group by cast( d.ConferenceDescription as nvarchar(1000))

GO
/****** Object:  StoredProcedure [dbo].[usp_ReportGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportGamification] (@IdEvent as bigint)
as
Select
Cast(Pregunta as nvarchar(1000)) as Pregunta, sum(PuntosGanados) as PuntosGanados,sum(PVez) as PVez, sum(SVez) as SVez 
from
(
Select Cast(g.Pregunta as nvarchar(1000)) as Pregunta, a.PuntosGanados,0 PVez,0 as SVez from dbo.Gamification as g 
left join dbo.Answers a on g.GUID=a.GUID and g.IdEvento=@IdEvent 
where a.IdUsuario not in (Select Id from [Users] where [Administrador]=1)
union all
Select Cast(g.Pregunta as nvarchar(1000)) as Pregunta, 0 as PuntosGanados,count(a.IdUsuario) as PVez,0 as SVez from dbo.Gamification as g 
left join dbo.Answers a on g.GUID=a.GUID and g.IdEvento=@IdEvent 
where a.Intento=1 and a.IdUsuario not in (Select Id from [Users] where [Administrador]=1)
group by Cast(g.Pregunta as nvarchar(1000))
union all
Select Cast(g.Pregunta as nvarchar(1000)) as Pregunta, 0 as PuntosGanados,0 as PVez,count(a.IdUsuario) as SVez from dbo.Gamification as g 
left join dbo.Answers a on g.GUID=a.GUID  and g.IdEvento=@IdEvent
where a.Intento=2 and a.IdUsuario not in (Select Id from [Users] where [Administrador]=1)
group by Cast(g.Pregunta as nvarchar(1000))
) as Datos 
group by Cast(Pregunta as nvarchar(1000))

GO
/****** Object:  StoredProcedure [dbo].[usp_ReportProducts]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportProducts] (@IdEvent as bigint)
as
Declare 
@valores as VARCHAR(1000),
@baseURL as nvarchar(100)
Select @baseURL=baseURLRoot from ConfigRoot

SELECT RIGHT('00000000'+CAST(ch.IdCotizacion AS VARCHAR(3)),8) AS Folio, UPPER(CONCAT(u.FirstName, ' ', u.LastName)) AS Nombre, d.Decription AS Distribuidor, 
cp.Nombre as Producto, hp.Cantidad, 
CONCAT('$', CONVERT(varchar, CAST(hp.Total AS money), 1)) AS Total, 
ch.FechaHr,
CONCAT(@baseURL,'ws/cotizaciones/', REPLACE(ch.Cotizacion, '.pdf', '_cotizacion.pdf')) AS cotizacion
FROM Calc_Historico_Productos AS hp 
LEFT JOIN Calc_Historico AS ch ON hp.IdCotizacion = ch.IdCotizacion
LEFT JOIN Calc_Productos AS cp ON cp.id = hp.IdProducto
LEFT JOIN Users AS u ON u.id = ch.idUsuario  
LEFT JOIN Distributor AS d ON u.idDistribuidor = d.id
WHERE u.Administrador != 1 AND ch.idEvento = @IdEvent


GO
/****** Object:  StoredProcedure [dbo].[usp_ReportSeeds]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportSeeds] (@IdEvent as bigint)
as
Declare 
@valores as VARCHAR(1000),
@baseURL as nvarchar(100)
Select @baseURL=baseURLRoot from ConfigRoot

SELECT RIGHT('00000000'+CAST(ch.IdCotizacion AS VARCHAR(3)),8) AS Folio, UPPER(CONCAT(u.FirstName, ' ', u.LastName)) AS Nombre, d.Decription AS Distribuidor, 
cp.Nombre as Variedad, hp.Cantidad, 
CONCAT('$', CONVERT(varchar, CAST(hp.Total AS money), 1)) AS Total, 
ch.FechaHr,
CONCAT(@baseURL,'ws/cotizaciones/', REPLACE(ch.Cotizacion, '.pdf', '_cotizacion.pdf')) AS cotizacion
FROM Calc_Historico_Semillas AS hp 
LEFT JOIN Calc_Historico AS ch ON hp.IdCotizacion = ch.IdCotizacion
LEFT JOIN Calc_Variedades AS cp ON cp.id = hp.IdSemilla
LEFT JOIN Users AS u ON u.id = ch.idUsuario  
LEFT JOIN Distributor AS d ON u.idDistribuidor = d.id
WHERE u.Administrador != 1 AND ch.idEvento = @IdEvent


GO
/****** Object:  StoredProcedure [dbo].[usp_ReportSurvey]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportSurvey] (@IdEvent as bigint)
as
declare
@total as int
Select @total=case count(*) when 0 then 1 else count(*) end
from dbo.Survey as s  where s.IdEvento=@IdEvent

CREATE TABLE #tablaTemporalSurvey( Id INT , Description NVARCHAR(500) )
insert into 
#tablaTemporalSurvey
select 
1,'Por favor indique que tan satisfecho quedo con el evento:'
union
select 
2,'¿Qué le pareció la planeación y coordinación del evento?'
union
select 
3,'¿Qué tal le pareció la logística de traslados para llegar y regresar del evento?'
union
select 
4,'¿Qué tal le parecieron las instalaciones y alimentos de la sede?'
union
select 
5,'¿Fue relevante para usted el contenido de las presentaciones?'

Select (select Description from #tablaTemporalSurvey where Id=1) as Pregunta,
isnull(
(Select sum(Respuesta1) 
from dbo.Survey as s  where s.IdEvento=@IdEvent and s.IdUsuario not in (Select Id from [Users] where [Administrador]=1) )
/
(@total),0) as Promedio
union all
Select (select Description from #tablaTemporalSurvey where Id=2) as Pregunta,
isnull(
(Select sum(Respuesta2) 
from dbo.Survey as s  where s.IdEvento=@IdEvent  and s.IdUsuario not in (Select Id from [Users] where [Administrador]=1))
/
(@total),0) as Promedio
union all
Select (select Description from #tablaTemporalSurvey where Id=3) as Pregunta,
isnull(
(Select sum(Respuesta3) 
from dbo.Survey as s  where s.IdEvento=@IdEvent  and s.IdUsuario not in (Select Id from [Users] where [Administrador]=1))
/
(@total),0) as Promedio
union all
Select (select Description from #tablaTemporalSurvey where Id=4) as Pregunta,
isnull(
(Select sum(Respuesta4) 
from dbo.Survey as s  where s.IdEvento=@IdEvent  and s.IdUsuario not in (Select Id from [Users] where [Administrador]=1))
/
(@total),0) as Promedio
union all
Select (select Description from #tablaTemporalSurvey where Id=5) as Pregunta,
isnull(
(Select sum(Respuesta5) 
from dbo.Survey as s  where s.IdEvento=@IdEvent  and s.IdUsuario not in (Select Id from [Users] where [Administrador]=1))
/
(@total),0) as Promedio

drop table #tablaTemporalSurvey


GO
/****** Object:  StoredProcedure [dbo].[usp_ReportSurveyComments]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportSurveyComments] (@IdEvent as bigint)
as

Select Concat(u.Firstname,' ',u.LastName) Nombre, s.Comentarios from dbo.Survey as s 
inner join dbo.Users as u on s.IdUsuario=u.Id 
where s.IdEvento=@IdEvent
order by Concat(u.Firstname,' ',u.LastName)

GO
/****** Object:  StoredProcedure [dbo].[usp_ReportUsers]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ReportUsers] (@IdEvent as bigint)
as
Declare 
@valores as VARCHAR(1000),
@baseURL as nvarchar(100)
Select @baseURL=baseURLRoot from ConfigRoot

CREATE TABLE #tablaTemporal( IdUser INT , Companions NVARCHAR(400) )

DECLARE @IdUser AS bigint
DECLARE crsUsuarios CURSOR FOR SELECT Id FROM dbo.Users
OPEN crsUsuarios
FETCH NEXT FROM crsUsuarios INTO @IdUser
WHILE @@fetch_status = 0
BEGIN
	set @valores=''
	SELECT @valores= COALESCE(@valores + ', ', '') + Datos.Datos FROM (Select [Companion] as Datos from [Companions] where IdUser=@IdUser and IdEvento=@IdEvent) as Datos
	insert into #tablaTemporal (IdUser,Companions) values (@IdUser,@valores)
    FETCH NEXT FROM crsUsuarios INTO @IdUser
END
CLOSE crsUsuarios
DEALLOCATE crsUsuarios

Select 
u.FirstName as 'Nombres',
u.LastName as 'Apellido',
u.CorreoElectronico as 'Correo',
u.PIN as 'PIN',
Concat(@baseURL,'Register.aspx?data=',u.PIN) as 'URLRegistro',
isnull((Select sum(a.PuntosGanados) from dbo.Answers as a where a.IdUsuario=u.Id and a.IdEvento=@IdEvent),0) as 'Puntos',
t.Companions as 'Acompaniantes',
(Select count(*) from dbo.Diary where IdEvent=@IdEvent) as 'Actividades',
0 as 'Calculadoras'
from 
dbo.Users as u 
left join #tablaTemporal as t on u.Id=t.IdUser 
where u.Id not in (Select Id from [Users] where [Administrador]=1)
drop table #tablaTemporal

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcGraficaDetalleProductos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcGraficaDetalleProductos]
@cantProductos INT,
@IdEvento BIGINT 
AS
SELECT Productos, sum(Total) as Total FROM (
select h.IdCotizacion, h.Total, h.IdEvento, 
    STUFF(( SELECT  distinct ', ' + CONVERT(VARCHAR(30), x.Nombre)
            FROM   (select p.*,pd.Nombre, ROW_NUMBER() OVER (ORDER BY p.IdProducto DESC) AS RowNum 
			from Calc_Historico_Productos p, Calc_Productos pd
            WHERE pd.Id = p.IdProducto
			AND p.IdCotizacion = h.IdCotizacion
			 ) x
            FOR XML PATH('')
            ), 1, 2, '')  Productos,
			count(p.IdProducto) AS CantProductos
from Calc_Historico h
LEFT JOIN Calc_Historico_Productos p
ON p.IdCotizacion = h.IdCotizacion
GROUP BY h.IdCotizacion, h.Total, h.IdEvento
) r
WHERE CantProductos = @cantProductos 
and IdEvento=@IdEvento
group by (Productos)

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcGraficaDistribuidor]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcGraficaDistribuidor]
@IdEvento BIGINT 
AS

 SELECT SUM(h.Total) AS Total, d.Id as IdDistribuidor , d.Decription AS Distribuidor FROM 
	Calc_Historico h,
	Users u,
	Distributor d
WHERE u.Id = h.IdUsuario
AND d.Id = u.IdDistribuidor 
AND h.IdEvento=@IdEvento 
GROUP BY d.Id,d.Decription

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcGraficaProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectCalcGraficaProducto] (@eventid int,@distribuidorid int)
AS
if(@distribuidorid<>0)
begin
	Select cp.Nombre,sum(chp.Total) from Calc_Productos as cp
	inner join Calc_Historico_Productos as chp on cp.Id=chp.IdProducto
	inner join Calc_Historico as ch on chp.IdCotizacion=ch.IdCotizacion
	inner join dbo.Users as u on ch.IdUsuario=u.Id
	where
	ch.IdEvento=@eventid 
	and
	u.IdDistribuidor=@distribuidorid
	group by cp.Nombre
end
else
begin
	Select cp.Nombre,sum(chp.Total) from Calc_Productos as cp
	inner join Calc_Historico_Productos as chp on cp.Id=chp.IdProducto
	inner join Calc_Historico as ch on chp.IdCotizacion=ch.IdCotizacion
	inner join dbo.Users as u on ch.IdUsuario=u.Id
	where
	ch.IdEvento=@eventid 
	group by cp.Nombre
end


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcGraficaProductos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcGraficaProductos]
@IdEvento BIGINT 
AS

SELECT CantProductos, SUM(Total) AS Total FROM 
(
SELECT h.IdCotizacion, COUNT(p.IdProducto) AS CantProductos, h.Total FROM Calc_Historico h
LEFT JOIN Calc_Historico_Productos p on h.IdCotizacion = p.IdCotizacion 
Where h.IdEvento=@IdEvento
GROUP BY h.IdCotizacion, h.Total
) x
 GROUP BY CantProductos

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcIncidences]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SelectCalcIncidences] (@frase AS TEXT) AS

SELECT * FROM Calc_Incidencias
WHERE Nombre LIKE CONCAT('%',@frase,'%') 
ORDER BY Nombre


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcPorcentajes]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcPorcentajes] AS

SELECT Id, 
isnull(Cantidad,0) as Cantidad, 
isnull(Paquetes,0) as Paquetes, 
isnull(Porcentaje,0) as Porcentaje, 
isnull(PorcentajeSemilla,0) as PorcentajeSemilla, 
isnull(MontoProducto,0) as MontoProducto 
FROM Calc_Productos_Porcentaje
ORDER BY Cantidad, Paquetes

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcProductos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcProductos] (@frase AS TEXT) AS

SELECT * FROM Calc_Productos
WHERE Nombre LIKE CONCAT('%',@frase,'%') 
ORDER BY Nombre

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcRegalos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcRegalos] (@frase AS TEXT) AS

SELECT * FROM Calc_Regalos
WHERE Nombre LIKE CONCAT('%',@frase,'%') 
ORDER BY Nombre


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCalcVarieties]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectCalcVarieties] (@frase AS TEXT) AS

SELECT * FROM Calc_Variedades
WHERE Nombre LIKE CONCAT('%',@frase,'%') 
ORDER BY Nombre


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectCotizaciones]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectCotizaciones](@IdUsuario as bigint,@IdEvento as int)
as

Select IdCotizacion, Replace(concat('../ws/cotizaciones/',Cotizacion),'.pdf','_cotizacion.pdf') as Cotizacion,Replace(concat('../ws/cotizaciones/',Cotizacion),'.pdf','_contrato.pdf') as Contrato, FechaHr from Calc_Historico 
where 
IdUsuario=@IdUsuario 
and 
IdEvento =@IdEvento
order by FechaHr desc


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SelectDiary] (@parametro AS INT,@frase AS TEXT) AS
IF(@parametro=0)
BEGIN
	SELECT Id, Name, Lastname,CONCAT(Name,' ', Lastname) AS fullname, ConferenceDescription, ConferenceTime, InformationLecturer, Ubication,
	CASE ISNULL(PhotoLecturer,'')
	WHEN '' THEN 'photos/sinimagen.png' 
	ELSE 
	CONCAT('files/',PhotoLecturer) END AS PhotoLecturer, 
	CASE ISNULL(AttachedImageLecturer,'')
	WHEN '' THEN 'photos/sinimagen.png' 
	ELSE 
	CONCAT('files/',AttachedImageLecturer) END AttachedImageLecturer,Presentation, IdEvent,ConferenceTimeEnd,CONCAT(ConferenceTime,' - ',ConferenceTimeEnd) AS Time, ConferenceDate, 
	CASE WHEN Active IS NULL OR Active = 0 THEN 'No'
	ELSE 'Si'
	END Active,
	CASE Detail WHEN 1 THEN 'Si' ELSE 'No' END AS Detail
	 FROM [Diary]
	 WHERE 
	Name LIKE CONCAT('%',@frase,'%') 
	OR 
	Lastname LIKE CONCAT('%',@frase,'%') 
	OR 
	ConferenceDescription LIKE CONCAT('%',@frase,'%') 
	OR 
	InformationLecturer LIKE CONCAT('%',@frase,'%') 
	OR 
	Presentation LIKE CONCAT('%',@frase,'%')
	ORDER BY ConferenceTime
END
ELSE
BEGIN
	SELECT Id, Name, Lastname,CONCAT(Name,' ', Lastname) AS fullname, ConferenceDescription, ConferenceTime, InformationLecturer, Ubication,
	CASE ISNULL(PhotoLecturer,'')
	WHEN '' THEN 'photos/sinimagen.png' 
	ELSE 
	CONCAT('files/',PhotoLecturer) END AS PhotoLecturer, 
	CASE ISNULL(AttachedImageLecturer,'')
	WHEN '' THEN 'photos/sinimagen.png' 
	ELSE 
	CONCAT('files/',AttachedImageLecturer) END AttachedImageLecturer, Presentation, IdEvent,ConferenceTimeEnd,CONCAT(ConferenceTime,' - ',ConferenceTimeEnd) AS Time, ConferenceDate, 
	CASE WHEN Active IS NULL OR Active = 0 THEN 'No'
	ELSE 'Si'
	END Active, 
	CASE Detail WHEN 1 THEN 'Si' ELSE 'No' END AS Detail
	FROM [Diary] 
	WHERE IdEvent IN (SELECT Id FROM Events WHERE Id=@parametro) 
	AND 
	(
	Name LIKE CONCAT('%',@frase,'%') 
	OR 
	Lastname LIKE CONCAT('%',@frase,'%') 
	OR 
	ConferenceDescription LIKE CONCAT('%',@frase,'%') 
	OR 
	InformationLecturer LIKE CONCAT('%',@frase,'%') 
	OR 
	Presentation LIKE CONCAT('%',@frase,'%')
	)
	ORDER BY ConferenceTime
END


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectDistributor]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectDistributor] (@parametro AS NVARCHAR(100))
AS
SELECT  Id, Decription 
FROM Distributor
ORDER BY Id


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEscalarUser]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectEscalarUser](@username AS NVARCHAR(100),@password AS NVARCHAR(50))
AS
SELECT COUNT(*) AS Existe FROM [Users] 
WHERE CorreoElectronico=@username AND PASSWORD=@password
		AND Active = 1 AND Administrador = 1


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectEvents]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SelectEvents] (@parametro AS NVARCHAR(100))
AS
SELECT  Id, Name, Location, WelcomeEmail, Logo, Date, CalculatorDisplayTime, 
UserModify, DatetimeModify, CASE Active WHEN 1 THEN 'Si' ELSE 'No' END AS Active, 
Address, Latitude, Length  
FROM Events
WHERE 
Name LIKE CONCAT('%',@parametro,'%') 
OR 
Location LIKE CONCAT('%',@parametro,'%') 
OR 
WelcomeEmail LIKE CONCAT('%',@parametro,'%') 
OR 
Address LIKE CONCAT('%',@parametro,'%')
ORDER BY Date


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectGamification] (@parametro AS NVARCHAR(500), @idEvento AS INT)
AS
Declare 
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

IF @idEvento = 0
	SELECT 
	g.Id AS Id, 
	CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') AS CodigoQR,
	g.Pregunta AS Pregunta, 
	g.Respuesta1 AS Respuesta1, 
	g.Respuesta2 AS Respuesta2, 
	g.Respuesta3 AS Respuesta3, 
	g.RespuestaCorrecta AS RespuestaCorrecta, 
	g.Pregunta2 AS Pregunta2, 
	g.Respuesta2_1 AS Respuesta2_1, 
	g.Respuesta2_2 AS Respuesta2_2, 
	g.Respuesta2_3 AS Respuesta2_3, 
	g.RespuestaCorrecta2 AS RespuestaCorrecta2, 
	g.Pregunta3 AS Pregunta3, 
	g.Respuesta3_1 AS Respuesta3_1, 
	g.Respuesta3_2 AS Respuesta3_2, 
	g.Respuesta3_3 AS Respuesta3_3, 
	g.RespuestaCorrecta3 AS RespuestaCorrecta3, 
	g.Puntos AS Puntos, 
	g.PuntosFallo AS PuntosFallo, 
	g.IdEvento AS IdEvento, 
	e.Name AS Name,
	g.FechaCreacion AS FechaCreacion, 
	g.FechaModificacion AS FechaModificacion, 
	g.UsuarioModificacion AS UsuarioModificacion, 
	g.Activo AS Activo,
	CASE g.Activo WHEN 1 THEN 'Si' ELSE 'No' END AS ActivoTexto,
	GUID
	FROM [Gamification] AS g 
	INNER JOIN [Events] AS e ON e.Id=g.IdEvento

	WHERE 
	(
		Pregunta LIKE CONCAT('%',@parametro,'%') 
		OR 
		Pregunta2 LIKE CONCAT('%',@parametro,'%') 
		OR 
		Pregunta3 LIKE CONCAT('%',@parametro,'%') 
	)
ELSE
	SELECT 
	g.Id AS Id, 
	CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') AS CodigoQR,
	g.Pregunta AS Pregunta, 
	g.Respuesta1 AS Respuesta1, 
	g.Respuesta2 AS Respuesta2, 
	g.Respuesta3 AS Respuesta3, 
	g.RespuestaCorrecta AS RespuestaCorrecta, 
	g.Pregunta2 AS Pregunta2, 
	g.Respuesta2_1 AS Respuesta2_1, 
	g.Respuesta2_2 AS Respuesta2_2, 
	g.Respuesta2_3 AS Respuesta2_3, 
	g.RespuestaCorrecta2 AS RespuestaCorrecta2, 
	g.Pregunta3 AS Pregunta3, 
	g.Respuesta3_1 AS Respuesta3_1, 
	g.Respuesta3_2 AS Respuesta3_2, 
	g.Respuesta3_3 AS Respuesta3_3, 
	g.RespuestaCorrecta3 AS RespuestaCorrecta3, 
	g.Puntos AS Puntos, 
	g.PuntosFallo AS PuntosFallo, 
	g.IdEvento AS IdEvento, 
	e.Name AS Name,
	g.FechaCreacion AS FechaCreacion, 
	g.FechaModificacion AS FechaModificacion, 
	g.UsuarioModificacion AS UsuarioModificacion, 
	g.Activo AS Activo,
	CASE g.Activo WHEN 1 THEN 'Si' ELSE 'No' END AS ActivoTexto,
	GUID
	FROM [Gamification] AS g 
	INNER JOIN [Events] AS e ON e.Id=g.IdEvento
	WHERE g.IdEvento = @idEvento
	AND
	(
		Pregunta LIKE CONCAT('%',@parametro,'%') 
		OR 
		Pregunta2 LIKE CONCAT('%',@parametro,'%') 
		OR 
		Pregunta3 LIKE CONCAT('%',@parametro,'%') 
	)



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectInformative] (@parametro AS NVARCHAR(500), @idEvento AS INT) 
AS
IF @idEvento = 0
	SELECT Id, Title, Substring(Description,0,200) as Description, Image,  UserModify, DatetimeModify, Active, 
	CASE Active 
	WHEN 1 then 'Sí' 
	ELSE 'No' 
	END AS ActiveText, 
	Event, URL 
	FROM Informative
	WHERE 
	(
		Title LIKE CONCAT('%',@parametro,'%') 
		OR 
		Description LIKE CONCAT('%',@parametro,'%') 
	)
ELSE
	SELECT Id, Title, Substring(Description,0,200) as Description, Image,  UserModify, DatetimeModify, Active, 
	CASE Active 
	WHEN 1 then 'Sí' 
	ELSE 'No' 
	END AS ActiveText, 
	Event, URL 
	FROM Informative
	WHERE Event = @idEvento
	AND
	(
		Title LIKE CONCAT('%',@parametro,'%') 
		OR 
		Description LIKE CONCAT('%',@parametro,'%') 
	)



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SelectSpecificDiary] (@id AS INT)
AS
SELECT 
Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer, PhotoLecturer, 
AttachedImageLecturer, Presentation, IdEvent,ConferenceTimeEnd, FORMAT( ConferenceDate,'yyyy-MM-dd') as ConferenceDate, Ubication, Detail, Active
FROM Diary 
WHERE Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificEvents]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SelectSpecificEvents] (@id AS INT)
AS
if(@id>0)
begin
SELECT Id, Name, Location, WelcomeEmail, Logo, FORMAT( Date,'yyyy-MM-dd') AS Date, 
	CalculatorDisplayTime, UserModify, DatetimeModify, Active, Mapa, Latitude,Length,
	isnull(Gamification,0) as Gamification, isnull(Calculator,0) as Calculator,isnull(Survey,0) as Survey,GamificationTime,SurveyTime 
FROM Events 
WHERE Id=@id
end
else
begin
SELECT Id, Name, Location, WelcomeEmail, Logo, FORMAT( Date,'yyyy-MM-dd') AS Date, 
	CalculatorDisplayTime, UserModify, DatetimeModify, Active, Mapa, Latitude,Length,
	isnull(Gamification,0) as Gamification, isnull(Calculator,0) as Calculator,isnull(Survey,0) as Survey,GamificationTime,SurveyTime 
FROM Events 
WHERE Id in (Select Id from Events where Active=1)
end


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectSpecificGamification] (@id as int)
as
Select 
Id, 
Pregunta, 
Respuesta1, 
Respuesta2, 
Respuesta3, 
RespuestaCorrecta,
Pregunta2, 
Respuesta2_1, 
Respuesta2_2, 
Respuesta2_3, 
RespuestaCorrecta2,
Pregunta3, 
Respuesta3_1, 
Respuesta3_2, 
Respuesta3_3, 
RespuestaCorrecta3,
Puntos,
PuntosFallo,
PuntosFallo3er,
IdEvento, 
FORMAT( FechaCreacion,'yyyy-MM-dd') FechaCreacion, 
FORMAT( FechaModificacion,'yyyy-MM-dd') FechaModificacion, 
UsuarioModificacion, 
Activo,
GUID 
from 
[Gamification]
where Id=@id



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificIncidencia]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectSpecificIncidencia] (@Id AS INT) AS

SELECT
		  i.Nombre
		, Productos = STUFF((
          SELECT ',' +  CAST(ip.IdProducto AS VARCHAR)
          FROM Calc_Incidencias_Productos ip
          WHERE i.Id = ip.IdIncidencia
          FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
FROM Calc_Incidencias i
WHERE i.Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectSpecificInformative] (@id as int)
as
Select 
Id, Title, Description, Image, UserModify, DatetimeModify, Active, Event, URL from Informative 
where Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificPorcentaje]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectSpecificPorcentaje] (@Id AS INT) AS

SELECT * FROM Calc_Productos_Porcentaje
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectSpecificProducto] (@Id AS INT) AS

SELECT * FROM Calc_Productos
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificRegalo]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectSpecificRegalo] (@Id AS INT) AS

SELECT * FROM Calc_Regalos
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificUsers]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectSpecificUsers] (@id as int)
as
Select u.Id, u.Username, u.Password, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, isnull(u.Administrador,0) as Administrador,u.IdEvento, 
(Select isnull(sum(Points),0) as Puntos from dbo.Score where IdUser=u.Id and IdEvent=(Select Id from dbo.Events where Active=1)) as Puntaje
,case isnull(u.Photo  ,'')
when '' then 'photos/sinimagen.png' 
else 
CONCAT('photos/',u.Photo) end as Photo
,PIN 
,Companions
,PreRegister
,Telefono
,Ocupacion
,Estado
,Municipio 
,IdDistribuidor
from [Users] as u where u.Id=@id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificUsersbyUsername]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectSpecificUsersbyUsername] (@username as nvarchar(100))
AS
SELECT Id, Username, Password, FirstName, LastName, CorreoElectronico, UserModify, ISNULL(Photo,'') Photo
FROM Users WHERE CorreoElectronico=@username



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificUsersEvents]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectSpecificUsersEvents] (@id as int,@idEvento as bigint)
as

if(@idEvento=0)
begin
	Select u.Id, u.Username, u.Password, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, isnull(u.Administrador,0) as Administrador,u.IdEvento, 
	(
		Select sum(Puntos)
	from
		(
	Select isnull(sum(PuntosGanados),0) as Puntos 
	from 
	dbo.answers 
	where IdUsuario=@id and IdEvento = (Select Id from dbo.Events where Active=1)
	union all
	Select 
	s.Points as Puntos 
	from Score as s where 
	s.IdUser=@id and s.IdEvent = (Select Id from dbo.Events where Active=1) and IdAnswer=0 and IdQuestion=0
	) P
	) as Puntaje
	,case isnull(u.Photo  ,'')
	when '' then 'photos/sinimagen.png' 
	else 
	CONCAT('photos/',u.Photo) end as Photo
	,PIN 
	,(select count(*) from Companions where IdUser=u.Id and IdEvento = (Select Id from dbo.Events where Active=1)) as Companions
	,PreRegister
	,Telefono
	,Ocupacion
	,Estado
	,Municipio 
	,IdDistribuidor
	from [Users] as u where u.Id=@id
end
else
begin
	Select u.Id, u.Username, u.Password, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, isnull(u.Administrador,0) as Administrador,u.IdEvento, 
	(
		Select sum(Puntos)
	from
		(
	Select 
	isnull(sum(PuntosGanados),0) as Puntos 
	from 
	dbo.answers where IdUsuario=@id and IdEvento=@idEvento
	union all
	Select 
	s.Points as Puntos 
	from Score as s where 
	s.IdUser=@id and s.IdEvent=@idEvento and IdAnswer=0 and IdQuestion=0
	) P
	) as Puntaje
	,case isnull(u.Photo  ,'')
	when '' then 'photos/sinimagen.png' 
	else 
	CONCAT('photos/',u.Photo) end as Photo
	,PIN 
	,(select count(*) from Companions where IdUser=u.Id and IdEvento=@idEvento) as Companions
	,PreRegister
	,Telefono
	,Ocupacion
	,Estado
	,Municipio 
	,IdDistribuidor
	from [Users] as u where u.Id=@id
end



GO
/****** Object:  StoredProcedure [dbo].[usp_SelectSpecificVariedad]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectSpecificVariedad] (@Id AS INT) AS

SELECT * FROM Calc_Variedades
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUserCompanions]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_SelectUserCompanions](@IdUser as bigint)
as
select Companion,Age from [Companions] 
where IdUser=@IdUser 
and IdEvento in (Select Id from dbo.Events where Active=1) 

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUserDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectUserDiary](@IdUser as bigint, @IdEvento as int)
as

Select 
case a.Pregunta123 
when 1 then Cast(g.Pregunta as nvarchar(400)) 
when 2 then Cast(g.Pregunta2 as nvarchar(400)) 
when 3 then Cast(g.Pregunta3 as nvarchar(400)) end as 'Gamificacion' 
,sum(a.PuntosGanados) as 'Puntos' 
from dbo.answers as a 
inner join dbo.gamification as g on a.GUID=g.GUID and g.IdEvento =@IdEvento
where 
a.IdUsuario=@IdUser and a.IdEvento=@IdEvento
group by
a.IdUsuario,
case a.Pregunta123 
when 1 then Cast(g.Pregunta as nvarchar(400)) 
when 2 then Cast(g.Pregunta2 as nvarchar(400)) 
when 3 then Cast(g.Pregunta3 as nvarchar(400)) end
union all
Select 
'Pre registro',
s.Points
from Score as s where 
s.IdUser=@IdUser and s.IdEvent=@IdEvento and IdAnswer=0 and IdQuestion=0

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUserLogin]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectUserLogin](@username AS NVARCHAR(100))
AS
SELECT Id, Username, Password, FirstName, LastName, CorreoElectronico, UserModify, ISNULL(Photo,'') Photo
FROM Users WHERE CorreoElectronico=@username
AND Active = 1 AND Administrador = 1


GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUSerPIN]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectUSerPIN](@pin as nvarchar(7))
as

select Id
,(Select Concat('admin/files/',Logo) as Logo from Events where Active=1) as Logo
 from [Users] 
where
PIN=@pin

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUsers]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectUsers] (@parametro as nvarchar(100))
as
Select u.Id, u.Username, u.Password, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, 
case ISNULL(u.Photo,'')
when '' then 'assets/img/person-flat.png' 
else 
CONCAT('photos/',u.Photo) end as Photo
, case isnull(Administrador,0) when 0 then 'No' else 'Sí' end as Administrador
, CONCAT(u.FirstName,' ', u.LastName) as fullname
,u.PIN
 from [Users] u 
 where 
 u.Username like concat('%',@parametro,'%') 
 or 
 u.FirstName like concat('%',@parametro,'%') 
 or
 u.LastName like concat('%',@parametro,'%') 
 or
 u.CorreoElectronico like concat('%',@parametro,'%')

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUsersbyEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_SelectUsersbyEvent]
(@IdEvento as int)
as
Select Id, Username, Password, FirstName, LastName, CorreoElectronico, UserModify, DateTimeCreate, DateTimeModify, Active, Administrador, IdEvento 
from 
[Users] 
where 
IdEvento=@IdEvento 
order by LastName,FirstName asc

GO
/****** Object:  StoredProcedure [dbo].[usp_SelectUsersEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_SelectUsersEvent] (@parametro as nvarchar(100),@IdEvento as bigint)
as
Select u.Id, u.Username, u.Password, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, 
case ISNULL(u.Photo,'')
when '' then 'photos/sinimagen.png' 
else 
CONCAT('photos/',u.Photo) end as Photo
, case isnull(Administrador,0) when 0 then 'No' else 'Sí' end as Administrador
, CONCAT(u.FirstName,' ', u.LastName) as fullname
,u.PIN
 from [Users] u 
 where 
 u.Username like concat('%',@parametro,'%') 
 or 
 u.FirstName like concat('%',@parametro,'%') 
 or
 u.LastName like concat('%',@parametro,'%') 
 or
 u.CorreoElectronico like concat('%',@parametro,'%')

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateDiary] 
(
@Id AS BIGINT,
@Name AS NVARCHAR(100), 
@Lastname AS NVARCHAR(100), 
@ConferenceDescription AS TEXT, 
@ConferenceTime AS TIME(7), 
@InformationLecturer AS TEXT, 
@PhotoLecturer AS NVARCHAR(100), 
@AttachedImageLecturer AS NVARCHAR(100), 
@Presentation AS NVARCHAR(100),
@IdEvent AS BIGINT,
@ConferenceTimeEnd AS TIME(7),
@ConferenceDate AS DATE,
@Ubication AS NVARCHAR(250) = '',
@Detail BIT = 0,
@Active as int = 1
)
AS
UPDATE Diary
SET
Name=@Name, 
Lastname=@Lastname, 
ConferenceDescription=@ConferenceDescription, 
ConferenceTime=@ConferenceTime, 
InformationLecturer=@InformationLecturer,
IdEvent=@IdEvent,
ConferenceTimeEnd=@ConferenceTimeEnd,
ConferenceDate=@ConferenceDate,
Ubication = @Ubication,
Detail = @Detail,
Active = @Active
WHERE 
Id=@Id

IF (@PhotoLecturer<>'')
	UPDATE Diary 
	SET 
	PhotoLecturer=@PhotoLecturer
	WHERE
	Id=@Id

IF(@AttachedImageLecturer<>'')
	UPDATE Diary 
	SET 
	AttachedImageLecturer=@AttachedImageLecturer
	WHERE
	Id=@Id

IF(@Presentation<>'')
	UPDATE Diary
	SET 
	Presentation=@Presentation 
	WHERE
	Id=@Id


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateEvent]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateEvent] 
(
@Id AS INT,
@Name AS NVARCHAR(100), 
@Location AS TEXT, 
@WelcomeEmail AS TEXT, 
@Logo AS NVARCHAR(100), 
@Date AS DATE, 
@CalculatorDisplayTime AS TIME(7), 
@Active AS INT,
@Address AS TEXT,
@Mapa AS NVARCHAR(100),
@Latitude AS NVARCHAR(100),
@Length AS NVARCHAR(100),
@Gamification AS BIT = 0,
@Calculator AS BIT = 0,
@Survey AS BIT = 0,
@GamificationTime AS TIME(7), 
@SurveyTime AS TIME(7) 
)
AS
UPDATE Events 
SET 
Name=@Name, 
Location=@Location, 
WelcomeEmail=@WelcomeEmail, 
Logo=@Logo, 
Date=@Date, 
CalculatorDisplayTime=@CalculatorDisplayTime, 
UserModify='admin', 
DatetimeModify=GETDATE(),
Active=@Active,
Address=@Address,
Mapa=@Mapa,
Latitude=@Latitude,
Length=@Length,
Gamification = @Gamification,
Calculator = @Calculator,
Survey=@Survey,
GamificationTime=@GamificationTime,
SurveyTime=@SurveyTime
WHERE 
Id=@Id

if(@Active=1)
BEGIN
	UPDATE Events SET Active=0 WHERE Id<>@Id
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_UpdateGamification] 
(
@Id as bigint,
@Pregunta as text, 
@Respuesta1 as text, 
@Respuesta2 as text, 
@Respuesta3 as text, 
@RespuestaCorrecta as int,
@IdEvento as bigint, 
@UsuarioModificacion as nvarchar(100),
@Pregunta2 as text, 
@Respuesta2_1 as text, 
@Respuesta2_2 as text, 
@Respuesta2_3 as text, 
@RespuestaCorrecta2 as int,
@Pregunta3 as text, 
@Respuesta3_1 as text, 
@Respuesta3_2 as text, 
@Respuesta3_3 as text, 
@RespuestaCorrecta3 as int,
@Puntos as decimal(18,0), 
@PuntosFallo as decimal(18,0),
@Activo as int,
@PuntosFallo3er as decimal(18,0)
)
as
Update [Gamification] 
set
Pregunta=@Pregunta, 
Respuesta1=@Respuesta1, 
Respuesta2=@Respuesta2, 
Respuesta3=@Respuesta3, 
RespuestaCorrecta=@RespuestaCorrecta,
IdEvento=@IdEvento, 
UsuarioModificacion=@UsuarioModificacion,
Pregunta2=@Pregunta2, 
Respuesta2_1=@Respuesta2_1, 
Respuesta2_2=@Respuesta2_2, 
Respuesta2_3=@Respuesta2_3, 
RespuestaCorrecta2=@RespuestaCorrecta2,
Pregunta3=@Pregunta3, 
Respuesta3_1=@Respuesta3_1, 
Respuesta3_2=@Respuesta3_2, 
Respuesta3_3=@Respuesta3_3, 
RespuestaCorrecta3=@RespuestaCorrecta3,
Puntos=@Puntos, 
PuntosFallo=@PuntosFallo,
Activo=@Activo,
PuntosFallo3er=@PuntosFallo3er
where
Id=@Id

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateIncidenciaProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateIncidenciaProducto]
(
@Id AS INT,
@Nombre AS NVARCHAR(50),
@Productos AS VARCHAR(8000)
)
AS
DELETE FROM Calc_Incidencias_Productos
WHERE IdIncidencia = @Id

UPDATE Calc_Incidencias SET Nombre = @Nombre
WHERE Id = @Id


INSERT INTO Calc_Incidencias_Productos
(
IdIncidencia,
IdProducto
) 
SELECT @Id, splitdata from dbo.fnSplitString(@Productos,',')

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_UpdateInformative] 
(
@Id as bigint,
@Title as text, 
@Description as text, 
@Image as nvarchar(100), 
@UserModify as nvarchar(100), 
@Active as int, 
@Event as bigint,
@URL as text
)
as
Update Informative 
set
Title=@Title, 
Description=@Description, 
UserModify=@UserModify, 
DatetimeModify=GETDATE(), 
Active=@Active, 
Event=@Event,
URL=@URL
where 
Id=@Id

if (@Image<>'')
	Update Informative 
	set 
	Image=@Image
	where
	Id=@Id

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdatePorcentaje]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdatePorcentaje]
(
@Id AS INT,
@Cantidad AS INT,
@Paquetes AS INT,
@Porcentaje AS DECIMAL (18,2),
@PorcentajeSemilla AS DECIMAL (18,2),
@MontoProducto AS DECIMAL (18,2)
)
AS

UPDATE Calc_Productos_Porcentaje 
SET Cantidad = @Cantidad,
Paquetes = @Paquetes,
Porcentaje = @Porcentaje,
PorcentajeSemilla = @PorcentajeSemilla,
MontoProducto=@MontoProducto 
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateProducto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateProducto]
(
@Id AS INT,
@Nombre AS NVARCHAR(50),
@PrecioRegular AS DECIMAL(18,2),
@PrecioPreventa AS DECIMAL(18,2),
@Has AS DECIMAL(18,2),
@CantPorPaquete AS INT
)
AS

UPDATE Calc_Productos 
SET Nombre = @Nombre,
PrecioRegular = @PrecioRegular,
PrecioPreventa = @PrecioPreventa,
Has = @Has,
CantPorPaquete = @CantPorPaquete
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateRegalo]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateRegalo]
(
@Id AS INT,
@Nombre AS NVARCHAR(50), 
@Cantidad AS INT,
@Sacos AS INT,
@Precio AS DECIMAL(18,2)
)
AS

UPDATE Calc_Regalos 
SET Nombre = @Nombre,
Cantidad = @Cantidad,
Sacos = @Sacos,
Precio = @Precio
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateUser]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_UpdateUser]
(
@Id as int,
@Username as nvarchar(50), 
@Password as nvarchar(50), 
@FirstName as nvarchar(100), 
@LastName as nvarchar(100), 
@CorreoElectronico as nvarchar(100),
@Administrador as int
,@Photo as nvarchar(100)
,@Telefono as nvarchar(50)
,@Ocupacion as nvarchar(100)
,@Estado as nvarchar(50)
,@Municipio as nvarchar(100)
,@Distribuidor as int
)
as
if(@Password<>'')
begin
Update Users set 
Username=@Username, 
Password=@Password, 
FirstName=@FirstName, 
LastName=@LastName, 
CorreoElectronico=@CorreoElectronico, 
UserModify='admin', 
DateTimeModify=GETDATE(),
Administrador=@Administrador
,Photo=@Photo
,Telefono=@Telefono
,Ocupacion=@Ocupacion
,Estado=@Estado
,Municipio=@Municipio
,IdDistribuidor=@Distribuidor
where 
Id=@Id
end
else
begin
Update Users set 
Username=@Username, 
FirstName=@FirstName, 
LastName=@LastName, 
CorreoElectronico=@CorreoElectronico, 
UserModify='admin', 
DateTimeModify=GETDATE(),
Administrador=@Administrador
,Photo=@Photo
,Telefono=@Telefono
,Ocupacion=@Ocupacion
,Estado=@Estado
,Municipio=@Municipio
,IdDistribuidor=@Distribuidor
where 
Id=@Id
end

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateVariedad]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateVariedad]
(
@Id AS INT,
@Nombre AS NVARCHAR(50),
@PrecioRegular AS DECIMAL(18,2),
@PrecioPreventa AS DECIMAL(18,2),
@Sacos AS INT,
@SemillasPorSaco AS INT,
@Has AS DECIMAL(18,2)
)
AS

UPDATE Calc_Variedades 
SET Nombre = @Nombre,
PrecioRegular = @PrecioRegular,
PrecioPreventa = @PrecioPreventa,
Sacos = @Sacos,
SemillasPorSacos = @SemillasPorSaco,
HasPorSaco = @Has
WHERE Id = @Id

GO
/****** Object:  StoredProcedure [dbo].[usp_ws_CalcCatalogos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ws_CalcCatalogos]
AS
Declare 
@ContadorCI int=0,
@ContadorCIP int=0,
@ContadorCP int=0,
@ContadorCPP int=0,
@ContadorCIR int=0,
@ContadorCV int=0

/*
SELECT * FROM Calc_Incidencias
SELECT * FROM Calc_Incidencias_Productos
SELECT * FROM Calc_Productos
SELECT Id, Cantidad, Paquetes as MontoSemilla, Porcentaje, PorcentajeSemilla, MontoProducto FROM Calc_Productos_Porcentaje ORDER BY Cantidad, Paquetes
SELECT * FROM Calc_Regalos
SELECT * FROM Calc_Variedades
*/

SELECT @ContadorCI=count(*) FROM Calc_Incidencias
SELECT @ContadorCIP=count(*) FROM Calc_Incidencias_Productos
SELECT @ContadorCP=count(*) FROM Calc_Productos
SELECT @ContadorCPP=count(*) FROM Calc_Productos_Porcentaje
SELECT @ContadorCIR=count(*) FROM Calc_Regalos
SELECT @ContadorCV=count(*) FROM Calc_Variedades

if(@ContadorCI>0)
begin
	SELECT *,0 as 'vacio' FROM Calc_Incidencias
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Incidencias
end
if(@ContadorCIP>0)
begin
SELECT *,0 as 'vacio' FROM Calc_Incidencias_Productos
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Incidencias_Productos
end
if(@ContadorCP>0)
begin
SELECT *,0 as 'vacio' FROM Calc_Productos
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Productos
end
if(@ContadorCPP>0)
begin
SELECT Id, Cantidad, Paquetes as MontoSemilla, Porcentaje, PorcentajeSemilla, MontoProducto,0 as 'vacio' FROM Calc_Productos_Porcentaje ORDER BY Cantidad, Paquetes
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Productos_Porcentaje
end
if(@ContadorCIR>0)
begin
SELECT *,0 as 'vacio' FROM Calc_Regalos
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Regalos
end
if(@ContadorCV>0)
begin
SELECT *,0 as 'vacio' FROM Calc_Variedades
end
else
begin
	Select * from (Select 1 as 'vacio') as Calc_Variedades
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsGenerateToken]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsGenerateToken] (@username as nvarchar(100),@Date as datetime)
as
Declare
@IdUser as bigint

DECLARE @datetime datetime = @Date;
insert into Tokens 
(IdUser, Token, DateBeginningValid, DateEndingValid, Valid)
values
((Select Id from Users where CorreoElectronico=@username),replace(newid(),'-',''),@datetime,DATEADD(minute,43800,@datetime),1)

delete from Tokens where DateEndingValid<getdate()

GO
/****** Object:  StoredProcedure [dbo].[usp_wsInsertAnswers]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsInsertAnswers] (@token as nvarchar(100),@IdRespuesta as int)
as
Declare 
@Intento as int=0,
@Puntos as decimal=0,
@ConteoScore as int=0,
@IdUsuario as int=0,
@IdPregunta as bigint,
@GUID as nvarchar(100),
@Pregunta123 as int,
@Correcta1erIntento as int=0,
@Correcta2doIntento as int=0,
@RespuestaCorrecta as int=0,
@Indicador as int=0
	
Select @IdUsuario=IdUser from [Tokens] where Token=@token

Select distinct @GUID=GUID from [Answers] where IdUsuario=@IdUsuario and IdRespuesta is null

Select @IdPregunta=IdPregunta, @Pregunta123=Pregunta123 from [Answers] where IdUsuario=@IdUsuario and GUID =@GUID

if(@GUID<>'')
begin
select @Indicador=sum(PuntosGanados) from dbo.Answers where GUID=@GUID and IdUsuario=@IdUsuario and IdEvento in (Select Id from Events where Active=1)
if(@Indicador=0)
begin
select @Intento=sum(isnull(Intento,0)) from [Answers] where GUID=@GUID and IdUsuario=@IdUsuario and IdEvento in (Select Id from Events where Active=1)
	if(@Intento<2)
		begin
		if(@Intento=0)
		begin
			if(@Pregunta123=1)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta from Gamification where GUID=@GUID
			end
			if(@Pregunta123=2)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta2 from Gamification where GUID=@GUID
			end
			if(@Pregunta123=3)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta3 from Gamification where GUID=@GUID
			end
			if(@RespuestaCorrecta=@IdRespuesta)
			begin	
				Select @Puntos=Puntos from [Gamification] 
				where 
				GUID=@GUID 
				and IdEvento in (Select Id from Events where Active=1) 

				update [Answers] 
				set 
					Intento=1,
					PuntosGanados=@Puntos,
					IdRespuesta= @IdRespuesta
				where
					IdUsuario=@IdUsuario 
					and GUID=@GUID 
					and IdEvento in (Select Id from Events where Active=1)
			end
			update [Answers] 
			set 
				Intento=1,
				PuntosGanados=@Puntos
			where
				IdUsuario=@IdUsuario 
				and GUID=@GUID 
				and IdEvento in (Select Id from Events where Active=1)
		end
		if(@Intento=1)
		begin
			if(@Pregunta123=1)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta from Gamification where GUID=@GUID
			end
			if(@Pregunta123=2)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta2 from Gamification where GUID=@GUID
			end
			if(@Pregunta123=3)
			begin
				Select @RespuestaCorrecta=RespuestaCorrecta3 from Gamification where GUID=@GUID
			end
			if(@RespuestaCorrecta=@IdRespuesta)
			begin	
				Select @Puntos=PuntosFallo from [Gamification] 
				where 
				GUID=@GUID 
				and IdEvento in (Select Id from Events where Active=1) 
			end
			else
			begin
				Select @Puntos=PuntosFallo3er from [Gamification] 
				where 
				GUID=@GUID 
				and IdEvento in (Select Id from Events where Active=1) 
			end
			
			update [Answers] 
			set 
			Intento=2,
			PuntosGanados=@Puntos,
			IdRespuesta=@IdRespuesta
			where
			IdUsuario=@IdUsuario 
			and GUID=@GUID 
			and IdEvento in (Select Id from Events where Active=1)
		end		

			Select  @ConteoScore=count(*) from [Score] where [IdUser]=@IdUsuario and [IdEvent] in (Select Id from Events where Active=1) and IdQuestion=
			(Select Id from [Gamification] where GUID=@GUID and IdEvento in (Select Id from Events where Active=1))

			if(@ConteoScore=0)
			begin
				insert into [Score] (IdUser, IdEvent, IdQuestion, IdAnswer, Points, DateTime)
				values 
				(@IdUsuario,(Select Id from Events where Active=1),(Select Id from [Gamification] where GUID=@GUID and IdEvento in (Select Id from Events where Active=1)),null,@Puntos,GETDATE())
			end
			else
			begin
				update [Score] set [Points]=@Puntos where
				@IdUsuario=@IdUsuario and
				IdEvent=(Select Id from Events where Active=1) and 
				IdQuestion=(Select Id from [Gamification] where GUID=@GUID and IdEvento in (Select Id from Events where Active=1))
			end
			if(@RespuestaCorrecta=@IdRespuesta)
			begin
				select @Puntos as 'PuntosGanados', 'true' as 'RespuestaCorrecta', 1-@Intento as 'IntentosRestantes',101 as 'error_message'
			end
			if(@RespuestaCorrecta<>@IdRespuesta)
			begin
				if((1-@Intento)=1)
				begin
					select @Puntos as 'PuntosGanados', 'false' as 'RespuestaCorrecta', 1-@Intento as 'IntentosRestantes',102 as 'error_message'
				end
				if((1-@Intento)=0)
				begin
					select @Puntos as 'PuntosGanados', 'false' as 'RespuestaCorrecta', 1-@Intento as 'IntentosRestantes',103 as 'error_message'
				end
			end
		end
	else
		begin
			select 1 as 'error_message'
		end
end
else
	begin
		Select 1 as 'error_message'
	end
end
else
begin
--Ya ha respondido esta pregunta
	Select 2 as 'error_message'
end



GO
/****** Object:  StoredProcedure [dbo].[usp_wsInsertaRespuesta]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsInsertaRespuesta]
(
@Respuesta1 as int,
@Respuesta2 as int,
@Respuesta3 as int,
@Respuesta4 as int,
@Respuesta5 as int,
@Respuesta6 as bigint,
@Comentarios as text,
@token as nvarchar(100)
)
as
Declare 
@Contador as int,
@IdUsuario as bigint,
@IdEvento as bigint

Select distinct @IdUsuario=IdUser from [Tokens] where Token=@token
Select top 1 @IdEvento=Id from Events where Active=1
Select @Contador=count(*) from [Survey] where 
IdUsuario=(Select distinct IdUser from [Tokens] where Token=@token) 
and 
IdEvento=(Select top 1 Id from Events where Active=1)

if(@Contador>0)
begin
	Select 0 as 'error_message', 'Ya se respondio la encuesta' as mensaje
end
else
begin
	Insert into [Survey] 
	(IdUsuario, IdEvento, Respuesta1, Respuesta2, Respuesta3, Respuesta4, Respuesta5, Respuesta6, Datetime,Comentarios)
	values
	(@IdUsuario,@IdEvento,@Respuesta1,@Respuesta2,@Respuesta3,@Respuesta4,@Respuesta5,@Respuesta6,GETDATE(),@Comentarios)
	Select 0 as 'error_message', 'Informacion guardada con exito' as mensaje
end


GO
/****** Object:  StoredProcedure [dbo].[usp_wsLoginWithPIN]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_wsLoginWithPIN] (@pin AS NVARCHAR(10))
AS
Declare 
@Count as int,
@datetime datetime = getdate(),
@token as nvarchar(100),
@IdUsuario as bigint,
@username as nvarchar(100)

Select @Count=count(*) from [Users] as u where u.PIN=@pin
Select @IdUsuario=u.Id,@username=u.username from [Users] as u where u.PIN=@pin

if(@Count>0)
begin

exec usp_wsGenerateToken @username,@datetime

Select @token=Token from Tokens where IdUser in 
(
Select Id from Users 
where 
PIN=@pin 
)
and DateBeginningValid=@datetime
Select 
	u.Id as userID, 
	u.Username as userUsername, 
	u.FirstName as userFirstName, 
	u.LastName as userLastName, 
	u.CorreoElectronico as userEmail, 
	u.DateTimeCreate as userDateTimeCreated, 
	u.DateTimeModify as userDateTimeModified, 
	u.Active as userActive, 
	u.Administrador as userIsAdministrator,
	e.Name as eventName,
	e.Date as eventDate,
	e.Location as eventLocation,
	@token as token,
	0 as error_message
	 from [Users] u,[Events] e
	 where u.PIN= @pin
	 and e.Active=1
end
else
begin
	Select '1' as error_message, '','PIN invalido'
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsLoginWithPoints]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_wsLoginWithPoints] (@pin AS NVARCHAR(10))
AS
Declare 
@Count as int,
@datetime datetime = getdate(),
@token as nvarchar(100),
@IdUsuario as bigint,
@username as nvarchar(100),
@valores as VARCHAR(1000),
@indicador as int=0

Select distinct @IdUsuario=Id from dbo.Users where PIN=@pin
SELECT @valores= COALESCE(@valores + '|', '') + Datos.Datos FROM (Select [Companion] as Datos from [Companions] where IdUser=@IdUsuario and IdEvento in (Select top 1 Id from Events where Active=1)) as Datos


Select @Count=count(*) from [Users] as u where u.PIN=@pin
Select @IdUsuario=u.Id,@username=u.username from [Users] as u where u.PIN=@pin
Select @indicador=count(*) from Score where IdUser=@IdUsuario and IdEvent in (Select top 1 Id from Events where Active=1) and IdQuestion=0

if(@Count>0)
begin

if(@indicador=0)
begin
	insert into 
	[Score] 
	(IdUser, IdEvent, IdQuestion, IdAnswer, Points, DateTime)
	values 
	(@IdUsuario,(Select Id from Events where Active=1),0,0,10,GETDATE())
end
exec usp_wsGenerateToken @username,@datetime

Select @token=Token from Tokens where IdUser in 
(
Select Id from Users 
where 
PIN=@pin 
)
and DateBeginningValid=@datetime
Select 
	u.Id as userID, 
	u.Username as userUsername, 
	u.FirstName as userFirstName, 
	u.LastName as userLastName, 
	u.CorreoElectronico as userEmail, 
	u.DateTimeCreate as userDateTimeCreated, 
	u.DateTimeModify as userDateTimeModified, 
	u.Active as userActive, 
	u.Administrador as userIsAdministrator,
	@valores as Companions,
	e.Name as eventName,
	e.Date as eventDate,
	e.Location as eventLocation,
	@token as token,
	0 as error_message
	 from [Users] u,[Events] e
	 where u.PIN= @pin
	 and e.Active=1
end
else
begin
	Select '1' as error_message, '','PIN invalido'
end



GO
/****** Object:  StoredProcedure [dbo].[usp_wsMenu]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsMenu] (@parametro as nvarchar(100))
as
Declare 
@IdUsuario as bigint,
@valores as VARCHAR(1000),
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

--SELECT @valores= COALESCE(@valores + ', ', '') + Datos.Datos FROM 
--(
--Select Concat(t.Fullname,'|',t.Score,'|',CONCAT(@baseURL,'photos/',t.Photo)) as Datos 
--from 
--(Select 
--top 3 
--Concat(u.FirstName,' ',u.LastName) as Fullname, 
--Photo, 
--sum(s.Points) as Score 
--from [Score] as s inner join [Users] as u on s.IdUser=u.Id where s.IdEvent=(Select Id from Events where Active=1) 
--group by Concat(u.FirstName,' ',u.LastName), Photo
--order by sum(s.Points) desc
--) as t
--) as Datos

select distinct @IdUsuario=IdUser from [Tokens] where Token=@parametro

Select 
(Select CalculatorDisplayTime from Events where Active=1) as CalculatorDisplayTime, 
--@valores as RankingTop3,
'' as RankingTop3,
Concat(u.FirstName,' ',u.LastName) as FullnameLoggedUser,
isnull(CONCAT(@baseURL,'photos/',u.Photo),'Sin foto') as Photo,
(Select case when DATEDIFF(minute,getdate(),Dateadd(minute,DATEPART(MINUTE,CalculatorDisplayTime),DATEADD(hour,DATEPART(hour,CalculatorDisplayTime),Date)))>0 then 'false' else 'true' end as Calculator from Events where Active=1) as Calculator,
(Select case when DATEDIFF(minute,getdate(),Dateadd(minute,DATEPART(MINUTE,GamificationTime),DATEADD(hour,DATEPART(hour,GamificationTime),Date)))<0 then 'false' else 'true' end as Gamification from Events where Active=1) as Gamification,
(Select case when DATEDIFF(minute,getdate(),Dateadd(minute,DATEPART(MINUTE,SurveyTime),DATEADD(hour,DATEPART(hour,SurveyTime),Date)))<0 then 'false' when (Select count(*) from Survey where IdUsuario=@IdUsuario and IdEvento in (Select Id from Events where Active=1))>0 then 'false' else 'true' end as Gamification from Events where Active=1) as Survey,
(
Select sum(T1.Points) from
(
Select Datos.ID, SUM(Datos.Points) AS Points from
(
SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
FROM Users u LEFT JOIN 
Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0
where u.Id=@IdUsuario
GROUP BY u.ID, u.FirstName, u.LastName, Photo
union all
SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
FROM Users u LEFT JOIN 
answers as a on u.Id=a.IdUsuario
where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1) 
and
u.Id=@IdUsuario
GROUP BY u.ID, u.FirstName, u.LastName, Photo
) as Datos group by Datos.ID


--select IdUser,Points from [Score] where IdUser=@IdUsuario and IdEvent in (Select top 1 Id from Events where Active=1)
) as T1
) as Points,
(Select isnull(GamificationTime,'') as GamificationTime from Events where Active=1) as GamificationTime,
(Select isnull(SurveyTime,'') as SurveyTime from Events where Active=1) as SurveyTime,
0 as 'error_message'
 from [Users] u 
 where
 u.Id=@IdUsuario



GO
/****** Object:  StoredProcedure [dbo].[usp_wsProfileEventActive]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsProfileEventActive] (@parametro as nvarchar(100))
as
Declare 
@IdUsuario as bigint,
@valores as VARCHAR(1000),
@valoresGam as VARCHAR(1000),
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config
Select distinct @IdUsuario=IdUser from [Tokens] where Token=@parametro
SELECT @valores= COALESCE(@valores + '|', '') + Datos.Datos FROM (Select [Companion] as Datos from [Companions] where IdUser=@IdUsuario and IdEvento in (Select top 1 Id from Events where Active=1)) as Datos
SELECT 
@valoresGam= COALESCE(@valoresGam + '|', '') + Datos.Datos 
FROM (
Select 
concat(
case isnull(a.Pregunta123 ,0)
when 1 then g.Pregunta 
when 2 then g.Pregunta2 
when 3 then g.Pregunta3
when 0 then 'Pre registro'
 end,
' (',a.PuntosGanados,')') as Datos 
from 
Score as s 
left join Answers as a on s.IdQuestion=a.IdPregunta and s.IdUser=a.IdUsuario 
left join [Gamification] as g on s.IdQuestion=g.Id where s.IdUser=@IdUsuario and s.IdEvent in (Select top 1 Id from Events where Active=1)
) as Datos

Select u.Id, u.Username, u.FirstName, u.LastName, u.CorreoElectronico, u.UserModify, 
case ISNULL(u.Photo,'')
when '' then concat(@baseURL,'photos/sinimagen.png') 
else 
CONCAT(@baseURL,'photos/',u.Photo) end as Photo,
isnull((
Select sum(T1.Points) from
(
Select SUM(Datos.Points) AS Points from
(
SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
FROM Users u LEFT JOIN 
Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0 
where u.Id=@IdUsuario 
GROUP BY u.ID, u.FirstName, u.LastName, Photo
union all
SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
FROM Users u LEFT JOIN 
answers as a on u.Id=a.IdUsuario
where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1) 
and u.Id=@IdUsuario
GROUP BY u.ID, u.FirstName, u.LastName, Photo
) as Datos 
--select Points from [Score] where IdUser=@IdUsuario and IdEvent in (Select top 1 Id from Events where Active=1)
) as T1
),0) as Points,
isnull(@valores,'') as Companions,
isnull(@valoresGam,'') as Gamifications,
u.PIN,
0 as 'error_message'
 from [Users] u 
 where
  u.Id=@IdUsuario


GO
/****** Object:  StoredProcedure [dbo].[usp_wsRankingTop3]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsRankingTop3] (@parametro as int)
as
Declare
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

SELECT TOP 3 
Score.Id, 
Concat(Score.FirstName, '', Score.LastName) AS Fullname, 
Score.Points as Score, 
CONCAT('http://cottonclub.motti.mx/admin/photos/', Score.photo) AS Photo 
FROM Users LEFT JOIN 
(
Select Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
(
SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
FROM Users u LEFT JOIN 
Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0
GROUP BY u.ID, u.FirstName, u.LastName, Photo
union all
SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
FROM Users u LEFT JOIN 
answers as a on u.Id=a.IdUsuario
where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1)
GROUP BY u.ID, u.FirstName, u.LastName, Photo
) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
) AS Score ON Users.Id = Score.ID ORDER BY Points DESC


--exec [usp_wsRankingTop3] ''

--exec [usp_Ranking] 1

GO
/****** Object:  StoredProcedure [dbo].[usp_wsRecoverPassword]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsRecoverPassword] (@parametro as nvarchar(100))
as
if(@parametro='')
begin
	Select 1 as 'error_message'
end
else
begin
	select Password,0 as 'error_message' from Users where CorreoElectronico=@parametro
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsRegresaDistribuidor]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsRegresaDistribuidor] (@token as nvarchar(100))
as
Declare 
@IdUsuario as int

Select @IdUsuario=IdUser from Tokens where Token=@token
Select d.Id,d.Decription from Users u inner join Distributor d on u.IdDistribuidor=d.Id where u.Id=@IdUsuario

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectDiary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_wsSelectDiary] (@parametro as int,@detail as int) as
Declare
@baseURL as nvarchar(100),
@Datos as int
Select @baseURL=baseURL from Config
--@detail
--Valores
--0 Sin detalle
--1 Con detalle
--2 Todos
if(@parametro=0)
	begin
			if(@detail=0)
				begin
				set @Datos=0
						Select @Datos=count(*)
						 from [Diary] 
						 where IdEvent in (select Id from Events where Active=1) 
						 and Detail=0 
						 and Active=1
						 
						 if(@Datos>0)
						 begin
								Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message',
								'Sin detalle' as Type, 
								Ubication
								 from [Diary] 
								 where IdEvent in (select Id from Events where Active=1) 
								 and Detail=0 
								 and Active=1
								 order by ConferenceTime asc
						 end
						 else
						 begin
							Select 1 as 'error_message'
						 end

				 end
				 if(@detail=1)
				 begin
				 set @Datos=0
				 			Select @Datos=count(*)
							 from [Diary] 
							 where IdEvent in (select Id from Events where Active=1) 
							 and Detail=1 
							 and Active=1
						

							 if(@Datos>0)
							 begin
				 				Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message',
									'Con detalle' as Type,
									Ubication
									 from [Diary] 
									 where IdEvent in (select Id from Events where Active=1) 
									 and Detail=1 
									 and Active=1
									 order by ConferenceTime asc
							 end
							 else
							 begin
								Select 1 as 'error_message'
							 end
				 end
				 if(@detail=2)
				 begin
				 set @Datos=0

				 			Select @Datos=count(*) 
							 from [Diary] 
							 where IdEvent in (select Id from Events where Active=1) 
							 and Active=1
						 
							 if(@Datos>0)
							 begin
				 				Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message',
									case Detail when 0 then 'Sin detalle' else 'Con detalle' end as Type,
									Ubication
									 from [Diary] 
									 where IdEvent in (select Id from Events where Active=1) 
									 and Active=1
									 order by ConferenceTime asc
							end
							else
							begin
								Select 1 as 'error_message'
							end
				 end
	end
else
	begin
			if(@detail=0)
				begin
				set @Datos=0

						Select @Datos=count(*) 
						 from [Diary] 
						 where IdEvent in (select Id from Events where Active=1) 
						 and Detail=0 
						 and Id=@parametro 
						 and Active=1
						 

						 if(@Datos>0)
						 begin
							Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message',
							case Detail when 0 then 'Sin detalle' else 'Con detalle' end as Type,
							Ubication
							 from [Diary] 
							 where IdEvent in (select Id from Events where Active=1) 
							 and Detail=0 
							 and Id=@parametro 
							 and Active=1
							 order by ConferenceTime asc
						 end
						 else
						 begin
							Select 1 as 'error_message'
						 end
				 end
			else
				 begin
				 set @Datos=0
				 
				 			Select @Datos=count(*) 
							 from [Diary] 
							 where IdEvent in (select Id from Events where Active=1) 
							 and Detail=1
							 and Id=@parametro 
							 and Active=1
						

					if(@Datos>0)
					begin
				 		Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message',
							case Detail when 0 then 'Sin detalle' else 'Con detalle' end as Type,
							Ubication
							 from [Diary] 
							 where IdEvent in (select Id from Events where Active=1) 
							 and Detail=1
							 and Id=@parametro 
							 and Active=1
							 order by ConferenceTime asc
					end
					else
					begin
						Select 1 as 'error_message'
					end
				 end
	end




GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectDiaryforSurvey]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectDiaryforSurvey] (@parametro as int) as
Declare
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config
Select Id, Name, Lastname, ConferenceDescription, ConferenceTime, InformationLecturer,CONCAT(@baseURL,'files/',PhotoLecturer) as  PhotoLecturer,CONCAT(@baseURL,'files/',AttachedImageLecturer) as  AttachedImageLecturer,CONCAT(@baseURL,'files/',Presentation) as  Presentation, IdEvent,ConferenceTimeEnd, ConferenceDate, 0 as 'error_message' 
from [Diary] 
where 
CONCAT(Name,Lastname,InformationLecturer,PhotoLecturer,AttachedImageLecturer,Presentation) <> '' 
and 
IdEvent in (select Id from Events where Active=1)

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectEscalarUser]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectEscalarUser] (@username as nvarchar(100),@password as nvarchar(50))
as
Declare 
@Count as int,
@datetime datetime = getdate(),
@token as nvarchar(100)

Select @Count=count(*) from [Users] as u where u.CorreoElectronico=@username --and u.Password=@password
if(@password='true')
begin
if(@Count>0)
begin
exec usp_wsGenerateToken @username,@datetime
Select @token=Token from Tokens where IdUser in 
(
Select Id from Users 
where 
CorreoElectronico=@username 
--and Password=@password
)
and DateBeginningValid=@datetime
Select 
	u.Id as userID, 
	u.Username as userUsername, 
	u.FirstName as userFirstName, 
	u.LastName as userLastName, 
	u.CorreoElectronico as userEmail, 
	u.DateTimeCreate as userDateTimeCreated, 
	u.DateTimeModify as userDateTimeModified, 
	u.Active as userActive, 
	u.Administrador as userIsAdministrator,
	e.Name as eventName,
	e.Date as eventDate,
	e.Location as eventLocation,
	@token as token,
	0 as error_message
	 from [Users] u,[Events] e
	 where u.CorreoElectronico=@username 
	 --and u.Password=@password 
	 and e.Active=1
end
else
begin
	Select '1' as error_message, '','usuario y contraseña invalidos'
end
end
else
begin
	Select '1' as error_message, '','usuario y contraseña invalidos'
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectEscalarUserPassword]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_wsSelectEscalarUserPassword] (@username as nvarchar(100))
as

Select 
	u.Password as Password
	 from [Users] u,[Events] e
	 where u.CorreoElectronico=@username and e.Active=1

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectEventsLocation]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectEventsLocation] (@parametro as int)
as
Declare
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config
if(@parametro=0)
begin
Select 
Id, Name, Location, 0 as 'error_message',
WelcomeEmail,
CONCAT(@baseURL,'files/',Logo) as Logo,
Date, 
CalculatorDisplayTime, 
Active, 
Address, 
CONCAT(@baseURL,'files/',Mapa) as Mapa, 
Latitude, 
Length
 from Events 
where Active=1
end 
else
begin
Select Id, Name, Location, 0 as 'error_message',
WelcomeEmail,
CONCAT(@baseURL,'files/',Logo) as Logo,
Date, 
CalculatorDisplayTime, 
Active, 
Address, 
CONCAT(@baseURL,'files/',Mapa) as Mapa, 
Latitude, 
Length
 from Events 
where Id=@parametro
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectGamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectGamification] (@parametro as nvarchar(100),@token as nvarchar(100))
as
Declare
@IdRespuestaExistente as int,
@IdRespuestaCorrecta as int,
@Conteo as int,
@Aleatorio as int,
@IdUsuario as bigint,
@IdPregunta as bigint,
@RegistroExistente as int,
@Pregunta123 as int,
@NumeroIntento as int,
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

DECLARE @Random INT;
DECLARE @Upper INT;
DECLARE @Lower INT
SET @Lower = 1 ---- The lowest random number
SET @Upper = 4 ---- One more than the highest random number

SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)

Select @Conteo=count(*)
from [Gamification] as g 
INNER JOIN [Events] as e on e.Id=g.IdEvento 
where 
g.GUID=@parametro

Select @IdPregunta=g.Id 
from [Gamification] as g 
INNER JOIN [Events] as e on e.Id=g.IdEvento 
where 
g.GUID=@parametro
	
Select @IdUsuario=IdUser from [Tokens] where Token=@token

Delete from dbo.Answers where IdUsuario=@IdUsuario and IdRespuesta  is null and Intento=0

Select @RegistroExistente=count(*) from [Answers] where IdUsuario=@IdUsuario and GUID=@parametro

Select @Pregunta123=Pregunta123, @IdRespuestaExistente=IdRespuesta from [Answers] where IdUsuario=@IdUsuario and GUID=@parametro

if(@RegistroExistente>0)
begin
	set @Random=@Pregunta123
end
--Numero de intentos

  SELECT @NumeroIntento=max(Intento)
  FROM [Answers] WHERE IdUsuario=@IdUsuario
  AND GUID=@parametro

  if(@Pregunta123=1)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta from Gamification where guid=@parametro
  end
    if(@Pregunta123=2)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta2 from Gamification where guid=@parametro
  end
    if(@Pregunta123=3)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta3 from Gamification where guid=@parametro
  end
  if(@IdRespuestaExistente=@IdRespuestaCorrecta)
  begin
	set @NumeroIntento=2
  end

if(@NumeroIntento>=2) 
BEGIN
--Ya existe una solicitud de pregunta previa
  SELECT 2 AS 'error_message'   
END 
ELSE 
BEGIN 
if(@Conteo>0)
begin
if(@parametro='')
	begin
		Select 1 as 'error_message'
	end
else
	begin
		if(@Random=1)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta as Pregunta, 
				g.Respuesta1 as Respuesta1, 
				g.Respuesta2 as Respuesta2, 
				g.Respuesta3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),1)
						end
			end
		if(@Random=2)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta2 as Pregunta, 
				g.Respuesta2_1 as Respuesta1, 
				g.Respuesta2_2 as Respuesta2, 
				g.Respuesta2_3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),2)
						end
			end
		if(@Random=3)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta3 as Pregunta, 
				g.Respuesta3_1 as Respuesta1, 
				g.Respuesta3_2 as Respuesta2, 
				g.Respuesta3_3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),3)
						end
			end
		end
	end
else
begin
	Select 1 as 'error_message'
end
END


GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectGamification_bak]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectGamification_bak] (@parametro as nvarchar(100),@token as nvarchar(100))
as
Declare
@IdRespuestaExistente as int,
@IdRespuestaCorrecta as int,
@Conteo as int,
@Aleatorio as int,
@IdUsuario as bigint,
@IdPregunta as bigint,
@RegistroExistente as int,
@Pregunta123 as int,
@NumeroIntento as int,
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

DECLARE @Random INT;
DECLARE @Upper INT;
DECLARE @Lower INT
SET @Lower = 1 ---- The lowest random number
SET @Upper = 4 ---- One more than the highest random number

SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)

Select @Conteo=count(*)
from [Gamification] as g 
INNER JOIN [Events] as e on e.Id=g.IdEvento 
where 
g.GUID=@parametro

Select @IdPregunta=g.Id 
from [Gamification] as g 
INNER JOIN [Events] as e on e.Id=g.IdEvento 
where 
g.GUID=@parametro
	
Select @IdUsuario=IdUser from [Tokens] where Token=@token

Select @RegistroExistente=count(*) from [Answers] where IdUsuario=@IdUsuario and GUID=@parametro

Select @Pregunta123=Pregunta123, @IdRespuestaExistente=IdRespuesta from [Answers] where IdUsuario=@IdUsuario and GUID=@parametro

Delete from dbo.Answers where IdUsuario=@IdUsuario and IdRespuesta  is null and Intento=0

if(@RegistroExistente>0)
begin
	set @Random=@Pregunta123
end
--Numero de intentos

  SELECT @NumeroIntento=max(Intento)
  FROM [Answers] WHERE IdUsuario=@IdUsuario
  AND GUID=@parametro
  SELECT @Pregunta123=Pregunta123
  FROM [Answers] WHERE IdUsuario=@IdUsuario
  AND GUID=@parametro 

  if(@Pregunta123=1)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta from Gamification where guid='25E68976D207403083A55DF5B6E8EB01'
  end
    if(@Pregunta123=2)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta2 from Gamification where guid='25E68976D207403083A55DF5B6E8EB01'
  end
    if(@Pregunta123=3)
  begin
	Select @IdRespuestaCorrecta=RespuestaCorrecta3 from Gamification where guid='25E68976D207403083A55DF5B6E8EB01'
  end
  if(@IdRespuestaExistente=@Pregunta123)
  begin
	set @NumeroIntento=2
  end

if(@NumeroIntento>=2) 
BEGIN
--Ya existe una solicitud de pregunta previa
  SELECT 2 AS 'error_message'   
END 
ELSE 
BEGIN 
if(@Conteo>0)
begin
if(@parametro='')
	begin
		Select 1 as 'error_message'
	end
else
	begin
		if(@Random=1)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta as Pregunta, 
				g.Respuesta1 as Respuesta1, 
				g.Respuesta2 as Respuesta2, 
				g.Respuesta3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),1)
						end
			end
		if(@Random=2)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta2 as Pregunta, 
				g.Respuesta2_1 as Respuesta1, 
				g.Respuesta2_2 as Respuesta2, 
				g.Respuesta2_3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),2)
						end
			end
		if(@Random=3)
			begin
				Select 
				g.Id as Id, 
				CONCAT(@baseURL,'codigoQR/qrcode_',g.Id,'.PNG') as CodigoQR,
				g.Pregunta3 as Pregunta, 
				g.Respuesta3_1 as Respuesta1, 
				g.Respuesta3_2 as Respuesta2, 
				g.Respuesta3_3 as Respuesta3, 
				0 as 'error_message'
				from [Gamification] as g 
				INNER JOIN [Events] as e on e.Id=g.IdEvento 
				where 
				g.Activo=1 and 
				g.GUID=@parametro
					if(@RegistroExistente=0)
						begin
							Insert into [Answers] 
							(IdUsuario, GUID, IdPregunta, IdRespuesta, Intento, PuntosGanados, IdEvento, DateTime, Pregunta123)
							values
							(@IdUsuario,@parametro,@IdPregunta,null,0,0,(Select top 1 Id from Events where Active=1),GETDATE(),3)
						end
			end
		end
	end
else
begin
	Select 1 as 'error_message'
end
END


GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectInformative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectInformative] (@parametro as int) as
Declare
@Conteo as int=0,
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

Select @Conteo=count(*) from [Informative]
 where Event in (select Id from Events where Active=1)

if(@Conteo>0)
begin
if(@parametro=0)
begin
Select Id, Title, Description,CONCAT(@baseURL,'files/informative/',Image) as Image, UserModify, DatetimeModify, Active, Event as IdEvent, URL,0 as 'error_message' from [Informative]
 where Event in (select Id from Events where Active=1)
 and Active=1 
 order by DatetimeModify desc
end
else
begin
Select Id, Title, Description,CONCAT(@baseURL,'files/informative/',Image) as Image, UserModify, DatetimeModify, Active, Event as IdEvent, URL,0 as 'error_message' from [Informative]
where Event in (select Id from Events where Active=1) and Id=@parametro 
and Active=1 
order by DatetimeModify desc
end
end
else
begin
Select 1 as 'error_message'
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsSelectUsersScore]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsSelectUsersScore] (@token as nvarchar(100))
as
DECLARE @userId INT,
@Contador as int,
@baseURL as nvarchar(100)
Select @baseURL=baseURL from Config

Select @Contador=count(*) 
 from [Users] u left join Answers s on u.Id=s.IdUsuario 
 where s.IdEvento in (Select Id from [Events] where Active=1)

Select distinct @userId=IdUser from [Tokens] where Token=@token

SELECT IDENTITY(INT, 1, 1) AS RankPos,
    IdUsuario, 
    SUM(PuntosGanados) Puntos
INTO #ScoreTemp
FROM Answers
WHERE IdEvento in (Select Id from dbo.Events where Active=1)
GROUP BY IdUsuario
ORDER BY Puntos DESC

DECLARE @userRank INT
SET @userRank = (SELECT RankPos FROM #ScoreTemp WHERE IdUsuario = @userId)

 if(@Contador>0)
 begin
	IF(@userRank < 11)
		BEGIN
		Select * from
		(
				SELECT 
				TOP 10 0 as 'vacio', 
				ROW_NUMBER() OVER (ORDER BY Points DESC) AS Posicion, 
				Users.Id, 
				Users.Username,
				Users.FirstName, 
				Users.LastName, 
				Ranking.Points as Score, 
				CONCAT('http://cottonclub.motti.mx/admin/photos/', Users.photo) AS Photo ,
				0 as 'error_message'
				FROM (
						SELECT 
						Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
								(
								SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
								FROM Users u LEFT JOIN 
								Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								union all
								SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
								FROM Users u LEFT JOIN 
								answers as a on u.Id=a.IdUsuario
								where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1)
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
					) AS Ranking inner JOIN 
				Users ON Users.Id = Ranking.ID
				) Final where Posicion is not null 
				ORDER BY Final.Score DESC
		END
	ELSE
		BEGIN
		Select * from 
		(

				SELECT 
				TOP 10 0 as 'vacio', 
				ROW_NUMBER() OVER (ORDER BY Points DESC) AS Posicion, 
				Users.Id, 
				Users.Username,
				Users.FirstName, 
				Users.LastName, 
				Ranking.Points as Score, 
				CONCAT('http://cottonclub.motti.mx/admin/photos/', Users.photo) AS Photo ,
				0 as 'error_message'
				FROM (
						SELECT 
						Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
								(
								SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
								FROM Users u LEFT JOIN 
								Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								union all
								SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
								FROM Users u LEFT JOIN 
								answers as a on u.Id=a.IdUsuario
								where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1)
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
					) AS Ranking inner JOIN 
				Users ON Users.Id = Ranking.ID
				--) Final where Posicion is not null 
				--ORDER BY Final.Score DESC

				union

				SELECT 
				0 as 'vacio', 
				@userRank AS Posicion, 
				Users.Id, 
				Users.Username,
				Users.FirstName, 
				Users.LastName, 
				Ranking.Points as Score, 
				CONCAT('http://cottonclub.motti.mx/admin/photos/', Users.photo) AS Photo ,
				0 as 'error_message'
				FROM (
						SELECT 
						Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
								(
								SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
								FROM Users u LEFT JOIN 
								Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0 
								where u.Id=@userId
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								union all
								SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
								FROM Users u LEFT JOIN 
								answers as a on u.Id=a.IdUsuario
								where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1) 
								and u.Id=@userId
								GROUP BY u.ID, u.FirstName, u.LastName, Photo
								) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
					) AS Ranking inner JOIN 
				Users ON Users.Id = Ranking.ID
				) Final where Posicion is not null 

				--SELECT 
				--TOP 10 0 as 'vacio', 
				--@userRank AS Posicion, 
				--Users.Id, 
				--Users.Username,
				--Users.FirstName, 
				--Users.LastName, 
				--Ranking.Points as Score, 
				--CONCAT('http://cottonclub.motti.mx/admin/photos/', Users.photo) AS Photo ,
				--0 as 'error_message'
				--FROM (
				--		SELECT 
				--		Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
				--				(
				--				SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
				--				FROM Users u LEFT JOIN 
				--				Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0
				--				GROUP BY u.ID, u.FirstName, u.LastName, Photo
				--				union all
				--				SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
				--				FROM Users u LEFT JOIN 
				--				answers as a on u.Id=a.IdUsuario
				--				where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1)
				--				GROUP BY u.ID, u.FirstName, u.LastName, Photo
				--				) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
				--	) AS Ranking inner JOIN 
				--Users ON Users.Id = Ranking.ID 

--				union

--				SELECT 
--				0 as 'vacio', 
--				ROW_NUMBER() OVER (ORDER BY Points DESC) AS Posicion, 
--				Users.Id, 
--				Users.Username,
--				Users.FirstName, 
--				Users.LastName, 
--				Points as Score, 
--				CONCAT('http://cottonclub.motti.mx/admin/photos/', Users.photo) AS Photo ,
--				0 as 'error_message'
--				FROM (
--						SELECT 
--						Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo, SUM(Datos.Points) AS Points from
--								(
--								SELECT u.ID, u.FirstName, u.LastName, Photo, SUM(s.Points) AS Points 
--								FROM Users u LEFT JOIN 
--								Score as s on u.Id=s.IdUser and IdEvent in (Select top 1 Id from Events where Active=1) and IdAnswer=0 and IdQuestion=0 
----								where u.Id=@userId
--								GROUP BY u.ID, u.FirstName, u.LastName, Photo
--								union all
--								SELECT u.ID, u.FirstName, u.LastName, Photo, isnull(sum(a.PuntosGanados),0) AS Points 
--								FROM Users u LEFT JOIN 
--								answers as a on u.Id=a.IdUsuario
--								where a.IdUsuario=u.Id and a.IdEvento  in (Select top 1 Id from Events where Active=1) 
--								--and u.Id=@userId
--								GROUP BY u.ID, u.FirstName, u.LastName, Photo
--								) as Datos group by Datos.ID, Datos.FirstName, Datos.LastName, Datos.Photo
--					) AS Ranking inner JOIN 
--				Users ON Users.Id = Ranking.ID
--				--where Users.Id=@userId 
				--) Final where Posicion is not null 
				--ORDER BY Final.Score,Final.FirstName DESC
		END
end
else
	begin
		Select 
				1 as 'vacio' , 0 as 'error_message'
	end
	DROP TABLE #ScoreTemp

GO
/****** Object:  StoredProcedure [dbo].[usp_wsUploadPhoto]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsUploadPhoto]
(
@token as nvarchar(100),
@Photo as nvarchar(100)
)
as
Declare 
@Contador as int,
@IdUsuario as bigint,
@IdEvento as bigint

Select distinct @IdUsuario=IdUser from [Tokens] where Token=@token
/*
Select top 1 @IdEvento=Id from Events where Active=1
Select @Contador=count(*) from [Survey] where 
IdUsuario=(Select distinct IdUser from [Tokens] where Token=@token) 
and 
IdEvento=(Select top 1 Id from Events where Active=1)

if(@Contador=0)
begin
*/
	Update Users set 
		Photo=@Photo
	where 
	Id=@IdUsuario
/*end
*/
Select 'Imagen subida con exito' as message, 0 as error_message



GO
/****** Object:  StoredProcedure [dbo].[usp_wsValidateSurvey]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsValidateSurvey] 
(
@token as nvarchar(100)
)
as
Declare 
@Contador as int,
@IdUsuario as bigint,
@IdEvento as bigint

Select distinct @IdUsuario=IdUser from [Tokens] where Token=@token
Select top 1 @IdEvento=Id from Events where Active=1
Select @Contador=count(*) from [Survey] where 
IdUsuario=(Select distinct IdUser from [Tokens] where Token=@token) 
and 
IdEvento=(Select top 1 Id from Events where Active=1)

if(@Contador>0)
begin
	Select 0 as 'error_message', 'true' as mensaje
end
else
begin
	Select 0 as 'error_message', 'false' as mensaje
end

GO
/****** Object:  StoredProcedure [dbo].[usp_wsValidToken]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_wsValidToken] (@token as nvarchar(100))
as
Declare 
@Count as int

delete from Tokens where DateEndingValid<getdate()
Select @Count=count(*) from Tokens where Token=@token
if(@Count=0)
begin
	Select 0 as Valido, 1 as error_message
end
else
begin
	Select 1 as Valido, 0 as error_message
end

GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitString]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnSplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END

GO
/****** Object:  Table [dbo].[Answers]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[IdUsuario] [bigint] NULL,
	[GUID] [nvarchar](100) NULL,
	[IdPregunta] [bigint] NULL,
	[IdRespuesta] [int] NULL,
	[Intento] [int] NULL,
	[PuntosGanados] [decimal](18, 0) NULL,
	[IdEvento] [bigint] NULL,
	[DateTime] [datetime] NULL,
	[Pregunta123] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calc_Historico]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calc_Historico](
	[IdCotizacion] [bigint] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[Cotizacion] [varchar](256) NOT NULL,
	[IdEvento] [bigint] NOT NULL,
	[FechaHr] [datetime] NOT NULL,
	[Total] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[IdCotizacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Calc_Historico_Productos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calc_Historico_Productos](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCotizacion] [bigint] NOT NULL,
	[IdProducto] [bigint] NOT NULL,
	[Cantidad] [bigint] NULL,
	[Total] [decimal](18, 2) NOT NULL DEFAULT ((0.0)),
 CONSTRAINT [PK_Calc_Historico_Productos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calc_Historico_Semillas]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calc_Historico_Semillas](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCotizacion] [bigint] NOT NULL,
	[IdSemilla] [bigint] NOT NULL,
	[Cantidad] [bigint] NULL,
	[Total] [decimal](18, 2) NOT NULL DEFAULT ((0.0)),
 CONSTRAINT [PK_Calc_Historico_Semillas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calc_Incidencias]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calc_Incidencias](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Calc_Incidencias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Calc_Incidencias_Productos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calc_Incidencias_Productos](
	[IdIncidencia] [bigint] NOT NULL,
	[IdProducto] [bigint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calc_Productos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calc_Productos](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PrecioRegular] [decimal](18, 2) NOT NULL,
	[PrecioPreventa] [decimal](18, 2) NOT NULL,
	[Has] [decimal](18, 2) NOT NULL,
	[CantPorPaquete] [int] NOT NULL,
 CONSTRAINT [PK_Calc_Productos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Calc_Productos_Porcentaje]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calc_Productos_Porcentaje](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Paquetes] [int] NOT NULL,
	[Porcentaje] [decimal](18, 2) NOT NULL,
	[PorcentajeSemilla] [decimal](18, 2) NULL,
	[MontoProducto] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Productos_Porcentaje] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Calc_Regalos]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calc_Regalos](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Sacos] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Calc_Regalos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Calc_Variedades]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calc_Variedades](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PrecioRegular] [decimal](18, 2) NOT NULL,
	[PrecioPreventa] [decimal](18, 2) NOT NULL,
	[Sacos] [int] NOT NULL,
	[SemillasPorSacos] [int] NOT NULL,
	[HasPorSaco] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Calc_Variedades] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Companions]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companions](
	[IdUser] [bigint] NULL,
	[IdEvento] [bigint] NULL,
	[Companion] [nvarchar](100) NULL,
	[Age] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Config]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Config](
	[baseURL] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConfigRoot]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfigRoot](
	[baseURLRoot] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Diary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Diary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Lastname] [nvarchar](100) NULL,
	[ConferenceDescription] [text] NULL,
	[ConferenceTime] [time](7) NULL,
	[InformationLecturer] [text] NULL,
	[PhotoLecturer] [nvarchar](100) NULL,
	[AttachedImageLecturer] [nvarchar](100) NULL,
	[Presentation] [nvarchar](100) NULL,
	[IdEvent] [bigint] NULL,
	[ConferenceTimeEnd] [time](7) NULL,
	[ConferenceDate] [date] NULL,
	[Active] [int] NULL,
	[Ubication] [nvarchar](250) NULL,
	[Detail] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Distributor]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distributor](
	[Id] [bigint] NULL,
	[Decription] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Events]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Location] [text] NULL,
	[WelcomeEmail] [text] NULL,
	[Logo] [nvarchar](100) NULL,
	[Date] [datetime] NULL,
	[CalculatorDisplayTime] [time](7) NULL,
	[UserModify] [nvarchar](50) NULL,
	[DatetimeModify] [datetime] NULL,
	[Active] [int] NULL,
	[Address] [text] NULL,
	[Mapa] [nvarchar](100) NULL,
	[Length] [nvarchar](100) NULL,
	[Latitude] [nvarchar](100) NULL,
	[Gamification] [bit] NULL,
	[Calculator] [bit] NULL,
	[Survey] [bit] NULL,
	[GamificationTime] [time](7) NULL,
	[SurveyTime] [time](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Gamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gamification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Pregunta] [text] NULL,
	[Respuesta1] [text] NULL,
	[Respuesta2] [text] NULL,
	[Respuesta3] [text] NULL,
	[IdEvento] [bigint] NULL,
	[FechaCreacion] [date] NULL,
	[FechaModificacion] [date] NULL,
	[UsuarioModificacion] [nvarchar](100) NULL,
	[Activo] [int] NULL,
	[RespuestaCorrecta] [int] NULL,
	[Pregunta2] [text] NULL,
	[Respuesta2_1] [text] NULL,
	[Respuesta2_2] [text] NULL,
	[Respuesta2_3] [text] NULL,
	[RespuestaCorrecta2] [int] NULL,
	[Pregunta3] [text] NULL,
	[Respuesta3_1] [text] NULL,
	[Respuesta3_2] [text] NULL,
	[Respuesta3_3] [text] NULL,
	[RespuestaCorrecta3] [int] NULL,
	[Puntos] [decimal](18, 0) NULL,
	[PuntosFallo] [decimal](18, 0) NULL,
	[GUID] [nvarchar](100) NULL,
	[PuntosFallo3er] [decimal](18, 0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Informative]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Informative](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [text] NULL,
	[Description] [text] NULL,
	[Image] [nvarchar](100) NULL,
	[UserModify] [nvarchar](100) NULL,
	[DatetimeModify] [datetime] NULL,
	[Active] [int] NULL,
	[Event] [bigint] NULL,
	[URL] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Registration]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registration](
	[IdUser] [bigint] NULL,
	[IdEvent] [bigint] NULL,
	[RegistrationDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score](
	[IdUser] [bigint] NULL,
	[IdEvent] [bigint] NULL,
	[IdQuestion] [bigint] NULL,
	[IdAnswer] [bigint] NULL,
	[Points] [decimal](18, 0) NULL,
	[DateTime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Survey]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Survey](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [bigint] NULL,
	[IdEvento] [bigint] NULL,
	[Respuesta1] [int] NULL,
	[Respuesta2] [int] NULL,
	[Respuesta3] [int] NULL,
	[Respuesta4] [int] NULL,
	[Respuesta5] [int] NULL,
	[Respuesta6] [bigint] NULL,
	[Datetime] [datetime] NULL,
	[Comentarios] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tokens]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tokens](
	[IdUser] [bigint] NULL,
	[Token] [nvarchar](100) NULL,
	[DateBeginningValid] [datetime] NULL,
	[DateEndingValid] [datetime] NULL,
	[Valid] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User_Diary]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Diary](
	[IdUser] [bigint] NULL,
	[IdDiary] [bigint] NULL,
	[DateTime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User_Gamification]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Gamification](
	[IdUsuario] [bigint] NULL,
	[IdGamificacion] [bigint] NULL,
	[DateTime] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 18/05/2017 07:50:22 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[CorreoElectronico] [nvarchar](100) NULL,
	[UserModify] [nvarchar](50) NULL,
	[DateTimeCreate] [datetime] NULL,
	[DateTimeModify] [datetime] NULL,
	[Active] [int] NULL,
	[Administrador] [int] NULL,
	[IdEvento] [bigint] NULL,
	[photo] [nvarchar](100) NULL,
	[PIN] [nvarchar](7) NULL,
	[Companions] [int] NULL,
	[PreRegister] [int] NULL,
	[Telefono] [nvarchar](50) NULL,
	[Ocupacion] [nvarchar](100) NULL,
	[Estado] [nvarchar](50) NULL,
	[Municipio] [nvarchar](100) NULL,
	[IdDistribuidor] [bigint] NULL
) ON [PRIMARY]

GO
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'25E68976D207403083A55DF5B6E8EB01', 1, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:19:40.693' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (233, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 13:09:24.107' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (132, N'25E68976D207403083A55DF5B6E8EB01', 1, 2, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:11:31.037' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:14:38.060' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (230, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:52.450' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:05.893' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (225, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:06.330' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (230, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:46.587' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (139, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, NULL, 0, CAST(0 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:15.293' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:19.723' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:59:38.490' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (217, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:04.790' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (228, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:34.180' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (240, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:37.893' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:21.130' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:21.333' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (244, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:21.997' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:16:56.157' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:37.160' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'25E68976D207403083A55DF5B6E8EB01', 1, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:46:55.473' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (217, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 13:20:31.520' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (237, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 13:57:53.197' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (132, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:38.990' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'625921E2EB844AC18DE7B5A032284387', 14, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:45.927' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (226, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:46.477' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'625921E2EB844AC18DE7B5A032284387', 14, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:57.307' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'625921E2EB844AC18DE7B5A032284387', 14, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:18.800' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'713D4953538849859520E5499830303B', 2, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:54.383' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (139, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:55.070' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:06.087' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:06.830' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'713D4953538849859520E5499830303B', 2, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:31.203' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (241, N'25E68976D207403083A55DF5B6E8EB01', 1, NULL, 1, CAST(0 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:14:40.613' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:03.733' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (236, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:04.157' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (139, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:06.043' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (233, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:11.993' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (226, N'C883BCBC964E4CD19B6905940C977D56', 11, 3, 2, CAST(3 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:12.470' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:13.040' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (142, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:17.600' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (139, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:20.370' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:20.410' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (132, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:21.180' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:23.230' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:23.397' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (244, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:27.913' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (225, N'C883BCBC964E4CD19B6905940C977D56', 11, 3, 2, CAST(3 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:27.980' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (231, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 2, CAST(3 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:30.573' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (181, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:54.713' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:58:24.650' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (227, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 2, CAST(3 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:58:44.927' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:14.297' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (226, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:14.553' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (218, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:15.887' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:16.130' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (234, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:17.403' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (142, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:17.700' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:38.067' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:45.140' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-31 00:01:18.623' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:56.370' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:07.253' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (236, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:18.010' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (230, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:34.857' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'410A597CA9E94AD9819EB3291DB0948C', 12, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-31 00:05:22.333' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:19:21.417' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'25E68976D207403083A55DF5B6E8EB01', 1, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:51:26.513' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (230, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:05.387' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (231, N'713D4953538849859520E5499830303B', 2, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:02.533' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (244, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:02:40.137' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (217, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:22.540' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (240, N'C883BCBC964E4CD19B6905940C977D56', 11, 3, 2, CAST(3 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:28.403' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'25E68976D207403083A55DF5B6E8EB01', 1, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 22:56:42.557' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'6F27BE813B444E138642742D66806560', 4, 2, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-31 00:03:30.230' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (139, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:18.297' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (217, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:49.180' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (180, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:02.707' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:07.230' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (231, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:00:11.210' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (234, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:25.903' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (236, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:58:43.780' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (225, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:19.477' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (231, N'25E68976D207403083A55DF5B6E8EB01', 1, 2, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:41:42.637' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (228, N'25E68976D207403083A55DF5B6E8EB01', 1, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:47:27.247' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:06.360' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:17.450' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (225, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 12:12:21.733' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (234, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 13:12:48.147' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'25E68976D207403083A55DF5B6E8EB01', 1, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 13:49:45.660' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (216, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 14:37:00.290' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (236, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:45.600' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'625921E2EB844AC18DE7B5A032284387', 14, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:47.330' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:48.300' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'625921E2EB844AC18DE7B5A032284387', 14, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:33:54.290' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (242, N'625921E2EB844AC18DE7B5A032284387', 14, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:04.580' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (218, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:15.280' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (180, N'625921E2EB844AC18DE7B5A032284387', 14, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:41.147' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (135, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:52.040' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (235, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:54.103' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (226, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:34:54.703' AS DateTime), 2)
GO
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (218, N'713D4953538849859520E5499830303B', 2, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:08.657' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (234, N'713D4953538849859520E5499830303B', 2, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:11.733' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (225, N'713D4953538849859520E5499830303B', 2, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:14.323' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'713D4953538849859520E5499830303B', 2, 2, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:16.547' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (236, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:31.817' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (132, N'713D4953538849859520E5499830303B', 2, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:35:31.840' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (142, N'25E68976D207403083A55DF5B6E8EB01', 1, 3, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:56:34.877' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (234, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:05.813' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:06.377' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (153, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:10.470' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (229, N'7AFAF5DF72844992ABBD61181CE0810E', 15, 1, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 15:59:34.797' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (220, N'C883BCBC964E4CD19B6905940C977D56', 11, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:20.903' AS DateTime), 1)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (218, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:46.357' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (224, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 16:57:50.067' AS DateTime), 3)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (222, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 2, CAST(5 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:36:46.543' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (244, N'6F27BE813B444E138642742D66806560', 4, 3, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-30 17:47:36.070' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'C883BCBC964E4CD19B6905940C977D56', 11, 2, 1, CAST(10 AS Decimal(18, 0)), 1, CAST(N'2017-03-31 00:00:24.153' AS DateTime), 2)
INSERT [dbo].[Answers] ([IdUsuario], [GUID], [IdPregunta], [IdRespuesta], [Intento], [PuntosGanados], [IdEvento], [DateTime], [Pregunta123]) VALUES (1, N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', 13, 1, 2, CAST(7 AS Decimal(18, 0)), 1, CAST(N'2017-03-31 00:06:04.480' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Calc_Historico] ON 

INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (121, 178, N'be6548a8-55de-4862-ac93-be4781c0f15b.pdf', 1, CAST(N'2017-03-02 00:18:23.417' AS DateTime), CAST(36969029.32 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (123, 187, N'9a36b6ba-8573-40df-8e8c-a96496b04b8b.pdf', 1, CAST(N'2017-03-02 19:48:55.857' AS DateTime), CAST(8546113.44 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (124, 148, N'612d74ae-828a-45f7-83e1-c1db9838f1c3.pdf', 1, CAST(N'2017-03-02 19:55:29.927' AS DateTime), CAST(10843835.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (125, 141, N'd3278798-2a54-4a51-ac07-9dc61ebe6ed8.pdf', 1, CAST(N'2017-03-02 19:58:04.797' AS DateTime), CAST(3982127.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (126, 146, N'49982243-b32b-448a-9aff-260a98ac4ed8.pdf', 1, CAST(N'2017-03-02 19:59:42.080' AS DateTime), CAST(8835395.76 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (127, 165, N'115c4216-ca4f-4393-b6ab-da0319b1895e.pdf', 1, CAST(N'2017-03-02 20:01:23.220' AS DateTime), CAST(5539597.96 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (128, 141, N'033d32ba-3f48-406b-b67e-233c3e7ae7ac.pdf', 1, CAST(N'2017-03-02 20:02:19.407' AS DateTime), CAST(3308860.80 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (129, 177, N'5cfd2dd1-878d-4052-81e6-2de32c8d2ebc.pdf', 1, CAST(N'2017-03-02 20:03:59.573' AS DateTime), CAST(2020509.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (130, 141, N'9daf1a06-6ea6-49b8-9423-c3612b70a153.pdf', 1, CAST(N'2017-03-02 20:04:38.110' AS DateTime), CAST(3094360.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (131, 161, N'2807da85-2ac4-43b2-82b5-8ccdcc210d8f.pdf', 1, CAST(N'2017-03-02 20:05:59.297' AS DateTime), CAST(5663552.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (132, 132, N'79a58518-8cfc-4180-9d9c-57f447c43af6.pdf', 1, CAST(N'2017-03-02 20:06:11.827' AS DateTime), CAST(43199660.32 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (133, 212, N'205c375a-ebc7-4937-aaab-6f9fd55ef1c1.pdf', 1, CAST(N'2017-03-02 20:08:52.673' AS DateTime), CAST(13266360.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (134, 161, N'7fbf2b99-9cd5-4d0e-8842-c077557385dc.pdf', 1, CAST(N'2017-03-02 20:12:27.853' AS DateTime), CAST(2607220.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (135, 157, N'3a6fa57a-8ebc-4f41-87f2-2483c74d8cd4.pdf', 1, CAST(N'2017-03-02 20:12:45.000' AS DateTime), CAST(11387051.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (136, 154, N'14fda83a-8768-4427-8063-fa791faca8ff.pdf', 1, CAST(N'2017-03-02 20:12:49.070' AS DateTime), CAST(28533555.04 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (137, 134, N'7d0ccac9-8016-4bea-931f-4564f90b45e0.pdf', 1, CAST(N'2017-03-02 20:13:42.090' AS DateTime), CAST(55488679.48 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (138, 176, N'7eb3699b-11f8-452c-adae-66ecdccdbdd5.pdf', 1, CAST(N'2017-03-02 20:15:58.450' AS DateTime), CAST(2237676.56 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (139, 150, N'a7c01df3-0e56-4525-9426-1d2334622341.pdf', 1, CAST(N'2017-03-02 20:17:40.013' AS DateTime), CAST(63867923.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (140, 212, N'f8c6bc2a-b8a5-46b9-8293-4cc4beb0e118.pdf', 1, CAST(N'2017-03-02 20:20:31.977' AS DateTime), CAST(1441479.16 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (141, 141, N'fbb409b6-3ad9-4cf6-aadd-a80e0054cab3.pdf', 1, CAST(N'2017-03-02 20:21:12.197' AS DateTime), CAST(4434779.72 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (142, 180, N'e285f1d8-abcf-4680-a184-512aa77bdf7d.pdf', 1, CAST(N'2017-03-02 20:27:12.200' AS DateTime), CAST(2633932.96 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (143, 172, N'dfa597e5-35e0-4aea-980e-b4be3f8384ef.pdf', 1, CAST(N'2017-03-02 20:32:19.587' AS DateTime), CAST(5443288.24 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (144, 173, N'63d617b1-65bd-45da-9cec-e26d10401ca2.pdf', 1, CAST(N'2017-03-02 20:33:52.560' AS DateTime), CAST(6107994.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (145, 152, N'fc0125a5-d0ee-4b6d-8e5a-c45d302ab348.pdf', 1, CAST(N'2017-03-02 20:36:42.907' AS DateTime), CAST(13398893.20 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (146, 196, N'0da5c928-733e-42ed-9048-d584062bf295.pdf', 1, CAST(N'2017-03-02 20:37:09.000' AS DateTime), CAST(1199436.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (147, 135, N'b2c01f28-2c5a-4f2c-9ad2-dfbc7c79bac3.pdf', 1, CAST(N'2017-03-02 20:38:56.567' AS DateTime), CAST(3710219.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (148, 135, N'134add51-789f-428a-95bc-9a305a1de675.pdf', 1, CAST(N'2017-03-02 20:44:45.340' AS DateTime), CAST(13147604.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (149, 140, N'82dfae00-b1ac-4a83-99c3-4238a08a73c7.pdf', 1, CAST(N'2017-03-02 20:46:46.850' AS DateTime), CAST(10570810.16 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (150, 207, N'58e7ccf3-5d60-4035-ade0-b9bda74a82e1.pdf', 1, CAST(N'2017-03-02 20:49:13.623' AS DateTime), CAST(9121750.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (151, 207, N'9fd162a6-d526-4aee-998b-e5649f33fa7e.pdf', 1, CAST(N'2017-03-02 20:50:29.550' AS DateTime), CAST(1087841.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (152, 137, N'edc3e75e-4cb4-456b-b6cb-a7eda8c6291a.pdf', 1, CAST(N'2017-03-02 21:47:39.630' AS DateTime), CAST(14232058.40 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (155, 1, N'bf4d44fc-d1dd-41d4-88ed-67c772e31bd9.pdf', 1, CAST(N'2017-03-08 12:04:00.733' AS DateTime), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (156, 1, N'15e5d269-3eeb-4871-8c33-71e45125655f.pdf', 1, CAST(N'2017-03-09 16:23:27.127' AS DateTime), CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico] ([IdCotizacion], [IdUsuario], [Cotizacion], [IdEvento], [FechaHr], [Total]) VALUES (157, 1, N'9e8ab414-dac3-4c69-933d-a5a798035956.pdf', 1, CAST(N'2017-03-09 16:29:55.330' AS DateTime), CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Calc_Historico] OFF
SET IDENTITY_INSERT [dbo].[Calc_Historico_Productos] ON 

INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (128, 121, 1, 0, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (129, 121, 3, 0, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (130, 121, 4, 0, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (134, 123, 1, 166, CAST(411348.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (135, 123, 2, 498, CAST(845106.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (136, 123, 3, 498, CAST(422304.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (137, 123, 4, 498, CAST(241032.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (138, 123, 5, 1992, CAST(139440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (139, 123, 6, 250, CAST(509250.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (140, 123, 7, 1992, CAST(760944.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (141, 123, 8, 498, CAST(1050780.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (142, 124, 1, 441, CAST(1092798.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (143, 125, 1, 166, CAST(411348.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (144, 126, 1, 333, CAST(825174.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (145, 126, 3, 999, CAST(847152.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (146, 127, 1, 183, CAST(453474.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (147, 127, 3, 549, CAST(465552.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (148, 127, 4, 549, CAST(265716.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (149, 127, 5, 2196, CAST(153720.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (150, 128, 1, 120, CAST(297360.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (151, 129, 1, 75, CAST(185850.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (152, 129, 3, 225, CAST(190800.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (153, 130, 1, 113, CAST(280014.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (154, 132, 1, 1466, CAST(3632748.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (155, 132, 3, 4398, CAST(3729504.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (156, 132, 5, 17592, CAST(1231440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (157, 133, 1, 500, CAST(1239000.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (158, 133, 3, 1500, CAST(1272000.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (159, 134, 1, 109, CAST(270102.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (160, 135, 1, 416, CAST(1030848.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (161, 135, 3, 1248, CAST(1058304.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (162, 135, 5, 4992, CAST(349440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (163, 136, 1, 1058, CAST(2621724.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (164, 136, 3, 3174, CAST(2691552.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (165, 137, 4, 6423, CAST(3108732.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (166, 137, 5, 25692, CAST(1798440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (167, 138, 1, 89, CAST(220542.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (168, 138, 5, 1068, CAST(74760.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (169, 139, 5, 33996, CAST(2379720.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (170, 140, 4, 159, CAST(76956.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (171, 140, 5, 636, CAST(44520.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (172, 141, 1, 175, CAST(433650.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (173, 142, 1, 93, CAST(230454.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (174, 142, 3, 279, CAST(236592.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (175, 142, 5, 1116, CAST(78120.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (176, 143, 1, 192, CAST(475776.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (177, 143, 3, 576, CAST(488448.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (178, 144, 1, 216, CAST(535248.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (179, 144, 3, 648, CAST(549504.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (180, 145, 1, 535, CAST(1325730.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (181, 146, 1, 50, CAST(123900.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (182, 147, 5, 1992, CAST(139440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (183, 149, 1, 366, CAST(906948.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (184, 149, 4, 1098, CAST(531432.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (185, 149, 5, 4392, CAST(307440.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (186, 151, 1, 41, CAST(101598.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (187, 151, 3, 123, CAST(104304.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (188, 152, 1, 558, CAST(1382724.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (189, 152, 5, 6696, CAST(468720.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (194, 155, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (195, 155, 3, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (196, 156, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (197, 156, 3, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (198, 157, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Productos] ([Id], [IdCotizacion], [IdProducto], [Cantidad], [Total]) VALUES (199, 157, 3, NULL, CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Calc_Historico_Productos] OFF
SET IDENTITY_INSERT [dbo].[Calc_Historico_Semillas] ON 

INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (70, 121, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (71, 121, 4, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (72, 121, 5, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (74, 123, 5, 664, CAST(4165909.44 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (75, 124, 1, 1452, CAST(7808391.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (76, 124, 4, 312, CAST(1942646.16 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (77, 125, 1, 664, CAST(3644032.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (78, 126, 1, 1332, CAST(7163069.76 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (79, 127, 5, 164, CAST(1028929.44 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (80, 127, 4, 164, CAST(1021134.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (81, 127, 1, 400, CAST(2151072.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (82, 128, 5, 480, CAST(3011500.80 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (83, 129, 1, 264, CAST(1419707.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (84, 129, 4, 36, CAST(224151.48 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (85, 130, 4, 452, CAST(2814346.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (86, 131, 1, 920, CAST(5048960.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (87, 131, 4, 96, CAST(614592.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (88, 132, 1, 2344, CAST(12605281.92 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (89, 132, 5, 1760, CAST(11042169.60 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (90, 132, 4, 1760, CAST(10958516.80 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (91, 133, 1, 2000, CAST(10755360.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (92, 134, 1, 416, CAST(2237114.88 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (93, 134, 4, 8, CAST(49811.44 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (94, 134, 5, 8, CAST(50191.68 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (95, 135, 1, 1664, CAST(8948459.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (96, 136, 1, 3664, CAST(19703819.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (97, 136, 4, 464, CAST(2889063.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (98, 136, 5, 100, CAST(627396.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (99, 137, 5, 3032, CAST(19022646.72 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (100, 137, 1, 3400, CAST(18284112.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (101, 137, 4, 2132, CAST(13274748.76 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (102, 138, 1, 324, CAST(1742368.32 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (103, 138, 4, 16, CAST(99622.88 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (104, 138, 5, 16, CAST(100383.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (105, 139, 1, 10664, CAST(57347579.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (106, 139, 4, 532, CAST(3312460.76 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (107, 139, 5, 132, CAST(828162.72 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (108, 140, 4, 212, CAST(1320003.16 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (109, 141, 1, 400, CAST(2151072.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (110, 141, 4, 148, CAST(921511.64 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (111, 141, 5, 148, CAST(928546.08 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (112, 142, 1, 240, CAST(1290643.20 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (113, 142, 4, 104, CAST(647548.72 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (114, 142, 5, 24, CAST(150575.04 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (115, 143, 1, 368, CAST(1978986.24 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (116, 143, 4, 200, CAST(1245286.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (117, 143, 5, 200, CAST(1254792.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (118, 144, 1, 432, CAST(2323157.76 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (119, 144, 4, 216, CAST(1344908.88 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (120, 144, 5, 216, CAST(1355175.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (121, 145, 1, 1480, CAST(7958966.40 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (122, 145, 4, 560, CAST(3486800.80 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (123, 145, 5, 100, CAST(627396.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (124, 146, 1, 200, CAST(1075536.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (125, 147, 1, 664, CAST(3570779.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (126, 148, 1, 780, CAST(4280640.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (127, 148, 5, 920, CAST(5845220.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (128, 148, 4, 472, CAST(3021744.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (129, 149, 1, 364, CAST(1957475.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (130, 149, 4, 712, CAST(4433218.16 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (131, 149, 5, 388, CAST(2434296.48 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (132, 150, 5, 500, CAST(3176750.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (133, 150, 4, 500, CAST(3201000.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (134, 150, 1, 500, CAST(2744000.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (135, 151, 1, 164, CAST(881939.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (136, 152, 1, 1764, CAST(9486227.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (137, 152, 4, 352, CAST(2191703.36 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (138, 152, 5, 112, CAST(702683.52 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (141, 155, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (142, 156, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Historico_Semillas] ([Id], [IdCotizacion], [IdSemilla], [Cantidad], [Total]) VALUES (143, 157, 1, NULL, CAST(0.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Calc_Historico_Semillas] OFF
SET IDENTITY_INSERT [dbo].[Calc_Incidencias] ON 

INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (2, N'Suelen haber heladas tempranas en mi región')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (3, N'Problemas con Thrips al inicio del ciclo')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (4, N'Problemas de viruela del algodón.')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (5, N'Necesito un fertilizante foliar completo')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (6, N'Resistencia de malezas a herbicidas')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (7, N'¿Sueles tener descuentos por fibra manchada?')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (8, N'Tengo problemas con picudo o mosca blanca')
INSERT [dbo].[Calc_Incidencias] ([Id], [Nombre]) VALUES (9, N'Tengo problemas de araña roja')
SET IDENTITY_INSERT [dbo].[Calc_Incidencias] OFF
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (4, 2)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (7, 3)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (9, 8)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (6, 7)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (3, 4)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (5, 5)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (2, 1)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (2, 3)
INSERT [dbo].[Calc_Incidencias_Productos] ([IdIncidencia], [IdProducto]) VALUES (8, 6)
SET IDENTITY_INSERT [dbo].[Calc_Productos] ON 

INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (1, N'FINISH PRO 2X9.46L BOT', CAST(2555.00 AS Decimal(18, 2)), CAST(2478.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (2, N'CONSIST MAX 12X1L BOT', CAST(1749.00 AS Decimal(18, 2)), CAST(1697.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (3, N'GINSTAR 8X1L BOT', CAST(874.00 AS Decimal(18, 2)), CAST(848.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (4, N'MURALLA MAX 20X500ML BOT', CAST(499.00 AS Decimal(18, 2)), CAST(484.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (5, N'BAYFOLAN FORTE 12X1L BOT', CAST(72.00 AS Decimal(18, 2)), CAST(70.00 AS Decimal(18, 2)), CAST(0.50 AS Decimal(18, 2)), 12)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (6, N'CALYPSO 12X1L BOT', CAST(2100.00 AS Decimal(18, 2)), CAST(2037.00 AS Decimal(18, 2)), CAST(4.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (7, N'FINALE ULTRA 12X1L BOT', CAST(394.00 AS Decimal(18, 2)), CAST(382.00 AS Decimal(18, 2)), CAST(0.50 AS Decimal(18, 2)), 12)
INSERT [dbo].[Calc_Productos] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Has], [CantPorPaquete]) VALUES (8, N'OBERON 12X1L', CAST(2175.00 AS Decimal(18, 2)), CAST(2110.00 AS Decimal(18, 2)), CAST(2.00 AS Decimal(18, 2)), 3)
SET IDENTITY_INSERT [dbo].[Calc_Productos] OFF
SET IDENTITY_INSERT [dbo].[Calc_Productos_Porcentaje] ON 

INSERT [dbo].[Calc_Productos_Porcentaje] ([Id], [Cantidad], [Paquetes], [Porcentaje], [PorcentajeSemilla], [MontoProducto]) VALUES (1, 1, 250000, CAST(0.03 AS Decimal(18, 2)), CAST(0.03 AS Decimal(18, 2)), CAST(120000.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Calc_Productos_Porcentaje] OFF
SET IDENTITY_INSERT [dbo].[Calc_Variedades] ON 

INSERT [dbo].[Calc_Variedades] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Sacos], [SemillasPorSacos], [HasPorSaco]) VALUES (1, N'FM 2484B2F CMN 220K', CAST(5488.00 AS Decimal(18, 2)), CAST(5377.68 AS Decimal(18, 2)), 4, 220000, CAST(1.50 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Variedades] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Sacos], [SemillasPorSacos], [HasPorSaco]) VALUES (4, N'FM 2334GLT CMN 220K', CAST(6402.00 AS Decimal(18, 2)), CAST(6226.43 AS Decimal(18, 2)), 4, 220000, CAST(1.50 AS Decimal(18, 2)))
INSERT [dbo].[Calc_Variedades] ([Id], [Nombre], [PrecioRegular], [PrecioPreventa], [Sacos], [SemillasPorSacos], [HasPorSaco]) VALUES (5, N'FM 1830GLT CMN 220K', CAST(6353.50 AS Decimal(18, 2)), CAST(6273.96 AS Decimal(18, 2)), 4, 220000, CAST(1.50 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Calc_Variedades] OFF
INSERT [dbo].[Companions] ([IdUser], [IdEvento], [Companion], [Age]) VALUES (1, 1, N'Prueba 1', 18)
INSERT [dbo].[Companions] ([IdUser], [IdEvento], [Companion], [Age]) VALUES (1, 1, N'Prueba 2', 15)
INSERT [dbo].[Companions] ([IdUser], [IdEvento], [Companion], [Age]) VALUES (3, 1, N'Carlos Pérez', 78)
INSERT [dbo].[Companions] ([IdUser], [IdEvento], [Companion], [Age]) VALUES (3, 1, N'Juan Solis', 39)
INSERT [dbo].[Companions] ([IdUser], [IdEvento], [Companion], [Age]) VALUES (12, 1, N'Mariana Silva', 32)
INSERT [dbo].[Config] ([baseURL]) VALUES (N'http://cottonclub.motti.mx/admin/')
INSERT [dbo].[ConfigRoot] ([baseURLRoot]) VALUES (N'http://cottonclub.motti.mx/')
SET IDENTITY_INSERT [dbo].[Diary] ON 

INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (17, N'Hugo', N'Cruz Hipolito', N'Finale, alternativa para control de malezas', CAST(N'15:50:00' AS Time), N'Ingeniero agrónomo Parasitología Agrícola
Doctorado en Producción y Protección Vegetal con especialidad de Resistencia de Malezas a Herbicidas
2004-2011 He trabajado en la Universidad de Córdoba, en la confirmación de resistencia a herbicidas en especies provenientes de Latinoamérica. También en los estudios  bioquímicos y moleculares  para determinar el/los mecanismos de resistencia implicados.
Bayer desde 2011 a 2013 como Representante de Desarrollo Agronómico en R2
2013-2016 Gerente de Desarrollo de Herbicidas
2017 Technical Services para Herbicidas y Fungicidas
Trabajamos en la detección, confirmación y caracterización de la resistencia de malezas a herbicidas en varios cultivos de México.', N'Hugo.jpg', N'Digital.png', N'Hugo.pdf', 1, CAST(N'16:20:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (18, N'', N'', N'Salida a Tour', CAST(N'18:05:00' AS Time), N'', N'', N'', N'', 1, CAST(N'19:05:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Recepción de Hotel', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (3, N'------------', N'------------', N'Registro y Buffet de Bienvenida', CAST(N'12:00:00' AS Time), N'------------', N'', N'', N'', 1, CAST(N'14:00:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (19, N'', N'', N'Tour por Chihuahua', CAST(N'19:05:00' AS Time), N'', N'', N'', N'', 1, CAST(N'20:35:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Ciudad de Chihuahua', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (20, N'', N'', N'Traslado a Hotel', CAST(N'20:35:00' AS Time), N'', N'', N'', N'', 1, CAST(N'21:05:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Ciudad de Chihuahua', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (6, N'Bitia', N'Osorio', N'Bienvenida al Bayer Cotton Club 2017.', CAST(N'14:00:00' AS Time), N'Bayer Cotton Club 2017', N'Bitia.jpg', N'Bitiaalg.jpg', N'', 1, CAST(N'14:20:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (7, N'German', N'Tapia Galvan', N'Conferencia', CAST(N'12:30:00' AS Time), N'Descripcion', N'', N'', N'', 2, CAST(N'13:30:00' AS Time), CAST(N'0002-01-20' AS Date), 1, N'ubicacion', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (8, N'Abraham', N'Sandoval', N'Seeds Tracking', CAST(N'14:50:00' AS Time), N'Seeds Tracking una apuesta para el futuro de algodón en México.', N'ASR.jpg', N'Digital.JPG', N'SDS_MKT_SeedsTracking_SP.pdf', 1, CAST(N'15:20:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (9, N'Josefina', N'Perea', N'Cumplimiento regulatorio', CAST(N'14:20:00' AS Time), N' Ingeniero Agrónoma especializada en manejo de plagas y enfermedades. 

Ha estudiado una maestría de agricultura en Israel. Ha vivido y trabajado en el extranjero en países como Alemania, Israel y Estados Unidos. Le gusta viajar y hacer recorridos de caminata en los bosques. Quiere ser también apicultora. ', N'Josefina.jpg', N'PB151459.JPG', N'Josefina.pdf', 1, CAST(N'14:50:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (10, N'Pedro', N'Flores', N'Acondicionadores para la cosecha de Algodón', CAST(N'15:20:00' AS Time), N'Field Marketing Specialist de Bayer en México

Originario de Cuauhtémoc, Chihuahua cuenta con una amplia experiencia en los cultivos de Algodón, Maíz, Nogal, Manzano y Hortalizas.
', N'FullSizeRender.jpg', N'Ginstar.jpg', N'Pedro.pdf', 1, CAST(N'15:50:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (11, N'------------', N'------------', N'Coffe Break', CAST(N'16:20:00' AS Time), N'', N'', N'', N'', 1, CAST(N'16:35:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (21, N'', N'', N'Cena y premiacion', CAST(N'21:05:00' AS Time), N'', N'', N'', N'', 1, CAST(N'23:05:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (14, N'Gerardo', N'González', N'Oferta de valor de las nuevas variedades', CAST(N'17:05:00' AS Time), N'Oferta de valor de las nuevas variedades', N'Gerardo.jpg', N'Banco.jpg', N'Presentation BCC 2 Marzo 2017.pdf', 1, CAST(N'17:35:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (16, N'------------', N'------------', N'Foto grupal de Bayer Cotton Club', CAST(N'17:35:00' AS Time), N'', N'', N'', N'', 1, CAST(N'18:05:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'', 0)
INSERT [dbo].[Diary] ([Id], [Name], [Lastname], [ConferenceDescription], [ConferenceTime], [InformationLecturer], [PhotoLecturer], [AttachedImageLecturer], [Presentation], [IdEvent], [ConferenceTimeEnd], [ConferenceDate], [Active], [Ubication], [Detail]) VALUES (12, N'Diego', N'Mérida', N'Resultados de variedades en 2017', CAST(N'16:35:00' AS Time), N'Resultados de ensayos con variedades de algodón 2017', N'Diego.jpg', N'siembra.jpg', N'PRESENTACION  DIEGO.pdf', 1, CAST(N'17:05:00' AS Time), CAST(N'1900-01-01' AS Date), 1, N'Salón "Courtyard"', 1)
SET IDENTITY_INSERT [dbo].[Diary] OFF
INSERT [dbo].[Distributor] ([Id], [Decription]) VALUES (1, N'SEMILLAS E INSUMOS RODRIGUEZ S.A. DE C.V.')
INSERT [dbo].[Distributor] ([Id], [Decription]) VALUES (2, N'SEMILLAS ESCALERA DE LA LAGUNA  S.A. DE C.V.')
SET IDENTITY_INSERT [dbo].[Events] ON 

INSERT [dbo].[Events] ([Id], [Name], [Location], [WelcomeEmail], [Logo], [Date], [CalculatorDisplayTime], [UserModify], [DatetimeModify], [Active], [Address], [Mapa], [Length], [Latitude], [Gamification], [Calculator], [Survey], [GamificationTime], [SurveyTime]) VALUES (1, N'Bayer Cotton Club 2017 VIP', N'Courtyard Chihuahua, Vía Lombardía 5702, Citadela D1, Saucito, 31114 Chihuahua, CHIH, Mexico', N'<p>   </p>
', N'logo_cotton@2x.png', CAST(N'2017-03-31 00:00:00.000' AS DateTime), CAST(N'23:59:00' AS Time), N'admin', CAST(N'2017-03-30 23:08:50.830' AS DateTime), 0, N'Courtyard Chihuahua, Vía Lombardía 5702, Citadela D1, Saucito, 31114 Chihuahua, CHIH, Mexico', N'Croquis.jpg', N'-106.12759', N'28.66272', 1, 0, 1, CAST(N'22:59:00' AS Time), CAST(N'20:00:00' AS Time))
INSERT [dbo].[Events] ([Id], [Name], [Location], [WelcomeEmail], [Logo], [Date], [CalculatorDisplayTime], [UserModify], [DatetimeModify], [Active], [Address], [Mapa], [Length], [Latitude], [Gamification], [Calculator], [Survey], [GamificationTime], [SurveyTime]) VALUES (2, N'Bayer Cotton Club 2018 VIP', N'Courtyard Chihuahua, Vía Lombardía 5702, Citadela D1, Saucito, 31114 Chihuahua, CHIH, Mexico', N'<p>¡Bienvenidos a Cotton Club 2017!</p>
', N'Aqua0.jpg', CAST(N'2018-03-30 00:00:00.000' AS DateTime), CAST(N'23:00:00' AS Time), N'admin', CAST(N'2017-03-31 00:08:53.083' AS DateTime), 1, N'Courtyard Chihuahua, Vía Lombardía 5702, Citadela D1, Saucito, 31114 Chihuahua, CHIH, Mexico', N'candado.jpg', N'-106.12759', N'28.66272', 1, 0, 1, CAST(N'20:00:00' AS Time), CAST(N'20:00:00' AS Time))
SET IDENTITY_INSERT [dbo].[Events] OFF
SET IDENTITY_INSERT [dbo].[Gamification] ON 

INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (1, N'¿Cuál es el principal beneficio de Finish 6 Pro?', N'A. Ser amigable con el medio ambiente', N'B. Optimizar la cosecha del algodón (menos pasadas)', N'C. Uniformizar el tamaño de las bellotas', 1, CAST(N'2017-01-29' AS Date), CAST(N'2017-01-29' AS Date), N'test@dominio.com', 1, 2, N'¿Cuál es la dosis que deberías de ocupar de Finish 6 Pro?', N'A. 2.5 litros / hectárea', N'B. 1.0 litros / hectárea', N'C. 4.0 litros / hectárea.', 1, N'¿Cuál es el complemento perfecto de Finish 6 Pro?', N'A. Muralla Max', N'B. Fibermax', N'C. Ginstar', 3, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'25E68976D207403083A55DF5B6E8EB01', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (2, N'¿Con base en que información se debe de ajustar la dosis de Ginstar?', N'A. Humedad relativa', N'B. Velocidad de viento', N'C. Temperatura', 1, CAST(N'2017-02-01' AS Date), CAST(N'2017-02-01' AS Date), N'test@dominio.com', 1, 3, N'¿Cuál es el principal beneficio de Ginstar?', N'A. Proteger el valor de tu cosecha', N'B. Funcionar en temperaturas bajas y altas', N'C. Acelerar la apertura de las bellotas.', 1, N'¿Cuál es la dosis mínima y máxima recomendada?', N'A. 0.5 a 0.8 litros / hectárea', N'B. 1 a 2 litros', N'C. Depende de la dosis de Finish', 1, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'713D4953538849859520E5499830303B', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (11, N'SON ESTADOS PRODUCTORES DE ALGODÓN ACTUALMENTE  EN MÉXICO', N'TAMAULIPAS', N'NUEVO LEÓN', N'CAMPECHE', 1, CAST(N'2017-03-02' AS Date), CAST(N'2017-03-02' AS Date), N'test@dominio.com', 1, 1, N'¿SON DOS ENFERMEDADES TOLERADAS POR LAS VARIEDADES FIBERMAX?', N'VERTICILLIUM Y CENICILLA', N'BACTERIA Y VERTICILLIUM', N'BACTERIA Y VIRUELA', 2, N'LA TECNOLOGIA BAYER GLT TE DA PROTECCION CONTRA:', N'MALEZAS Y VIRUELA', N'PRINCIPALES GUSANOS Y MALEZAS', N'VIRUELA', 2, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'C883BCBC964E4CD19B6905940C977D56', CAST(3 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (4, N'¿Que controla Muralla Max en algodón?', N'A. Control de Thrips', N'B. Control de viruela del algodonero.', N'C. Control de Verticillium', 1, CAST(N'2017-02-12' AS Date), CAST(N'2017-02-12' AS Date), N'test@dominio.com', 1, 1, N'¿Hasta cuantos días de protección ofrece Muralla Max?', N'A. 5 días', N'B. 10 días', N'C. 15 días', 3, N'¿Cuál es el principal beneficio del uso de Muralla Max?', N'A. Mejor alternativa a productos viejos y tóxicos.', N'B. Plántulas de 8 a 10 hojas verdaderas sanas', N'C. Cobertura y retención del producto por su novedosa formulación.', 2, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'6F27BE813B444E138642742D66806560', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (12, N'¿Con base en que caracteristica castigan mas el precio de la fibra en el despepite?', N'Grado y basura', N'Longitud, resistencia y micro', N'Short fiber index, madurez y uniformidad.', 1, CAST(N'2017-03-02' AS Date), CAST(N'2017-03-02' AS Date), N'test@dominio.com', 1, 1, N'¿Que caracteristicas le importan mas a la empresa textil?', N'Micro, Longitud y resistencia', N'Grado y basura', N'Uniformidad y UV', 1, N'¿Que tendria que hacer yo como agricultor para obtener el maximo valor de mi fibra?', N'Buscar la comercialización directa con fábricas.', N'Sacar muchas pacas de calidad regular.', N'Usar semilla con buena genética de producción.', 3, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'410A597CA9E94AD9819EB3291DB0948C', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (13, N'¿Cuales son las variedades mas resistentes a Verticillium?', N'FM 2334GLT y FM 2484B2F', N'FM 1740B2F y FM 2484B2F', N'DP 0912B2RF y FM 1880B2F', 1, CAST(N'2017-03-02' AS Date), CAST(N'2017-03-02' AS Date), N'test@dominio.com', 1, 1, N'Ofertas de valor que ofrecen las variedades de Bayer', N'Venta de pacas, clases de cultivo y platicas comerciales', N'Germoplasma Elite, tolerancia a enfermedades y calidad de fibra', N'Capacitación, venta de alimentos y oferta de productos', 2, N'¿Cuales son los productos del tratamiento de semilla Fibermax?', N'Metalaxil, penflufen y cloro', N'Imidacloprid, metalaxil y penflufen', N'Tebuconazole, metalaxil y cobre', 2, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'2AD1BB45EC8C49DDBBC625C0FF8DF2F0', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (15, N'¿Cuál es la mejor etapa de aplicación de Finale Ultra en algodón GLT?', N'Altura menor a 10cm', N'Altura de 20cm a 30cm', N'Mayor de 30cm', 1, CAST(N'2017-03-30' AS Date), CAST(N'2017-03-30' AS Date), N'test@dominio.com', 1, 1, N'Una de las ventajas de incluir Finale Ultra en algodón es:', N'Controla malezas grandes', N'Porque es un herbicida nuevo', N'Un nuevo modo de acción', 3, N'Finale Ultra puede ser aplicado en la tecnologia:', N'GLT y GL', N'B2F', N'Convencional', 1, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'7AFAF5DF72844992ABBD61181CE0810E', CAST(5 AS Decimal(18, 0)))
INSERT [dbo].[Gamification] ([Id], [Pregunta], [Respuesta1], [Respuesta2], [Respuesta3], [IdEvento], [FechaCreacion], [FechaModificacion], [UsuarioModificacion], [Activo], [RespuestaCorrecta], [Pregunta2], [Respuesta2_1], [Respuesta2_2], [Respuesta2_3], [RespuestaCorrecta2], [Pregunta3], [Respuesta3_1], [Respuesta3_2], [Respuesta3_3], [RespuestaCorrecta3], [Puntos], [PuntosFallo], [GUID], [PuntosFallo3er]) VALUES (14, N'¿Cuáles son las áreas prohibidas para la siembra de algodon transgénico?', N'Zonas agrícolas', N'Áreas naturales protegidas', N'Montañas', 1, CAST(N'2017-03-02' AS Date), CAST(N'2017-03-02' AS Date), N'test@dominio.com', 1, 2, N'¿Cuál es el objetivo de la siembra de refugio?', N'Prevenir el desarrollo de resistencia', N'Generar resistencia en insectos', N'Regenerar el suelo', 1, N'¿Cuál es el instrumento legal que regula la biotecnologia en México?', N'Ley de inversión extranjera', N'Ley de minas', N'Ley de Bioseguridad', 3, CAST(10 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'625921E2EB844AC18DE7B5A032284387', CAST(5 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Gamification] OFF
SET IDENTITY_INSERT [dbo].[Informative] ON 

INSERT [dbo].[Informative] ([Id], [Title], [Description], [Image], [UserModify], [DatetimeModify], [Active], [Event], [URL]) VALUES (1, N'¿Que se puede hacer con una paca de algodón?', N'Los procesos industriales del algodón son: el algodón se transforma en hilo, el tejido de los hilos, el teñido y estampado y otros procesos de acabado para mejorar su aspecto y cualidades.
El algodón es una fibra vegetal natural de gran importancia económica como materia prima para la fabricación de tejidos y prendas de vestir.
La generalización de su uso se debe sobre todo a la facilidad con que la fibra se puede trenzar en hilos. La resistencia, la absorbencia y la facilidad con que se lava y se tiñe también contribuyen a que el algodón se preste a la elaboración de géneros textiles muy variados.', N'bale.jpg', N'test@dominio.com', CAST(N'2017-03-21 13:23:47.017' AS DateTime), 1, 1, N'https://es.wikipedia.org/wiki/Procesos_industriales_del_algod%C3%B3n')
INSERT [dbo].[Informative] ([Id], [Title], [Description], [Image], [UserModify], [DatetimeModify], [Active], [Event], [URL]) VALUES (2, N'El algodón de EE.UU. está en la cuerda floja por los planes de Trump', N'No hay nada más característico de Estados Unidos —y de México— que un par de jeans.

Los fardos de algodón se recolectan desde Texas a las Carolinas y se despachan como lanilla, hilo o tela a México, donde son cortados, cosidos y ensamblados. Alrededor de 40% de los jeans de hombres y niños en EE.UU. son importados del país vecino, según la Administración de Comercio Internacional estadounidense. Algunas de las mayores compañías de jeans del mundo, incluidas Levi Strauss y VF Corporation, dueña de las marcas Lee y Wrangler, tienen presencia en México.

“México es mi mercado interno”, afirma Alan Underwood, operador de algodón en Lubbock, Texas. Sus fardos de algodón son transportados a la frontera, a unas cinco horas de distancia, y terminan en las fábricas textiles de Ciudad de México, donde son usados en la confección de indumentaria. Underwood dice que le queda más cerca y le cuesta menos enviar algodón a México que a gran parte de EE.UU.', N'trump.jpg', N'test@dominio.com', CAST(N'2017-03-21 13:25:35.843' AS DateTime), 1, 1, N'http://lat.wsj.com/articles/SB11319438290653513351704582620022327730652')
INSERT [dbo].[Informative] ([Id], [Title], [Description], [Image], [UserModify], [DatetimeModify], [Active], [Event], [URL]) VALUES (4, N'¿Qué es el algodón?', N'El algodón es la planta textil de fibra suave más importante del mundo y su cultivo es de los más antiguos. En un principio la palabra algodón significaba un tejido fino. El algodón fue el primer textil en la India. Los primeros escritos del algodón son textos hindúes, himnos que datan 1500 años A.C. y libros religiosos de 800 años A.C.', N'algo.jpg', N'test@dominio.com', CAST(N'2017-03-13 16:59:34.327' AS DateTime), 1, 1, N'http://www.conacyt.mx/cibiogem/index.php/algodon')
INSERT [dbo].[Informative] ([Id], [Title], [Description], [Image], [UserModify], [DatetimeModify], [Active], [Event], [URL]) VALUES (5, N'El algodón aguanta su precio', N'El gigante asiático liberalizará hasta 30.000 toneladas de algodón al día hasta finales de agosto y se prevé que esta cifra pueda aumentar si los precios de esta materia prima continúan en ascenso.', N'bolsa.png', N'test@dominio.com', CAST(N'2017-03-21 13:24:50.117' AS DateTime), 1, 1, N'http://www.modaes.com/entorno/20170302/el-algodon-aguanta-su-precio-a-las-puertas-de-la-subasta-de-stocks-de-china.html')
SET IDENTITY_INSERT [dbo].[Informative] OFF
INSERT [dbo].[Registration] ([IdUser], [IdEvent], [RegistrationDate]) VALUES (1, 1, CAST(N'2017-01-16 21:47:29.597' AS DateTime))
INSERT [dbo].[Registration] ([IdUser], [IdEvent], [RegistrationDate]) VALUES (2, 1, CAST(N'2017-01-16 21:47:29.597' AS DateTime))
INSERT [dbo].[Registration] ([IdUser], [IdEvent], [RegistrationDate]) VALUES (3, 1, CAST(N'2017-01-16 21:47:29.597' AS DateTime))
INSERT [dbo].[Registration] ([IdUser], [IdEvent], [RegistrationDate]) VALUES (4, 1, CAST(N'2017-01-16 21:47:29.597' AS DateTime))
INSERT [dbo].[Registration] ([IdUser], [IdEvent], [RegistrationDate]) VALUES (5, 1, CAST(N'2017-01-16 21:47:29.597' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (225, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:12:27.017' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:20:43.650' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:21:07.990' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:51:55.787' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (233, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:10:23.320' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (132, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:11:45.387' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:14:52.367' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (236, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:00.570' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:15.423' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:20.423' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (180, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:46.380' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:50.200' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (226, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:06.997' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:16.563' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:19.487' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (236, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:44.220' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:45.063' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:45.480' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:45.643' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (142, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:57:16.727' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (139, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:24.123' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:26.543' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (225, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:33.537' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (240, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:45.820' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (230, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:47.663' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:50.117' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (241, 1, 1, NULL, CAST(0 AS Decimal(18, 0)), CAST(N'2017-03-30 16:15:23.090' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (217, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:36.857' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (240, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:41.460' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (230, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:01.603' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (139, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:27.333' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (226, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:36.753' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (181, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:41.613' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:57.070' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (225, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:59:11.897' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:59:12.023' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (236, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:26.230' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (226, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:32.277' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (142, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:05.863' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:09.893' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (244, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:10.243' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:12.337' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (225, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:12.613' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (230, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:22.330' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:29.047' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:38:16.223' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 1, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 22:56:53.357' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-31 00:06:51.043' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:17:08.380' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:47:13.397' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:17.017' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:27.970' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (217, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:40.603' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (228, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:44.130' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (236, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:46.960' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 16:00:10.167' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (142, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:38.523' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (234, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:39.180' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:44.330' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:53.263' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:56.740' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (228, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:48:06.737' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:00:38.320' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (217, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:20:55.800' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (217, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:00.110' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (234, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:17.057' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (244, 1, 1, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 16:06:50.470' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (132, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:40.370' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (242, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:46.810' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (244, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:18.980' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (244, 1, 4, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 17:48:31.033' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 11, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-31 00:01:07.387' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (231, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 12:41:59.067' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (237, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:58:55.530' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:13.943' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:33.890' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:48.777' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (132, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:54.890' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (233, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:17.647' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:20.250' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (180, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:42.870' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (231, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:55.300' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (227, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:59:33.067' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:01:09.737' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 2, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-31 00:02:43.820' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 4, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-31 00:03:36.120' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (1, 1, 12, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-31 00:05:26.980' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (234, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:13:01.857' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 1, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 13:50:40.520' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (231, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:07.373' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:57.910' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:52.710' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (220, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:09.330' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (236, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:48.863' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:37:04.047' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (216, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 14:37:31.580' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (132, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:33:51.280' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (218, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:01.070' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:27.373' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:27.437' AS DateTime))
GO
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (139, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:36.037' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (218, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:45.700' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (230, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:50.010' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (222, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:57.327' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (229, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:39.407' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (235, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:59:56.517' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (231, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 16:00:45.040' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (153, 1, 15, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 16:00:46.543' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (218, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:58:01.907' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (234, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:45.717' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (224, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:39:07.910' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (226, 1, 14, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:33:58.117' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (139, 1, 14, NULL, CAST(10 AS Decimal(18, 0)), CAST(N'2017-03-30 15:34:31.770' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (234, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:38.427' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (225, 1, 2, NULL, CAST(5 AS Decimal(18, 0)), CAST(N'2017-03-30 15:35:39.287' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (135, 1, 11, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 16:57:34.110' AS DateTime))
INSERT [dbo].[Score] ([IdUser], [IdEvent], [IdQuestion], [IdAnswer], [Points], [DateTime]) VALUES (218, 1, 13, NULL, CAST(7 AS Decimal(18, 0)), CAST(N'2017-03-30 17:36:40.673' AS DateTime))
SET IDENTITY_INSERT [dbo].[Survey] ON 

INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (1, 1, 1, 3, 3, 4, 3, 0, 1, CAST(N'2017-02-28 22:01:49.937' AS DateTime), N'Cotton Club')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (2, 165, 1, 5, 5, 5, 5, 5, 5, CAST(N'2017-03-02 17:24:21.463' AS DateTime), N'Mayor eficiencia en cuanto a invitación ')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (3, 149, 1, 5, 5, 5, 5, 5, 6, CAST(N'2017-03-02 17:33:31.470' AS DateTime), N'Falta mucho mejorar Logística en entrega de semilla ')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (4, 148, 1, 5, 5, 5, 5, 5, 3, CAST(N'2017-03-02 17:41:54.827' AS DateTime), N'Entregar la semilla al tiempo')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (5, 176, 1, 0, 0, 0, 0, 0, 6, CAST(N'2017-03-02 17:44:04.370' AS DateTime), N'')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (7, 191, 1, 0, 0, 0, 0, 0, 4, CAST(N'2017-03-02 19:23:14.237' AS DateTime), N'Variedades precoces')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (9, 183, 1, 4, 5, 5, 5, 5, 7, CAST(N'2017-03-02 19:42:18.770' AS DateTime), N'La calidad de las proyecciones fue muy mala')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (12, 193, 1, 4, 3, 5, 3, 5, 6, CAST(N'2017-03-04 20:16:49.833' AS DateTime), N'')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (6, 197, 1, 5, 5, 5, 5, 5, 1, CAST(N'2017-03-02 17:54:56.477' AS DateTime), N'')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (8, 204, 1, 5, 5, 5, 5, 5, 9, CAST(N'2017-03-02 19:33:38.770' AS DateTime), N'En mi experiencia Fibermax es una variedad muy buena y fácil de manejar en diferentes tipos de terrenos.  Una opción en mejor Fibermax es una resistencia contra la viruela.')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (10, 171, 1, 5, 5, 5, 5, 5, 4, CAST(N'2017-03-03 07:51:58.340' AS DateTime), N'Las actividades de recreación fueron las que  quedaron a deber. ')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (13, 211, 1, 5, 5, 5, 5, 5, 9, CAST(N'2017-03-12 10:36:16.343' AS DateTime), N'')
INSERT [dbo].[Survey] ([Id], [IdUsuario], [IdEvento], [Respuesta1], [Respuesta2], [Respuesta3], [Respuesta4], [Respuesta5], [Respuesta6], [Datetime], [Comentarios]) VALUES (11, 150, 1, 5, 5, 5, 5, 5, 6, CAST(N'2017-03-03 21:00:14.470' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[Survey] OFF
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (214, N'B1D963094C0E4EAD8193F1532FAB78FB', CAST(N'2017-03-28 16:53:41.563' AS DateTime), CAST(N'2017-04-28 02:53:41.563' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (181, N'D73424EF4BCA4733817EF6C5EE6C1944', CAST(N'2017-03-29 09:56:51.693' AS DateTime), CAST(N'2017-04-28 19:56:51.693' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (216, N'31451156BC5343CC8FDFBC332044D9A0', CAST(N'2017-03-29 17:49:07.820' AS DateTime), CAST(N'2017-04-29 03:49:07.820' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (217, N'A9ADB47B99324FF0B2144E4D3AEC7A88', CAST(N'2017-03-30 09:15:17.527' AS DateTime), CAST(N'2017-04-29 19:15:17.527' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (167, N'66687E07C48F432295B5CADC4360E189', CAST(N'2017-03-30 11:30:19.623' AS DateTime), CAST(N'2017-04-29 21:30:19.623' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (218, N'6F73523994884C39A689A429958351F0', CAST(N'2017-03-30 11:36:24.560' AS DateTime), CAST(N'2017-04-29 21:36:24.560' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (220, N'86A244ECD680495F9E2B8FBFAEA0D94C', CAST(N'2017-03-30 11:47:47.740' AS DateTime), CAST(N'2017-04-29 21:47:47.740' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (222, N'7A95B3F77FB44BE587161459DB708B16', CAST(N'2017-03-30 11:52:09.543' AS DateTime), CAST(N'2017-04-29 21:52:09.543' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (224, N'909B6AD16313449DA77EA19585E6F48A', CAST(N'2017-03-30 12:17:34.787' AS DateTime), CAST(N'2017-04-29 22:17:34.787' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (226, N'BDD60F6AC5E24F7EBA9D61F3CEB258BE', CAST(N'2017-03-30 12:21:37.177' AS DateTime), CAST(N'2017-04-29 22:21:37.177' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (231, N'7A165910C79441F0AA7FED90FF5D5C06', CAST(N'2017-03-30 12:40:56.397' AS DateTime), CAST(N'2017-04-29 22:40:56.397' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (228, N'D819759CD17249FD8DB2F60C2691AA39', CAST(N'2017-03-30 12:46:22.910' AS DateTime), CAST(N'2017-04-29 22:46:22.910' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (183, N'CC4466B6222042F88228674BB8D6F781', CAST(N'2017-03-02 09:03:40.557' AS DateTime), CAST(N'2017-04-01 19:03:40.557' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (185, N'40D85A631AA74518B42DF37FBBE86FD9', CAST(N'2017-03-30 13:06:39.150' AS DateTime), CAST(N'2017-04-29 23:06:39.150' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (235, N'6E6438F844A649459312919332CA21E4', CAST(N'2017-03-30 13:48:54.317' AS DateTime), CAST(N'2017-04-29 23:48:54.317' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (237, N'855AD3D505FA42929EDD015BAFEF7313', CAST(N'2017-03-30 13:55:54.587' AS DateTime), CAST(N'2017-04-29 23:55:54.587' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (239, N'57E1C4F5C0DF41B6A5A85B35DD0BA96C', CAST(N'2017-03-30 14:06:45.243' AS DateTime), CAST(N'2017-04-30 00:06:45.243' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (211, N'73785E1DEC7542EC9DC97F8B2A69CC58', CAST(N'2017-03-30 14:14:54.363' AS DateTime), CAST(N'2017-04-30 00:14:54.363' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (153, N'79F5C59CE9EE4A4DA22BAFE84F9A3D55', CAST(N'2017-03-30 14:40:54.407' AS DateTime), CAST(N'2017-04-30 00:40:54.407' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (240, N'2DE294E1ADB8457B95ECBD12135237DE', CAST(N'2017-03-30 14:44:15.310' AS DateTime), CAST(N'2017-04-30 00:44:15.310' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (225, N'2E550CFA6F2F4C58ADDB39C73ADDE2E6', CAST(N'2017-03-30 14:44:23.117' AS DateTime), CAST(N'2017-04-30 00:44:23.117' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (132, N'2FE116C25CAE4E6DA07583933F54F130', CAST(N'2017-03-30 15:11:08.427' AS DateTime), CAST(N'2017-04-30 01:11:08.427' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (242, N'A2422F66D5FE411B8CC89515105734A7', CAST(N'2017-03-30 15:14:08.203' AS DateTime), CAST(N'2017-04-30 01:14:08.203' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (236, N'421C136402344B08A4B6F37537544F63', CAST(N'2017-03-30 15:15:13.550' AS DateTime), CAST(N'2017-04-30 01:15:13.550' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (228, N'B978A21E874343B4B87E17D59660F4EE', CAST(N'2017-03-30 15:43:27.750' AS DateTime), CAST(N'2017-04-30 01:43:27.750' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (241, N'8F966A7675D54B7B86DBE8EF4A937EFC', CAST(N'2017-03-30 16:10:57.700' AS DateTime), CAST(N'2017-04-30 02:10:57.700' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (216, N'9CD8078407C54EDEB5EA26AB5CF0955B', CAST(N'2017-03-30 17:05:11.140' AS DateTime), CAST(N'2017-04-30 03:05:11.140' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'E68BE24872944868B9D2BB8491B6B2DA', CAST(N'2017-03-01 10:48:22.390' AS DateTime), CAST(N'2017-03-31 20:48:22.390' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'34A53392E5564F8987703F925914E9EE', CAST(N'2017-03-01 21:25:53.683' AS DateTime), CAST(N'2017-04-01 07:25:53.683' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'860C8EE46EDA48DAAC36C5F8ACAB6E14', CAST(N'2017-03-01 23:22:30.387' AS DateTime), CAST(N'2017-04-01 09:22:30.387' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'3F4D3A3D4AB94CA7B01466570850C98A', CAST(N'2017-03-02 00:31:45.337' AS DateTime), CAST(N'2017-04-01 10:31:45.337' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (179, N'8C6C812485624603AAC5E3B52C13BE9C', CAST(N'2017-03-02 08:18:52.530' AS DateTime), CAST(N'2017-04-01 18:18:52.530' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (182, N'FABCCC3034884FAEBB3157FAB0B7E8BE', CAST(N'2017-03-02 09:08:06.227' AS DateTime), CAST(N'2017-04-01 19:08:06.227' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'630B2AC60CBB4F889B13D2FE74D1D4D7', CAST(N'2017-03-02 11:09:49.580' AS DateTime), CAST(N'2017-04-01 21:09:49.580' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'DCF110C71C9A4187A32B7C42709A6BAC', CAST(N'2017-03-02 11:28:38.840' AS DateTime), CAST(N'2017-04-01 21:28:38.840' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'8975DF848036437AB7E56C14FAE5FA9F', CAST(N'2017-03-02 11:47:37.817' AS DateTime), CAST(N'2017-04-01 21:47:37.817' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'6E820A779569493596043896EED3B8EC', CAST(N'2017-03-02 11:55:22.867' AS DateTime), CAST(N'2017-04-01 21:55:22.867' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'2799C107B0B24A55B2D17307340BFB37', CAST(N'2017-03-02 11:58:10.497' AS DateTime), CAST(N'2017-04-01 21:58:10.497' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (188, N'5D088257E9144F0693AC902DA1A068DA', CAST(N'2017-03-02 12:08:08.200' AS DateTime), CAST(N'2017-04-01 22:08:08.200' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (189, N'E1236F6493D44718A9A94C40445BF8CE', CAST(N'2017-03-02 12:16:10.733' AS DateTime), CAST(N'2017-04-01 22:16:10.733' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'9E0AE2D3EDD14E0A84815EDF41C643CE', CAST(N'2017-03-02 12:32:13.733' AS DateTime), CAST(N'2017-04-01 22:32:13.733' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (193, N'E5FC1025D59E4A71B41044129C6C4425', CAST(N'2017-03-02 12:33:57.863' AS DateTime), CAST(N'2017-04-01 22:33:57.863' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (194, N'ED52C5136BDF462A8E594A0E4F81BAAA', CAST(N'2017-03-02 12:36:11.583' AS DateTime), CAST(N'2017-04-01 22:36:11.583' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (181, N'DC66006E11E646E6AB3C9EB7DB32F079', CAST(N'2017-03-02 12:49:11.150' AS DateTime), CAST(N'2017-04-01 22:49:11.150' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'31B7F5A01EEA46749F8E2E4EECB88DA8', CAST(N'2017-03-02 13:52:44.537' AS DateTime), CAST(N'2017-04-01 23:52:44.537' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (157, N'0B19D4AEB61D4D8B91AF5DACA4E591C6', CAST(N'2017-03-02 13:58:22.330' AS DateTime), CAST(N'2017-04-01 23:58:22.330' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (149, N'CECBB907FDA84E04A10D65AB4376E790', CAST(N'2017-03-02 14:16:28.080' AS DateTime), CAST(N'2017-04-02 00:16:28.080' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (171, N'1EF5F6E2FF72400D837DF3DEF7761936', CAST(N'2017-03-02 14:20:44.737' AS DateTime), CAST(N'2017-04-02 00:20:44.737' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (196, N'9DB4709BA93F47C8A8893944562FA633', CAST(N'2017-03-02 14:33:46.677' AS DateTime), CAST(N'2017-04-02 00:33:46.677' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (141, N'4B31A4F7794A461BB35EDB8BE57AFB69', CAST(N'2017-03-02 14:38:33.510' AS DateTime), CAST(N'2017-04-02 00:38:33.510' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (142, N'8AD4B9E1F1F443F584E64F79B7C4F71F', CAST(N'2017-03-02 14:38:59.397' AS DateTime), CAST(N'2017-04-02 00:38:59.397' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (165, N'4FB7B11C1CE042F1BA41DC70F06A3D38', CAST(N'2017-03-02 14:42:14.020' AS DateTime), CAST(N'2017-04-02 00:42:14.020' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (198, N'C38F755A0E6F484A84AD6BB149AEAD66', CAST(N'2017-03-02 14:50:03.947' AS DateTime), CAST(N'2017-04-02 00:50:03.947' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (191, N'B7704BA98B654D71AD4F961F9A8DFCAF', CAST(N'2017-03-02 14:52:20.807' AS DateTime), CAST(N'2017-04-02 00:52:20.807' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (199, N'77A9206CA097496EBD71E87721E40188', CAST(N'2017-03-02 14:54:15.493' AS DateTime), CAST(N'2017-04-02 00:54:15.493' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (188, N'E3D90FEBB3104C57AB7BB30CB096F60F', CAST(N'2017-03-02 15:00:00.390' AS DateTime), CAST(N'2017-04-02 01:00:00.390' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (140, N'05A9F6C50F5949E099A65DB8B0799ECA', CAST(N'2017-03-02 15:01:02.517' AS DateTime), CAST(N'2017-04-02 01:01:02.517' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (140, N'7F222692128C494CA25BC5AF6C38A044', CAST(N'2017-03-02 15:02:25.877' AS DateTime), CAST(N'2017-04-02 01:02:25.877' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (201, N'134CDEE7B03E4DB3B0C78A000E291842', CAST(N'2017-03-02 15:09:44.260' AS DateTime), CAST(N'2017-04-02 01:09:44.260' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (154, N'634ED981965F4CB29639D29C7C97DB78', CAST(N'2017-03-02 15:10:06.910' AS DateTime), CAST(N'2017-04-02 01:10:06.910' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (137, N'9D2A8BF39774411D81381916B782FE1F', CAST(N'2017-03-02 15:15:30.713' AS DateTime), CAST(N'2017-04-02 01:15:30.713' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (233, N'71B6BEEF89474BF2881F6AD5AFD37005', CAST(N'2017-03-30 13:01:49.760' AS DateTime), CAST(N'2017-04-29 23:01:49.760' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (131, N'EEFDCCDC28954281BC5BC06DC7A4E85D', CAST(N'2017-03-02 08:42:54.893' AS DateTime), CAST(N'2017-04-01 18:42:54.893' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'F8BCE9FAE1F64BBCA1071C098C108A9A', CAST(N'2017-03-02 08:49:45.453' AS DateTime), CAST(N'2017-04-01 18:49:45.453' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'E6EE639C937844CA940DB73E9A58B79C', CAST(N'2017-03-18 13:17:40.427' AS DateTime), CAST(N'2017-04-17 23:17:40.427' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'E0702B02717D4313A83AD90E64048F47', CAST(N'2017-03-21 13:20:48.947' AS DateTime), CAST(N'2017-04-20 23:20:48.947' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'1830FB5E10B64939BA24E0C49984023C', CAST(N'2017-03-22 09:29:13.427' AS DateTime), CAST(N'2017-04-21 19:29:13.427' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (184, N'32B21B0997964C26BCE69478825AAC63', CAST(N'2017-03-02 09:45:52.453' AS DateTime), CAST(N'2017-04-01 19:45:52.453' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'90F436E1E55C4CAD8B0CC65F77C342ED', CAST(N'2017-03-02 13:30:01.377' AS DateTime), CAST(N'2017-04-01 23:30:01.377' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (150, N'2A74ECBDF03848F3B27328E886E3C2F0', CAST(N'2017-03-02 15:08:41.390' AS DateTime), CAST(N'2017-04-02 01:08:41.390' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (155, N'504290F1A20D45EEA3F4C445F23389EE', CAST(N'2017-03-02 15:14:30.050' AS DateTime), CAST(N'2017-04-02 01:14:30.050' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (132, N'87E121CA4B094186A2D66609BA8E184E', CAST(N'2017-03-02 15:17:38.867' AS DateTime), CAST(N'2017-04-02 01:17:38.867' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (131, N'D24C0E2A91E44A6CABC60672DD810AC8', CAST(N'2017-03-02 15:22:58.230' AS DateTime), CAST(N'2017-04-02 01:22:58.230' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (148, N'F41F4782151541F49E78725C3614A042', CAST(N'2017-03-02 15:26:45.627' AS DateTime), CAST(N'2017-04-02 01:26:45.627' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (135, N'B47C40CDA0BB456E89547A9A29D7D228', CAST(N'2017-03-02 15:41:25.427' AS DateTime), CAST(N'2017-04-02 01:41:25.427' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (170, N'1A956E441FEA4494B18FC5645C775BEC', CAST(N'2017-03-02 15:47:52.363' AS DateTime), CAST(N'2017-04-02 01:47:52.363' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (202, N'618E7EADD1214D1A9E713BAEF9C82065', CAST(N'2017-03-02 15:49:30.810' AS DateTime), CAST(N'2017-04-02 01:49:30.810' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (152, N'54948C05A5544BF881D65AD1303464C0', CAST(N'2017-03-02 16:13:52.540' AS DateTime), CAST(N'2017-04-02 02:13:52.540' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (146, N'1B079788C2404F70AB97EF8910FF9BFC', CAST(N'2017-03-02 16:18:33.817' AS DateTime), CAST(N'2017-04-02 02:18:33.817' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (135, N'0004B243F28A43F9AC6F15C16EB8BDDB', CAST(N'2017-03-02 16:34:53.783' AS DateTime), CAST(N'2017-04-02 02:34:53.783' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (204, N'EBF624E741474AAA859C666AB2DA4666', CAST(N'2017-03-02 16:34:54.210' AS DateTime), CAST(N'2017-04-02 02:34:54.210' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (135, N'1602F3B8FA994CFD8E0F63940969538F', CAST(N'2017-03-02 16:35:25.853' AS DateTime), CAST(N'2017-04-02 02:35:25.853' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (204, N'0A23AB978DA14B76B89FE7BE6088847C', CAST(N'2017-03-02 16:36:48.570' AS DateTime), CAST(N'2017-04-02 02:36:48.570' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (208, N'FDD3A958BA8047BAB5E56EC7E1E43793', CAST(N'2017-03-02 17:01:06.890' AS DateTime), CAST(N'2017-04-02 03:01:06.890' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (208, N'ED4D381B60AE44E7A97C6E3C20DA5E87', CAST(N'2017-03-02 17:02:16.337' AS DateTime), CAST(N'2017-04-02 03:02:16.337' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (134, N'4FB731E354E24482BDB3029998E95A07', CAST(N'2017-03-02 17:21:22.300' AS DateTime), CAST(N'2017-04-02 03:21:22.300' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (201, N'6A6AC1DB40064ABA9E75521A89B1CC1D', CAST(N'2017-03-02 18:00:42.080' AS DateTime), CAST(N'2017-04-02 04:00:42.080' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'600C7CABACE74FF98DD5E7E166F5D768', CAST(N'2017-03-02 18:02:30.130' AS DateTime), CAST(N'2017-04-02 04:02:30.130' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'F7DFE8611C4946F6A6103D2F1188FA34', CAST(N'2017-03-02 18:03:11.717' AS DateTime), CAST(N'2017-04-02 04:03:11.717' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (211, N'354ED1BC1F414C5FA99491895964DC04', CAST(N'2017-03-02 18:09:26.430' AS DateTime), CAST(N'2017-04-02 04:09:26.430' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (194, N'B2B9B7C4618040719F9B42C121240897', CAST(N'2017-03-02 18:17:43.100' AS DateTime), CAST(N'2017-04-02 04:17:43.100' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (154, N'9DB7B4682D7D4429A787A4DB2D6448BB', CAST(N'2017-03-02 18:18:15.190' AS DateTime), CAST(N'2017-04-02 04:18:15.190' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (171, N'62235ACFD523451EA3B7E9548492B955', CAST(N'2017-03-02 18:43:10.860' AS DateTime), CAST(N'2017-04-02 04:43:10.860' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (190, N'8C18718BCF554A67B5A5FE61A158329F', CAST(N'2017-03-02 19:04:02.280' AS DateTime), CAST(N'2017-04-02 05:04:02.280' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (188, N'454E26DA173A41A8B6864E234C241274', CAST(N'2017-03-02 19:21:52.313' AS DateTime), CAST(N'2017-04-02 05:21:52.313' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'139F851332B2493391E5898FCF6C2388', CAST(N'2017-03-02 19:35:02.437' AS DateTime), CAST(N'2017-04-02 05:35:02.437' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'AAABC6D0013949B3A549970B343AF915', CAST(N'2017-03-02 19:37:43.630' AS DateTime), CAST(N'2017-04-02 05:37:43.630' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (212, N'80821E6894CC4CAD824AE942F251FCB2', CAST(N'2017-03-02 19:53:16.530' AS DateTime), CAST(N'2017-04-02 05:53:16.530' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (196, N'0208FA6A7F564D5497AD6D80AA16C7DB', CAST(N'2017-03-02 19:53:26.350' AS DateTime), CAST(N'2017-04-02 05:53:26.350' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (197, N'D28DEBBCBC9340319A3D125B49FD2DF0', CAST(N'2017-03-02 19:55:24.743' AS DateTime), CAST(N'2017-04-02 05:55:24.743' AS DateTime), 1)
GO
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'920E638A5EB14292A71D55C3B0790854', CAST(N'2017-03-02 20:09:48.957' AS DateTime), CAST(N'2017-04-02 06:09:48.957' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'BE0E15C31C9742F484D5240A9DF0B6CB', CAST(N'2017-03-02 20:12:04.730' AS DateTime), CAST(N'2017-04-02 06:12:04.730' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (180, N'8FC8289A4C0447279DF25BD73ABC7B92', CAST(N'2017-03-02 20:18:02.863' AS DateTime), CAST(N'2017-04-02 06:18:02.863' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (172, N'58580E8B264F4FBC831D2337D3790378', CAST(N'2017-03-02 20:19:57.000' AS DateTime), CAST(N'2017-04-02 06:19:57.000' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (173, N'2A30FA8EABF548B78F51CC989E92D9F0', CAST(N'2017-03-02 20:22:25.163' AS DateTime), CAST(N'2017-04-02 06:22:25.163' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (172, N'BAE6C97525984A54B09288BA259EE615', CAST(N'2017-03-02 20:23:42.103' AS DateTime), CAST(N'2017-04-02 06:23:42.103' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'F43A3B8548AE4E768C79F03F52582028', CAST(N'2017-03-02 21:02:18.890' AS DateTime), CAST(N'2017-04-02 07:02:18.890' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (145, N'CBA0E5CF662647E3962600EFFDD23F66', CAST(N'2017-03-02 23:58:15.273' AS DateTime), CAST(N'2017-04-02 09:58:15.273' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (193, N'A10E086A93A04953BD274F57D4766376', CAST(N'2017-03-04 20:04:55.283' AS DateTime), CAST(N'2017-04-04 06:04:55.283' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (196, N'ED5DD21D921945AFA4C055649ECDC2BD', CAST(N'2017-03-07 12:20:55.410' AS DateTime), CAST(N'2017-04-06 22:20:55.410' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'E3D14C9314F8499E9FA063EF53F1D6BC', CAST(N'2017-03-08 13:30:37.267' AS DateTime), CAST(N'2017-04-07 23:30:37.267' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (132, N'6B1F67F31BE04EAA9D3554BFCEAFF57E', CAST(N'2017-03-13 21:31:00.300' AS DateTime), CAST(N'2017-04-13 07:31:00.300' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'AE31074956F64F639119A278C767C29B', CAST(N'2017-03-14 08:37:38.057' AS DateTime), CAST(N'2017-04-13 18:37:38.057' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'F1772E4EF47B4A56B9E4F56081A8F094', CAST(N'2017-03-22 09:42:46.240' AS DateTime), CAST(N'2017-04-21 19:42:46.240' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'F1184B2FE05645D393B0F6E4C65B0428', CAST(N'2017-03-22 11:00:03.467' AS DateTime), CAST(N'2017-04-21 21:00:03.467' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (214, N'8A535391E95F4A0BB3F7EF8F400981F0', CAST(N'2017-03-29 08:38:09.330' AS DateTime), CAST(N'2017-04-28 18:38:09.330' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'5DC3CCA33B3346BDA28CA54CE81A802C', CAST(N'2017-03-02 00:16:07.847' AS DateTime), CAST(N'2017-04-01 10:16:07.847' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'04FE4B904B8048369871AA748BC7DA06', CAST(N'2017-03-02 00:36:10.890' AS DateTime), CAST(N'2017-04-01 10:36:10.890' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'5391C2DFAEA24B9DB6F0BCC77428CA54', CAST(N'2017-03-02 11:03:44.403' AS DateTime), CAST(N'2017-04-01 21:03:44.403' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (142, N'D40F2C990D8D41C08699BB66744064CD', CAST(N'2017-03-02 14:37:42.077' AS DateTime), CAST(N'2017-04-02 00:37:42.077' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'873F469FEF4D417FAC8B0B5499E8E81D', CAST(N'2017-03-29 10:20:02.607' AS DateTime), CAST(N'2017-04-28 20:20:02.607' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (234, N'350BB6BAD5FE470BA40D016CA4F0431C', CAST(N'2017-03-30 13:12:02.300' AS DateTime), CAST(N'2017-04-29 23:12:02.300' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (236, N'347FF8ECF76442A3BDDF2C65172FF57C', CAST(N'2017-03-30 13:50:11.227' AS DateTime), CAST(N'2017-04-29 23:50:11.227' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (189, N'F581F654C1EA4D03A066461B7E546E34', CAST(N'2017-03-02 14:59:46.570' AS DateTime), CAST(N'2017-04-02 00:59:46.570' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (192, N'B5BBB4B076F746F7B1F5CBC96E5D879C', CAST(N'2017-03-02 15:38:21.557' AS DateTime), CAST(N'2017-04-02 01:38:21.557' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (199, N'098BCA5D701241E8AC5608FB24F17A13', CAST(N'2017-03-02 19:47:05.613' AS DateTime), CAST(N'2017-04-02 05:47:05.613' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (188, N'61ED17C197354EBFBE3F3A1ACD5EAE66', CAST(N'2017-03-02 20:01:34.073' AS DateTime), CAST(N'2017-04-02 06:01:34.073' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (157, N'22C46F74ECD745F0923DFCC88C0DD9CA', CAST(N'2017-03-07 12:06:53.180' AS DateTime), CAST(N'2017-04-06 22:06:53.180' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (142, N'69CF7A5786504155835F0BE1202E81C0', CAST(N'2017-03-30 15:56:05.253' AS DateTime), CAST(N'2017-04-30 01:56:05.253' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'641CCB9824344F51B2788E4584D3C1CF', CAST(N'2017-03-01 16:39:10.713' AS DateTime), CAST(N'2017-04-01 02:39:10.713' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'FB588EF0370640C0B5EAE2EEF84B1162', CAST(N'2017-03-01 17:14:05.747' AS DateTime), CAST(N'2017-04-01 03:14:05.747' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'D5A103688BC149BA924777E8B0129E81', CAST(N'2017-03-02 00:07:13.310' AS DateTime), CAST(N'2017-04-01 10:07:13.310' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (180, N'C6713E87F9AF425BBB96E01FFE23F752', CAST(N'2017-03-02 08:51:58.857' AS DateTime), CAST(N'2017-04-01 18:51:58.857' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (192, N'88A12F4565184348BA8272B25D3078CF', CAST(N'2017-03-02 12:32:08.967' AS DateTime), CAST(N'2017-04-01 22:32:08.967' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (196, N'A9380D33BB1C43D4A5F5DE47F5D000C5', CAST(N'2017-03-02 14:31:45.513' AS DateTime), CAST(N'2017-04-02 00:31:45.513' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (165, N'CDCD5D3BFC7047DEA351A3A24E132548', CAST(N'2017-03-02 14:36:24.280' AS DateTime), CAST(N'2017-04-02 00:36:24.280' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (198, N'08526EB46438461D890B24B26C4671A5', CAST(N'2017-03-02 14:47:27.907' AS DateTime), CAST(N'2017-04-02 00:47:27.907' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (203, N'BD68C777560F405085E170D4F40AB19A', CAST(N'2017-03-02 16:00:41.790' AS DateTime), CAST(N'2017-04-02 02:00:41.790' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (133, N'3C5709A2A5474F079D58C0FA11972828', CAST(N'2017-03-02 16:05:58.607' AS DateTime), CAST(N'2017-04-02 02:05:58.607' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (176, N'2B8145006AD84F938F3062F444114D93', CAST(N'2017-03-02 16:06:46.363' AS DateTime), CAST(N'2017-04-02 02:06:46.363' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (209, N'172D55430424471580987C6306FD204C', CAST(N'2017-03-15 08:53:00.773' AS DateTime), CAST(N'2017-04-14 18:53:00.773' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (214, N'8EF0CD495CF646C0B00DA84961554012', CAST(N'2017-03-30 02:27:29.600' AS DateTime), CAST(N'2017-04-29 12:27:29.600' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (188, N'CC392BDF71C4466784F8D12F09977ABA', CAST(N'2017-03-30 13:25:08.747' AS DateTime), CAST(N'2017-04-29 23:25:08.747' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (133, N'ADCD1EC1D3474441A3B54820E14B6CB7', CAST(N'2017-03-02 16:10:21.643' AS DateTime), CAST(N'2017-04-02 02:10:21.643' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (172, N'40215B1ADF734AC79706842ADC1A1CB4', CAST(N'2017-03-02 16:14:27.270' AS DateTime), CAST(N'2017-04-02 02:14:27.270' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (208, N'DF67CE874BF34D22813A8CA9695A7134', CAST(N'2017-03-02 17:58:39.153' AS DateTime), CAST(N'2017-04-02 03:58:39.153' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (210, N'4A4AC47FAD77410EBB873673FAEF0FCD', CAST(N'2017-03-02 18:23:03.890' AS DateTime), CAST(N'2017-04-02 04:23:03.890' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (152, N'9BD740592DCB4EB5AFDF688E3ACF08F7', CAST(N'2017-03-02 20:25:29.447' AS DateTime), CAST(N'2017-04-02 06:25:29.447' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (171, N'810FAB2F31C54691AA1267367CABD155', CAST(N'2017-03-02 21:03:34.870' AS DateTime), CAST(N'2017-04-02 07:03:34.870' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'8722CC82AA9B493691CA2C9AAF454B82', CAST(N'2017-03-03 10:16:51.067' AS DateTime), CAST(N'2017-04-02 20:16:51.067' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (181, N'DAEEE78790994C909E542DB78CB8CEC7', CAST(N'2017-03-29 09:45:07.840' AS DateTime), CAST(N'2017-04-28 19:45:07.840' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (153, N'6A6339DC291B477493007E15744680B7', CAST(N'2017-03-30 12:27:04.147' AS DateTime), CAST(N'2017-04-29 22:27:04.147' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (154, N'53721D849CC44BA287ACA1F674DF1409', CAST(N'2017-03-02 08:41:35.560' AS DateTime), CAST(N'2017-04-01 18:41:35.560' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'0033D8F35C19411292D047F39C87411F', CAST(N'2017-03-02 12:12:29.983' AS DateTime), CAST(N'2017-04-01 22:12:29.983' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (191, N'E7E4C24C9A6D491BA8599250DB9E1106', CAST(N'2017-03-02 13:07:28.737' AS DateTime), CAST(N'2017-04-01 23:07:28.737' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (146, N'D5F515ED91D941B6BFBCF22696C20BD7', CAST(N'2017-03-02 14:08:50.247' AS DateTime), CAST(N'2017-04-02 00:08:50.247' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (171, N'88C72C09330C4C289EBD5151533868C4', CAST(N'2017-03-02 14:22:26.847' AS DateTime), CAST(N'2017-04-02 00:22:26.847' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (165, N'9AF2C6475B7F4324BB2985DB276B5FA2', CAST(N'2017-03-02 14:41:18.670' AS DateTime), CAST(N'2017-04-02 00:41:18.670' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (150, N'E5A65B99F75E439EBF7D7BF495A3FB02', CAST(N'2017-03-02 15:07:32.400' AS DateTime), CAST(N'2017-04-02 01:07:32.400' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (154, N'7322A3A1272F4D17B719283E480E63A8', CAST(N'2017-03-02 15:11:02.940' AS DateTime), CAST(N'2017-04-02 01:11:02.940' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (132, N'5CE218992C044E2ABAA9FD8E68E22F8C', CAST(N'2017-03-02 15:16:20.127' AS DateTime), CAST(N'2017-04-02 01:16:20.127' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (137, N'923C801E997B4BD099C9CCF693B0A35B', CAST(N'2017-03-02 15:17:15.057' AS DateTime), CAST(N'2017-04-02 01:17:15.057' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (131, N'01B3ECE6C7B646919BF7464A3D16B9CF', CAST(N'2017-03-02 15:21:37.757' AS DateTime), CAST(N'2017-04-02 01:21:37.757' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (148, N'289BB1D157DE49BA9668ACA89F8BA1EB', CAST(N'2017-03-02 15:25:18.533' AS DateTime), CAST(N'2017-04-02 01:25:18.533' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (176, N'2BD4A2D6188A447CB10F0C3008E77146', CAST(N'2017-03-02 16:09:11.417' AS DateTime), CAST(N'2017-04-02 02:09:11.417' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (152, N'69CF7639CED14A4A9B7383E9801B3D36', CAST(N'2017-03-02 16:12:33.030' AS DateTime), CAST(N'2017-04-02 02:12:33.030' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (210, N'7CF946C18DFB4FB5B2D3D5A9FBCFA278', CAST(N'2017-03-02 17:11:48.557' AS DateTime), CAST(N'2017-04-02 03:11:48.557' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (157, N'817586AC15CB438297145F5828836C82', CAST(N'2017-03-02 18:14:32.643' AS DateTime), CAST(N'2017-04-02 04:14:32.643' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (195, N'379F92B2A99847F7890A160E64B13378', CAST(N'2017-03-02 18:39:32.413' AS DateTime), CAST(N'2017-04-02 04:39:32.413' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'53048B00E47C44AC8433463A58DF5A9D', CAST(N'2017-03-02 19:36:13.333' AS DateTime), CAST(N'2017-04-02 05:36:13.333' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (176, N'F1F5DD8A49C44A1FA54EA3B8D4EAE14B', CAST(N'2017-03-02 20:00:27.893' AS DateTime), CAST(N'2017-04-02 06:00:27.893' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (213, N'896BF58DA5674B4C8DF7A1656A824C7C', CAST(N'2017-03-02 20:03:23.450' AS DateTime), CAST(N'2017-04-02 06:03:23.450' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'10707514BF9048DB927DF59F7C7E28B9', CAST(N'2017-03-02 20:05:41.070' AS DateTime), CAST(N'2017-04-02 06:05:41.070' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (140, N'AECF1C72ABCF4CD0A47BBFF539966900', CAST(N'2017-03-02 20:06:59.430' AS DateTime), CAST(N'2017-04-02 06:06:59.430' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'C7E3B9B22D9D45BF9A5F27743F50DD81', CAST(N'2017-03-09 08:55:24.990' AS DateTime), CAST(N'2017-04-08 18:55:24.990' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'9AD0EEC87038477AA480FA5E75CE790C', CAST(N'2017-03-02 09:19:30.150' AS DateTime), CAST(N'2017-04-01 19:19:30.150' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (177, N'4E06B9CD2552470C9D062F50D538CB6B', CAST(N'2017-03-02 10:46:58.993' AS DateTime), CAST(N'2017-04-01 20:46:58.993' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (187, N'E93B789DCE214B3791F6561A187E3349', CAST(N'2017-03-02 13:50:32.837' AS DateTime), CAST(N'2017-04-01 23:50:32.837' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (133, N'F5A653D35C2E4DBCAF33DFF6244917F0', CAST(N'2017-03-02 16:11:07.300' AS DateTime), CAST(N'2017-04-02 02:11:07.300' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (197, N'89E2CEF4F3E747BA872D5CD86EA6D8AB', CAST(N'2017-03-02 16:15:45.630' AS DateTime), CAST(N'2017-04-02 02:15:45.630' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (197, N'4152762C1E8C48B0A4BEB192C69CFE49', CAST(N'2017-03-02 16:16:18.370' AS DateTime), CAST(N'2017-04-02 02:16:18.370' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (161, N'555D87C698854FBC8F7189D1C21D351F', CAST(N'2017-03-02 16:24:58.113' AS DateTime), CAST(N'2017-04-02 02:24:58.113' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (207, N'05D186B07874428892D86549D4D57AFE', CAST(N'2017-03-02 16:51:29.293' AS DateTime), CAST(N'2017-04-02 02:51:29.293' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (238, N'74F4C7CF032B4D49B9AE0666995E60A2', CAST(N'2017-03-30 14:05:55.630' AS DateTime), CAST(N'2017-04-30 00:05:55.630' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (227, N'C4DE7DF5DDB5435A918950CEF0C803E8', CAST(N'2017-03-30 15:36:59.053' AS DateTime), CAST(N'2017-04-30 01:36:59.053' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (244, N'12146B507CAC4C7EAA745A4EBF427DBC', CAST(N'2017-03-30 16:02:05.943' AS DateTime), CAST(N'2017-04-30 02:02:05.943' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (241, N'6D477FDE2F924BF9AC4909BA2098D6AB', CAST(N'2017-03-30 16:14:15.757' AS DateTime), CAST(N'2017-04-30 02:14:15.757' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'44E90058E01B4B898C74301D766E2D29', CAST(N'2017-03-01 17:33:11.327' AS DateTime), CAST(N'2017-04-01 03:33:11.327' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (173, N'507431A1EDE443B1944DCFEA5B0B061F', CAST(N'2017-03-02 09:07:29.747' AS DateTime), CAST(N'2017-04-01 19:07:29.747' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (178, N'95F9E99559704E269B5EADE25EFD647F', CAST(N'2017-03-02 12:35:16.907' AS DateTime), CAST(N'2017-04-01 22:35:16.907' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (195, N'D170387AEC814B878C152E260C4C34E9', CAST(N'2017-03-02 12:44:18.570' AS DateTime), CAST(N'2017-04-01 22:44:18.570' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (149, N'DE2DEB1286A3435187579FF642C4C88D', CAST(N'2017-03-02 14:15:36.623' AS DateTime), CAST(N'2017-04-02 00:15:36.623' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (201, N'BE118BD69065423580EF10954F7C960B', CAST(N'2017-03-02 15:11:38.390' AS DateTime), CAST(N'2017-04-02 01:11:38.390' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (155, N'31B3196210AA407AB07A9FE14811FC84', CAST(N'2017-03-02 15:13:32.937' AS DateTime), CAST(N'2017-04-02 01:13:32.937' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (187, N'7A65B2E7721E44E39765DBF9FFF0F205', CAST(N'2017-03-02 16:07:55.880' AS DateTime), CAST(N'2017-04-02 02:07:55.880' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (206, N'8D8E0B21B42846449A20D60848BFE4EF', CAST(N'2017-03-02 16:44:06.340' AS DateTime), CAST(N'2017-04-02 02:44:06.340' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (209, N'81A7779FEF734B94A4911E18621F6848', CAST(N'2017-03-02 17:07:09.747' AS DateTime), CAST(N'2017-04-02 03:07:09.747' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (139, N'19FA78112FB54F288F1DA1320AB89208', CAST(N'2017-03-02 17:30:27.483' AS DateTime), CAST(N'2017-04-02 03:30:27.483' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'CCF3FF4F7A8F4B70A7EFEAF2C2D60006', CAST(N'2017-03-02 20:57:00.990' AS DateTime), CAST(N'2017-04-02 06:57:00.990' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (225, N'653F5BB9426A4B7AB6A4BF4A4E1A4789', CAST(N'2017-03-30 12:09:10.510' AS DateTime), CAST(N'2017-04-29 22:09:10.510' AS DateTime), 1)
GO
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (227, N'25E3B9566D9D42B091820713F0A3A694', CAST(N'2017-03-30 12:27:37.733' AS DateTime), CAST(N'2017-04-29 22:27:37.733' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (1, N'C3A315ACE1C14FDDAACFA64779590D7A', CAST(N'2017-03-30 22:56:05.253' AS DateTime), CAST(N'2017-04-30 08:56:05.253' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (186, N'5A8BB29DC9CF4E1EB5D1AFE4B408D436', CAST(N'2017-03-02 16:04:13.893' AS DateTime), CAST(N'2017-04-02 02:04:13.893' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (197, N'42E761FDF5E34D8E9E3D1F8E38D0DCF6', CAST(N'2017-03-02 20:15:39.283' AS DateTime), CAST(N'2017-04-02 06:15:39.283' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (132, N'A373FFC352144CA7B03C32FF9D4A06C5', CAST(N'2017-03-03 10:19:09.910' AS DateTime), CAST(N'2017-04-02 20:19:09.910' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (215, N'9DBC195E2CFD4443AD6085EB1EFC6FB5', CAST(N'2017-03-29 08:06:48.890' AS DateTime), CAST(N'2017-04-28 18:06:48.890' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (214, N'BDD6D971916A4E8FB893B0C9FC0A04F9', CAST(N'2017-03-29 10:36:38.847' AS DateTime), CAST(N'2017-04-28 20:36:38.847' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (230, N'67C0AC73018A42DFB07BA32CF4F8C462', CAST(N'2017-03-30 12:37:26.933' AS DateTime), CAST(N'2017-04-29 22:37:26.933' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (229, N'025A4659E429424389C5FFE30D9FB64A', CAST(N'2017-03-30 12:50:42.783' AS DateTime), CAST(N'2017-04-29 22:50:42.783' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (135, N'2D32BB10AD7B4E3DBE0645F66D522685', CAST(N'2017-03-30 12:59:06.217' AS DateTime), CAST(N'2017-04-29 22:59:06.217' AS DateTime), 1)
INSERT [dbo].[Tokens] ([IdUser], [Token], [DateBeginningValid], [DateEndingValid], [Valid]) VALUES (216, N'69B327FB6BC14D1AA0B0F427D2FFBCC9', CAST(N'2017-03-30 18:23:25.533' AS DateTime), CAST(N'2017-04-30 04:23:25.533' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (1, N'test@dominio.com', N'nu0ASlIhWB1oH99vCONBjA==', N'Abraham', N'Sandoval', N'test@dominio.com', N'admin', NULL, CAST(N'2017-02-27 13:20:19.297' AS DateTime), 1, 1, 1, N'2aae8d55-eb50-4067-9633-444ee262969f.jpg', N'CA2E835', 2, 1, N'', N'', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (2, N'tangamampilia@gmail.com', N'0OlC3HUwRZmijVTRofuRSg==', N'Daniel', N'Fernandez', N'tangamampilia@gmail.com', N'admin', NULL, CAST(N'2017-02-07 09:32:25.193' AS DateTime), 1, 1, 1, N'', N'2795412', 0, 1, N'', N'', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (3, N'daniel@motti.mx', N'6AQ3Fblq1jwGs/SJVgIixg==', N'Daniel', N'Fernández', N'daniel@motti.mx', N'admin', CAST(N'2016-12-16 08:35:56.180' AS DateTime), CAST(N'2017-02-07 21:37:50.233' AS DateTime), 1, 0, 1, N'', N'AEEAAA9', 2, 1, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (4, N'test4', N'gj844K4rP1FaV1rFspxYmQ==', N'test4', N'test4', N'test4@dominio.com', N'admin', CAST(N'2016-12-19 01:06:54.907' AS DateTime), CAST(N'2016-12-19 01:06:54.907' AS DateTime), 1, NULL, 1, NULL, N'C4C5CB1', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (11, N'otro@otro.com', N'+GqikBoZWZL7KHATpgPM7Q==', N'otro', N'otro', N'otro@otro.com', N'admin', CAST(N'2017-02-02 18:39:50.840' AS DateTime), CAST(N'2017-02-08 22:55:35.673' AS DateTime), 1, 0, NULL, N'', N'E4FDA6A', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (7, N'abraham.sandoval12@bayer.com', N'pyjUStaGHgwY9vMdjZz9cQ==', N'Abraham ', N'Sandoval Rodriguez', N'abraham.sandoval12@bayer.com', N'admin', CAST(N'2017-01-20 09:16:43.087' AS DateTime), CAST(N'2017-02-27 10:26:31.700' AS DateTime), 1, 0, NULL, N'', N'6944026', NULL, NULL, N'', N'', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (8, N'test6_modificado', N'AE4BacKJNhXS0h1TdBDfeQ==', N'Test 6', N'.', N'test6@dominio.com', N'admin', CAST(N'2017-01-26 15:42:52.937' AS DateTime), CAST(N'2017-01-26 17:06:02.350' AS DateTime), 1, 1, NULL, NULL, N'6DAD7AC', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (10, N'gtapiag@outlook.com', N'UCrc+hjfU0j7FTk5NsQBGw==', N'German', N'Tapia Galvan', N'gtapiag@outlook.com', N'admin', CAST(N'2017-02-02 17:14:46.583' AS DateTime), CAST(N'2017-03-02 09:26:42.260' AS DateTime), 1, 1, NULL, N'76e3e557-4166-4a02-86de-08850cd86e1a.jpg', N'8A94657', NULL, NULL, N'', N'', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (180, N'BITIA.OSORIO@BAYER.COM', N'P9taK3yLtEMInt6hxzSwFg==', N'BITIA', N'OSORIO', N'BITIA.OSORIO@BAYER.COM', N'admin', CAST(N'2017-03-02 08:20:00.280' AS DateTime), CAST(N'2017-03-02 08:20:00.280' AS DateTime), 1, 0, NULL, N'1a0fa151-694f-49fb-915f-6894060a7824.jpg', N'7F8E210', NULL, NULL, N'5541922296', N'SEEDS MANAGER', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (187, N'MATIAS.CORRECH@BAYER.COM', N'etlmBHuxj9BBjiM/MEskPw==', N'MATIAS', N'CORRECH', N'MATIAS.CORRECH@BAYER.COM', N'admin', CAST(N'2017-03-02 11:26:46.147' AS DateTime), CAST(N'2017-03-13 14:34:18.597' AS DateTime), 1, 1, NULL, N'ca0ef40c-2813-4b98-ba3a-000ebfa4944a.jpg', N'EB9BFE5', NULL, NULL, N'5532333480', N'COUNTRY HEAD', N' Ciudad de México', N'MIGUEL HIDALGO', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (194, N'MIGUELANGEL.PEREZ1@BAYER.COM', N'jY0/XSmIIk0RySp/ymcYlg==', N'MIGUEL ANGEL', N'PEREZ', N'MIGUELANGEL.PEREZ1@BAYER.COM', N'admin', CAST(N'2017-03-02 12:35:01.173' AS DateTime), CAST(N'2017-03-02 12:35:01.173' AS DateTime), 1, 0, NULL, N'', N'178A92D', NULL, NULL, N'8441038009', N'RTV', N' Coahuila', N'SALTILLO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (196, N'ingsergioalvaradom@hotmal.com', N'EQY3I/PvCfUsMO7lmVgrlw==', N'Sergio', N'Alvarado', N'ingsergioalvaradom@hotmal.com', N'admin', CAST(N'2017-03-02 14:14:45.263' AS DateTime), CAST(N'2017-03-02 14:14:45.263' AS DateTime), 1, 0, NULL, N'', N'B112C1B', NULL, NULL, N'6361118749', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (197, N'pedro.hiebertape@gmail.com', N'sZkjpVdjwG8BXqrG8dxfEQ==', N'Peter', N' Hiebert', N'pedro.hiebertape@gmail.com', N'admin', CAST(N'2017-03-02 14:25:07.610' AS DateTime), CAST(N'2017-03-02 14:25:07.610' AS DateTime), 1, 0, NULL, N'91cebc5c-4f4e-4556-b29c-383432d7d50b.jpg', N'1F89D7F', NULL, NULL, N'6261008708', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (210, N'ITZI.rangel@bayer.com', N'5J8jvU3P14cDetUxQVicLA==', N'ITZI', N'RANGEL', N'ITZI.rangel@bayer.com', N'admin', CAST(N'2017-03-02 17:10:26.270' AS DateTime), CAST(N'2017-03-02 17:10:26.270' AS DateTime), 1, 0, NULL, N'83444328-a56b-4d82-8b37-e87068d2e3cb.jpg', N'4CD4C29', NULL, NULL, N'5554320717', N'EMPLEADO BAYER', N' Estado de México', N'Toluca', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (214, N'Felipe.absalon@bayer.com', N'T6nuFQoGyoaFtPj0nIrgyQ==', N'Felipe', N'Absalon', N'Felipe.absalon@bayer.com', N'admin', CAST(N'2017-03-28 16:53:07.127' AS DateTime), CAST(N'2017-03-28 16:53:07.127' AS DateTime), 1, 1, NULL, N'', N'ECF71D0', NULL, NULL, N'57283000', N'IT', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (216, N'Asaro16@gmail.com', N'qojd+K7EYpfwtl/BH+0YmA==', N'Abraham', N'Sanchez', N'Asaro16@gmail.com', N'admin', CAST(N'2017-03-29 17:41:53.350' AS DateTime), CAST(N'2017-03-29 17:47:01.570' AS DateTime), 1, 0, NULL, N'', N'03A19B0', NULL, NULL, N'5576717146', N'Agronomo', N' Chihuahua', N'Aldama', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (219, N'lesliefcr@outlook.com', N'PapeSgzrujy4dwSCLPBRaA==', N'Juan', N'Neufeld', N'lesliefcr@outlook.com', N'admin', CAST(N'2017-03-30 11:34:05.993' AS DateTime), CAST(N'2017-03-30 11:34:05.993' AS DateTime), 1, 0, NULL, N'', N'553AE75', NULL, NULL, N'361212409', N'Agricultor', N' Chihuahua', N'Nuevo Casas Grandes', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (220, N'klassneufeld@gmail.com', N'qX2D0Eh7cWzm1iw1C20mcA==', N'Neufeld', N'Klass', N'klassneufeld@gmail.com', N'admin', CAST(N'2017-03-30 11:45:46.843' AS DateTime), CAST(N'2017-03-30 11:45:46.843' AS DateTime), 1, 0, NULL, N'ffabe32f-aa79-4544-98a5-a5b72aa8fadc.jpg', N'BAA319E', NULL, NULL, N'6251259505', N'Agricultor', N' Chihuahua', N'Villa Ahumada', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (221, N'josefina.perea@bayer.com', N'oexGKV7XBiVvaeENBwcxPA==', N'Josefina', N'Perea', N'josefina.perea@bayer.com', N'admin', CAST(N'2017-03-30 11:46:49.420' AS DateTime), CAST(N'2017-03-30 11:46:49.420' AS DateTime), 1, 1, NULL, N'', N'9564269', NULL, NULL, N'5548803010', N'REGULATORY AFFAIRS', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (222, N'less2707@gmail.com', N'jCgdxoQQYicd/LSeRUReQQ==', N'Jacob', N'Banman Schmitt', N'less2707@gmail.com', N'admin', CAST(N'2017-03-30 11:50:31.430' AS DateTime), CAST(N'2017-03-30 11:52:22.840' AS DateTime), 1, 0, NULL, N'', N'4B6D320', NULL, NULL, N'6361098515', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (223, N'neufarm.cn@gmail.com', N'unItKMjM1rSUjDyyq1M3YA==', N'Cornelio', N'Neufeld', N'neufarm.cn@gmail.com', N'admin', CAST(N'2017-03-30 11:53:35.963' AS DateTime), CAST(N'2017-03-30 11:55:50.580' AS DateTime), 1, 0, NULL, N'', N'9E22538', NULL, NULL, N'6261013891', N'Agricultor', N' Chihuahua', N'Ojinaga', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (132, N'juan_wall@msn.com', N'MDDV8rDxNixRxxonjvmbsw==', N'JUAN', N'WALL REIMER', N'juan_wall@msn.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.180' AS DateTime), 1, 0, 1, N'c4f8c6b1-bd20-46f3-8fea-1c2400321219.jpg', N'1B66EB1', 0, 0, N'6261013956', N'GERENTE GENERAL OASIS', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (224, N'abeteichroebr@gmail.com', N'moCABLUJGK1BpE6t+13ckg==', N'Abraham', N'Teichroeb', N'abeteichroebr@gmail.com', N'admin', CAST(N'2017-03-30 12:00:08.693' AS DateTime), CAST(N'2017-03-30 12:19:48.453' AS DateTime), 1, 0, NULL, N'ca6106d2-519f-43c4-a6e3-79410ce1181f.jpg', N'218D380', NULL, NULL, N'6365367726', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (131, N'algodonera_pancho@hotmail.com', N'spuezMciPEhsYhyXP7Atsg==', N'FRANZ', N'LOEWEN VOTH', N'algodonera_pancho@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.180' AS DateTime), 1, 0, 1, N'f57759fa-b222-481e-83cd-966e58d8ecb4.jpg', N'2ABDD71', 0, 0, N'6261013807', N'GERENTE DE COMPRAS OASIS', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (133, N'aflores@anh.mx', N'A6c44dOcbpA21WYztTN/OQ==', N'ANTONIO', N'FLORES', N'aflores@anh.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.183' AS DateTime), 1, 0, 1, N'38bddc48-941b-4bf3-bf54-cabced423742.jpg', N'C04188F', 0, 0, N'6261005390', N'GERENTE DE COMPRAS NUEVA HOLANDA', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (134, N'klasswall@hotmail.com', N'HBs7she/juSIdsaa25LpVw==', N'NICO', N'WALL', N'klasswall@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.183' AS DateTime), 1, 0, 1, N'605f5dc7-39a7-4f6a-a77a-01131a285a68.jpg', N'05533AE', 0, 0, N'6261005390', N'RESPONSABLE DE PAGOS NUEVA HOLANDA', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (135, N'letkemanbc@gmail.com', N'Bc12BRCs7hsclVl6RHxI7w==', N'CORNELIO', N'LETKEMAN', N'letkemanbc@gmail.com', N'admin', NULL, CAST(N'2017-03-30 12:59:51.260' AS DateTime), 1, 0, 1, N'711fe469-03cf-447f-8e0b-3d32fe2fbd65.jpg', N'EC0201A', 0, 0, N'6261005397', N'GENRENTE GENERAL UNION DEL PROGRESO', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (136, N'sin_correo_3@dominio.com', N'Znu9FOxTtvydZ5lcrXYklg==', N'JACOBO', N'REMPEL', N'sin_correo_3@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.187' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'6C68F7A', 0, 0, N'6251056630', N'?', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (137, N'algodoneralaperla@yahoo.com.mx', N'I9mD5JSgVauoophzmCmWmQ==', N'ING JESUS', N'MONTANTES', N'algodoneralaperla@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.187' AS DateTime), 1, 0, 1, N'65559d91-b2aa-4d71-8b85-30dec154faa7.jpg', N'8B8D948', 0, 0, N'6261017929', N'GERENTE GENERAL LA PERLA', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (138, N'sin_correo_4@dominio.com', N'zu+S1B+gBJj19KKLuY80zw==', N'PEDRO', N'HIEBERT', N'sin_correo_4@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.187' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'F825AD5', 0, 0, N'6261005207', N'PRESIDENTE LA PERLA', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (139, N'f.wiebe@hotmail.com', N'PeruWm6GBdMk/VaVvG1QbA==', N'FRANZ', N'WIEBE DYCK', N'f.wiebe@hotmail.com', N'admin', NULL, CAST(N'2017-03-30 12:21:43.597' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'D3DDBE5', 0, 0, N'6261009006', N'GERENTE GENERAL ALGODONERA TARAHUMARA', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (140, N'da.p.s@hotmail.com', N'bV4eX2I17F36+pjTT+UBBw==', N'FRANZ', N'WIEBE NEUFELD', N'da.p.s@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.190' AS DateTime), 1, 0, 1, N'f9903203-27e3-4e6c-92c6-bd9f002efdc6.jpg', N'456D1EC', 0, 0, N'6261005396', N'?', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (141, N'eroyromero@hotmail.com', N'aIkxMAm2WVCQPktxCUH9Hg==', N'EROY', N'ROMERO', N'eroyromero@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.190' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'2A47BFA', 0, 0, N'6261005339', N'GERENTE GENERAL ALGODONERA LOS REYES', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (142, N'sin_correo_5@dominio.com', N'3VHExUFRXrgreSQeGkb8FQ==', N'PEDRO', N'REMPEL', N'sin_correo_5@dominio.com', N'admin', NULL, CAST(N'2017-03-30 15:56:31.803' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'D6337D9', 0, 0, N'6251037297', N'PRESIDENTE ALGODONERA LOS REYES', N' Aguascalientes', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (143, N'bravoquebec8@msn.com', N'mkgy91U5cWzbzn9kL8Yu8g==', N'LIC. FRANCISCO', N'NEGRETE', N'bravoquebec8@msn.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.193' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'E7ABFA8', 0, 0, N'6142168383', N'GERENTE ALGODONERA DEDESA (DELICIAS, CHIH.)', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (144, N'sin_correo_6@dominio.com', N'pW5OojjdusO8sUoUKDNLqw==', N'ING RIGOBERTO', N'RAMIREZ RAMIREZ', N'sin_correo_6@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.193' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'71E301D', 0, 0, N'6563537239', N'GERENTE TECNICO GRUPO 38', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (145, N'sin_correo_7@dominio.com', N'Yt70/ztofwkXRi7wbLV1Ig==', N'ING ANGELICA', N'RODRIGUEZ BARRAZA', N'sin_correo_7@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.193' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'965AE10', 0, 0, N'6562659339', N'GERENTE GENERAL GURPO 38', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (146, N'compras1@agate.com.mx', N'vI0VAcj5yyaRUeC1h9PhIQ==', N'FRANCISCO', N'DYCK W', N'compras1@agate.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.197' AS DateTime), 1, 0, 1, N'9bfe436a-0ffd-4cf8-84a3-f643db68d1c8.jpg', N'A803686', 0, 0, N'6255917934', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (147, N'compras2@agate.com.mx', N'EKswvIK0qWpyhY4VL9PyfQ==', N'BENJAMIN', N'DYCK F', N'compras2@agate.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.197' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'91BD445', 0, 0, N'6561995565', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (148, N'hermansusy@hotmail.com', N'lgaAgneEM1KqSoxW+GAzkg==', N'HERMAN', N'HARMS ', N'hermansusy@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.197' AS DateTime), 1, 0, 1, N'0be70f3b-b772-4b96-adee-39973b483364.jpg', N'CB20595', 0, 0, N'6251189260', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (149, N'dtorres@unamsa.mx', N'rMFNFEIjWREMINfBplfSww==', N'DIEGO', N'TORRES', N'dtorres@unamsa.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.197' AS DateTime), 1, 0, 1, N'62e7c086-3e99-4341-8b22-f7a1f4cb11fc.jpg', N'9CC7378', 0, 0, N'6141965787', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (150, N'familiabartch@yahoo.com.mx', N'S6yZhHqKvsVqx3v5nuTGjw==', N'EDWIEN', N'BARTCH U', N'familiabartch@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.200' AS DateTime), 1, 0, 1, N'195ab898-7d1e-4d53-aee0-a8da207f4e96.jpg', N'58F8991', 0, 0, N'6361037930', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (151, N'abrahamwiebe59@yahoo.com', N'XerELFwy1VGiZ39cdGCdlw==', N'ABRAHAM', N'WIEBE W', N'abrahamwiebe59@yahoo.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.200' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'C5D7577', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (152, N'klassenpeter62@gmail.com', N'IgEcPgujJHDe6+OqTMOh2A==', N'PETER', N'KLASSEN V', N'klassenpeter62@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.200' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'88BCC42', 0, 0, N'6251256265', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (153, N'heinrich100766@gmail.com', N'7qEGtyJDPlXJAimRQdXYxQ==', N'ENRIQUE', N'NEUFELD ', N'heinrich100766@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.203' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'B61914C', 0, 0, N'6251044393', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (154, N'granos_y_semillas1@hotmail.com', N'qfgDTOKRikZfb34dW5VT+w==', N'JOHAN', N'HILDEBRAND S', N'granos_y_semillas1@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.203' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'233AED4', 0, 0, N'6361120817', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (155, N'granos_y_semillas2@hotmail.com', N'1tSUqlONIPYNOEduTDfZ2A==', N'JACOB', N'BANMAN', N'granos_y_semillas2@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.203' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'B726031', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (156, N'sin_correo_1@dominio.com', N'5JdTABE177f4H2Rk9E6NOw==', N'ERNESTO', N'DYCK ', N'sin_correo_1@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.207' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'6EAF64B', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (157, N'sin_correo_2@dominio.com', N'nr5ruXJyIFMxRmy0a0Rfhg==', N'DAVID', N'FEHR ', N'sin_correo_2@dominio.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.207' AS DateTime), 1, 0, 1, N'f4874b2d-4e73-48ad-b083-25650841b1b8.jpg', N'6F745C8', 0, 0, N'6361115367', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (158, N'agrobuena3@hotmail.com', N'8FMLp2P8T8TJ4AgeoGz7lw==', N'JOHAN', N'WIEBE K', N'agrobuena3@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.207' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'14A624F', 0, 0, N'6361098510', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (159, N'agrobuena4@hotmail.com', N'o+rsf67NwO9QTqknkmre4w==', N'ABRAHAM', N'BERGEN P', N'agrobuena4@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.207' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'1EAFFAF', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (160, N'davidwiebe08@hotmail.com', N'IJ3llzUSdKSb1DuraFFcSQ==', N'DAVID', N'WIEBE W', N'davidwiebe08@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.210' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'7882ED5', 0, 0, N'6361098511', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (161, N'victor_enns@hotmail.com', N'c4b5DeMsNPUHK0QYbe3XRQ==', N'VICTOR', N'ENNS F', N'victor_enns@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.210' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'3C00C0C', 0, 0, N'6361098521', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (162, N'jcnevarez2012_1@hotmail.com', N'hEMw4Vs4t/R5JLD5IndISg==', N'JACOB', N'HIEBERT K', N'jcnevarez2012_1@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.210' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'D3C22F8', 0, 0, N'6361098958', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (163, N'jcnevarez2012_2@hotmail.com', N'MxDcuad4UGNyPbgJpy8SmA==', N'JUAN', N'REMPEL ', N'jcnevarez2012_2@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.213' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'55B54BE', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (164, N'algodonera_ba@hotmail.com', N'Kp6/UpU2XkR37u/F+m+wXA==', N'ISACK', N'BERGEN', N'algodonera_ba@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.213' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'B3D0D90', 0, 0, N'6361023885', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (165, N'ingluisceballos@hotmail.com', N'zhuW+Pe1FVU2ANpBncQK9Q==', N'ADRIAN', N'CEBALLOS', N'ingluisceballos@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.213' AS DateTime), 1, 0, 1, N'ae7259be-7cb3-4264-a3ed-adbe97c4be75.jpg', N'2E1A115', 0, 0, N'6366991796', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (166, N'cppradera.dd1@gmail.com', N'mkKrROiZvD7XLqQP9T+iwg==', N'JACOB', N'FRIESEN ', N'cppradera.dd1@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.217' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'7BBD26A', 0, 0, N'6251153754', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (167, N'cppradera.dd2@gmail.com', N'h5i+JC2GBGalQMHoiM+SEQ==', N'ABRAHAM', N'FEHER ', N'cppradera.dd2@gmail.com', N'admin', NULL, CAST(N'2017-03-30 11:30:43.423' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'DDFD9F2', 0, 0, N'6361020444', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (168, N'camposagricolasunidos1@prodigy.net.mx', N't8Sik8snQm2qjuxamM/oPQ==', N'ABRAHAM', N'PETERS R', N'camposagricolasunidos1@prodigy.net.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.217' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'68902CF', 0, 0, N'6361007276', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (169, N'camposagricolasunidos2@prodigy.net.mx', N'e4iXAC2vSprmP5hf8+BV2g==', N'OSCAR', N'HERNANDEZ', N'camposagricolasunidos2@prodigy.net.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.217' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'490B240', 0, 0, N'6361007209', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (170, N'algodoneralasaguilas1@gmail.com', N'TjpGI8cCGVKCoUn7tLREcA==', N'PEDRO', N'FRIESEN ', N'algodoneralasaguilas1@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.220' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'155054E', 0, 0, N'6361007343', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (171, N'algodoneralasaguilas2@gmail.com', N'3HbBNG9DNb64gPaogIXvBQ==', N'JUAN', N'SANCHEZ C', N'algodoneralasaguilas2@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.220' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'9924A4F', 0, 0, N'6361007312', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (172, N'upasa_9906_1@yahoo.com.mx', N'67T/4sMT/8Np6D3mKB8k5w==', N'FRANZ', N'PETERS', N'upasa_9906_1@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.220' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'2C9D1D2', 0, 0, N'6366991412', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (173, N'upasa_9906_2@yahoo.com.mx', N'cKnvbJsiuuvypgmiHqY7MA==', N'FRANCISCO', N'WIEBE W', N'upasa_9906_2@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.223' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'A746552', 0, 0, N'6366991843', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (174, N'eiasa_chona@yahoo.com.mx', N'zd3H1Jm4hSlSTzt/UIiRKQ==', N'ENRIQUE', N'MORENO', N'eiasa_chona@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.223' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'5052422', 0, 0, N'6366991499', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (175, N'alianza_ascencion@yahoo.com.mx', N'6XfA/xx0nAv70TDjpzODyQ==', N'CARLOS', N'MARTINEZ', N'alianza_ascencion@yahoo.com.mx', N'admin', NULL, CAST(N'2016-12-16 08:35:56.223' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'391353A', 0, 0, N'', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (178, N'JORGE.COLIN@BAYER.COM', N'rGZ8MHX4pZg+JjpvsTykMQ==', N'JORGE ABRAHAM', N'COLIN', N'JORGE.COLIN@BAYER.COM', N'admin', CAST(N'2017-03-02 00:06:29.470' AS DateTime), CAST(N'2017-03-02 09:11:29.303' AS DateTime), 1, 1, NULL, N'imagen-978307200.0.jpg', N'E58510D', NULL, NULL, N'5561667206', N'PROJECT MANAGER', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (188, N'RHGM_ESCALERA@HOTMAIL.COM', N'/V78GJ1jXVNTFDM9jcSUMQ==', N'RICARDO', N'HACES', N'RHGM_ESCALERA@HOTMAIL.COM', N'admin', CAST(N'2017-03-02 12:07:21.843' AS DateTime), CAST(N'2017-03-02 12:07:21.843' AS DateTime), 1, 0, NULL, N'fa4d5ce6-b932-495b-9583-eb428958ae90.jpg', N'A8EFCE5', NULL, NULL, N'8712119308', N'DISTRIBUIDOR', N' Coahuila', N'TORREON', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (192, N'JOEL.PRIETO@BAYER.COM', N'ahMj/U1s5gAINqBeI9gQQw==', N'JOEL', N'PRIETO', N'JOEL.PRIETO@BAYER.COM', N'admin', CAST(N'2017-03-02 12:31:41.203' AS DateTime), CAST(N'2017-03-02 12:31:41.203' AS DateTime), 1, 0, NULL, N'', N'B694D5B', NULL, NULL, N'8348537561', N'RTV', N' Tamaulipas', N'CIUDAD VICTORIA', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (199, N'ricardo_l65@hotmail.com', N'u/mgpONanm0U6a14WO5omA==', N'Ricardo', N'Lujan', N'ricardo_l65@hotmail.com', N'admin', CAST(N'2017-03-02 14:53:11.900' AS DateTime), CAST(N'2017-03-02 14:53:11.900' AS DateTime), 1, 0, NULL, N'406b7354-e14a-42ec-b103-ee235b0b0741.jpg', N'DAD8DF8', NULL, NULL, N'6366922116', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (200, N'Hein@hotmail.com', N'z8Tp6v1iUuKtYdBM3xvQzA==', N'HEINRICH', N'LOEWEN', N'Hein@hotmail.com', N'admin', CAST(N'2017-03-02 14:57:30.633' AS DateTime), CAST(N'2017-03-02 14:57:30.633' AS DateTime), 1, 0, NULL, N'', N'359F2E9', NULL, NULL, N'6367002335', N'AGRICULTOR', N' Chihuahua', N'JANOS', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (217, N'Hector.ornelas@bayer.com', N'AZa66dQAgdLDcg8Cpq7Uxw==', N'Hector', N'Ornelas', N'Hector.ornelas@bayer.com', N'admin', CAST(N'2017-03-30 09:14:41.573' AS DateTime), CAST(N'2017-03-30 09:14:41.573' AS DateTime), 1, 1, NULL, N'06130690-ab7b-476b-aa7e-a263c07a0c78.jpg', N'EE1C7EE', NULL, NULL, N'57283005', N'RH', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (226, N'davidpeterst@gmail.com', N'8wy2leHSx+SDrK3bwNRyCw==', N'david', N'peters thiessen', N'davidpeterst@gmail.com', N'admin', CAST(N'2017-03-30 12:20:27.743' AS DateTime), CAST(N'2017-03-30 12:20:27.743' AS DateTime), 1, 0, NULL, N'', N'3F93AD1', NULL, NULL, N'6261005396', N'agricultor', N' Chihuahua', N'ojinaga', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (227, N'isidrowiebe@hotmail.com', N'Vq5OqWXXW3D5+G+gLc8w/w==', N'Isidro', N'Wiebe', N'isidrowiebe@hotmail.com', N'admin', CAST(N'2017-03-30 12:23:41.713' AS DateTime), CAST(N'2017-03-30 12:23:41.713' AS DateTime), 1, 0, NULL, N'', N'DD719F5', NULL, NULL, N'6261013912', N'Agricultor', N' Chihuahua', N'Oasis', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (228, N'Daniela.aguirre@bayer.com', N'01pcFeuWroETmU0404CIpw==', N'Daniela', N'Aguirre', N'Daniela.aguirre@bayer.com', N'admin', CAST(N'2017-03-30 12:28:15.050' AS DateTime), CAST(N'2017-03-30 12:28:15.050' AS DateTime), 1, 1, NULL, N'', N'CA6E13F', NULL, NULL, N'5533335700', N'Marketing Manager', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (229, N'dneufeld2014@gmail.com', N'8k4KYutJscP5TpXGZVxMXg==', N'david', N'neufeld', N'dneufeld2014@gmail.com', N'admin', CAST(N'2017-03-30 12:30:45.250' AS DateTime), CAST(N'2017-03-30 12:30:45.250' AS DateTime), 1, 0, NULL, N'', N'297C3FC', NULL, NULL, N'6251044404', N'agricultor', N' Chihuahua', N'buenaventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (231, N'peterwiebe18@gmail.com', N'MhtjLLLvwc0jh3Zx0mybJw==', N'Peter', N'Wiebe', N'peterwiebe18@gmail.com', N'admin', CAST(N'2017-03-30 12:39:01.107' AS DateTime), CAST(N'2017-03-30 12:43:49.957' AS DateTime), 1, 0, NULL, N'', N'7900164', NULL, NULL, N'6361011949', N'Agricultor', N' Chihuahua', N'Ascención', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (233, N'elias.tapia@bayer.com', N'q/j9xTAZIVkkABKQbnGCLw==', N'Elias', N'Tapia', N'elias.tapia@bayer.com', N'admin', CAST(N'2017-03-30 13:01:05.677' AS DateTime), CAST(N'2017-03-30 13:03:40.367' AS DateTime), 1, 1, NULL, N'ec1f062f-9271-473d-9780-e3e0d2e0f95f.jpg', N'332FEBB', NULL, NULL, N'5585813896', N'Gerente de R&D', N' Ciudad de México', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (234, N'el8beto@hotmail.com', N'zpMTIrw9/taC9FogVljbxg==', N'Albert', N'Enns', N'el8beto@hotmail.com', N'admin', CAST(N'2017-03-30 13:09:54.507' AS DateTime), CAST(N'2017-03-30 13:09:54.507' AS DateTime), 1, 0, NULL, N'', N'76F09C8', NULL, NULL, N'6361110984', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (236, N'dav.knelsen@hotmail.com', N'pA6osXLvzZepZXrgmo0ESA==', N'David', N'Knelsen', N'dav.knelsen@hotmail.com', N'admin', CAST(N'2017-03-30 13:48:44.933' AS DateTime), CAST(N'2017-03-30 15:17:17.387' AS DateTime), 1, 0, NULL, N'5f216995-3f0a-4153-9eb9-6cd1039a4763.jpg', N'CCA8DC5', NULL, NULL, N'6261005069', N'Agricultor', N' Chihuahua', N'Ojinaga', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (238, N'efren2408@outlook.com', N'krzwUK3I4XlThggcm6OvxQ==', N'efren', N'hernandez', N'efren2408@outlook.com', N'admin', CAST(N'2017-03-30 13:56:44.443' AS DateTime), CAST(N'2017-03-30 13:56:44.443' AS DateTime), 1, 1, NULL, N'', N'EA17680', NULL, NULL, N'6361112140', N'tecnico', N' Chihuahua', N'casas grandes', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (242, N'johanrempel@yahoo.com.mx', N'd1FYuXBP97tmU1/CXClVxA==', N'Johan', N'Rempel', N'johanrempel@yahoo.com.mx', N'admin', CAST(N'2017-03-30 15:12:42.010' AS DateTime), CAST(N'2017-03-30 15:12:42.010' AS DateTime), 1, 0, NULL, N'', N'611DA1B', NULL, NULL, N'6251037718', N'Agricultor', N' Chihuahua', N'Cuauhtémoc', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (244, N'peterwiebeenns@hotmail.com', N'TX6dgUttHgyF67fDEDSi9g==', N'Peter', N'Wiebe Enns', N'peterwiebeenns@hotmail.com', N'admin', CAST(N'2017-03-30 16:00:08.277' AS DateTime), CAST(N'2017-03-30 16:02:44.520' AS DateTime), 1, 0, NULL, N'', N'8E9E732', NULL, NULL, N'6251151427', N'Agricultor', N' Chihuahua', N'Riva Palacio', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (176, N'albinorempel@gmail.com', N'PQS2oJwHUr7mqxyACHZ38Q==', N'ALBINO', N'REMPEL G', N'albinorempel@gmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.227' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'F0C17EB', 0, 0, N'6361098809', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (177, N'ronny_enns@hotmail.com', N'z27ww+4nEXjfWXDucgoVsQ==', N'RONNY', N'ENNS', N'ronny_enns@hotmail.com', N'admin', NULL, CAST(N'2016-12-16 08:35:56.227' AS DateTime), 1, 0, 1, N'sin_imagen.png', N'D023DF1', 0, 0, N'6251069462', N'', N'', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (179, N'diego.merida@bayer.com', N'Mc3yD44vdidE09sFNJK60Q==', N'DIEGO', N'MERIDA', N'diego.merida@bayer.com', N'admin', CAST(N'2017-03-02 08:16:26.293' AS DateTime), CAST(N'2017-03-02 08:16:26.293' AS DateTime), 1, 0, NULL, N'727eb5b2-e773-4d6c-b0ee-952a180eb549.jpg', N'B854B76', NULL, NULL, N'5555555555', N'DESARROLLO AGRONOMICO', N' Chihuahua', N'CHIHUAHUA', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (181, N'GERARDO.GONZALEZ@BAYER.COM', N'Og9SpIMajM1HNCmCdvf3MQ==', N'GERARDO', N'GONZALEZ', N'GERARDO.GONZALEZ@BAYER.COM', N'admin', CAST(N'2017-03-02 08:22:13.630' AS DateTime), CAST(N'2017-03-02 08:22:13.630' AS DateTime), 1, 0, NULL, N'1e58ca3e-5843-480a-b9e6-73e39ac77d2e.jpg', N'2F378C7', NULL, NULL, N'8311020083', N'CROP MANAGER', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (182, N'FRANCISCO.MEDINA@BAYER.COM', N'5BDw6akWDr+IMjJBYRkzvA==', N'FRANCISCO', N'MEDINA', N'FRANCISCO.MEDINA@BAYER.COM', N'admin', CAST(N'2017-03-02 08:37:40.130' AS DateTime), CAST(N'2017-03-02 08:37:40.130' AS DateTime), 1, 0, NULL, N'', N'76C6EEC', NULL, NULL, N'5540848124', N'COMUNICACION', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (183, N'NICOLAS.DIAZ@BAYER.COM', N'3BwZ3JYWVkCObFnlbu7lbg==', N'NICOLAS', N'DIAZ', N'NICOLAS.DIAZ@BAYER.COM', N'admin', CAST(N'2017-03-02 08:44:00.913' AS DateTime), CAST(N'2017-03-02 08:44:00.913' AS DateTime), 1, 0, NULL, N'', N'F946282', NULL, NULL, N'5554331797', N'REGULATORY AFFAIRS MANAGER', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (184, N'ENRIQUE.ALEMAN@BAYER.COM', N'nx/uTa1oE9khNH3KiyD85g==', N'ENRIQUE', N'ALEMAN', N'ENRIQUE.ALEMAN@BAYER.COM', N'admin', CAST(N'2017-03-02 09:19:48.843' AS DateTime), CAST(N'2017-03-02 09:19:48.843' AS DateTime), 1, 0, NULL, N'', N'FE49418', NULL, NULL, N'2484880384', N'ING TEXTIL', N' Puebla', N'PUEBLA', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (190, N'PEDRO.FLORES@BAYER.COM', N'fkIqnrNXZStQZoZX+Y7UXA==', N'PEDRO', N'FLORES', N'PEDRO.FLORES@BAYER.COM', N'admin', CAST(N'2017-03-02 12:20:36.570' AS DateTime), CAST(N'2017-03-02 12:20:36.570' AS DateTime), 1, 0, NULL, N'51ba391a-9586-4449-8604-ff30d8523f5f.jpg', N'F9E6890', NULL, NULL, N'6255846056', N'FIELD MARKETING SPECIALIST', N' Chihuahua', N'CUAUHTEMOC', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (193, N'RENA.MARIN@BAYER.COM', N'86Paxjimw+tGnQKiItaYWg==', N'RENE', N'MARIN', N'RENA.MARIN@BAYER.COM', N'admin', CAST(N'2017-03-02 12:33:19.543' AS DateTime), CAST(N'2017-03-02 12:33:19.543' AS DateTime), 1, 0, NULL, N'd6f97167-635b-49b3-a42d-0bd4d979793b.jpg', N'6D8D730', NULL, NULL, N'8712408677', N'RTV', N' Chihuahua', N'DELICIAS', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (198, N'abraham.sandoval@bayer.com', N'GjvLn3dkAlx4x53HZ1ZKFw==', N'Abraham', N'Loewen', N'abraham.sandoval@bayer.com', N'admin', CAST(N'2017-03-02 14:45:47.670' AS DateTime), CAST(N'2017-03-02 14:45:47.670' AS DateTime), 1, 0, NULL, N'', N'73C935D', NULL, NULL, N'6261013925', N'', N' Chihuahua', N'Aldama', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (202, N'rmacgregor@unamsa.mx', N'bFL1B6NE2aeQklTXm2TwBw==', N'Roberto', N'Mac Gregor', N'rmacgregor@unamsa.mx', N'admin', CAST(N'2017-03-02 15:49:00.500' AS DateTime), CAST(N'2017-03-02 15:49:00.500' AS DateTime), 1, 0, NULL, N'', N'8C542D9', NULL, NULL, N'6391003801', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (203, N'jlr-2013@hotmail.com', N'OfuoCCO9+xt1nCRyeXphMw==', N'Johan', N'Loewen', N'jlr-2013@hotmail.com', N'admin', CAST(N'2017-03-02 15:59:30.713' AS DateTime), CAST(N'2017-03-02 15:59:30.713' AS DateTime), 1, 0, NULL, N'', N'F049048', NULL, NULL, N'6261013895', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (206, N'jessica.puschies@bayer.com', N'wRbfm4SGt7WG6479fdcyxA==', N'Jessica ', N'Puschies', N'jessica.puschies@bayer.com', N'admin', CAST(N'2017-03-02 16:37:56.680' AS DateTime), CAST(N'2017-03-02 16:37:56.680' AS DateTime), 1, 0, NULL, N'', N'4D0D700', NULL, NULL, N'57283000', N'', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (207, N'daniel.flores0@icloud.com', N'GTnz95JXrUHD3IdDjr1yew==', N'Daniel', N'Flores', N'daniel.flores0@icloud.com', N'admin', CAST(N'2017-03-02 16:48:54.550' AS DateTime), CAST(N'2017-03-02 16:48:54.550' AS DateTime), 1, 0, NULL, N'', N'ED213D4', NULL, NULL, N'6366991052', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (213, N'jalicano@anh.mx', N'8qHhvl5qlauXPu3qFGSywA==', N'Javier', N'Alicano', N'jalicano@anh.mx', N'admin', CAST(N'2017-03-02 20:02:47.783' AS DateTime), CAST(N'2017-03-02 20:02:47.783' AS DateTime), 1, 0, NULL, N'', N'A0F4042', NULL, NULL, N'6261005397', N'Administrador', N' Chihuahua', N'Ojinaga', 2)
GO
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (232, N'abram_w.83@hotmail.com', N'VEfFZ0lij6mjmK50BoP+tg==', N'Abram', N'Neufeld', N'abram_w.83@hotmail.com', N'admin', CAST(N'2017-03-30 12:46:02.817' AS DateTime), CAST(N'2017-03-30 12:46:52.870' AS DateTime), 1, 0, NULL, N'', N'D742E05', NULL, NULL, N'6366991535', N'', N' Chihuahua', N'Janos', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (189, N'INSU_AGRO@HOTMAIL.COM', N'MaRc+MTG0Ey0Dt3N+f+zAA==', N'PABLO', N'RODRIGUEZ', N'INSU_AGRO@HOTMAIL.COM', N'admin', CAST(N'2017-03-02 12:12:54.620' AS DateTime), CAST(N'2017-03-02 12:12:54.620' AS DateTime), 1, 0, NULL, N'', N'3EDA632', NULL, NULL, N'6361092934', N'DISTRIBUIDOR', N' Chihuahua', N'CHIHUAHUA', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (215, N'jessica.alexander@bayer.com', N'iJ2Cdt+XZO8psvUJraRVMg==', N'Jessica', N'Alexander', N'jessica.alexander@bayer.com', N'admin', CAST(N'2017-03-29 07:51:35.267' AS DateTime), CAST(N'2017-03-29 07:51:35.267' AS DateTime), 1, 1, NULL, N'', N'758FCD1', NULL, NULL, N'5557283000', N'IT', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (239, N'j.danhiel19@hotmail.com', N'PCnNPnUZNtA+0859UWlz4Q==', N'daniel', N'garcia', N'j.danhiel19@hotmail.com', N'admin', CAST(N'2017-03-30 14:05:22.523' AS DateTime), CAST(N'2017-03-30 14:05:22.523' AS DateTime), 1, 1, NULL, N'', N'705B4E6', NULL, NULL, N'8442279561', N'tecnico', N' Chihuahua', N'casas grandes', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (241, N'chito.rsg@mail.com', N'Y3H2d8byOsyrWxcbxHILEQ==', N'Johan Chito', N'Froese', N'chito.rsg@mail.com', N'admin', CAST(N'2017-03-30 14:59:09.890' AS DateTime), CAST(N'2017-03-30 16:14:44.180' AS DateTime), 1, 0, NULL, N'', N'CCA35A5', NULL, NULL, N'6251037122', N'Agricultor', N' Chihuahua', N'Riva Palacio', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (243, N'ana.vargas@bayer.com', N'VmG+vYLqBgMb1p/erSqCAw==', N'John', N'Friesen', N'ana.vargas@bayer.com', N'admin', CAST(N'2017-03-30 15:28:51.993' AS DateTime), CAST(N'2017-03-30 15:31:08.993' AS DateTime), 1, 0, NULL, N'', N'38D7D13', NULL, NULL, N'6361109833', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (195, N'JUANMANUEL.SANCHEZ@BAYER.COM', N'Nl+LTNLwo4ea/SYv7O39sQ==', N'JUAN MANUEL', N'SANCHEZ', N'JUANMANUEL.SANCHEZ@BAYER.COM', N'admin', CAST(N'2017-03-02 12:36:55.403' AS DateTime), CAST(N'2017-03-02 12:36:55.403' AS DateTime), 1, 0, NULL, N'', N'F300140', NULL, NULL, N'8711104480', N'RTV', N' Coahuila', N'TORREON', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (204, N'ezfriessen@hotmail.com', N'OHLApE3F8knrfwLRVoEmqg==', N'Ernie', N'Friessen', N'ezfriessen@hotmail.com', N'admin', CAST(N'2017-03-02 16:33:49.480' AS DateTime), CAST(N'2017-03-02 16:33:49.480' AS DateTime), 1, 0, NULL, N'1cdc8c69-4a78-4027-b4c8-c914152fbf5d.jpg', N'0E0B2B2', NULL, NULL, N'6251307000', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (185, N'LESLIEFRANCIELA.CRUZRUIZ@BAYER.COM', N'45Rib+T1QQmcHGQZ3taU1g==', N'LESLIE', N'FRANCIELA', N'LESLIEFRANCIELA.CRUZRUIZ@BAYER.COM', N'admin', CAST(N'2017-03-02 09:31:17.940' AS DateTime), CAST(N'2017-03-02 09:31:17.940' AS DateTime), 1, 0, NULL, N'', N'E768235', NULL, NULL, N'2461289436', N'BRANDING & MARKETING COMMUNICATIONS', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (186, N'dilek.bocuk@bayer.com', N'Bde9mZnNIkzTRyYsxFAWug==', N'Dilek', N'Bocuk', N'dilek.bocuk@bayer.com', N'admin', CAST(N'2017-03-02 10:46:01.873' AS DateTime), CAST(N'2017-03-02 10:46:01.873' AS DateTime), 1, 0, NULL, N'13cb6b6e-4db9-4576-af1c-26d7baa84b72.jpg', N'87309EE', NULL, NULL, N'5557283000', N'IT Head', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (191, N'CARLOS.RAMIREZ@TREJO.COM', N'qA09qEB7aJl1P/tc06lFJA==', N'CARLOS', N'RAMIREZ', N'CARLOS.RAMIREZ@TREJO.COM', N'admin', CAST(N'2017-03-02 12:24:48.310' AS DateTime), CAST(N'2017-03-02 12:24:48.310' AS DateTime), 1, 0, NULL, N'7928b19a-f78d-4894-9300-be5f7bfaa241.jpg', N'278A42B', NULL, NULL, N'6394650005', N'RTV', N' Chihuahua', N'DELICIAS', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (201, N'dedesa1990@smart.net.mx', N't123wh7VnH5T2bdREYgXbg==', N'Juan', N'Chacón Aguirre', N'dedesa1990@smart.net.mx', N'admin', CAST(N'2017-03-02 15:08:46.537' AS DateTime), CAST(N'2017-03-02 15:08:46.537' AS DateTime), 1, 0, NULL, N'0b5eb79e-e160-449e-9d3f-53484d7d30d1.jpg', N'055075C', NULL, NULL, N'6394729566', N'Empleado', N' Chihuahua', N'Delicias', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (208, N'carlos.zavala65@outlook.com', N'XK4JTGJfzs9x3k7scGWh9w==', N'Juan', N'Zavala', N'carlos.zavala65@outlook.com', N'admin', CAST(N'2017-03-02 16:54:48.920' AS DateTime), CAST(N'2017-03-02 17:59:47.753' AS DateTime), 1, 0, NULL, N'', N'F5ACC2F', NULL, NULL, N'6361007351', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (209, N'fjwiebe@hotmail.com', N'fYF/VRYKM9hZRY/vW1KPKg==', N'Franz', N'Wiebe', N'fjwiebe@hotmail.com', N'admin', CAST(N'2017-03-02 17:04:10.333' AS DateTime), CAST(N'2017-03-02 17:04:10.333' AS DateTime), 1, 0, NULL, N'e6c58bb9-8ff5-4dec-a1d5-e42897c269fd.jpg', N'FF30B67', NULL, NULL, N'6361098530', N'', N' Chihuahua', N'', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (211, N'David.mireles@bayer.com', N'pYF6rnSn4FH/NRdy56/GsQ==', N'David ', N'Mireles', N'David.mireles@bayer.com', N'admin', CAST(N'2017-03-02 18:07:21.543' AS DateTime), CAST(N'2017-03-02 18:07:21.543' AS DateTime), 1, 0, NULL, N'', N'21016F4', NULL, NULL, N'8712779162', N'MANAGER', N' Coahuila', N'Torreon', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (212, N'jacobohiebertklassen@hotmail.com', N'z2qqb0VH8Fu1tdlykQYu4A==', N'Jacobo', N'Hiebert', N'jacobohiebertklassen@hotmail.com', N'admin', CAST(N'2017-03-02 19:52:35.647' AS DateTime), CAST(N'2017-03-02 19:52:35.647' AS DateTime), 1, 0, NULL, N'', N'DE9C989', NULL, NULL, N'6361098958', N'AGRICULTOR', N' Chihuahua', N'JANOS', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (235, N'adeandnettie@yahoo.com', N'TSxxxMXW6OlU6uXJvXcJOw==', N'Abraham', N'Giesbrecht', N'adeandnettie@yahoo.com', N'admin', CAST(N'2017-03-30 13:43:00.540' AS DateTime), CAST(N'2017-03-30 13:43:00.540' AS DateTime), 1, 0, NULL, N'6e54dce3-332c-4359-bada-04baaa3d70fa.jpg', N'40A1BAF', NULL, NULL, N'6261013950', N'Agricultor', N' Chihuahua', N'Ojinaga', 2)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (218, N'hugo.cruzhipolito@bayer.com', N'qUMROE7CZgEGhfyshoJyKg==', N'Hugo', N'Cruz Hipolito', N'hugo.cruzhipolito@bayer.com', N'admin', CAST(N'2017-03-30 11:33:28.213' AS DateTime), CAST(N'2017-03-30 11:33:28.213' AS DateTime), 1, 1, NULL, N'', N'0F93069', NULL, NULL, N'5560700233', N'Technical Services', N' Ciudad de México', N'MIGUEL HIDALGO', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (240, N'unger147@gmail.com', N'wMxfMQ+0mksf6FtAG+uFLg==', N'Cornelio', N'Unger', N'unger147@gmail.com', N'admin', CAST(N'2017-03-30 14:42:45.020' AS DateTime), CAST(N'2017-03-30 14:42:45.020' AS DateTime), 1, 0, NULL, N'', N'8B21C41', NULL, NULL, N'6361109042', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (225, N'jacobwiebe790301@gmail.com', N'FAq9CbTJLgdn0oQkTqHbyw==', N'Jacob', N'Wiebe Peters', N'jacobwiebe790301@gmail.com', N'admin', CAST(N'2017-03-30 12:07:14.687' AS DateTime), CAST(N'2017-03-30 12:09:43.147' AS DateTime), 1, 0, NULL, N'', N'B33F6AF', NULL, NULL, N'6361024464', N'Agricultor', N' Chihuahua', N'Buena Aventura', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (230, N'jpablo.zamudio@bayer.com', N'SfvsjEewZPrpdisuo5mQpw==', N'Juan Pablo', N'Zamudio', N'jpablo.zamudio@bayer.com', N'admin', CAST(N'2017-03-30 12:37:01.103' AS DateTime), CAST(N'2017-03-30 12:37:01.103' AS DateTime), 1, 1, NULL, N'', N'DD14C71', NULL, NULL, N'5554139248', N'Empleado', N' Ciudad de México', N'Miguel Hidalgo', 1)
INSERT [dbo].[Users] ([Id], [Username], [Password], [FirstName], [LastName], [CorreoElectronico], [UserModify], [DateTimeCreate], [DateTimeModify], [Active], [Administrador], [IdEvento], [photo], [PIN], [Companions], [PreRegister], [Telefono], [Ocupacion], [Estado], [Municipio], [IdDistribuidor]) VALUES (237, N'palmeraswiebe@yahoo.com', N'CXhKCkhYB7bqZ5ReE173aw==', N'Cornelio', N'Wiebe', N'palmeraswiebe@yahoo.com', N'admin', CAST(N'2017-03-30 13:52:53.653' AS DateTime), CAST(N'2017-03-30 13:52:53.653' AS DateTime), 1, 0, NULL, N'', N'EEE0892', NULL, NULL, N'6261047835', N'Agricultor', N' Chihuahua', N'Las Palmeras', 2)
SET IDENTITY_INSERT [dbo].[Users] OFF
USE [master]
GO
ALTER DATABASE [CottonClub] SET  READ_WRITE 
GO
