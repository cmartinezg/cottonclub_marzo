﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="CottonClub.Register" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	    <title>Registro</title>
	    
	    <link href="admin/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	    
	    <script src="admin/assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 
	    
	</head>
	<body class="register">
	    
		<div class="jumbotron vertical-center">
			<div class="container" id="register">
				<div class="col-sm-6 col-sm-offset-3 panel panel-default">
					<div class="panel-body">
						<form id="form1" runat="server" method="post" class="text-center">
							<!--<asp:Image ID="imgLogoBack" runat="server" ImageUrl="~/admin/assets/img/logo2.png" Width="180px" Height="120px"/>-->
                            <asp:Image ID="imgLogo" runat="server" ImageUrl="" Width="180px" Height="120px"/>
							<h1>REGISTRO</h1>
							<p>Por favor confirma tus datos e indica el número de acompañantes así como sus nombres y edades.</p>
							<div class="form-horizontal">
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre" class="form-control input-lg fm-control"></asp:TextBox> 
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtApellidos" runat="server" placeholder="Apellidos" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtTelefono" runat="server" placeholder="Teléfono" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtOcupacion" runat="server" placeholder="Ocupación" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									</div>
								</div>
								<div class="form-group">
                                        <asp:DropDownList ID="ddlEstado" runat="server"  class="form-control input-lg">
                                            <asp:ListItem Value=" Aguascalientes"> Aguascalientes</asp:ListItem>
<asp:ListItem Value=" Baja California"> Baja California</asp:ListItem>
<asp:ListItem Value=" Baja California Sur"> Baja California Sur</asp:ListItem>
<asp:ListItem Value=" Campeche"> Campeche</asp:ListItem>
<asp:ListItem Value=" Chiapas"> Chiapas</asp:ListItem>
<asp:ListItem Value=" Chihuahua"> Chihuahua</asp:ListItem>
<asp:ListItem Value=" Ciudad de México"> Ciudad de México</asp:ListItem>
<asp:ListItem Value=" Coahuila"> Coahuila</asp:ListItem>
<asp:ListItem Value=" Colima"> Colima</asp:ListItem>
<asp:ListItem Value=" Durango"> Durango</asp:ListItem>
<asp:ListItem Value=" Estado de México"> Estado de México</asp:ListItem>
<asp:ListItem Value=" Guanajuato"> Guanajuato</asp:ListItem>
<asp:ListItem Value=" Guerrero"> Guerrero</asp:ListItem>
<asp:ListItem Value=" Hidalgo"> Hidalgo</asp:ListItem>
<asp:ListItem Value=" Jalisco"> Jalisco</asp:ListItem>
<asp:ListItem Value=" Michoacán"> Michoacán</asp:ListItem>
<asp:ListItem Value=" Morelos"> Morelos</asp:ListItem>
<asp:ListItem Value=" Nayarit"> Nayarit</asp:ListItem>
<asp:ListItem Value=" Nuevo León"> Nuevo León</asp:ListItem>
<asp:ListItem Value=" Oaxaca"> Oaxaca</asp:ListItem>
<asp:ListItem Value=" Puebla"> Puebla</asp:ListItem>
<asp:ListItem Value=" Querétaro"> Querétaro</asp:ListItem>
<asp:ListItem Value=" Quintana Roo"> Quintana Roo</asp:ListItem>
<asp:ListItem Value=" San Luis Potosí"> San Luis Potosí</asp:ListItem>
<asp:ListItem Value=" Sin Localidad"> Sin Localidad</asp:ListItem>
<asp:ListItem Value=" Sinaloa"> Sinaloa</asp:ListItem>
<asp:ListItem Value=" Sonora"> Sonora</asp:ListItem>
<asp:ListItem Value=" Tabasco"> Tabasco</asp:ListItem>
<asp:ListItem Value=" Tamaulipas"> Tamaulipas</asp:ListItem>
<asp:ListItem Value=" Tlaxcala"> Tlaxcala</asp:ListItem>
<asp:ListItem Value=" Veracruz"> Veracruz</asp:ListItem>
<asp:ListItem Value=" Yucatán"> Yucatán</asp:ListItem>
<asp:ListItem Value=" Zacatecas"> Zacatecas</asp:ListItem>
                                        </asp:DropDownList>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtMunicipio" runat="server" placeholder="Municipio" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									</div>
								</div>
								<div class="form-group">
                                        <asp:DropDownList ID="ddlDistribuidor" runat="server" class="form-control input-lg">
                                        </asp:DropDownList>
								</div>
								<hr/>
								<div class="form-group">
									<h4>NÚMERO DE ACOMPAÑANTES</h4>
								</div>
								
								<div class="form-group" id="companions-select">
                                    <asp:DropDownList ID="ddlCompanions" runat="server" class="form-control input-lg">
                                        <asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                    </asp:DropDownList>
                                    <!--
									<select class="form-control input-lg">
										<option>0</option>
										<option>1</option>
										<option>2</option>
										<option>3</option>
									</select>
                                    -->
								</div>
								<hr/>
								
								<div id="companions-items">
									
									<div class="form-group text-left">
										<div class="col-sm-8">
											<label>Nombre del acompañante</label>
										</div>
										<div class="col-sm-4">
											<label>Edad</label>
										</div>
									</div>
																	
									<div class="form-group form-companions">
										<div class="col-sm-8">
											<!--<input type="text" class="form-control input-lg fm-control" />-->
                                            <asp:TextBox ID="txtCompanion1" runat="server" class="form-control input-lg fm-control"></asp:TextBox>
										</div>
										<div class="col-sm-4">
                                            <!--
											<select class="form-control input-lg">
												<option>0</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
											</select>
                                            -->
                                            <asp:DropDownList ID="ddlEdadCompanion1" runat="server" class="form-control input-lg">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>31</asp:ListItem>
                                                <asp:ListItem>32</asp:ListItem>
                                                <asp:ListItem>33</asp:ListItem>
                                                <asp:ListItem>34</asp:ListItem>
                                                <asp:ListItem>35</asp:ListItem>
                                                <asp:ListItem>36</asp:ListItem>
                                                <asp:ListItem>37</asp:ListItem>
                                                <asp:ListItem>38</asp:ListItem>
                                                <asp:ListItem>39</asp:ListItem>
                                                <asp:ListItem>40</asp:ListItem>
                                                <asp:ListItem>41</asp:ListItem>
                                                <asp:ListItem>42</asp:ListItem>
                                                <asp:ListItem>43</asp:ListItem>
                                                <asp:ListItem>44</asp:ListItem>
                                                <asp:ListItem>45</asp:ListItem>
                                                <asp:ListItem>46</asp:ListItem>
                                                <asp:ListItem>47</asp:ListItem>
                                                <asp:ListItem>48</asp:ListItem>
                                                <asp:ListItem>49</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                                <asp:ListItem>51</asp:ListItem>
                                                <asp:ListItem>52</asp:ListItem>
                                                <asp:ListItem>53</asp:ListItem>
                                                <asp:ListItem>54</asp:ListItem>
                                                <asp:ListItem>55</asp:ListItem>
                                                <asp:ListItem>56</asp:ListItem>
                                                <asp:ListItem>57</asp:ListItem>
                                                <asp:ListItem>58</asp:ListItem>
                                                <asp:ListItem>59</asp:ListItem>
                                                <asp:ListItem>60</asp:ListItem>
                                                <asp:ListItem>61</asp:ListItem>
                                                <asp:ListItem>62</asp:ListItem>
                                                <asp:ListItem>63</asp:ListItem>
                                                <asp:ListItem>64</asp:ListItem>
                                                <asp:ListItem>65</asp:ListItem>
                                                <asp:ListItem>66</asp:ListItem>
                                                <asp:ListItem>67</asp:ListItem>
                                                <asp:ListItem>68</asp:ListItem>
                                                <asp:ListItem>69</asp:ListItem>
                                                <asp:ListItem>70</asp:ListItem>
                                                <asp:ListItem>71</asp:ListItem>
                                                <asp:ListItem>72</asp:ListItem>
                                                <asp:ListItem>73</asp:ListItem>
                                                <asp:ListItem>74</asp:ListItem>
                                                <asp:ListItem>75</asp:ListItem>
                                                <asp:ListItem>76</asp:ListItem>
                                                <asp:ListItem>77</asp:ListItem>
                                                <asp:ListItem>78</asp:ListItem>
                                                <asp:ListItem>79</asp:ListItem>
                                                <asp:ListItem>80</asp:ListItem>
                                                <asp:ListItem>81</asp:ListItem>
                                                <asp:ListItem>82</asp:ListItem>
                                                <asp:ListItem>83</asp:ListItem>
                                                <asp:ListItem>84</asp:ListItem>
                                                <asp:ListItem>85</asp:ListItem>
                                                <asp:ListItem>86</asp:ListItem>
                                                <asp:ListItem>87</asp:ListItem>
                                                <asp:ListItem>88</asp:ListItem>
                                                <asp:ListItem>89</asp:ListItem>
                                                <asp:ListItem>90</asp:ListItem>
                                                <asp:ListItem>91</asp:ListItem>
                                                <asp:ListItem>92</asp:ListItem>
                                                <asp:ListItem>93</asp:ListItem>
                                                <asp:ListItem>94</asp:ListItem>
                                                <asp:ListItem>95</asp:ListItem>
                                                <asp:ListItem>96</asp:ListItem>
                                                <asp:ListItem>97</asp:ListItem>
                                                <asp:ListItem>98</asp:ListItem>
                                                <asp:ListItem>99</asp:ListItem>
                                            </asp:DropDownList>
										</div>
									</div>
									
									<div class="form-group form-companions">
										<div class="col-sm-8">
											<!--<input type="text" class="form-control input-lg fm-control" />-->
                                            <asp:TextBox ID="txtCompanion2" runat="server" class="form-control input-lg fm-control"></asp:TextBox>
										</div>
										<div class="col-sm-4">
                                            <!--
											<select class="form-control input-lg">
												<option>0</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
											</select>
                                            -->
                                            <asp:DropDownList ID="ddlEdadCompanion2" runat="server" class="form-control input-lg">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>31</asp:ListItem>
                                                <asp:ListItem>32</asp:ListItem>
                                                <asp:ListItem>33</asp:ListItem>
                                                <asp:ListItem>34</asp:ListItem>
                                                <asp:ListItem>35</asp:ListItem>
                                                <asp:ListItem>36</asp:ListItem>
                                                <asp:ListItem>37</asp:ListItem>
                                                <asp:ListItem>38</asp:ListItem>
                                                <asp:ListItem>39</asp:ListItem>
                                                <asp:ListItem>40</asp:ListItem>
                                                <asp:ListItem>41</asp:ListItem>
                                                <asp:ListItem>42</asp:ListItem>
                                                <asp:ListItem>43</asp:ListItem>
                                                <asp:ListItem>44</asp:ListItem>
                                                <asp:ListItem>45</asp:ListItem>
                                                <asp:ListItem>46</asp:ListItem>
                                                <asp:ListItem>47</asp:ListItem>
                                                <asp:ListItem>48</asp:ListItem>
                                                <asp:ListItem>49</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                                <asp:ListItem>51</asp:ListItem>
                                                <asp:ListItem>52</asp:ListItem>
                                                <asp:ListItem>53</asp:ListItem>
                                                <asp:ListItem>54</asp:ListItem>
                                                <asp:ListItem>55</asp:ListItem>
                                                <asp:ListItem>56</asp:ListItem>
                                                <asp:ListItem>57</asp:ListItem>
                                                <asp:ListItem>58</asp:ListItem>
                                                <asp:ListItem>59</asp:ListItem>
                                                <asp:ListItem>60</asp:ListItem>
                                                <asp:ListItem>61</asp:ListItem>
                                                <asp:ListItem>62</asp:ListItem>
                                                <asp:ListItem>63</asp:ListItem>
                                                <asp:ListItem>64</asp:ListItem>
                                                <asp:ListItem>65</asp:ListItem>
                                                <asp:ListItem>66</asp:ListItem>
                                                <asp:ListItem>67</asp:ListItem>
                                                <asp:ListItem>68</asp:ListItem>
                                                <asp:ListItem>69</asp:ListItem>
                                                <asp:ListItem>70</asp:ListItem>
                                                <asp:ListItem>71</asp:ListItem>
                                                <asp:ListItem>72</asp:ListItem>
                                                <asp:ListItem>73</asp:ListItem>
                                                <asp:ListItem>74</asp:ListItem>
                                                <asp:ListItem>75</asp:ListItem>
                                                <asp:ListItem>76</asp:ListItem>
                                                <asp:ListItem>77</asp:ListItem>
                                                <asp:ListItem>78</asp:ListItem>
                                                <asp:ListItem>79</asp:ListItem>
                                                <asp:ListItem>80</asp:ListItem>
                                                <asp:ListItem>81</asp:ListItem>
                                                <asp:ListItem>82</asp:ListItem>
                                                <asp:ListItem>83</asp:ListItem>
                                                <asp:ListItem>84</asp:ListItem>
                                                <asp:ListItem>85</asp:ListItem>
                                                <asp:ListItem>86</asp:ListItem>
                                                <asp:ListItem>87</asp:ListItem>
                                                <asp:ListItem>88</asp:ListItem>
                                                <asp:ListItem>89</asp:ListItem>
                                                <asp:ListItem>90</asp:ListItem>
                                                <asp:ListItem>91</asp:ListItem>
                                                <asp:ListItem>92</asp:ListItem>
                                                <asp:ListItem>93</asp:ListItem>
                                                <asp:ListItem>94</asp:ListItem>
                                                <asp:ListItem>95</asp:ListItem>
                                                <asp:ListItem>96</asp:ListItem>
                                                <asp:ListItem>97</asp:ListItem>
                                                <asp:ListItem>98</asp:ListItem>
                                                <asp:ListItem>99</asp:ListItem>
                                            </asp:DropDownList>
										</div>
									</div>
									
									<div class="form-group form-companions">
										<div class="col-sm-8">
											<!--<input type="text" class="form-control input-lg fm-control" />-->
                                            <asp:TextBox ID="txtCompanion3" runat="server" class="form-control input-lg fm-control"></asp:TextBox>
										</div>
										<div class="col-sm-4">
                                            <!--
											<select class="form-control input-lg">
												<option>0</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
											</select>
                                            -->
                                            <asp:DropDownList ID="ddlEdadCompanion3" runat="server" class="form-control input-lg">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>31</asp:ListItem>
                                                <asp:ListItem>32</asp:ListItem>
                                                <asp:ListItem>33</asp:ListItem>
                                                <asp:ListItem>34</asp:ListItem>
                                                <asp:ListItem>35</asp:ListItem>
                                                <asp:ListItem>36</asp:ListItem>
                                                <asp:ListItem>37</asp:ListItem>
                                                <asp:ListItem>38</asp:ListItem>
                                                <asp:ListItem>39</asp:ListItem>
                                                <asp:ListItem>40</asp:ListItem>
                                                <asp:ListItem>41</asp:ListItem>
                                                <asp:ListItem>42</asp:ListItem>
                                                <asp:ListItem>43</asp:ListItem>
                                                <asp:ListItem>44</asp:ListItem>
                                                <asp:ListItem>45</asp:ListItem>
                                                <asp:ListItem>46</asp:ListItem>
                                                <asp:ListItem>47</asp:ListItem>
                                                <asp:ListItem>48</asp:ListItem>
                                                <asp:ListItem>49</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                                <asp:ListItem>51</asp:ListItem>
                                                <asp:ListItem>52</asp:ListItem>
                                                <asp:ListItem>53</asp:ListItem>
                                                <asp:ListItem>54</asp:ListItem>
                                                <asp:ListItem>55</asp:ListItem>
                                                <asp:ListItem>56</asp:ListItem>
                                                <asp:ListItem>57</asp:ListItem>
                                                <asp:ListItem>58</asp:ListItem>
                                                <asp:ListItem>59</asp:ListItem>
                                                <asp:ListItem>60</asp:ListItem>
                                                <asp:ListItem>61</asp:ListItem>
                                                <asp:ListItem>62</asp:ListItem>
                                                <asp:ListItem>63</asp:ListItem>
                                                <asp:ListItem>64</asp:ListItem>
                                                <asp:ListItem>65</asp:ListItem>
                                                <asp:ListItem>66</asp:ListItem>
                                                <asp:ListItem>67</asp:ListItem>
                                                <asp:ListItem>68</asp:ListItem>
                                                <asp:ListItem>69</asp:ListItem>
                                                <asp:ListItem>70</asp:ListItem>
                                                <asp:ListItem>71</asp:ListItem>
                                                <asp:ListItem>72</asp:ListItem>
                                                <asp:ListItem>73</asp:ListItem>
                                                <asp:ListItem>74</asp:ListItem>
                                                <asp:ListItem>75</asp:ListItem>
                                                <asp:ListItem>76</asp:ListItem>
                                                <asp:ListItem>77</asp:ListItem>
                                                <asp:ListItem>78</asp:ListItem>
                                                <asp:ListItem>79</asp:ListItem>
                                                <asp:ListItem>80</asp:ListItem>
                                                <asp:ListItem>81</asp:ListItem>
                                                <asp:ListItem>82</asp:ListItem>
                                                <asp:ListItem>83</asp:ListItem>
                                                <asp:ListItem>84</asp:ListItem>
                                                <asp:ListItem>85</asp:ListItem>
                                                <asp:ListItem>86</asp:ListItem>
                                                <asp:ListItem>87</asp:ListItem>
                                                <asp:ListItem>88</asp:ListItem>
                                                <asp:ListItem>89</asp:ListItem>
                                                <asp:ListItem>90</asp:ListItem>
                                                <asp:ListItem>91</asp:ListItem>
                                                <asp:ListItem>92</asp:ListItem>
                                                <asp:ListItem>93</asp:ListItem>
                                                <asp:ListItem>94</asp:ListItem>
                                                <asp:ListItem>95</asp:ListItem>
                                                <asp:ListItem>96</asp:ListItem>
                                                <asp:ListItem>97</asp:ListItem>
                                                <asp:ListItem>98</asp:ListItem>
                                                <asp:ListItem>99</asp:ListItem>
                                            </asp:DropDownList>
										</div>
									</div>
									
									<div class="form-group form-companions">
										<div class="col-sm-8">
											<!--<input type="text" class="form-control input-lg fm-control" />-->
                                            <asp:TextBox ID="txtCompanion4" runat="server" class="form-control input-lg fm-control"></asp:TextBox>
										</div>
										<div class="col-sm-4">
                                            <!--
											<select class="form-control input-lg">
												<option>0</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
											</select>
                                            -->
                                                <asp:DropDownList ID="ddlEdadCompanion4" runat="server" class="form-control input-lg">
                                                    <asp:ListItem>1</asp:ListItem>
                                                    <asp:ListItem>2</asp:ListItem>
                                                    <asp:ListItem>3</asp:ListItem>
                                                    <asp:ListItem>4</asp:ListItem>
                                                    <asp:ListItem>5</asp:ListItem>
                                                    <asp:ListItem>6</asp:ListItem>
                                                    <asp:ListItem>7</asp:ListItem>
                                                    <asp:ListItem>8</asp:ListItem>
                                                    <asp:ListItem>9</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                    <asp:ListItem>24</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                    <asp:ListItem>26</asp:ListItem>
                                                    <asp:ListItem>27</asp:ListItem>
                                                    <asp:ListItem>28</asp:ListItem>
                                                    <asp:ListItem>29</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>31</asp:ListItem>
                                                    <asp:ListItem>32</asp:ListItem>
                                                    <asp:ListItem>33</asp:ListItem>
                                                    <asp:ListItem>34</asp:ListItem>
                                                    <asp:ListItem>35</asp:ListItem>
                                                    <asp:ListItem>36</asp:ListItem>
                                                    <asp:ListItem>37</asp:ListItem>
                                                    <asp:ListItem>38</asp:ListItem>
                                                    <asp:ListItem>39</asp:ListItem>
                                                    <asp:ListItem>40</asp:ListItem>
                                                    <asp:ListItem>41</asp:ListItem>
                                                    <asp:ListItem>42</asp:ListItem>
                                                    <asp:ListItem>43</asp:ListItem>
                                                    <asp:ListItem>44</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                    <asp:ListItem>46</asp:ListItem>
                                                    <asp:ListItem>47</asp:ListItem>
                                                    <asp:ListItem>48</asp:ListItem>
                                                    <asp:ListItem>49</asp:ListItem>
                                                    <asp:ListItem>50</asp:ListItem>
                                                    <asp:ListItem>51</asp:ListItem>
                                                    <asp:ListItem>52</asp:ListItem>
                                                    <asp:ListItem>53</asp:ListItem>
                                                    <asp:ListItem>54</asp:ListItem>
                                                    <asp:ListItem>55</asp:ListItem>
                                                    <asp:ListItem>56</asp:ListItem>
                                                    <asp:ListItem>57</asp:ListItem>
                                                    <asp:ListItem>58</asp:ListItem>
                                                    <asp:ListItem>59</asp:ListItem>
                                                    <asp:ListItem>60</asp:ListItem>
                                                    <asp:ListItem>61</asp:ListItem>
                                                    <asp:ListItem>62</asp:ListItem>
                                                    <asp:ListItem>63</asp:ListItem>
                                                    <asp:ListItem>64</asp:ListItem>
                                                    <asp:ListItem>65</asp:ListItem>
                                                    <asp:ListItem>66</asp:ListItem>
                                                    <asp:ListItem>67</asp:ListItem>
                                                    <asp:ListItem>68</asp:ListItem>
                                                    <asp:ListItem>69</asp:ListItem>
                                                    <asp:ListItem>70</asp:ListItem>
                                                    <asp:ListItem>71</asp:ListItem>
                                                    <asp:ListItem>72</asp:ListItem>
                                                    <asp:ListItem>73</asp:ListItem>
                                                    <asp:ListItem>74</asp:ListItem>
                                                    <asp:ListItem>75</asp:ListItem>
                                                    <asp:ListItem>76</asp:ListItem>
                                                    <asp:ListItem>77</asp:ListItem>
                                                    <asp:ListItem>78</asp:ListItem>
                                                    <asp:ListItem>79</asp:ListItem>
                                                    <asp:ListItem>80</asp:ListItem>
                                                    <asp:ListItem>81</asp:ListItem>
                                                    <asp:ListItem>82</asp:ListItem>
                                                    <asp:ListItem>83</asp:ListItem>
                                                    <asp:ListItem>84</asp:ListItem>
                                                    <asp:ListItem>85</asp:ListItem>
                                                    <asp:ListItem>86</asp:ListItem>
                                                    <asp:ListItem>87</asp:ListItem>
                                                    <asp:ListItem>88</asp:ListItem>
                                                    <asp:ListItem>89</asp:ListItem>
                                                    <asp:ListItem>90</asp:ListItem>
                                                    <asp:ListItem>91</asp:ListItem>
                                                    <asp:ListItem>92</asp:ListItem>
                                                    <asp:ListItem>93</asp:ListItem>
                                                    <asp:ListItem>94</asp:ListItem>
                                                    <asp:ListItem>95</asp:ListItem>
                                                    <asp:ListItem>96</asp:ListItem>
                                                    <asp:ListItem>97</asp:ListItem>
                                                    <asp:ListItem>98</asp:ListItem>
                                                    <asp:ListItem>99</asp:ListItem>
                                                </asp:DropDownList>
										</div>
									</div>
									
									<hr/>
								
								</div>
								
								<!--
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox ID="txtNumeroAcompaniantes" runat="server" placeholder="Numero de acompañantes" class="form-control input-lg fm-control"></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-plus"></i></span>
									</div>
								</div>
								<div class="form-group">
									<asp:DropDownList ID="ddlAcompaniantes" runat="server" class="form-control"></asp:DropDownList>
								</div>									
								<div class="form-group">
									<asp:GridView ID="gvwAcompaniantes" runat="server" AutoGenerateColumns="False" >
								        <Columns>
								            <asp:BoundField HeaderText="Nombres de Acompañantes" />
								            <asp:BoundField HeaderText="Edades" />
								        </Columns>
								    </asp:GridView>
								</div>
								-->
								<div class="form-group text-center">
									 <asp:Button ID="btnRegistro" runat="server" Text="Confirmar" class="btn btn-cons btn-lg" OnClick="btnRegistro_Click"/>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			jQuery("#companions-select select").change(function(){
				var value = jQuery(this).val();
				if (value == 0) {
					jQuery("#companions-items").hide();
				} else {
					jQuery("#companions-items").show();
					jQuery("#companions-items div.form-companions").each(function($i){
						if ($i < value) {
							jQuery(this).show();
						} else {
							jQuery(this).hide();	
						}
					})
				}

			});
			jQuery("#companions-select select").trigger("change");
		</script>
	    
	</body>
</html>
