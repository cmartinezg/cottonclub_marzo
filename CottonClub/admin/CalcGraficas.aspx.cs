﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace CottonClub.admin
{
    public partial class CalcGraficas : System.Web.UI.Page
    {
        string strItemToSelect = "0";
        static int intIdEvento = 0;
        static int intIdDistribuidor = 0;
        static int intcantProductos = 0;
        static int intcantSemillas = 0;
        static int intVeces = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                if (intVeces == 0)
                {
                    if (Request.QueryString.Count > 0)
                    {
                        if (Request.QueryString["eventid"] != null)
                            if (!Request.QueryString["eventid"].ToString().Equals(""))
                            {
                                intIdEvento = Convert.ToInt32(Request.QueryString["eventid"].ToString());
                            }
                        if (Request.QueryString["distribuidorid"] != null)
                            if (!Request.QueryString["distribuidorid"].ToString().Equals(""))
                            {
                                intIdDistribuidor = Convert.ToInt32(Request.QueryString["distribuidorid"].ToString());
                            }
                        if (Request.QueryString["cantProductos"] != null)
                            if (!Request.QueryString["cantProductos"].ToString().Equals(""))
                            {
                                intcantProductos = Convert.ToInt32(Request.QueryString["cantProductos"].ToString());
                            }
                        if (Request.QueryString["cantSemillas"] != null)
                            if (!Request.QueryString["cantSemillas"].ToString().Equals(""))
                            {
                                intcantSemillas = Convert.ToInt32(Request.QueryString["cantSemillas"].ToString());
                            }

                    }
                    else
                    {
                        intIdEvento = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
                    }
                }
            }
        }

        [WebMethod]
        public static string GraficaDetalleProdcutos()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramCantidad = new SqlParameter();
            paramCantidad.ParameterName = "@cantProductos";
            paramCantidad.SqlDbType = SqlDbType.Int;
            paramCantidad.Value = intcantProductos;
            pArray.Add(paramCantidad);

            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@IdEvento";
            paramEvento.SqlDbType = SqlDbType.BigInt;
                paramEvento.Value = intIdEvento;
                //paramEvento.Value = 1;
            pArray.Add(paramEvento);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaDetalleProductos", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaDetalleSemillas()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramCantidad = new SqlParameter();
            paramCantidad.ParameterName = "@cantSemillas";
            paramCantidad.SqlDbType = SqlDbType.Int;
            paramCantidad.Value = intcantSemillas;
            pArray.Add(paramCantidad);

            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@IdEvento";
            paramEvento.SqlDbType = SqlDbType.BigInt;
            paramEvento.Value = intIdEvento;
            //paramEvento.Value = 1;
            pArray.Add(paramEvento);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaDetalleSemillas", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaProdcutos()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@IdEvento";
            paramEvento.SqlDbType = SqlDbType.BigInt;
                paramEvento.Value = intIdEvento;
                //paramEvento.Value = 1;
            pArray.Add(paramEvento);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaProductos", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaSemillas()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@IdEvento";
            paramEvento.SqlDbType = SqlDbType.BigInt;
            paramEvento.Value = intIdEvento;
            //paramEvento.Value = 1;
            pArray.Add(paramEvento);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaSemillas", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaDistribuior()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@IdEvento";
            paramEvento.SqlDbType = SqlDbType.BigInt;
                paramEvento.Value = intIdEvento;
                //paramEvento.Value = 1;
            pArray.Add(paramEvento);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaDistribuidor", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaProducto()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@eventid";
            paramEvento.SqlDbType = SqlDbType.BigInt;
            paramEvento.Value = intIdEvento;
            pArray.Add(paramEvento);

            SqlParameter paramDistribuidor = new SqlParameter();
            paramDistribuidor.ParameterName = "@distribuidorid";
            paramDistribuidor.SqlDbType = SqlDbType.BigInt;
            paramDistribuidor.Value = intIdDistribuidor;
            pArray.Add(paramDistribuidor);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaProducto", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        [WebMethod]
        public static string GraficaSemilla()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter paramEvento = new SqlParameter();
            paramEvento.ParameterName = "@eventid";
            paramEvento.SqlDbType = SqlDbType.BigInt;
            paramEvento.Value = intIdEvento;
            pArray.Add(paramEvento);

            SqlParameter paramDistribuidor = new SqlParameter();
            paramDistribuidor.ParameterName = "@distribuidorid";
            paramDistribuidor.SqlDbType = SqlDbType.BigInt;
            paramDistribuidor.Value = intIdDistribuidor;
            pArray.Add(paramDistribuidor);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcGraficaSemilla", pArray);
            objCnn.Close();

            string json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.None);
            return json;
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                //ddlEvento.Items.Insert(0, "Seleccione...");

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}