﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub.admin
{
    public partial class CalcIncidencesDetail : System.Web.UI.Page
    {
        const string ID_SESION = "IdIncidencia";
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdAccion = 0;
            int Id = 0;
            //lblMensaje.Visible = false;
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (!IsPostBack)
            {
                FillProductos();
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                Id = Convert.ToInt32(Request.QueryString["id"]);
                Session[ID_SESION] = Id;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    btnGuardar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectData(Id);
                }

            }

        }

        protected void FillProductos()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@frase";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectCalcProductos", pArray);
            if (ds.Tables[0] != null)
            {
                lstProductos.DataSource = ds.Tables[0].DefaultView;
                lstProductos.DataTextField = "Nombre";
                lstProductos.DataValueField = "Id";
                lstProductos.DataBind();
            }
        }

        protected void LimpiaCampos()
        {
            txtNombre.Text = "";
            lstProductos.ClearSelection();
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
        }
        protected void SelectData(int Id)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@Id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = Id;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificIncidencia", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombre.Text = ds.Tables[0].Rows[0]["Nombre"].ToString();
                string ids = "";
                if (ds.Tables[0].Rows[0]["Productos"] != null)
                    ids = ds.Tables[0].Rows[0]["Productos"].ToString();

                foreach (string id in ids.Split(','))
                {
                    foreach (ListItem item in lstProductos.Items)
                    {
                        if (item.Value == id)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
            }
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();

                ArrayList pArray = ReadParams();

                objData.ExecuteInsertSP("usp_InsertIncidenciaProducto", pArray);
                Response.Redirect("CalcIncidences.aspx?notificacion=1");
            }
        }
        private ArrayList ReadParams()
        {
            ArrayList pArray = new ArrayList();

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@Nombre";
            param2.SqlDbType = SqlDbType.NVarChar;
            param2.Value = txtNombre.Text;
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@Productos";
            param3.SqlDbType = SqlDbType.VarChar;
            param3.Value = SelectedData(lstProductos);
            pArray.Add(param3);

            return pArray;
        }

        private string SelectedData(ListBox lb)
        {
            string result = "";
            foreach (ListItem item in lb.Items)
            {
                if(item.Selected)
                    result += item.Value + ",";
            }
            if (result.Length > 0)
                result.Substring(0, result.Length - 1);

            return result;
        }
        private bool PageIsValid()
        {

            if (txtNombre.Text == "")
            {
                lblMensaje.Text = "El Nombre del regalo es un dato requerido";
                return false;
            }

            if (string.IsNullOrWhiteSpace(SelectedData(lstProductos)))
            {
                lblMensaje.Text = "Debe seleccionar al menos un producto";
                return false;
            }

            lblMensaje.Visible = true;
            return true;
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = ReadParams();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session[ID_SESION];
                pArray.Add(ParamId);

                objData.ExecuteInsertSP("usp_UpdateIncidenciaProducto", pArray);
                Response.Redirect("CalcIncidences.aspx?notificacion=1");
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CalcIncidences.aspx");
        }
        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session[ID_SESION];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteIncidenciaProducto", pArray);
            Response.Redirect("CalcIncidences.aspx?notificacion=1");
        }
    }
}