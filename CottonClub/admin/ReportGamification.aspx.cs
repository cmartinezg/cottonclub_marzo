﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Net;

namespace CottonClub.admin
{
    public partial class ReportGamification : System.Web.UI.Page
    {
        string json = "";
        string strItemToSelect = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
                    SelectGamificacion();
                    gvwGamificacion.Visible = true;
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                //ddlEvento.Items.Insert(0, "Seleccione...");

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void SelectGamificacion()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.BigInt;
            int parametro;
            if(!IsPostBack)
            { 
                parametro = Convert.ToInt32(strItemToSelect);
            }
            else
            {
                parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            } 
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_ReportGamification", pArray);
            if (ds.Tables[0] != null)
            {
                //gvwUsuarios.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwGamificacion.DataSource = ds;
                gvwGamificacion.DataBind();
            }

            json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
            strJson.InnerHtml = json;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            //ExportGridToExcel();
            ExportToCSV_Gamificacion();
        }

        private void ExportGridToExcel()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page page = new Page();
            HtmlForm form = new HtmlForm();
            gvwGamificacion.EnableViewState = false;

            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(gvwGamificacion);
            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Gamificacion_" + DateTime.Now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        }

        public void ExportToCSV_Gamificacion()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Gamificacion_" + DateTime.Now + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            gvwGamificacion.AllowPaging = false;
            gvwGamificacion.DataBind();
            gvwGamificacion.EnableViewState = false;
            for (int index = 0; index < gvwGamificacion.Columns.Count; index++)
            {
                sBuilder.Append(WebUtility.HtmlDecode(gvwGamificacion.Columns[index].HeaderText) + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < gvwGamificacion.Rows.Count; i++)
            {
                for (int k = 0; k < gvwGamificacion.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(WebUtility.HtmlDecode(gvwGamificacion.Rows[i].Cells[k].Text).Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }

        protected void gvwGamificacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwGamificacion.PageIndex = e.NewPageIndex;
            SelectGamificacion();
        }
    }
}