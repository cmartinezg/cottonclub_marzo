﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="ReportProducts.aspx.cs" Inherits="CottonClub.admin.ReportProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
			    
			    <div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-6">
			    		<h3 class="section-header">Reporte Productos</h3>
				    </div>
				    <div class="col-xs-12 col-sm-6 col-md-6 text-right">
			    		<asp:DropDownList ID="ddlEvento" runat="server" class="select-event" AutoPostBack="True"></asp:DropDownList>
				    </div>
			    </div>

			    <div class="row">
				    
				    <div class="col-xs-12">
				    
					    <ul class="nav nav-tabs" role="tablist">
						  	<li class="active"><a href="#tab-products" aria-controls="tab-products" role="tab" data-toggle="tab">Productos</a></li>
						  	<li><a href="#tab-seeds" aria-controls="tab-seeds" role="tab" data-toggle="tab">Semillas</a></li>
						</ul>
			    		<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-products">			                      		                  
						        <div class="row table-responsive">
							        <asp:GridView class="table no-more-tables table-listing" ID="gvwProductos" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="gvwProductos_PageIndexChanging" AutoGenerateColumns="False">
								        <AlternatingRowStyle BackColor="White" />
								        <Columns>
                                            <asp:BoundField DataField="Folio" HeaderText="Folio" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                            <asp:BoundField DataField="Distribuidor" HeaderText="Distribuidor" />
                                            <asp:BoundField DataField="Producto" HeaderText="Producto" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" />
                                            <asp:BoundField DataField="FechaHr" HeaderText="FechaHr" />
                                            <asp:BoundField DataField="cotizacion" HeaderText="cotizacion" />
                                        </Columns>
								        <EditRowStyle BackColor="#2461BF" />
								        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
								        <RowStyle BackColor="#EFF3FB" />
								        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
								        <SortedAscendingCellStyle BackColor="#F5F7FB" />
								        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
								        <SortedDescendingCellStyle BackColor="#E9EBEF" />
								        <SortedDescendingHeaderStyle BackColor="#4870BE" />
							        </asp:GridView>
						        </div>
                                <div class="row text-right">
			                        <div class="col-xs-12">
						                <asp:Button ID="btnExportar" runat="server" CssClass="btn btn-success" OnClick="btnExportar_Click" Text="Exportar" />
			                        </div>
		                        </div>
                    	    </div>
						    <div role="tabpanel" class="tab-pane" id="tab-seeds">			                      		                  
						        <div class="row table-responsive">
							        <asp:GridView class="table no-more-tables table-listing" ID="gvwSeeds" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="gvwSeeds_PageIndexChanging" AutoGenerateColumns="False" >
								        <AlternatingRowStyle BackColor="White" />
								        <Columns>
                                            <asp:BoundField DataField="Folio" HeaderText="Folio" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                            <asp:BoundField DataField="Distribuidor" HeaderText="Distribuidor" />
                                            <asp:BoundField DataField="Variedad" HeaderText="Variedad" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" />
                                            <asp:BoundField DataField="FechaHr" HeaderText="FechaHr" />
                                            <asp:BoundField DataField="cotizacion" HeaderText="Cotizacion" />
                                        </Columns>
								        <EditRowStyle BackColor="#2461BF" />
								        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
								        <RowStyle BackColor="#EFF3FB" />
								        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
								        <SortedAscendingCellStyle BackColor="#F5F7FB" />
								        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
								        <SortedDescendingCellStyle BackColor="#E9EBEF" />
								        <SortedDescendingHeaderStyle BackColor="#4870BE" />
							        </asp:GridView>
						        </div>
                                    <div class="row text-right">
			                            <div class="col-xs-12">
						                    <asp:Button ID="btnExportSeeds" runat="server" CssClass="btn btn-success" Text="Exportar" OnClick="btnExportSeeds_Click" />
			                            </div>
		                            </div>
                    	        </div>          
		        </div>
	    	</div>
		</div>
	</div>
    		</div>
	</div>
</asp:Content>



