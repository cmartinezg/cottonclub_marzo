﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Ranking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
            SelectRanking();
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    string strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void SelectRanking()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.Int;
            int parametro = 0;
            parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            ParamEvento.Value = parametro; ;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_Ranking", pArray);
            if (ds.Tables[0] != null)
            {
                gvwRanking.DataSource = ds;
                gvwRanking.DataBind();
            }
        }


        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRanking();
        }

        protected void gvwRanking_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwRanking.PageIndex = e.NewPageIndex;
            SelectRanking();
        }
    }
}