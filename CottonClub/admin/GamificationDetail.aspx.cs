﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;
using Gma.QrCodeNet;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.IO;
using System.Drawing;
using System.ServiceModel;
using System.Net;
using System.Drawing.Imaging;

namespace CottonClub
{
    public partial class GamificacionDetalle : System.Web.UI.Page
    {
        int IdAccion = 0;
        int IdGamificacion = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {

                FillEvento();
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdGamificacion = Convert.ToInt32(Request.QueryString["id"]);
                Session["IdGamificacion"] = IdGamificacion;
                Session["GUIDGamificacion"] = "";
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    ActivarCampos();
                    btnCancelar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectGamificacion(IdGamificacion);
                    GeneraCodigoQR();
                    imgQR.ImageUrl = "codigoQR/qrcode_" + Session["IdGamificacion"].ToString()+".png";
                    lnkCodigoQR.Target = "_blank";
                    //lnkCodigoQR.NavigateUrl = "codigoQR/qrcode_" + Session["IdGamificacion"].ToString()+".png";
                    lnkCodigoQR.NavigateUrl = "QRCode.aspx";
                }

            }
            
        }

        protected void LimpiaCampos()
        {
            txtPregunta.ReadOnly = false;
            txtPuntos.ReadOnly = false;
            txtPuntosFallo.ReadOnly = false;
            txtRespuestaOpcion1.ReadOnly = false;
            txtRespuestaOpcion2.ReadOnly = false;
            txtRespuestaOpcion3.ReadOnly = false;
            //Pregunta2
            txtPregunta2.ReadOnly = false;
            txtRespuestaOpcion2_1.ReadOnly = false;
            txtRespuestaOpcion2_2.ReadOnly = false;
            txtRespuestaOpcion2_3.ReadOnly = false;
            //Pregunta3
            txtPregunta3.ReadOnly = false;
            txtRespuestaOpcion3_1.ReadOnly = false;
            txtRespuestaOpcion3_2.ReadOnly = false;
            txtRespuestaOpcion3_3.ReadOnly = false;

            cbActivo.Enabled = true;
            ddlEvento.Enabled = true;
            ddlRespuestaCorrecta.Enabled = true;
            ddlRespuestaCorrecta2.Enabled = true;
            ddlRespuestaCorrecta3.Enabled = true;
            txtPregunta.Text = "";
            txtPuntos.Text = "";
            txtPuntosFallo.Text = "";
            txtRespuestaOpcion1.Text = "";
            txtRespuestaOpcion2.Text = "";
            txtRespuestaOpcion3.Text = "";
            //Pregunta2
            txtPregunta2.Text = "";
            txtRespuestaOpcion2_1.Text = "";
            txtRespuestaOpcion2_2.Text = "";
            txtRespuestaOpcion2_3.Text = "";
            //Pregunta3
            txtPregunta3.Text = "";
            txtRespuestaOpcion3_1.Text = "";
            txtRespuestaOpcion3_2.Text = "";
            txtRespuestaOpcion3_3.Text = "";

            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }
        protected string SelectEventos(int IdEvento)
        {
            string Evento = "";
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdEvento;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificEvents", pArray);
            if (ds.Tables[0] != null)
            {
                Evento= ds.Tables[0].Rows[0]["Name"].ToString();
            }
            return Evento;
        }
        protected void SelectGamificacion(int IdGamificacion)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdGamificacion;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificGamification", pArray);
            if (ds.Tables[0] != null)
            {
                txtPuntos.Text = ds.Tables[0].Rows[0]["Puntos"].ToString();
                txtPregunta.Text = ds.Tables[0].Rows[0]["Pregunta"].ToString();
                txtRespuestaOpcion1.Text = ds.Tables[0].Rows[0]["Respuesta1"].ToString();
                txtRespuestaOpcion2.Text = ds.Tables[0].Rows[0]["Respuesta2"].ToString();
                txtRespuestaOpcion3.Text = ds.Tables[0].Rows[0]["Respuesta3"].ToString();

                
                ddlEvento.SelectedValue= ds.Tables[0].Rows[0]["IdEvento"].ToString();
                ddlRespuestaCorrecta.SelectedValue = ds.Tables[0].Rows[0]["RespuestaCorrecta"].ToString();
                //Pregunta2
                txtPregunta2.Text = ds.Tables[0].Rows[0]["Pregunta2"].ToString();
                txtRespuestaOpcion2_1.Text = ds.Tables[0].Rows[0]["Respuesta2_1"].ToString();
                txtRespuestaOpcion2_2.Text = ds.Tables[0].Rows[0]["Respuesta2_2"].ToString();
                txtRespuestaOpcion2_3.Text = ds.Tables[0].Rows[0]["Respuesta2_3"].ToString();
                ddlRespuestaCorrecta2.SelectedValue = ds.Tables[0].Rows[0]["RespuestaCorrecta2"].ToString();

                //Pregunta3
                txtPregunta3.Text = ds.Tables[0].Rows[0]["Pregunta3"].ToString();
                txtRespuestaOpcion3_1.Text = ds.Tables[0].Rows[0]["Respuesta3_1"].ToString();
                txtRespuestaOpcion3_2.Text = ds.Tables[0].Rows[0]["Respuesta3_2"].ToString();
                txtRespuestaOpcion3_3.Text = ds.Tables[0].Rows[0]["Respuesta3_3"].ToString();
                ddlRespuestaCorrecta3.SelectedValue = ds.Tables[0].Rows[0]["RespuestaCorrecta3"].ToString();

                txtPuntos.Text = ds.Tables[0].Rows[0]["Puntos"].ToString();
                txtPuntosFallo.Text = ds.Tables[0].Rows[0]["PuntosFallo"].ToString();
                txtPuntosFallo3er.Text = ds.Tables[0].Rows[0]["PuntosFallo3er"].ToString();
                if (ds.Tables[0].Rows[0]["Activo"].ToString().Equals("1"))
                {
                    cbActivo.Checked = true;
                }
                else
                {
                    cbActivo.Checked = false;
                }
                Session["GUIDGamificacion"]=ds.Tables[0].Rows[0]["GUID"].ToString();
            }
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField= "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();
            }
        }
        protected void btnEditar_Click(object sender, EventArgs e)
        {
            ActivarCampos();
        }

        private bool PageIsValid()
        {
            int intContador = 0;
            lblMensaje.Text = "";
            if (txtPregunta.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Por favor escriba una pregunta en el campo de Pregunta 1";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion1.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 1 para la pregunta 1";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion2.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 2 para la pregunta 1";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion3.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 3 para la pregunta 1";
                intContador = intContador + 1;
                //return false;
            }

            if (txtPregunta2.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Por favor escriba una pregunta en el campo de Pregunta 2";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion2_1.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 1 para la pregunta 2";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion2_2.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 2 para la pregunta 2";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion2_3.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 3 para la pregunta 2";
                intContador = intContador + 1;
                //return false;
            }

            if (txtPregunta3.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Por favor escriba una pregunta en el campo de Pregunta 3";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion3_1.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 1 para la pregunta 3";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion3_2.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 2 para la pregunta 3";
                intContador = intContador + 1;
                //return false;
            }

            if (txtRespuestaOpcion3_3.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Escriba la opción de respuesta 3 para la pregunta 3";
                intContador = intContador + 1;
                //return false;
            }

            if (txtPuntos.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El campo Puntos es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }
            else
            {
                decimal temp;
                if (!decimal.TryParse(txtPuntos.Text, out temp))
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El campo Puntos no tiene el formato correcto, debe ser un dato numérico con decimales.";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            if (txtPuntosFallo.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El campo Puntos Fallo es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }

            if (txtPuntosFallo3er.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El campo Puntos Fallo del 3er intento es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }
            else
            {
                decimal temp;
                if (!decimal.TryParse(txtPuntosFallo.Text, out temp))
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El campo Puntos Fallo no tiene el formato correcto, debe ser un dato numérico con decimales.";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            if (intContador > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamPregunta = new SqlParameter();
                ParamPregunta.ParameterName = "@Pregunta";
                ParamPregunta.SqlDbType = SqlDbType.Text;
                ParamPregunta.Value = txtPregunta.Text;
                pArray.Add(ParamPregunta);
                SqlParameter ParamRespuesta1 = new SqlParameter();
                ParamRespuesta1.ParameterName = "@Respuesta1";
                ParamRespuesta1.SqlDbType = SqlDbType.Text;
                ParamRespuesta1.Value = txtRespuestaOpcion1.Text;
                pArray.Add(ParamRespuesta1);
                SqlParameter ParameterRespuesta2 = new SqlParameter();
                ParameterRespuesta2.ParameterName = "@Respuesta2";
                ParameterRespuesta2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta2.Value = txtRespuestaOpcion2.Text;
                pArray.Add(ParameterRespuesta2);
                SqlParameter ParamRespuesta3 = new SqlParameter();
                ParamRespuesta3.ParameterName = "@Respuesta3";
                ParamRespuesta3.SqlDbType = SqlDbType.Text;
                ParamRespuesta3.Value = txtRespuestaOpcion3.Text;
                pArray.Add(ParamRespuesta3);
                SqlParameter ParamRespuestaCorrecta = new SqlParameter();
                ParamRespuestaCorrecta.ParameterName = "@RespuestaCorrecta";
                ParamRespuestaCorrecta.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta.Value = Convert.ToInt32(ddlRespuestaCorrecta.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta);

                //Pregunta 2

                SqlParameter ParamPregunta2 = new SqlParameter();
                ParamPregunta2.ParameterName = "@Pregunta2";
                ParamPregunta2.SqlDbType = SqlDbType.Text;
                ParamPregunta2.Value = txtPregunta2.Text;
                pArray.Add(ParamPregunta2);
                SqlParameter ParamRespuesta2_1 = new SqlParameter();
                ParamRespuesta2_1.ParameterName = "@Respuesta2_1";
                ParamRespuesta2_1.SqlDbType = SqlDbType.Text;
                ParamRespuesta2_1.Value = txtRespuestaOpcion2_1.Text;
                pArray.Add(ParamRespuesta2_1);
                SqlParameter ParameterRespuesta2_2 = new SqlParameter();
                ParameterRespuesta2_2.ParameterName = "@Respuesta2_2";
                ParameterRespuesta2_2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta2_2.Value = txtRespuestaOpcion2_2.Text;
                pArray.Add(ParameterRespuesta2_2);
                SqlParameter ParamRespuesta2_3 = new SqlParameter();
                ParamRespuesta2_3.ParameterName = "@Respuesta2_3";
                ParamRespuesta2_3.SqlDbType = SqlDbType.Text;
                ParamRespuesta2_3.Value = txtRespuestaOpcion2_3.Text;
                pArray.Add(ParamRespuesta2_3);
                SqlParameter ParamRespuestaCorrecta2 = new SqlParameter();
                ParamRespuestaCorrecta2.ParameterName = "@RespuestaCorrecta2";
                ParamRespuestaCorrecta2.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta2.Value = Convert.ToInt32(ddlRespuestaCorrecta2.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta2);

                //Pregunta 3

                SqlParameter ParamPregunta3 = new SqlParameter();
                ParamPregunta3.ParameterName = "@Pregunta3";
                ParamPregunta3.SqlDbType = SqlDbType.Text;
                ParamPregunta3.Value = txtPregunta3.Text;
                pArray.Add(ParamPregunta3);
                SqlParameter ParamRespuesta3_1 = new SqlParameter();
                ParamRespuesta3_1.ParameterName = "@Respuesta3_1";
                ParamRespuesta3_1.SqlDbType = SqlDbType.Text;
                ParamRespuesta3_1.Value = txtRespuestaOpcion3_1.Text;
                pArray.Add(ParamRespuesta3_1);
                SqlParameter ParameterRespuesta3_2 = new SqlParameter();
                ParameterRespuesta3_2.ParameterName = "@Respuesta3_2";
                ParameterRespuesta3_2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta3_2.Value = txtRespuestaOpcion3_2.Text;
                pArray.Add(ParameterRespuesta3_2);
                SqlParameter ParamRespuesta3_3 = new SqlParameter();
                ParamRespuesta3_3.ParameterName = "@Respuesta3_3";
                ParamRespuesta3_3.SqlDbType = SqlDbType.Text;
                ParamRespuesta3_3.Value = txtRespuestaOpcion3_3.Text;
                pArray.Add(ParamRespuesta3_3);
                SqlParameter ParamRespuestaCorrecta3 = new SqlParameter();
                ParamRespuestaCorrecta3.ParameterName = "@RespuestaCorrecta3";
                ParamRespuestaCorrecta3.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta3.Value = Convert.ToInt32(ddlRespuestaCorrecta3.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta3);

                SqlParameter ParamPuntos = new SqlParameter();
                ParamPuntos.ParameterName = "@Puntos";
                ParamPuntos.SqlDbType = SqlDbType.Decimal;
                ParamPuntos.Value = txtPuntos.Text;
                pArray.Add(ParamPuntos);
                SqlParameter ParamPuntosFallo = new SqlParameter();
                ParamPuntosFallo.ParameterName = "@PuntosFallo";
                ParamPuntosFallo.SqlDbType = SqlDbType.Decimal;
                ParamPuntosFallo.Value = txtPuntosFallo.Text;
                pArray.Add(ParamPuntosFallo);

                SqlParameter ParamPuntosFallo3er = new SqlParameter();
                ParamPuntosFallo3er.ParameterName = "@PuntosFallo3er";
                ParamPuntosFallo3er.SqlDbType = SqlDbType.Decimal;
                ParamPuntosFallo3er.Value = txtPuntosFallo3er.Text;
                pArray.Add(ParamPuntosFallo3er);

                SqlParameter ParamIdEvento = new SqlParameter();
                ParamIdEvento.ParameterName = "@IdEvento";
                ParamIdEvento.SqlDbType = SqlDbType.BigInt;
                ParamIdEvento.Value = Convert.ToInt32(ddlEvento.SelectedValue);
                pArray.Add(ParamIdEvento);
                SqlParameter ParamUsuarioModificacion = new SqlParameter();
                ParamUsuarioModificacion.ParameterName = "@UsuarioModificacion";
                ParamUsuarioModificacion.SqlDbType = SqlDbType.NVarChar;
                ParamUsuarioModificacion.Value = Session["Username"];
                pArray.Add(ParamUsuarioModificacion);


                objData.ExecuteInsertSP("usp_InsertGamification", pArray);
                Response.Redirect("Gamification.aspx?notificacion=1");
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session["IdGamificacion"];
                pArray.Add(ParamId);
                SqlParameter ParamPregunta = new SqlParameter();
                ParamPregunta.ParameterName = "@Pregunta";
                ParamPregunta.SqlDbType = SqlDbType.Text;
                ParamPregunta.Value = txtPregunta.Text;
                pArray.Add(ParamPregunta);
                SqlParameter ParamRespuesta1 = new SqlParameter();
                ParamRespuesta1.ParameterName = "@Respuesta1";
                ParamRespuesta1.SqlDbType = SqlDbType.Text;
                ParamRespuesta1.Value = txtRespuestaOpcion1.Text;
                pArray.Add(ParamRespuesta1);
                SqlParameter ParameterRespuesta2 = new SqlParameter();
                ParameterRespuesta2.ParameterName = "@Respuesta2";
                ParameterRespuesta2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta2.Value = txtRespuestaOpcion2.Text;
                pArray.Add(ParameterRespuesta2);
                SqlParameter ParamRespuesta3 = new SqlParameter();
                ParamRespuesta3.ParameterName = "@Respuesta3";
                ParamRespuesta3.SqlDbType = SqlDbType.Text;
                ParamRespuesta3.Value = txtRespuestaOpcion3.Text;
                pArray.Add(ParamRespuesta3);
                SqlParameter ParamRespuestaCorrecta = new SqlParameter();
                ParamRespuestaCorrecta.ParameterName = "@RespuestaCorrecta";
                ParamRespuestaCorrecta.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta.Value = Convert.ToInt32(ddlRespuestaCorrecta.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta);

                //Pregunta 2

                SqlParameter ParamPregunta2 = new SqlParameter();
                ParamPregunta2.ParameterName = "@Pregunta2";
                ParamPregunta2.SqlDbType = SqlDbType.Text;
                ParamPregunta2.Value = txtPregunta2.Text;
                pArray.Add(ParamPregunta2);
                SqlParameter ParamRespuesta2_1 = new SqlParameter();
                ParamRespuesta2_1.ParameterName = "@Respuesta2_1";
                ParamRespuesta2_1.SqlDbType = SqlDbType.Text;
                ParamRespuesta2_1.Value = txtRespuestaOpcion2_1.Text;
                pArray.Add(ParamRespuesta2_1);
                SqlParameter ParameterRespuesta2_2 = new SqlParameter();
                ParameterRespuesta2_2.ParameterName = "@Respuesta2_2";
                ParameterRespuesta2_2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta2_2.Value = txtRespuestaOpcion2_2.Text;
                pArray.Add(ParameterRespuesta2_2);
                SqlParameter ParamRespuesta2_3 = new SqlParameter();
                ParamRespuesta2_3.ParameterName = "@Respuesta2_3";
                ParamRespuesta2_3.SqlDbType = SqlDbType.Text;
                ParamRespuesta2_3.Value = txtRespuestaOpcion2_3.Text;
                pArray.Add(ParamRespuesta2_3);
                SqlParameter ParamRespuestaCorrecta2 = new SqlParameter();
                ParamRespuestaCorrecta2.ParameterName = "@RespuestaCorrecta2";
                ParamRespuestaCorrecta2.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta2.Value = Convert.ToInt32(ddlRespuestaCorrecta2.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta2);

                //Pregunta 3

                SqlParameter ParamPregunta3 = new SqlParameter();
                ParamPregunta3.ParameterName = "@Pregunta3";
                ParamPregunta3.SqlDbType = SqlDbType.Text;
                ParamPregunta3.Value = txtPregunta3.Text;
                pArray.Add(ParamPregunta3);
                SqlParameter ParamRespuesta3_1 = new SqlParameter();
                ParamRespuesta3_1.ParameterName = "@Respuesta3_1";
                ParamRespuesta3_1.SqlDbType = SqlDbType.Text;
                ParamRespuesta3_1.Value = txtRespuestaOpcion3_1.Text;
                pArray.Add(ParamRespuesta3_1);
                SqlParameter ParameterRespuesta3_2 = new SqlParameter();
                ParameterRespuesta3_2.ParameterName = "@Respuesta3_2";
                ParameterRespuesta3_2.SqlDbType = SqlDbType.Text;
                ParameterRespuesta3_2.Value = txtRespuestaOpcion3_2.Text;
                pArray.Add(ParameterRespuesta3_2);
                SqlParameter ParamRespuesta3_3 = new SqlParameter();
                ParamRespuesta3_3.ParameterName = "@Respuesta3_3";
                ParamRespuesta3_3.SqlDbType = SqlDbType.Text;
                ParamRespuesta3_3.Value = txtRespuestaOpcion3_3.Text;
                pArray.Add(ParamRespuesta3_3);
                SqlParameter ParamRespuestaCorrecta3 = new SqlParameter();
                ParamRespuestaCorrecta3.ParameterName = "@RespuestaCorrecta3";
                ParamRespuestaCorrecta3.SqlDbType = SqlDbType.Int;
                ParamRespuestaCorrecta3.Value = Convert.ToInt32(ddlRespuestaCorrecta3.SelectedValue);
                pArray.Add(ParamRespuestaCorrecta3);

                SqlParameter ParamPuntos = new SqlParameter();
                ParamPuntos.ParameterName = "@Puntos";
                ParamPuntos.SqlDbType = SqlDbType.Decimal;
                ParamPuntos.Value = txtPuntos.Text;
                pArray.Add(ParamPuntos);
                SqlParameter ParamPuntosFallo = new SqlParameter();
                ParamPuntosFallo.ParameterName = "@PuntosFallo";
                ParamPuntosFallo.SqlDbType = SqlDbType.Decimal;
                ParamPuntosFallo.Value = txtPuntosFallo.Text;
                pArray.Add(ParamPuntosFallo);

                SqlParameter ParamPuntosFallo3er = new SqlParameter();
                ParamPuntosFallo3er.ParameterName = "@PuntosFallo3er";
                ParamPuntosFallo3er.SqlDbType = SqlDbType.Decimal;
                ParamPuntosFallo3er.Value = txtPuntosFallo3er.Text;
                pArray.Add(ParamPuntosFallo3er);

                SqlParameter ParamIdEvento = new SqlParameter();
                ParamIdEvento.ParameterName = "@IdEvento";
                ParamIdEvento.SqlDbType = SqlDbType.BigInt;
                ParamIdEvento.Value = Convert.ToInt32(ddlEvento.SelectedValue);
                pArray.Add(ParamIdEvento);
                SqlParameter ParamUsuarioModificacion = new SqlParameter();
                ParamUsuarioModificacion.ParameterName = "@UsuarioModificacion";
                ParamUsuarioModificacion.SqlDbType = SqlDbType.NVarChar;
                ParamUsuarioModificacion.Value = Session["Username"];
                pArray.Add(ParamUsuarioModificacion);
                SqlParameter ParamActivo = new SqlParameter();
                ParamActivo.ParameterName = "@Activo";
                ParamActivo.SqlDbType = SqlDbType.Int;
                int Activo = 0;
                if (cbActivo.Checked == true)
                {
                    Activo = 1;
                }
                ParamActivo.Value = Activo;
                pArray.Add(ParamActivo);
                objData.ExecuteInsertSP("usp_UpdateGamification", pArray);
                Response.Redirect("Gamification.aspx?notificacion=1");
            }
            /*
            txtPregunta.ReadOnly = true;
            txtPregunta2.ReadOnly = true;
            txtPregunta3.ReadOnly = true;
            txtPuntos.ReadOnly = true;
            txtPuntosFallo.ReadOnly = true;
            txtRespuestaOpcion1.ReadOnly = true;
            txtRespuestaOpcion2.ReadOnly = true;
            txtRespuestaOpcion3.ReadOnly = true;
            txtRespuestaOpcion2_1.ReadOnly = true;
            txtRespuestaOpcion2_2.ReadOnly = true;
            txtRespuestaOpcion2_3.ReadOnly = true;
            txtRespuestaOpcion3_1.ReadOnly = true;
            txtRespuestaOpcion3_2.ReadOnly = true;
            txtRespuestaOpcion3_3.ReadOnly = true;
            ddlEvento.Enabled = false;
            ddlRespuestaCorrecta.Enabled = false;
            ddlRespuestaCorrecta2.Enabled = false;
            ddlRespuestaCorrecta3.Enabled = false;
            cbActivo.Enabled = false;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEditar.Visible = true;
            SelectGamificacion(Convert.ToInt32(Session["IdGamificacion"]));
            */
        }

        protected void btnGuardarActualizacion_Click(object sender, EventArgs e)
        {

        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@id";

            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdGamificacion"];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteGamificacion", pArray);
            Response.Redirect("Gamification.aspx?notificacion=1");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Gamification.aspx");
        }

        protected void ActivarCampos()
        {
            txtPuntos.ReadOnly = false;
            txtPuntosFallo.ReadOnly = false;
            txtPuntosFallo3er.ReadOnly = false;
            txtPregunta.ReadOnly = false;
            txtRespuestaOpcion1.ReadOnly = false;
            txtRespuestaOpcion2.ReadOnly = false;
            txtRespuestaOpcion3.ReadOnly = false;
            txtPregunta2.ReadOnly = false;
            txtRespuestaOpcion2_1.ReadOnly = false;
            txtRespuestaOpcion2_2.ReadOnly = false;
            txtRespuestaOpcion2_3.ReadOnly = false;
            //Pregunta3
            txtPregunta3.ReadOnly = false;
            txtRespuestaOpcion3_1.ReadOnly = false;
            txtRespuestaOpcion3_2.ReadOnly = false;
            txtRespuestaOpcion3_3.ReadOnly = false;

            ddlEvento.Enabled = true;
            ddlRespuestaCorrecta.Enabled = true;
            ddlRespuestaCorrecta2.Enabled = true;
            ddlRespuestaCorrecta3.Enabled = true;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        [OperationBehavior(Impersonation = ImpersonationOption.Allowed)]
        protected void GeneraCodigoQR()
        {
            try
            {
                var qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
                var qrCode = qrEncoder.Encode(Session["GUIDGamificacion"].ToString());
                var renderer = new GraphicsRenderer(new FixedModuleSize(50, QuietZoneModules.Two), Brushes.Black, Brushes.White);
                using (var stream = new FileStream(Server.MapPath(@"codigoQR/qrcode_"+ Session["IdGamificacion"].ToString() + ".png"), FileMode.Create))
                //using (var stream = new FileStream("codigoQR/qrcode_" + Session["IdGamificacion"].ToString() + ".png", FileMode.Create))
                    renderer.WriteToStream(qrCode.Matrix, System.Drawing.Imaging.ImageFormat.Png, stream);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}