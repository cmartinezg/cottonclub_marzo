﻿<%@ Page Title="Momentos Bayer | Usuarios Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="UserDetail.aspx.cs" Inherits="CottonClub.UserDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-header">Usuarios Detalle</h3>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-horizontal">

                            <div class="form-group">
								<div class="col-sm-10">
									<asp:Label ID="lblMensaje" runat="server" ForeColor ="Red"></asp:Label>
								</div>
							</div>

							<div class="form-group">
								<label for="txtNombres" class="col-sm-2 control-label">Nombre</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtNombres" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label for="txtApellidos" class="col-sm-2 control-label">Apellido</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtApellidos" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label for="txtEmail" class="col-sm-2 control-label">Correo</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtEmail" runat="server" ReadOnly="True"></asp:TextBox>
                                    <asp:RegularExpressionValidator ControlToValidate="txtEmail" ID="validatorEmail" ForeColor="Red" runat="server" Display="Dynamic" Text="El correo electrónico debe tener un formato válido" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$">
                                    </asp:RegularExpressionValidator>
								</div>
							</div>
														
							<div class="form-group">
								<label for="txtApellidos" class="col-sm-2 control-label">Contraseña</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtContrasenia" runat="server" ReadOnly="True" TextMode="Password"></asp:TextBox>
									<div id="password-metter"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="txtApellidos" class="col-sm-2 control-label">Confirmar</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtContraseniaConfirmar" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator_Contrasenia" runat="server" ControlToCompare="txtContrasenia" ControlToValidate="txtContraseniaConfirmar" ErrorMessage="Las contraseñas no coinciden" ForeColor="Red" ></asp:CompareValidator>
								</div>
							</div>
							
							<div class="form-group">
								<label for="fuFotografia" class="col-sm-2 control-label">Fotografía</label>
								<div class="col-sm-10">
									<asp:Image ID="imgFoto" runat="server" Width="50px" />
									<asp:FileUpload ID="fuFotografia" runat="server" style="display:inline-block;" />
								</div>
							</div>
							
							<div class="form-group">
								<label for="lblTelefono" class="col-sm-2 control-label">Teléfono</label>
								<div class="col-sm-10">
									<asp:TextBox ID="txtTelefono" runat="server" class="form-control"></asp:TextBox>
								</div>
							</div>

							<div class="form-group">
								<label for="lblOcupacion" class="col-sm-2 control-label">Ocupación</label>
								<div class="col-sm-10">
									<asp:TextBox ID="txtOcupacion" runat="server" class="form-control"></asp:TextBox>
								</div>
							</div>

							<div class="form-group">
								<label for="lblEstado" class="col-sm-2 control-label">Estado</label>
								<div class="col-sm-10">
                                        <asp:DropDownList ID="ddlEstado" runat="server"  class="form-control">
                                            <asp:ListItem Value=" Aguascalientes"> Aguascalientes</asp:ListItem>
                                            <asp:ListItem Value=" Baja California"> Baja California</asp:ListItem>
                                            <asp:ListItem Value=" Baja California Sur"> Baja California Sur</asp:ListItem>
                                            <asp:ListItem Value=" Campeche"> Campeche</asp:ListItem>
                                            <asp:ListItem Value=" Chiapas"> Chiapas</asp:ListItem>
                                            <asp:ListItem Value=" Chihuahua"> Chihuahua</asp:ListItem>
                                            <asp:ListItem Value=" Ciudad de México"> Ciudad de México</asp:ListItem>
                                            <asp:ListItem Value=" Coahuila"> Coahuila</asp:ListItem>
                                            <asp:ListItem Value=" Colima"> Colima</asp:ListItem>
                                            <asp:ListItem Value=" Durango"> Durango</asp:ListItem>
                                            <asp:ListItem Value=" Estado de México"> Estado de México</asp:ListItem>
                                            <asp:ListItem Value=" Guanajuato"> Guanajuato</asp:ListItem>
                                            <asp:ListItem Value=" Guerrero"> Guerrero</asp:ListItem>
                                            <asp:ListItem Value=" Hidalgo"> Hidalgo</asp:ListItem>
                                            <asp:ListItem Value=" Jalisco"> Jalisco</asp:ListItem>
                                            <asp:ListItem Value=" Michoacán"> Michoacán</asp:ListItem>
                                            <asp:ListItem Value=" Morelos"> Morelos</asp:ListItem>
                                            <asp:ListItem Value=" Nayarit"> Nayarit</asp:ListItem>
                                            <asp:ListItem Value=" Nuevo León"> Nuevo León</asp:ListItem>
                                            <asp:ListItem Value=" Oaxaca"> Oaxaca</asp:ListItem>
                                            <asp:ListItem Value=" Puebla"> Puebla</asp:ListItem>
                                            <asp:ListItem Value=" Querétaro"> Querétaro</asp:ListItem>
                                            <asp:ListItem Value=" Quintana Roo"> Quintana Roo</asp:ListItem>
                                            <asp:ListItem Value=" San Luis Potosí"> San Luis Potosí</asp:ListItem>
                                            <asp:ListItem Value=" Sin Localidad"> Sin Localidad</asp:ListItem>
                                            <asp:ListItem Value=" Sinaloa"> Sinaloa</asp:ListItem>
                                            <asp:ListItem Value=" Sonora"> Sonora</asp:ListItem>
                                            <asp:ListItem Value=" Tabasco"> Tabasco</asp:ListItem>
                                            <asp:ListItem Value=" Tamaulipas"> Tamaulipas</asp:ListItem>
                                            <asp:ListItem Value=" Tlaxcala"> Tlaxcala</asp:ListItem>
                                            <asp:ListItem Value=" Veracruz"> Veracruz</asp:ListItem>
                                            <asp:ListItem Value=" Yucatán"> Yucatán</asp:ListItem>
                                            <asp:ListItem Value=" Zacatecas"> Zacatecas</asp:ListItem>
                                        </asp:DropDownList>
								</div>
							</div>

							<div class="form-group">
								<label for="lblMunicipio" class="col-sm-2 control-label">Municipio</label>
								<div class="col-sm-10">
									<asp:TextBox ID="txtMunicipio" runat="server" class="form-control"></asp:TextBox>
								</div>
							</div>

							<div class="form-group">
								<label for="lblDistribuidor" class="col-sm-2 control-label">Distribuidor</label>
								<div class="col-sm-10">
                                    <asp:DropDownList ID="ddlDistribuidor" runat="server" class="form-control"></asp:DropDownList>
								</div>
							</div>
							
							<div class="form-group">
								<!--<label name="lblPinAcceso" for="lblPinAcceso" class="col-sm-2 control-label">PIN acceso</label>-->
                                <asp:Label ID="lblPinAcceso" runat="server" Text="PIN acceso" class="col-sm-2 control-label"></asp:Label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPIN" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group">
								<label for="cbActivo" class="col-sm-2 control-label">¿Es administrador?</label>
								<div class="col-sm-10">
									 <asp:CheckBox ID="cbAdministrador" runat="server" />
								</div>
							</div>
							
							<hr/>
							
							<div class="form-group">
								<label for="lblEvento" class="col-sm-2 control-label">Evento</label>
								<div class="col-sm-10">
                                    <asp:DropDownList ID="ddlEvento" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlEvento_SelectedIndexChanged"></asp:DropDownList>
								</div>
							</div>

							<div class="form-group">
								<!--<label name="lblPuntos" for="lblPuntos" class="col-sm-2 control-label">Puntos</label>-->
                                <asp:Label ID="lblPuntos" runat="server" Text="Puntos" class="col-sm-2 control-label"></asp:Label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPuntos" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2 table-responsive">
									<asp:GridView class="table no-more-tables table-listing" ID="gvwUserDiary" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
                                            <asp:BoundField DataField="Gamificacion" HeaderText="Gamificación" />
                                            <asp:BoundField DataField="Puntos" HeaderText="Puntos" />
                                        </Columns>
										<EditRowStyle BackColor="#2461BF" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
								</div>
							</div>
							
							<div class="form-group">
								<!--<label name="lblCalculadora" for="lblCalculadora" class="col-sm-2 control-label">Calculadora</label>-->
                                <asp:Label ID="lblCalculadora" runat="server" Text="Calculadora" class="col-sm-2 control-label"></asp:Label>
								<div class="col-sm-10">
									<asp:GridView class="table no-more-tables table-listing" ID="gvwPDFs" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False" OnPageIndexChanging="gvwPDFs_PageIndexChanging" PageSize="5" OnRowDeleting="gvwPDFs_RowDeleting">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGvwId" runat="server" Text='<%# Bind("IdCotizacion") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:HyperLinkField DataNavigateUrlFields="Cotizacion" HeaderText="Propuestas" Target="_blank" Text="[Descargar archivo]" />
                                            <asp:HyperLinkField DataNavigateUrlFields="Contrato" HeaderText="Contrato" Target="_blank" Text="[Descargar archivo]" />
                                            <asp:BoundField DataField="FechaHr" HeaderText="Fecha de generación" />
                                            <asp:CommandField ButtonType="Button" ShowDeleteButton="True">
                                            <ControlStyle CssClass="btn btn-success" />
                                            </asp:CommandField>
                                        </Columns>
										<EditRowStyle BackColor="#2461BF" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
								</div>
							</div>

                            <hr/>

							<div class="form-group">
								<!--<label name="lblAcompaniantes" for="lblAcompaniantes" class="col-sm-2 control-label">Acompañantes</label>-->
                                <asp:Label ID="lblAcompaniantes" runat="server" Text="Acompañantes" class="col-sm-2 control-label"></asp:Label>
								<div class="col-sm-10">
									<asp:TextBox ID="txtAcompaniantes" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
								</div>
							</div>
							
                        <div class="form-group">
								<div class="col-sm-10 col-sm-offset-2 table-responsive">
									<asp:GridView class="table no-more-tables table-listing" ID="gvwCompanions" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
                                            <asp:BoundField DataField="Companion" HeaderText="Nombre" />
                                            <asp:BoundField DataField="Age" HeaderText="Edad" />
                                        </Columns>
										<EditRowStyle BackColor="#2461BF" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
								</div>
							</div>
							
							<hr/>
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<asp:Button ID="btnEditar" class="btn btn-warning" runat="server" Text="Editar" OnClick="btnEditar_Click" />
                                    <asp:Button ID="btnAgregar" runat="server" class="btn btn-success" OnClick="btnAgregar_Click" Text="Agregar" />
                                    <asp:Button ID="btnGuardar" class="btn btn-primary"  runat="server" Text="Guardar" OnClick="btnGuardar_Click" Visible="False" />
                                    <asp:Button ID="btnGuardarActualizacion" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" OnClick="btnGuardarActualizacion_Click" />
                                    <asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click" onclientclick="return confirm('¿Desea eliminar este registro?');"/>
                                    <asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" CausesValidation="false" OnClick="btnCancelar_Click" Visible="False" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		
		$('#ContentPlaceHolder1_txtContrasenia').strengthMeter('text', {
            container: $('#password-metter'),
            hierarchy: {
                '0':  ['text-danger',  'Débil'],
                '5':  ['text-warning', 'Insuficiente'],
                '10': ['text-warning', 'Suficiente'],
                '15': ['text-success', 'Fuerte']
            }
        });
		
	</script>
	


</asp:Content>