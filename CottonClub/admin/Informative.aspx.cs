﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Informative : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
            SelectInformativo();
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    string strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }
        protected void SelectInformativo()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();

            SqlParameter Paramfiltro = new SqlParameter();
            Paramfiltro.ParameterName = "@parametro";
            Paramfiltro.SqlDbType = SqlDbType.NVarChar;
            Paramfiltro.Value = txtBuscar.Text;
            pArray.Add(Paramfiltro);

            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@idEvento";
            ParamEvento.SqlDbType = SqlDbType.Int;
            int parametro = 0;
            parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectInformative", pArray);
            if (ds.Tables[0] != null)
            {
                gvwInformative.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwInformative.DataSource = ds;
                gvwInformative.DataBind();
            }
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("InformativeDetail.aspx?action=1");
        }
        
        protected void gvwInformative_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Label lblId = gvwInformative.Rows[e.NewSelectedIndex].FindControl("lblGvwId") as Label;
            Response.Redirect("InformativeDetail.aspx?action=0&id=" + lblId.Text);
        }

        protected void ddlMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectInformativo();
        }

        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SelectInformativo();
        }

        protected void gvwInformative_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwInformative.PageIndex = e.NewPageIndex;
            SelectInformativo();
        }
    }
}