﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub.admin
{
    public partial class CalcPercentagesDetail : System.Web.UI.Page
    {
        const string ID_SESION = "IdPorcentaje";
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdAccion = 0;
            int Id = 0;
            //lblMensaje.Visible = false;
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                Id = Convert.ToInt32(Request.QueryString["id"]);
                Session[ID_SESION] = Id;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    btnGuardar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectData(Id);
                }

            }

        }

        protected void LimpiaCampos()
        {
            txtCantidad.Text = "";
            txtPaquetes.Text = "";
            txtPorcentaje.Text = "";
            txtPorcentajeSemilla.Text = "";
            txtMontoProducto.Text = "";
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
        }

        protected void SelectData(int Id)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@Id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = Id;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificPorcentaje", pArray);
            if (ds.Tables[0] != null)
            {
                txtCantidad.Text = ds.Tables[0].Rows[0]["Cantidad"].ToString();
                txtPaquetes.Text = ds.Tables[0].Rows[0]["Paquetes"].ToString();
                txtPorcentaje.Text = ds.Tables[0].Rows[0]["Porcentaje"].ToString();
                txtPorcentajeSemilla.Text = ds.Tables[0].Rows[0]["PorcentajeSemilla"].ToString();
                txtMontoProducto.Text = ds.Tables[0].Rows[0]["MontoProducto"].ToString();
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();

                ArrayList pArray = ReadParams();

                objData.ExecuteInsertSP("usp_InsertPorcentaje", pArray);
                Response.Redirect("CalcPercentages.aspx?notificacion=1");
            }
        }

        private ArrayList ReadParams()
        {
            ArrayList pArray = new ArrayList();
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Cantidad";
            param1.SqlDbType = SqlDbType.Int;
            param1.Value = txtCantidad.Text;
            pArray.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@Paquetes";
            param2.SqlDbType = SqlDbType.Int;
            param2.Value = int.Parse(txtPaquetes.Text);
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@Porcentaje";
            param3.SqlDbType = SqlDbType.Decimal;
            param3.Precision = 18;
            param3.Scale = 2;
            param3.Value = Decimal.Parse(txtPorcentaje.Text);
            pArray.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "@PorcentajeSemilla";
            param4.SqlDbType = SqlDbType.Decimal;
            param4.Precision = 18;
            param4.Scale = 2;
            param4.Value = Decimal.Parse(txtPorcentajeSemilla.Text);
            pArray.Add(param4);

            SqlParameter param5 = new SqlParameter();
            param5.ParameterName = "@MontoProducto";
            param5.SqlDbType = SqlDbType.Decimal;
            param5.Precision = 18;
            param5.Scale = 2;
            param5.Value = Decimal.Parse(txtMontoProducto.Text);
            pArray.Add(param5);

            return pArray;
        }

        private bool PageIsValid()
        {
            if (string.IsNullOrWhiteSpace(txtCantidad.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad";
                return false;
            }
            else
            {
                int i = 0;
                if (!int.TryParse(txtCantidad.Text, out i))
                {
                    lblMensaje.Text = "La cantidad ingresada no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtPaquetes.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad de paquetes";
                return false;
            }
            else
            {
                int i = 0;
                if (!int.TryParse(txtPaquetes.Text, out i))
                {
                    lblMensaje.Text = "La cantidad de paquetes ingresada no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtPorcentaje.Text))
            {
                lblMensaje.Text = "Debe ingresar un porcentaje";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtPorcentaje.Text, out i))
                {
                    lblMensaje.Text = "El porcentaje ingresado no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtPorcentajeSemilla.Text))
            {
                lblMensaje.Text = "Debe ingresar un porcentaje de semilla";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtPorcentajeSemilla.Text, out i))
                {
                    lblMensaje.Text = "El porcentaje de semilla ingresado no tiene un formato correcto";
                    return false;
                }
            }
            /*
            if (string.IsNullOrWhiteSpace(txtMontoProducto.Text))
            {
                lblMensaje.Text = "Debe ingresar un monto de producto";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtMontoProducto.Text, out i))
                {
                    lblMensaje.Text = "El porcentaje de monto de producto no tiene un formato correcto";
                    return false;
                }
            }
            */
            lblMensaje.Visible = true;
            return true;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = ReadParams();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session[ID_SESION];
                pArray.Add(ParamId);

                objData.ExecuteInsertSP("usp_UpdatePorcentaje", pArray);
                Response.Redirect("CalcPercentages.aspx?notificacion=1");
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CalcPercentages.aspx");
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session[ID_SESION];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeletePorcentaje", pArray);
            Response.Redirect("CalcPercentages.aspx?notificacion=1");
        }
    }
}