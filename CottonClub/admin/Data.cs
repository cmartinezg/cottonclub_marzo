﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace CottonClub
{
    public class Data
    {
        private static SqlConnection MsgrConnection;
        public SqlConnection Connection()
        {
            try {
                //string conn = "Data Source=184.168.47.15;Integrated Security=False;User ID=cotton;pwd=P@ssw0rd;Connect Timeout=15;Encrypt=False;Packet Size=4096";
                string conn = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection cn = new SqlConnection(conn);
                return cn;
            }
            catch(Exception ex) {
                throw new ArgumentException("Error al conectar",ex);
            }
        }


        public int ExecuteSelectEscalarSP(string strName,ArrayList arrParameters)
        {
            MsgrConnection = Connection();
            SqlCommand cmd = new SqlCommand();
            int resultado = 0;

            cmd.CommandText = strName;
            cmd.CommandType = CommandType.StoredProcedure;
            foreach(SqlParameter parameter in arrParameters)
            {
                cmd.Parameters.Add(parameter);
            }
            cmd.Connection = MsgrConnection;
            MsgrConnection.Open();
            resultado = (int)cmd.ExecuteScalar();
            cmd.Dispose();
            MsgrConnection.Close();
            System.Data.SqlClient.SqlConnection.ClearAllPools();
            return resultado;
        }

        public DataSet ExecuteSelectSP(string strName, ArrayList arrParameters)
        {
            MsgrConnection = Connection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter objDa = new SqlDataAdapter();
            DataSet dsReturn = new DataSet();

            cmd.CommandText = strName;
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in arrParameters)
            {
                cmd.Parameters.Add(parameter);
            }
            cmd.Connection = MsgrConnection;
            MsgrConnection.Open();
            objDa.SelectCommand = cmd;
            objDa.Fill(dsReturn);
            cmd.Dispose();
            MsgrConnection.Close();
            System.Data.SqlClient.SqlConnection.ClearAllPools();
            return dsReturn;
        }

        public  int ExecuteInsertSP(string strName, ArrayList arrParameters)
        {
            MsgrConnection = Connection();
            SqlCommand cmd = new SqlCommand();
            int rows;

            cmd.CommandText = strName;
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in arrParameters)
            {
                cmd.Parameters.Add(parameter);
            }
            cmd.Connection = MsgrConnection;
            MsgrConnection.Open();
            rows = cmd.ExecuteNonQuery();
            cmd.Dispose();
            MsgrConnection.Close();
            System.Data.SqlClient.SqlConnection.ClearAllPools();
            return rows;
        }
    }
}