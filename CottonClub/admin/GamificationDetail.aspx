﻿<%@ Page Title="Momentos Bayer | Gamificación Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="GamificationDetail.aspx.cs" Inherits="CottonClub.GamificacionDetalle" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
			<div class="col-md-12">
			
				<h3 class="section-header">Gamificación Detalle</h3>
			
				<div class="panel panel-default">
			
					<div class="panel-body">
			
						<div class="form-horizontal">
							
                            <div class="form-group">
								<div class="col-sm-10">
									<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
								</div>
							</div>

							<div class="form-group">
								<label for="txtNombre" class="col-sm-2 control-label">Pregunta 1</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPregunta" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Opciones</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion1" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion2" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion3" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="ddlRespuestaCorrecta" class="col-sm-2 control-label">Respuesta</label>
	
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlRespuestaCorrecta" runat="server" Enabled="False" CssClass="form-control">
										<asp:ListItem Value="1">Respuesta 1</asp:ListItem>
										<asp:ListItem Value="2">Respuesta 2</asp:ListItem>
										<asp:ListItem Value="3">Respuesta 3</asp:ListItem>
									</asp:DropDownList>
								</div>
							</div>
							
							<hr/>
							
							<div class="form-group">
								<label for="txtPregunta2" class="col-sm-2 control-label">Pregunta 2</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPregunta2" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Opciones</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion2_1" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion2_2" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion2_3" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="ddlRespuestaCorrecta2" class="col-sm-2 control-label">Respuesta</label>
	
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlRespuestaCorrecta2" runat="server" Enabled="False" CssClass="form-control">
										<asp:ListItem Value="1">Respuesta 1</asp:ListItem>
										<asp:ListItem Value="2">Respuesta 2</asp:ListItem>
										<asp:ListItem Value="3">Respuesta 3</asp:ListItem>
									</asp:DropDownList>
								</div>
							</div>
							
							<hr/>
							
							<div class="form-group">
								<label for="txtPregunta3" class="col-sm-2 control-label">Pregunta 3</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPregunta3" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Opciones</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion3_1" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion3_2" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
										<div class="col-sm-4">
											<asp:TextBox class="form-control" ID="txtRespuestaOpcion3_3" runat="server" ReadOnly="True"></asp:TextBox>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="ddlRespuestaCorrecta3" class="col-sm-2 control-label">Respuesta</label>
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlRespuestaCorrecta3" runat="server" Enabled="False" CssClass="form-control">
										<asp:ListItem Value="1">Respuesta 1</asp:ListItem>
										<asp:ListItem Value="2">Respuesta 2</asp:ListItem>
										<asp:ListItem Value="3">Respuesta 3</asp:ListItem>
									</asp:DropDownList>
								</div>
							</div>
							
							<hr/>
							
							<div class="form-group">
								<label for="lbl1erIntento" class="col-sm-2 control-label">Puntos (1er intento)</label>
	
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPuntos" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
                            <div class="form-group">
								<label for="lbl2doIntento" class="col-sm-2 control-label">Puntos (2do intento)</label>
	
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPuntosFallo" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
                            <div class="form-group">
								<label for="lbl3erIntento" class="col-sm-2 control-label">Puntos (3er intento)</label>
	
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtPuntosFallo3er" runat="server"></asp:TextBox>
								</div>
							</div>							
							
							
							<div class="form-group">
								<label for="ddlEvento" class="col-sm-2 control-label">Evento</label>
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlEvento" class="form-control" runat="server" Enabled="False"></asp:DropDownList>
								</div>
							</div>
							<div class="form-group">
								<label for="txtPuntos3" class="col-sm-2 control-label">Activo</label>
								<div class="col-sm-10">
									<asp:CheckBox ID="cbActivo" runat="server" />
								</div>
							</div>
							<div class="form-group">
								<label for="txtPuntos3" class="col-sm-2 control-label">Código QR</label>
								<div class="col-sm-10">
									<asp:Image ID="imgQR" runat="server" Height="50px" Width="50px" />
									<br />
									<asp:HyperLink ID="lnkCodigoQR" runat="server">Descargar</asp:HyperLink>
								</div>
								<asp:Label ID="lblError" runat="server"></asp:Label>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<asp:Button ID="btnEditar" class="btn btn-warning" runat="server" Text="Editar" OnClick="btnEditar_Click" />
									<asp:Button ID="btnAgregar" runat="server" class="btn btn-success" Text="Agregar" OnClick="btnAgregar_Click" />
									<asp:Button ID="btnGuardar" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" OnClick="btnGuardar_Click" />
									<asp:Button ID="btnGuardarActualizacion" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" OnClick="btnGuardarActualizacion_Click" />
									<asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click"  onclientclick="return confirm('¿Desea eliminar este registro?');"/>
									<asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" Visible="False" OnClick="btnCancelar_Click" />
								</div>
							</div>							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>