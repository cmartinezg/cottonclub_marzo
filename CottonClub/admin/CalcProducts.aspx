﻿<%@ Page Title="Momentos Bayer | Productos" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="CalcProducts.aspx.cs" Inherits="CottonClub.admin.CalcProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="section-header">Productos</h3>
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-6">
                                Mostrar 
		                        <asp:DropDownList ID="ddlMostrar" runat="server" OnSelectedIndexChanged="ddlMostrar_SelectedIndexChanged" class="select-pagination" AutoPostBack="True">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 text-right">
                                <div class="form-inline">
                                    <div class="form-group form-search">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtBuscar" class="search-query" placeholder="Buscar..." runat="server"></asp:TextBox>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="Button1" runat="server" class="btn-add" Text="+" OnClick="btnAgregar_Click" />
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="panel-body">

                        <div class="row table-responsive">
                            <asp:GridView class="table no-more-tables table-listing" ID="gvwDiary" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnSelectedIndexChanging="gvwDiary_SelectedIndexChanging" OnPageIndexChanging="gvwDiary_PageIndexChanging" ShowFooter="True" ShowHeaderWhenEmpty="True">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvwId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="PrecioRegular" HeaderText="Precio regular" />
                                    <asp:BoundField DataField="PrecioPreventa" HeaderText="Precio preventa" />
                                    <asp:BoundField DataField="Has" HeaderText="Has" />
                                    <asp:BoundField DataField="CantPorPaquete" HeaderText="Cant x paquete" />
                                    <asp:CommandField ButtonType="Button" DeleteImageUrl="~/images/icono_detalle_2020.png" EditImageUrl="~/images/icono_detalle_2020.png" InsertImageUrl="~/images/icono_detalle_2020.png" NewImageUrl="~/images/icono_detalle_2020.png" SelectImageUrl="~/images/icono_detalle_2020.png" SelectText="Detalle" ShowSelectButton="True" UpdateImageUrl="~/images/icono_detalle_2020.png">
                                        <ControlStyle CssClass="btn btn-success" />
                                    </asp:CommandField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
