﻿<%@ Page Title="Momentos Bayer | Gamificación" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="Gamification.aspx.cs" Inherits="CottonClub.Gamification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="clearfix"></div>
	<div class="content">
		<div class="row">
		    <div class="col-md-12">
			    
			    <h3 class="section-header">Gamificación</h3>
			    
		        <div class="panel panel-default">
			        
			        <div class="panel-heading">
				        
				        <div class="row">
					        
							<div class="col-xs-12 col-sm-6 col-md-6">
								Mostrar 
                                <asp:DropDownList ID="ddlMostrar" runat="server" class="select-pagination" OnSelectedIndexChanged="ddlMostrar_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                </asp:DropDownList>
							</div>
							
							<div class="col-xs-12 col-sm-6 col-md-6 text-right">
								<div class="form-inline">
									<div class="form-group form-search">
										<div class="input-group">
											<asp:TextBox ID="txtBuscar" class="search-query" placeholder="Buscar..."  runat="server"></asp:TextBox>
											<span class="input-group-btn">
												<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
											</span>
										</div>											
									</div>
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlEvento" class="select-event" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvento_SelectedIndexChanged">
                                      </asp:DropDownList>
                                    </div>
									<div class="form-group">
										<asp:Button ID="btnAgregar" runat="server" class="btn-add" Text="+" OnClick="btnAgregar_Click" />
									</div>
								</div>
							</div>
							
		                </div>
				        
			        </div>
		            
		            <div class="panel-body">
		                  		                  
		                  <div class="row table-responsive">
			                  <asp:GridView class="table no-more-tables table-listing" ID="gvwGamification" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" Width="100%" OnSelectedIndexChanging="gvwGamification_SelectedIndexChanging" ShowFooter="True" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwGamification_PageIndexChanging">
                                  <AlternatingRowStyle BackColor="White" />
                                  <Columns>
                                      <asp:TemplateField Visible="False">
                                          <ItemTemplate>
                                              <asp:Label ID="lblGvwId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:ImageField DataImageUrlField="CodigoQR" HeaderText="Código QR" ControlStyle-CssClass="img-qr">
                                          <ControlStyle Height="50px" Width="50px"  />
                                      </asp:ImageField>
                                      <asp:BoundField ApplyFormatInEditMode="True" DataField="Pregunta" HeaderText="Pregunta 1" />
                                      <asp:BoundField DataField="Pregunta2" HeaderText="Pregunta 2" />
                                      <asp:BoundField DataField="Pregunta3" HeaderText="Pregunta 3" />
                                      <asp:BoundField DataField="Puntos" HeaderText="Puntos" />
                                      <asp:BoundField DataField="ActivoTexto" HeaderText="Activo" />
                                      <asp:CommandField ButtonType="Button" DeleteImageUrl="~/images/icono_detalle_2020.png" EditImageUrl="~/images/icono_detalle_2020.png" InsertImageUrl="~/images/icono_detalle_2020.png" NewImageUrl="~/images/icono_detalle_2020.png" SelectImageUrl="~/images/icono_detalle_2020.png" SelectText="Detalle" ShowSelectButton="True" UpdateImageUrl="~/images/icono_detalle_2020.png" >
                                      <ControlStyle CssClass="btn btn-success" />
                                      </asp:CommandField>
                                  </Columns>
                                  <EditRowStyle BackColor="#2461BF" />
                                  <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
                                  <RowStyle BackColor="#EFF3FB" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                  <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                  <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                  <SortedDescendingHeaderStyle BackColor="#4870BE" />
                              </asp:GridView>
		                  </div>
		                  
		            </div>
		            
		        </div>
		    </div>
		</div>
	</div>
</asp:Content>