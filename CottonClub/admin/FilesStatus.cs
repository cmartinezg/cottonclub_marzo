﻿using System.IO;

namespace CottonClub
{
    internal class FilesStatus
    {
        private int contentLength;
        private FileInfo f;
        private string fullName;
        private string fullPath;

        public FilesStatus(FileInfo f)
        {
            this.f = f;
        }

        public FilesStatus(string fullName, int contentLength, string fullPath)
        {
            this.fullName = fullName;
            this.contentLength = contentLength;
            this.fullPath = fullPath;
        }
    }
}