﻿<%@ Page Title="Momentos Bayer | Informativo Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="InformativeDetail.aspx.cs" Inherits="CottonClub.InformationDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="section-header">Informativo Detalle</h3>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-horizontal">

                            <div class="form-group">
								<div class="col-sm-10">
									<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
								</div>
							</div>

							<div class="form-group">
								<label for="txtTitulo" class="col-sm-2 control-label">Título</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtTitulo" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label for="txtDescripcion" class="col-sm-2 control-label">Descripción</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtDescripcion" runat="server" ReadOnly="True" TextMode="MultiLine" Rows="5"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label for="fuImagen" class="col-sm-2 control-label">Imagen</label>
								<div class="col-sm-10">
									<asp:Image ID="imgImagen" runat="server" Height="50px"/>
									<asp:FileUpload ID="fuImagen" runat="server" Enabled="False" style="display: inline-block;" />
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div><small>En formato horizontal</small></div>
								</div>
							</div>
							<div class="form-group">
								<label for="ddlEvento" class="col-sm-2 control-label">Evento</label>
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlEvento" runat="server" Class="form-control"></asp:DropDownList>
								</div>
							</div>
                            <div class="form-group">
								<label for="ddlEvento" class="col-sm-2 control-label">URL</label>
								<div class="col-sm-10">
                                    <asp:TextBox ID="txtURL" runat="server" class="form-control"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label for="cbActivo" class="col-sm-2 control-label">Activo</label>
								<div class="col-sm-10">
									<asp:CheckBox ID="cbActivo" runat="server" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<asp:Button ID="btnEditar" class="btn btn-warning" runat="server" Text="Editar" OnClick="btnEditar_Click" />
									<asp:Button ID="btnAgregar" runat="server" class="btn btn-success" Text="Agregar" OnClick="btnAgregar_Click" />
									<asp:Button ID="btnGuardar" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" OnClick="btnGuardar_Click" />
									<asp:Button ID="btnGuardarActualizacion" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False"/>
									<asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click"  onclientclick="return confirm('¿Desea eliminar este registro?');"/>
									<asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" Visible="False" OnClick="btnCancelar_Click" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>