﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            //if(!IsPostBack)
            //{
            //    SelectUsuarios();
            //}
            SelectUsuarios();

        }

        protected void SelectUsuarios()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            string parametro = txtBuscar.Text;
            ParamUsername.Value = parametro;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUsers", pArray);
            if (ds.Tables[0] != null)
            {
                gvwUsers.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwUsers.DataSource = ds;
                gvwUsers.DataBind();
            }
        }

        protected void gvwUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void gvwUsers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvwUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Label lblId = gvwUsers.Rows[e.NewSelectedIndex].FindControl("lblGvwId") as Label;
            Response.Redirect("UserDetail.aspx?action=0&id=" + lblId.Text);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserDetail.aspx?action=1");
        }

        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SelectUsuarios();
        }

        protected void gvwUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwUsers.PageIndex = e.NewPageIndex;
            SelectUsuarios();
        }

        protected void ddlMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectUsuarios();
        }
    }
}