﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SelectDataUserSession(Session["Username"].ToString());
        }
        protected void SelectDataUserSession(string Username)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@username";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = Username;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificUsersbyUsername", pArray);
            if (ds.Tables[0] != null)
            {
                lblFirstNameDisplay.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                lblLastNameDisplay.Text = ds.Tables[0].Rows[0]["LastName"].ToString().Substring(0,1).ToUpper()+".";
                lnkEditProfile.NavigateUrl = "UserDetail.aspx?action=0&id=" + ds.Tables[0].Rows[0]["Id"].ToString();
                //assets/img/avatar_right.png
                string strPhoto = "assets/img/avatar_right.png";
                if (ds.Tables[0].Rows[0]["Photo"].ToString() != "")
                    strPhoto = "photos/" + ds.Tables[0].Rows[0]["Photo"].ToString();

                imgProfile.Attributes.Add("src", strPhoto);
            }
        }
    }
}