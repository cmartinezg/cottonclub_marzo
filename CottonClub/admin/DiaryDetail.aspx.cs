﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace CottonClub
{
    public partial class DiaryDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdAccion = 0;
            int IdAgenda = 0;
            //lblMensaje.Visible = false;
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack)
            {
                Boolean fileOK = false;
                String path = Server.MapPath("files/");
                //Foto del conferencista
                if (fuFotoConferencista.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(fuFotoConferencista.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        fuFotoConferencista.PostedFile.SaveAs(path
                            + fuFotoConferencista.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }

                //Imagen Anexa

                if (fuImagenAnexa.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(fuImagenAnexa.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        fuImagenAnexa.PostedFile.SaveAs(path
                            + fuImagenAnexa.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }

                //Presentacion

                if (fuPresentation.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(fuPresentation.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        fuPresentation.PostedFile.SaveAs(path
                            + fuPresentation.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }

            }


            if (!IsPostBack)
            {
                FillEvento();
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdAgenda = Convert.ToInt32(Request.QueryString["id"]);
                Session["IdAgenda"] = IdAgenda;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    ActivarCampos();
                    btnCancelar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectAgenda(IdAgenda);
                }

            }

        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();
            }
        }
        protected void LimpiaCampos()
        {
            txtNombre.ReadOnly = false;
            txtApellidos.ReadOnly = false;
            txtDescripcionConferencia.ReadOnly = false;
            txtHoraConferenciaInicio.ReadOnly = false;
            txtHoraConferenciaFin.ReadOnly = false;
            //txtFechaConferencia.ReadOnly = false;
            txtInformacionConferencista.ReadOnly = false;
            txtUbicacion.ReadOnly = false;
            fuFotoConferencista.Enabled = true;
            fuImagenAnexa.Enabled = true;
            fuPresentation.Enabled = true;
            txtNombre.Text = "";
            txtApellidos.Text = "";
            txtDescripcionConferencia.Text = "";
            txtHoraConferenciaInicio.Text = "";
            txtInformacionConferencista.Text = "";
            txtHoraConferenciaFin.Text = "";
            //txtFechaConferencia.Text = "";
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        protected void SelectAgenda(int IdAgenda)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdAgenda;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificDiary", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombre.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                txtApellidos.Text = ds.Tables[0].Rows[0]["Lastname"].ToString();
                txtDescripcionConferencia.Text = ds.Tables[0].Rows[0]["ConferenceDescription"].ToString();
                txtInformacionConferencista.Text = ds.Tables[0].Rows[0]["InformationLecturer"].ToString();
                txtHoraConferenciaInicio.Text = ds.Tables[0].Rows[0]["ConferenceTime"].ToString();

                imgFotoConferencista.ImageUrl = null;
                if(ds.Tables[0].Rows[0]["PhotoLecturer"].ToString()!="")
                {
                    imgFotoConferencista.ImageUrl = "files/" + ds.Tables[0].Rows[0]["PhotoLecturer"].ToString();
                }

                imgImagenAnexa.ImageUrl = null;
                if (ds.Tables[0].Rows[0]["AttachedImageLecturer"].ToString() != "")
                {
                    imgImagenAnexa.ImageUrl = "files/" + ds.Tables[0].Rows[0]["AttachedImageLecturer"].ToString();
                }

                if (ds.Tables[0].Rows[0]["Presentation"].ToString() == "")
                {
                    lblPresentacion.Text = "Archivo aun no subido";
                }
                else
                {
                    lblPresentacion.Visible = false;
                    lnkPresentacion.NavigateUrl = "files/"+ ds.Tables[0].Rows[0]["Presentation"].ToString();
                    lnkPresentacion.Text = ds.Tables[0].Rows[0]["Presentation"].ToString();
                    lnkPresentacion.Visible = true;
                }
                ddlEvento.SelectedValue= ds.Tables[0].Rows[0]["IdEvent"].ToString();
                //txtFechaConferencia.Text= ds.Tables[0].Rows[0]["ConferenceDate"].ToString();
                txtHoraConferenciaFin.Text = ds.Tables[0].Rows[0]["ConferenceTimeEnd"].ToString();
                txtUbicacion.Text = ds.Tables[0].Rows[0]["Ubication"].ToString();

                ddlDetalle.ClearSelection();
                ddlDetalle.Items.FindByValue((Convert.ToBoolean(ds.Tables[0].Rows[0]["Detail"]))?"1":"0").Selected = true;
                if (ds.Tables[0].Rows[0]["Active"].ToString() == "1")
                {
                    cbActivo.Checked = true;
                }
                else {
                    cbActivo.Checked = false;
                }
                DropDownList1_SelectedIndexChanged(null, null);
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamName = new SqlParameter();
                ParamName.ParameterName = "@Name";
                ParamName.SqlDbType = SqlDbType.NVarChar;
                ParamName.Value = txtNombre.Text;
                pArray.Add(ParamName);
                SqlParameter ParamLastname = new SqlParameter();
                ParamLastname.ParameterName = "@Lastname";
                ParamLastname.SqlDbType = SqlDbType.Text;
                ParamLastname.Value = txtApellidos.Text;
                pArray.Add(ParamLastname);
                SqlParameter ParameterConferenceDescription = new SqlParameter();
                ParameterConferenceDescription.ParameterName = "@ConferenceDescription";
                ParameterConferenceDescription.SqlDbType = SqlDbType.Text;
                ParameterConferenceDescription.Value = txtDescripcionConferencia.Text;
                pArray.Add(ParameterConferenceDescription);
                SqlParameter ParamConferenceTime = new SqlParameter();
                ParamConferenceTime.ParameterName = "@ConferenceTime";
                ParamConferenceTime.SqlDbType = SqlDbType.Time;
                ParamConferenceTime.Value = TimeSpan.Parse(txtHoraConferenciaInicio.Text);
                pArray.Add(ParamConferenceTime);
                SqlParameter ParamInformationLecturer = new SqlParameter();
                ParamInformationLecturer.ParameterName = "@InformationLecturer";
                ParamInformationLecturer.SqlDbType = SqlDbType.NVarChar;
                ParamInformationLecturer.Value = txtInformacionConferencista.Text;
                pArray.Add(ParamInformationLecturer);
                SqlParameter ParamPhotoLecturer = new SqlParameter();
                ParamPhotoLecturer.ParameterName = "@PhotoLecturer";
                ParamPhotoLecturer.SqlDbType = SqlDbType.NVarChar;
                ParamPhotoLecturer.Value = fuFotoConferencista.FileName;
                pArray.Add(ParamPhotoLecturer);
                SqlParameter ParamAttachedImageLecturer = new SqlParameter();
                ParamAttachedImageLecturer.ParameterName = "@AttachedImageLecturer";
                ParamAttachedImageLecturer.SqlDbType = SqlDbType.NVarChar;
                ParamAttachedImageLecturer.Value = fuImagenAnexa.FileName;
                pArray.Add(ParamAttachedImageLecturer);
                SqlParameter ParamPresentation = new SqlParameter();
                ParamPresentation.ParameterName = "@Presentation";
                ParamPresentation.SqlDbType = SqlDbType.NVarChar;
                ParamPresentation.Value = fuPresentation.FileName;
                pArray.Add(ParamPresentation);
                SqlParameter ParamIdEvent = new SqlParameter();
                ParamIdEvent.ParameterName = "@IdEvent";
                ParamIdEvent.SqlDbType = SqlDbType.BigInt;
                ParamIdEvent.Value = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
                pArray.Add(ParamIdEvent);
                SqlParameter ParamConferenceTimeEnd = new SqlParameter();
                ParamConferenceTimeEnd.ParameterName = "@ConferenceTimeEnd";
                ParamConferenceTimeEnd.SqlDbType = SqlDbType.Time;
                ParamConferenceTimeEnd.Value = TimeSpan.Parse(txtHoraConferenciaFin.Text);
                pArray.Add(ParamConferenceTimeEnd);
                SqlParameter ParamConferenceDate = new SqlParameter();
                ParamConferenceDate.ParameterName = "@ConferenceDate";
                ParamConferenceDate.SqlDbType = SqlDbType.Date;
                string strTimeAux = "01/01/1900";
                ParamConferenceDate.Value = strTimeAux;//DateTime.Parse(txtFechaConferencia.Text);
                pArray.Add(ParamConferenceDate);

                SqlParameter ParamUbication = new SqlParameter();
                ParamUbication.ParameterName = "@Ubication";
                ParamUbication.SqlDbType = SqlDbType.NVarChar;
                ParamUbication.Value = txtUbicacion.Text;
                pArray.Add(ParamUbication);

                SqlParameter ParamDetail = new SqlParameter();
                ParamDetail.ParameterName = "@Detail";
                ParamDetail.SqlDbType = SqlDbType.Bit;
                ParamDetail.Value = (ddlDetalle.SelectedIndex == 1);
                pArray.Add(ParamDetail);

                SqlParameter ParamActive = new SqlParameter();
                ParamActive.ParameterName = "@Active";
                ParamActive.SqlDbType = SqlDbType.Int;
                int intActive = 0;
                if (cbActivo.Checked)
                {
                    intActive = 1;
                }
                ParamActive.Value = intActive;
                pArray.Add(ParamActive);

                objData.ExecuteInsertSP("usp_InsertDiary", pArray);
                Response.Redirect("Diary.aspx?notificacion=1");
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            ActivarCampos();
        }

        private bool PageIsValid()
        {
            int intContador = 0;
            lblMensaje.Text = "";
            if (ddlDetalle.SelectedValue.ToString().Equals("1"))
            {
                if (txtNombre.Text == "")
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El Nombre del conferencista es un dato requerido";
                    intContador = intContador + 1;
                    //return false;
                }

                if (txtApellidos.Text == "")
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El dato de Apellidos no puede quedar vacío";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            if (txtDescripcionConferencia.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Debe agregar una descripción de la conferencia";
                intContador = intContador + 1;
                //return false;
            }

            /*
            if (txtFechaConferencia.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "La Fecha de la conferencia es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }
            else
            {
                DateTime dtTemp;

                if (!DateTime.TryParse(txtFechaConferencia.Text, out dtTemp))
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "La Fecha de la conferencia no tiene un formato correcto";
                    intContador = intContador + 1;
                    //return false;
                }
            }
            */

            if (txtHoraConferenciaInicio.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "La Hora de Inicio de la conferencia es obligatoria";
                intContador = intContador + 1;
                //return false;
            }
            else
            {
                TimeSpan tmTemp;
                if (!TimeSpan.TryParse(txtHoraConferenciaInicio.Text, out tmTemp))
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "La Hora de Inicio de la conferencia no tiene un formato correcto";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            if (txtHoraConferenciaFin.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "La Hora fin de la conferencia es obligatoria";
                intContador = intContador + 1;
                //return false;
            }
            else
            {
                TimeSpan tmTemp;
                if (!TimeSpan.TryParse(txtHoraConferenciaFin.Text, out tmTemp))
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "La Hora Fin de la conferencia no tiene un formato correcto";
                    intContador = intContador + 1;
                    //return false;
                }
            }
            lblMensaje.Visible = true;

            if (intContador > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session["IdAgenda"];
                pArray.Add(ParamId);
                SqlParameter ParamName = new SqlParameter();
                ParamName.ParameterName = "@Name";
                ParamName.SqlDbType = SqlDbType.NVarChar;
                ParamName.Value = txtNombre.Text;
                pArray.Add(ParamName);
                SqlParameter ParamLastname = new SqlParameter();
                ParamLastname.ParameterName = "@Lastname";
                ParamLastname.SqlDbType = SqlDbType.NVarChar;
                ParamLastname.Value = txtApellidos.Text;
                pArray.Add(ParamLastname);
                SqlParameter ParamConferenceDescription = new SqlParameter();
                ParamConferenceDescription.ParameterName = "@ConferenceDescription";
                ParamConferenceDescription.SqlDbType = SqlDbType.Text;
                ParamConferenceDescription.Value = txtDescripcionConferencia.Text;
                pArray.Add(ParamConferenceDescription);
                SqlParameter ParamConferenceTime = new SqlParameter();
                ParamConferenceTime.ParameterName = "@ConferenceTime";
                ParamConferenceTime.SqlDbType = SqlDbType.Time;
                string strTimeAux = "";
                if (txtHoraConferenciaInicio.Text.Equals(""))
                {
                    strTimeAux = "00:00:00";
                }
                else
                {
                    strTimeAux = txtHoraConferenciaInicio.Text;
                }
                ParamConferenceTime.Value = TimeSpan.Parse(strTimeAux);
                pArray.Add(ParamConferenceTime);
                SqlParameter ParamInformationLecturer = new SqlParameter();
                ParamInformationLecturer.ParameterName = "@InformationLecturer";
                ParamInformationLecturer.SqlDbType = SqlDbType.Text;
                ParamInformationLecturer.Value = txtInformacionConferencista.Text;
                pArray.Add(ParamInformationLecturer);
                SqlParameter ParamPhotoLecturer = new SqlParameter();
                ParamPhotoLecturer.ParameterName = "@PhotoLecturer";
                ParamPhotoLecturer.SqlDbType = SqlDbType.NVarChar;
                ParamPhotoLecturer.Value = fuFotoConferencista.FileName;
                pArray.Add(ParamPhotoLecturer);
                SqlParameter ParamAttachedImageLecturer = new SqlParameter();
                ParamAttachedImageLecturer.ParameterName = "@AttachedImageLecturer";
                ParamAttachedImageLecturer.SqlDbType = SqlDbType.NVarChar;
                ParamAttachedImageLecturer.Value = fuImagenAnexa.FileName;
                pArray.Add(ParamAttachedImageLecturer);
                SqlParameter ParamPresentation = new SqlParameter();
                ParamPresentation.ParameterName = "@Presentation";
                ParamPresentation.SqlDbType = SqlDbType.NVarChar;
                ParamPresentation.Value = fuPresentation.FileName;
                pArray.Add(ParamPresentation);
                SqlParameter ParamIdEvent = new SqlParameter();
                ParamIdEvent.ParameterName = "@IdEvent";
                ParamIdEvent.SqlDbType = SqlDbType.BigInt;
                ParamIdEvent.Value = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
                pArray.Add(ParamIdEvent);
                SqlParameter ParamConferenceTimeEnd = new SqlParameter();
                ParamConferenceTimeEnd.ParameterName = "@ConferenceTimeEnd";
                ParamConferenceTimeEnd.SqlDbType = SqlDbType.Time;
                if (txtHoraConferenciaFin.Text.Equals(""))
                {
                    strTimeAux = "00:00:00";
                }
                else
                {
                    strTimeAux = txtHoraConferenciaFin.Text;
                }
                ParamConferenceTimeEnd.Value = TimeSpan.Parse(strTimeAux);
                pArray.Add(ParamConferenceTimeEnd);
                SqlParameter ParamConferenceDate = new SqlParameter();
                ParamConferenceDate.ParameterName = "@ConferenceDate";
                ParamConferenceDate.SqlDbType = SqlDbType.Date;
                strTimeAux = "01/01/1900"; //"01 /01/1900";
                /*
                if (txtFechaConferencia.Text.Equals(""))
                {
                    strTimeAux = "01/01/1900";
                }
                else
                {
                    strTimeAux = txtFechaConferencia.Text;
                }
                */
                ParamConferenceDate.Value = DateTime.Parse(strTimeAux);
                pArray.Add(ParamConferenceDate);

                SqlParameter ParamUbication = new SqlParameter();
                ParamUbication.ParameterName = "@Ubication";
                ParamUbication.SqlDbType = SqlDbType.NVarChar;
                ParamUbication.Value = txtUbicacion.Text;
                pArray.Add(ParamUbication);

                SqlParameter ParamDetail = new SqlParameter();
                ParamDetail.ParameterName = "@Detail";
                ParamDetail.SqlDbType = SqlDbType.Bit;
                ParamDetail.Value = (ddlDetalle.SelectedIndex == 1);
                pArray.Add(ParamDetail);

                SqlParameter ParamActive = new SqlParameter();
                ParamActive.ParameterName = "@Active";
                ParamActive.SqlDbType = SqlDbType.Int;
                int intActive=0;
                if (cbActivo.Checked)
                {
                    intActive = 1;
                }
                ParamActive.Value = intActive;
                pArray.Add(ParamActive);

                objData.ExecuteInsertSP("usp_UpdateDiary", pArray);
                Response.Redirect("Diary.aspx?notificacion=1");
            }
            /*
            txtNombre.ReadOnly = true;
            txtApellidos.ReadOnly = true;
            txtDescripcionConferencia.ReadOnly = true;
            txtHoraConferenciaInicio.ReadOnly = true;
            txtInformacionConferencista.ReadOnly = true;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEditar.Visible = true;
            SelectAgenda(Convert.ToInt32(Session["IdAgenda"]));
            */
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Diary.aspx");
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdAgenda"];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteDiary", pArray);
            Response.Redirect("Diary.aspx?notificacion=1");
        }

        protected void ActivarCampos()
        {
            txtNombre.ReadOnly = false;
            txtApellidos.ReadOnly = false;
            txtDescripcionConferencia.ReadOnly = false;
            txtHoraConferenciaInicio.ReadOnly = false;
            txtInformacionConferencista.ReadOnly = false;
            txtHoraConferenciaFin.ReadOnly = false;
            //txtFechaConferencia.ReadOnly = false;
            txtUbicacion.ReadOnly = false;
            fuFotoConferencista.Enabled = true;
            fuImagenAnexa.Enabled = true;
            fuPresentation.Enabled = true;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDetalle.SelectedValue.ToString().Equals("0"))
            {
                divNombre.Visible = false;
                divApellidos.Visible = false;
                divConferencista.Visible = false;
                divFotoConferencista.Visible = false;
                divAnexa.Visible = false;
                divPresentacion.Visible = false;
                /*
                txtNombre.Visible = false;
                txtApellidos.Visible = false;
                txtInformacionConferencista.Visible = false;
                imgFotoConferencista.Visible = false;
                fuFotoConferencista.Visible = false;
                imgImagenAnexa.Visible = false;
                fuImagenAnexa.Visible = false;
                fuPresentation.Visible = false;
                */
            }
            else {
                divNombre.Visible = true;
                divApellidos.Visible = true;
                divConferencista.Visible = true;
                divFotoConferencista.Visible = true;
                divAnexa.Visible = true;
                divPresentacion.Visible = true;
                /*
                txtNombre.Visible = true;
                txtApellidos.Visible = true;
                txtInformacionConferencista.Visible = true;
                imgFotoConferencista.Visible = true;
                fuFotoConferencista.Visible = true;
                imgImagenAnexa.Visible = true;
                fuImagenAnexa.Visible = true;
                fuPresentation.Visible = true;
                lnkPresentacion.Visible = true;
                */
            }
        }
    }
}