﻿<%@ Page Title="Momentos Bayer | Reporte Agenda" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="ReportDiary.aspx.cs" Inherits="CottonClub.admin.ReportDiary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
			    
			    <div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-6">
			    		<h3 class="section-header">Reporte Agenda</h3>
				    </div>
				    <div class="col-xs-12 col-sm-6 col-md-6 text-right">
			    		<asp:DropDownList ID="ddlEvento" runat="server" class="select-event" AutoPostBack="True"></asp:DropDownList>
				    </div>
			    </div>
			  			    
			    <div class="row">
				    
				    <div class="col-xs-12">
				    
					    <ul class="nav nav-tabs" role="tablist">
						  	<li class="active"><a href="#tab-charts" aria-controls="tab-charts" role="tab" data-toggle="tab">Gráficas</a></li>
						  	<li><a href="#tab-registers" aria-controls="tab-registers" role="tab" data-toggle="tab">Registros</a></li>
						</ul>
						
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-charts">
								<div class="canvas-holder">
									<canvas id="charts"></canvas>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane " id="tab-registers">
								<div class="row table-responsive">
									<asp:GridView class="table no-more-tables table-listing" ID="gvwAgenda" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="gvwAgenda_PageIndexChanging" AutoGenerateColumns="False">
										<AlternatingRowStyle BackColor="White" />
										<Columns>
                                            <asp:BoundField DataField="Agenda" HeaderText="Agenda" />
                                            <asp:BoundField DataField="Votos" HeaderText="Votos" />
                                        </Columns>
										<EditRowStyle BackColor="#2461BF" />
										<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
										<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
										<RowStyle BackColor="#EFF3FB" />
										<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
										<SortedAscendingCellStyle BackColor="#F5F7FB" />
										<SortedAscendingHeaderStyle BackColor="#6D95E1" />
										<SortedDescendingCellStyle BackColor="#E9EBEF" />
										<SortedDescendingHeaderStyle BackColor="#4870BE" />
									</asp:GridView>
								</div>
							</div>
						</div>
					
				    </div>
				    
			    </div>
			    
			    <div class="row text-right">
				    <div class="col-xs-12">
				    	<div id="strJson" runat="server" style="display:none"></div>
						<asp:Button ID="btnExportar" runat="server" CssClass="btn btn-success" OnClick="btnExportar_Click" Text="Exportar" />
				    </div>
			    </div>
			    		        
	    	</div>
		</div>
	</div>
	
	<script type="text/javascript">
		
		charts();
		
		function charts() {
					
			var records = JSON.parse(jQuery("#ContentPlaceHolder1_strJson").text()).Table;
			var labels  = [];
			var dataset = [];
						
			for (var i in records) {
				labels.push(records[i].Agenda.substr(0, 15) + "...")
				dataset.push(records[i].Votos)
			}
			
			var data = {
			    labels: labels,
			    datasets: [
			        {
			            label: "Reporte Agenda",
			            borderWidth: 1,
			            data: dataset,
			        }
			    ]
			};
			
			
			var options = {
			    tooltips: {
			        enabled: true,
			        mode: 'label',
			        callbacks: {
			            title: function(tooltipItems, data) {
			                var idx = tooltipItems[0].index;
			                return records[idx].Agenda;//do something with title
			            }
			        }
			    },
			}
			
			var ctx   = document.getElementById("charts").getContext("2d")
			var chart = new Chart(ctx, {
			    type: 'bar',
			    data: data,
			    options: options
			});
		
		}

	</script>
	
	
    
</asp:Content>



