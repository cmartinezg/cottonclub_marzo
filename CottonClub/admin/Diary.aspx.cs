﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Diary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
            SelectAgenda();

        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    string strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void SelectAgenda()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.Int;
            int parametro = 0;
            parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            ParamUsername.Value = parametro;
            pArray.Add(ParamUsername);
            SqlParameter ParamFrase = new SqlParameter();
            ParamFrase.ParameterName = "@frase";
            ParamFrase.SqlDbType = SqlDbType.Text;
            ParamFrase.Value = txtBuscar.Text;
            pArray.Add(ParamFrase);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectDiary", pArray);
            if (ds.Tables[0] != null)
            {
                gvwDiary.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwDiary.DataSource = ds;
                gvwDiary.DataBind();
            }
        }
        protected void gvwDiary_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Label lblId = gvwDiary.Rows[e.NewSelectedIndex].FindControl("lblGvwId") as Label;
            Response.Redirect("DiaryDetail.aspx?action=0&id=" + lblId.Text);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("DiaryDetail.aspx?action=1");
        }

        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SelectAgenda();
        }

        protected void gvwDiary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwDiary.PageIndex = e.NewPageIndex;
            SelectAgenda();
        }

        protected void ddlMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectAgenda();
        }
    }
}