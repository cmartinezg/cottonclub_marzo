﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace CottonClub
{
    public partial class InformationDetail : System.Web.UI.Page
    {
        int IdAccion = 0;
        int IdInformative = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdInformative = Convert.ToInt32(Request.QueryString["id"]);
                Session["IdInformative"] = IdInformative;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    ActivarCampos();
                    btnCancelar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectInformativo(IdInformative);
                }

            }
            if (IsPostBack)
            {
                Boolean fileOK = false;
                String path = Server.MapPath("files/informative/");
                //Foto del conferencista
                if (fuImagen.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(fuImagen.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        fuImagen.PostedFile.SaveAs(path
                            + fuImagen.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }

                //Imagen Anexa
            }

            }

        protected void SelectInformativo(int IdInformativo)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdInformativo;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificInformative", pArray);
            if (ds.Tables[0] != null)
            {
                txtTitulo.Text = ds.Tables[0].Rows[0]["Title"].ToString();
                txtDescripcion.Text = ds.Tables[0].Rows[0]["Description"].ToString();
                imgImagen.ImageUrl= "files/informative/" + ds.Tables[0].Rows[0]["Image"].ToString();
                ddlEvento.SelectedValue = ds.Tables[0].Rows[0]["Event"].ToString();
                txtURL.Text = ds.Tables[0].Rows[0]["URL"].ToString();
                if (ds.Tables[0].Rows[0]["Active"].ToString().Equals("1"))
                {
                    cbActivo.Checked = true;
                }
                else
                {
                    cbActivo.Checked = false;
                }
            }
        }
        protected void LimpiaCampos()
        {
            txtTitulo.ReadOnly = false;
            txtDescripcion.ReadOnly = false;
            txtURL.ReadOnly = false;
            cbActivo.Enabled = true;
            ddlEvento.Enabled = true;
            fuImagen.Enabled = true;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }
        protected string SelectEventos(int IdEvento)
        {
            string Evento = "";
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdEvento;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificEvents", pArray);
            if (ds.Tables[0] != null)
            {
                Evento = ds.Tables[0].Rows[0]["Name"].ToString();
            }
            return Evento;
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();
            }
        }

        protected void ActivarCampos()
        {
            txtTitulo.ReadOnly = false;
            txtDescripcion.ReadOnly = false;
            txtURL.ReadOnly = false;
            fuImagen.Enabled = true;
            ddlEvento.Enabled = true;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            ActivarCampos();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamTitle = new SqlParameter();
                ParamTitle.ParameterName = "@Title";
                ParamTitle.SqlDbType = SqlDbType.NVarChar;
                ParamTitle.Value = txtTitulo.Text;
                pArray.Add(ParamTitle);
                SqlParameter ParamDescription = new SqlParameter();
                ParamDescription.ParameterName = "@Description";
                ParamDescription.SqlDbType = SqlDbType.Text;
                ParamDescription.Value = txtDescripcion.Text;
                pArray.Add(ParamDescription);
                SqlParameter ParameterImage = new SqlParameter();
                ParameterImage.ParameterName = "@Image";
                ParameterImage.SqlDbType = SqlDbType.NVarChar;
                ParameterImage.Value = fuImagen.FileName;
                pArray.Add(ParameterImage);
                SqlParameter ParamURL = new SqlParameter();
                ParamURL.ParameterName = "@URL";
                ParamURL.SqlDbType = SqlDbType.Text;
                ParamURL.Value = txtURL.Text;
                pArray.Add(ParamURL);
                SqlParameter ParamUserModify = new SqlParameter();
                ParamUserModify.ParameterName = "@UserModify";
                ParamUserModify.SqlDbType = SqlDbType.NVarChar;
                ParamUserModify.Value = Session["Username"];
                pArray.Add(ParamUserModify);
                SqlParameter ParamActive = new SqlParameter();
                ParamActive.ParameterName = "@Active";
                ParamActive.SqlDbType = SqlDbType.Int;
                int Activo = 0;
                if (cbActivo.Checked == true)
                {
                    Activo = 1;
                }
                ParamActive.Value = Activo;
                pArray.Add(ParamActive);
                SqlParameter ParamEvent = new SqlParameter();
                ParamEvent.ParameterName = "@Event";
                ParamEvent.SqlDbType = SqlDbType.BigInt;
                ParamEvent.Value = ddlEvento.SelectedValue;
                pArray.Add(ParamEvent);
                objData.ExecuteInsertSP("usp_InsertInformative", pArray);
                Response.Redirect("Informative.aspx?notificacion=1");
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("en-US");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.BigInt;
                ParamId.Value = Session["IdInformative"];
                pArray.Add(ParamId);
                SqlParameter ParamTitle = new SqlParameter();
                ParamTitle.ParameterName = "@Title";
                ParamTitle.SqlDbType = SqlDbType.Text;
                ParamTitle.Value = txtTitulo.Text;
                pArray.Add(ParamTitle);
                SqlParameter ParamDescription = new SqlParameter();
                ParamDescription.ParameterName = "@Description";
                ParamDescription.SqlDbType = SqlDbType.Text;
                ParamDescription.Value = txtDescripcion.Text;
                pArray.Add(ParamDescription);
                SqlParameter ParamImage = new SqlParameter();
                ParamImage.ParameterName = "@Image";
                ParamImage.SqlDbType = SqlDbType.NVarChar;
                ParamImage.Value = fuImagen.FileName;
                pArray.Add(ParamImage);
                SqlParameter ParamURL = new SqlParameter();
                ParamURL.ParameterName = "@URL";
                ParamURL.SqlDbType = SqlDbType.Text;
                ParamURL.Value = txtURL.Text;
                pArray.Add(ParamURL);
                SqlParameter ParamUserModify = new SqlParameter();
                ParamUserModify.ParameterName = "@UserModify";
                ParamUserModify.SqlDbType = SqlDbType.NVarChar;
                ParamUserModify.Value = Session["Username"];
                pArray.Add(ParamUserModify);
                SqlParameter ParamEvent = new SqlParameter();
                ParamEvent.ParameterName = "@Event";
                ParamEvent.SqlDbType = SqlDbType.BigInt;
                ParamEvent.Value = ddlEvento.SelectedItem.Value;
                pArray.Add(ParamEvent);
                SqlParameter ParamActive = new SqlParameter();
                ParamActive.ParameterName = "@Active";
                ParamActive.SqlDbType = SqlDbType.Int;
                int Activo = 0;
                if (cbActivo.Checked == true)
                {
                    Activo = 1;
                }
                ParamActive.Value = Activo;
                pArray.Add(ParamActive);
                objData.ExecuteInsertSP("usp_UpdateInformative", pArray);
                Response.Redirect("Informative.aspx?notificacion=1");
            }
            /*
            txtTitulo.ReadOnly = true;
            txtDescripcion.ReadOnly = true;
            txtURL.ReadOnly = true;
            ddlEvento.Enabled = false;
            cbActivo.Enabled = false;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEditar.Visible = true;
            SelectInformativo(Convert.ToInt32(Session["IdInformative"]));
            */
        }

        private bool PageIsValid()
        {
            int intContador = 0;
            lblMensaje.Text = "";
            if (txtTitulo.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El títuo es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }

            if (txtDescripcion.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Por favor describa el informativo en el campo Descripción";
                intContador = intContador + 1;
                //return false;
            }

            if (intContador > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@id";

            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdInformative"];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteInformative", pArray);
            Response.Redirect("Informative.aspx?notificacion=1");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Informative.aspx");
        }
    }
}