﻿<%@ Page Title="Momentos Bayer | Eventos Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="EventDetail.aspx.cs" Inherits="CottonClub.EventDetail" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <h3 class="section-header">Evento Detalle</h3>

                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="form-horizontal">

                            <div class="form-group">
                                <div class="col-sm-10">
                                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtNombreEvento" class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtNombreEvento" runat="server" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtUbicacion" class="col-sm-2 control-label">Dirección</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtUbicacion1" runat="server" ReadOnly="True" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="geolocate" class="col-sm-2 control-label">Ubicación</label>
                                <div class="col-sm-10">
                                    <div id="map" style="height: 300px; margin-bottom: 10px;"></div>
                                    <asp:TextBox ID="txtLatitud" runat="server" class="txt_latitud"></asp:TextBox>
                                    <asp:TextBox ID="txtLongitud" runat="server" class="txt_longitud"></asp:TextBox>
                                    <input id="geolocate" class="form-control" type="text" placeholder="Ingresa una dirección" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtCorreoBienvenida" class="col-sm-2 control-label">Correo bienvenida</label>
                                <div class="col-sm-10">
                                    <CKEditor:CKEditorControl ID="txtCorreoBienvenida" runat="server" Height="200" BasePath="~/ckeditor">
		    
                                    </CKEditor:CKEditorControl>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <asp:Button ID="btnEnvioMail" class="btn btn-success" runat="server" Text="Enviar invitación" OnClick="btnEnvioMail_Click" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="FileUpload1" class="col-sm-2 control-label">Logo</label>
                                <div class="col-sm-10">
                                    <asp:Image ID="imgLogo" runat="server" Width="50px" />
                                    <asp:FileUpload ID="FileUpload1" Style="display: inline-block" runat="server" />
                                </div>
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div><small>300x300 (preferentemente)</small></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="fuMapa" class="col-sm-2 control-label">Mapa</label>
                                <div class="col-sm-10">
                                    <asp:Image ID="imgMapa" runat="server" Width="50px" />
                                    <asp:FileUpload ID="fuMapa" Style="display: inline-block" runat="server" />
                                </div>
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div><small>Formato vertical</small></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtFechaEvento" class="col-sm-2 control-label">Fecha evento</label>
                                <div class="col-sm-10">
                                    <div class="input-group date" data-provide="datepicker">
                                        <asp:TextBox class="form-control datepicker" ID="txtFechaEvento" runat="server" ReadOnly="True"></asp:TextBox>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtHoraVisualizacionCalculadora" class="col-sm-2 control-label">Calculadora</label>
                                <div class="col-sm-10">
                                    <div class='input-group date' id='datetimepicker3'>
                                        <span class="input-group-addon" id="sizing-addon1">
                                            <asp:CheckBox ID="cbCalculadora" runat="server" Enabled="false" /></span>
                                        <asp:TextBox class="form-control timepicker" ID="txtHoraVisualizacionCalculadora" runat="server" ReadOnly="True" data-format="hh:mm:ss"></asp:TextBox>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div><small>Activar / Hora inicio</small></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="txtHoraVisualizacionCalculadora" class="col-sm-2 control-label">Gamificación</label>
                                <div class="col-sm-10">
                                    <div class='input-group date' id='datetimepicker4'>
                                        <span class="input-group-addon" id="sizing-addon1">
                                            <asp:CheckBox ID="cbGamificacion" runat="server" Enabled="false" /></span>
											<asp:TextBox ID="txtGamificationTime" class="form-control timepicker" runat="server"></asp:TextBox>
											<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
										</span>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div><small>Activar / Hora bloqueo</small></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="txtHoraVisualizacionEncuesta" class="col-sm-2 control-label">Encuesta</label>
                                <div class="col-sm-10">
                                    <div class='input-group date' id='datetimepicker4'>
                                        <span class="input-group-addon" id="sizing-addon1">
                                            <asp:CheckBox ID="cbSurvey" runat="server" /></span>
                                        <!--<input type="text" class="form-control timepicker"/>-->
                                        <asp:TextBox ID="txtSurveyTime" class="form-control timepicker" runat="server"></asp:TextBox>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div><small>Activar / Hora inicio</small></div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="cbActivo" class="col-sm-2 control-label">Activo</label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="cbActivo" runat="server" Enabled="false" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <asp:Button ID="btnEditar" class="btn btn-warning" runat="server" Text="Editar" OnClick="btnEditar_Click" />
                                    <asp:Button ID="btnAgregar" runat="server" class="btn btn-success" Text="Agregar" OnClick="btnAgregar_Click" />
                                    <asp:Button ID="btnGuardar" class="btn btn-primary" runat="server" Text="Guardar" Visible="False" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="btnGuardarActualizacion" class="btn btn-primary" runat="server" Text="Guardar" Visible="False" />
                                    <asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click" OnClientClick="return confirm('¿Desea eliminar este registro?');" />
                                    <asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" Visible="False" OnClick="btnCancelar_Click" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .txt_latitud, .txt_longitud {
            display: none;
        }
    </style>
    <script>
        $(document).ready(function () {

            var dfl = [19.440055, -99.2016455];
            var lat = $(".txt_latitud").val();
            var lng = $(".txt_longitud").val();

            if (lat == "" || lng == "") {
                var lat = $(".txt_latitud").val(dfl[0]);
                var lng = $(".txt_longitud").val(dfl[1]);
            } else {
                dfl = [lat, lng];
            }

            $(".txt_latitud").attr("data-geo", "lat")
            $(".txt_longitud").attr("data-geo", "lng")

            $("#geolocate").geocomplete({
                map: "#map",
                markerOptions: {
                    draggable: true,
                },
                mapOptions: {
                    scrollwheel: true,
                },
                location: dfl,
                blur: true,
                details: "form",
                detailsAttribute: 'data-geo'
            });

            $("#geolocate").bind("geocode:dragged", function (event, latLng) {
                $(".txt_latitud").val(latLng.lat());
                $(".txt_longitud").val(latLng.lng());
            });

        })
        $('.datepicker').datetimepicker({ format: "YYYY-MM-DD" });
        $('.timepicker').datetimepicker({ format: "HH:mm" });

        CKEDITOR.replace('ContentPlaceHolder1_txtCorreoBienvenida');
    </script>
</asp:Content>
