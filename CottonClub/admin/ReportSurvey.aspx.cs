﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Net;

namespace CottonClub.admin
{
    public partial class ReportSurvey : System.Web.UI.Page
    {
        string json = "";
        string strItemToSelect = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
                    SelectEncuesta();
                    gvwEncuesta.Visible = true;
                    SelectEncuestaComentarios();
                    gvwComments.Visible = true;
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                //ddlEvento.Items.Insert(0, "Seleccione...");

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void SelectEncuesta()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.BigInt;
            int parametro;
            if (!IsPostBack)
            {
                parametro = Convert.ToInt32(strItemToSelect);
            }
            else
            {
                parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            }
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_ReportSurvey", pArray);
            if (ds.Tables[0] != null)
            {
                //gvwUsuarios.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwEncuesta.DataSource = ds;
                gvwEncuesta.DataBind();
            }

            json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
            strJson.InnerHtml = json;
        }

        protected void SelectEncuestaComentarios()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.BigInt;
            int parametro;
            if (!IsPostBack)
            {
                parametro = Convert.ToInt32(strItemToSelect);
            }
            else
            {
                parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            }
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_ReportSurveyComments", pArray);
            if (ds.Tables[0] != null)
            {
                //gvwUsuarios.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwComments.DataSource = ds;
                gvwComments.DataBind();
            }

            json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
            strJsonComments.InnerHtml = json;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            //ExportGridToExcel();
            ExportToCSV_Survey();
        }

        private void ExportGridToExcel()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page page = new Page();
            HtmlForm form = new HtmlForm();
            gvwEncuesta.EnableViewState = false;

            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(gvwEncuesta);
            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Encuesta_" + DateTime.Now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        }

        public void ExportToCSV_Survey()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Encuesta_" + DateTime.Now + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            gvwEncuesta.AllowPaging = false;
            gvwEncuesta.DataBind();
            gvwEncuesta.EnableViewState = false;
            for (int index = 0; index < gvwEncuesta.Columns.Count; index++)
            {
                sBuilder.Append(WebUtility.HtmlDecode(gvwEncuesta.Columns[index].HeaderText) + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < gvwEncuesta.Rows.Count; i++)
            {
                for (int k = 0; k < gvwEncuesta.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(WebUtility.HtmlDecode(gvwEncuesta.Rows[i].Cells[k].Text).Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }

        public void ExportToCSV_SurveyComments()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Comentarios_" + DateTime.Now + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            gvwComments.AllowPaging = false;
            gvwComments.DataBind();
            gvwComments.EnableViewState = false;
            for (int index = 0; index < gvwComments.Columns.Count; index++)
            {
                sBuilder.Append(WebUtility.HtmlDecode(gvwComments.Columns[index].HeaderText) + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < gvwComments.Rows.Count; i++)
            {
                for (int k = 0; k < gvwComments.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(WebUtility.HtmlDecode(gvwComments.Rows[i].Cells[k].Text).Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }

        protected void gvwEncuesta_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwEncuesta.PageIndex = e.NewPageIndex;
            SelectEncuesta();
        }

        protected void btnExportarComentarios_Click(object sender, EventArgs e)
        {
            ExportToCSV_SurveyComments();
        }

        protected void gvwComments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwComments.PageIndex = e.NewPageIndex;
            SelectEncuestaComentarios();
        }
    }
}