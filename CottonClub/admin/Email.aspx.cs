﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace CottonClub.admin
{
    public partial class Email : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            //if(!IsPostBack)
            //{
            //    SelectUsuarios();
            //}
            SelectUsuarios();
        }

        protected void SelectUsuarios()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            string parametro = "";
            ParamUsername.Value = parametro;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUsers", pArray);
            if (ds.Tables[0] != null)
            {
                gvwUsers.PageSize = 10;
                gvwUsers.DataSource = ds;
                gvwUsers.DataBind();
            }
        }

        protected void btnEnvioMail_Click(object sender, EventArgs e)
        {

            try
            {
                MailMessage message = new MailMessage();
                //message.From = new MailAddress("contacto.emprende.negocio@gmail.com");
                //message.From = new MailAddress("momentos@bayer.com");
                message.From = new MailAddress(ConfigurationManager.AppSettings["accountSendMail"].ToString());

                CottonClub.Data objData = new Data();
                DataSet ds = new DataSet();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamIdEvento = new SqlParameter();
                ParamIdEvento.ParameterName = "@id";
                ParamIdEvento.SqlDbType = SqlDbType.Int;
                ParamIdEvento.Value = Session["IdEvento"];
                pArray.Add(ParamIdEvento);
                SmtpClient client = new SmtpClient();
                //client.Host = "relay-hosting.secureserver.net";
                client.Host = ConfigurationManager.AppSettings["smtpServer"].ToString();
                //client.Port = 25;
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["serverEmailPort"].ToString());
                ds = objData.ExecuteSelectSP("usp_SelectSpecificEvents", pArray);
                string strBody = "";
                if (ds.Tables[0] != null)
                {
                    message.Subject = "Bienvenido a " + ds.Tables[0].Rows[0]["Name"].ToString();
                    //strBody = ds.Tables[0].Rows[0]["WelcomeEmail"].ToString();
                    strBody = ds.Tables[0].Rows[0]["WelcomeEmail"].ToString();

                }

                pArray.Clear();
                SqlParameter ParamUsername = new SqlParameter();
                ParamUsername.ParameterName = "@parametro";
                ParamUsername.SqlDbType = SqlDbType.NVarChar;
                string parametro = "";
                ParamUsername.Value = parametro;
                pArray.Add(ParamUsername);
                ds.Clear();
                ds = objData.ExecuteSelectSP("usp_SelectUsers", pArray);
                if (ds.Tables[0] != null)
                {
                    foreach (DataRow fila in ds.Tables[0].Rows)
                    {
                        string valor = fila["CorreoElectronico"].ToString();//"cemg7@hotmail.com";
                        message.To.Add(new MailAddress(valor));
                        message.IsBodyHtml = true;
                        //message.Body = "<html><body>" + strBody + "<br><br><a href='"+ ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "/Register.aspx?data=" + fila["PIN"] + "'>Da clic aqui</a>" + "</html></body>";

                        message.Body = "<html><head><title>Invitación</title><meta charset=\"UTF-8\"/></head><body><div style=\"font-family: Helvetica; text-align: center; width: 600px; background-color: #433124; \"><div><a href = \"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "Register.aspx?data=" + fila["PIN"].ToString() + "\"><img src=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/mail_invitacion_1.jpg\" alt=\"Cotton Club\"/></a></div><div><a href=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "Register.aspx?data=" + fila["PIN"].ToString() + "\" style=\"color:#FFF; text-transform: uppercase; padding: 5px; display: inline-block; text-decoration: none;\">Regístrate aquí</a></div><div><a href=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "Register.aspx?data=" + fila["PIN"].ToString() + "\"><img src=\""+ ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/mail_invitacion_2.jpg\" alt=\"Cotton Club\"/></a></div></div></body></html>";

                        client.Send(message);
                        message.To.Clear();
                    }
                }


                lblMensaje.Text = "Enviado";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message + ex.Source + ex.StackTrace + ex.TargetSite;
            }
            //Response.Redirect("Events.aspx");

        }

        protected void gvwUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            gvwUsers.PageIndex = e.NewSelectedIndex;
            SelectUsuarios();
        }

        protected void gvwUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwUsers.PageIndex = e.NewPageIndex;
            SelectUsuarios();
        }
    }
}