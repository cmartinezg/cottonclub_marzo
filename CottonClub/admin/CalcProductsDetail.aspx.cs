﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub.admin
{
    public partial class CalcProductsDetail : System.Web.UI.Page
    {
        const string ID_SESION = "IdProducto";
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdAccion = 0;
            int Id = 0;
            //lblMensaje.Visible = false;
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                Id = Convert.ToInt32(Request.QueryString["id"]);
                Session[ID_SESION] = Id;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    btnGuardar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectData(Id);
                }

            }

        }
        protected void LimpiaCampos()
        {
            txtNombre.Text = "";
            txtPrecioRegular.Text = "";
            txtPrecioPreventa.Text = "";
            txtHas.Text = "";
            txtCantPorPaquete.Text = "";
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
        }
        protected void SelectData(int Id)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@Id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = Id;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificProducto", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombre.Text = ds.Tables[0].Rows[0]["Nombre"].ToString();
                txtPrecioRegular.Text = ds.Tables[0].Rows[0]["PrecioRegular"].ToString();
                txtPrecioPreventa.Text = ds.Tables[0].Rows[0]["PrecioPreventa"].ToString();
                txtHas.Text = ds.Tables[0].Rows[0]["Has"].ToString();
                txtCantPorPaquete.Text = ds.Tables[0].Rows[0]["CantPorPaquete"].ToString();
            }
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();

                ArrayList pArray = ReadParams();

                objData.ExecuteInsertSP("usp_InsertProducto", pArray);
                Response.Redirect("CalcProducts.aspx?notificacion=1");
            }
        }
        private ArrayList ReadParams()
        {
            ArrayList pArray = new ArrayList();
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Nombre";
            param1.SqlDbType = SqlDbType.NVarChar;
            param1.Value = txtNombre.Text;
            pArray.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@PrecioRegular";
            param2.SqlDbType = SqlDbType.Decimal;
            param2.Precision = 18;
            param2.Scale = 2;
            param2.Value = decimal.Parse(txtPrecioRegular.Text);
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@PrecioPreventa";
            param3.SqlDbType = SqlDbType.Decimal;
            param3.Precision = 18;
            param3.Scale = 2;
            param3.Value = decimal.Parse(txtPrecioPreventa.Text);
            pArray.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "@Has";
            param4.SqlDbType = SqlDbType.Decimal;
            param4.Precision = 18;
            param4.Scale = 2;
            param4.Value = Decimal.Parse(txtHas.Text);
            pArray.Add(param4);

            SqlParameter param5 = new SqlParameter();
            param5.ParameterName = "@CantPorPaquete";
            param5.SqlDbType = SqlDbType.Int;
            param5.Value = int.Parse(txtCantPorPaquete.Text);
            pArray.Add(param5);

            return pArray;
        }
        private bool PageIsValid()
        {

            if (txtNombre.Text == "")
            {
                lblMensaje.Text = "El Nombre del producto es un dato requerido";
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtPrecioRegular.Text))
            {
                lblMensaje.Text = "Debe ingresar un precio regular";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtPrecioRegular.Text, out i))
                {
                    lblMensaje.Text = "El precio regular ingresado no tiene un formato correcto";
                    return false;
                }
            }


            if (string.IsNullOrWhiteSpace(txtPrecioPreventa.Text))
            {
                lblMensaje.Text = "Debe ingresar un precio de preventa";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtPrecioPreventa.Text, out i))
                {
                    lblMensaje.Text = "El precio de preventa ingresado no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtHas.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad de Has";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtHas.Text, out i))
                {
                    lblMensaje.Text = "La cantidad de Has ingresada no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtCantPorPaquete.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad por paquete";
                return false;
            }
            else
            {
                int i = 0;
                if (!int.TryParse(txtCantPorPaquete.Text, out i))
                {
                    lblMensaje.Text = "La cantidad por paquete ingresada no tiene un formato correcto";
                    return false;
                }
            }

          
            lblMensaje.Visible = true;
            return true;
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = ReadParams();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session[ID_SESION];
                pArray.Add(ParamId);

                objData.ExecuteInsertSP("usp_UpdateProducto", pArray);
                Response.Redirect("CalcProducts.aspx?notificacion=1");
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CalcProducts.aspx");
        }
        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session[ID_SESION];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteProducto", pArray);
            Response.Redirect("CalcProducts.aspx?notificacion=1");
        }
    }
}