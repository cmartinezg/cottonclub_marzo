﻿<%@ Page Title="Momentos Bayer | Ranking" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="Ranking.aspx.cs" Inherits="CottonClub.Ranking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
		    <div class="col-md-12">
			    
			    <h3 class="section-header">Ranking</h3>
			    
		        <div class="panel panel-default">
			        
			        <div class="panel-heading">
				        
				        <div class="row">
					        
							<div class="col-xs-12 col-sm-12 col-md-12 text-right">
								<div class="form-inline">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlEvento" class="select-event" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvento_SelectedIndexChanged">
                                      </asp:DropDownList>
                                    </div>
								</div>
							</div>
							
		                </div>
				        
			        </div>
		            
		            <div class="panel-body">
		                  		                  
	                  	<div class="row table-responsive">
		                  
							<asp:GridView class="table no-more-tables table-listing" ID="gvwRanking" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="gvwRanking_PageIndexChanging">
								<AlternatingRowStyle BackColor="White" />
								<Columns>
									<asp:ImageField DataImageUrlField="Photo">
										<ControlStyle Height="50px" Width="50px" />
									</asp:ImageField>
									<asp:BoundField DataField="RankPos" HeaderText="Posición" />
									<asp:BoundField DataField="Fullname" HeaderText="Nombre" />
									<asp:BoundField DataField="Score" HeaderText="Puntos" />
								</Columns>
								<EditRowStyle BackColor="#2461BF" />
								<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
								<RowStyle BackColor="#EFF3FB" />
								<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
								<SortedAscendingCellStyle BackColor="#F5F7FB" />
								<SortedAscendingHeaderStyle BackColor="#6D95E1" />
								<SortedDescendingCellStyle BackColor="#E9EBEF" />
								<SortedDescendingHeaderStyle BackColor="#4870BE" />
							</asp:GridView>
		                  
		                
	                  	</div>
		                  
		            </div>
		            		            
		        </div>
		    </div>
		</div>
	</div>
</asp:Content>