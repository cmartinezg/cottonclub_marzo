﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Default1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            txt_login_username.Attributes["type"] = "email";
        }
        protected void login_toggle_Click(object sender, EventArgs e)
        {
            Session["Username"] = "";
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@username";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = txt_login_username.Text;
            pArray.Add(ParamUsername);

            objCnn.Open();
            DataSet dsUser = objData.ExecuteSelectSP("usp_SelectUserLogin", pArray);

            if (dsUser != null)
            {
                if (dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                {
                    string strDecryptedPwd = Encrypt.DecryptPassword(dsUser.Tables[0].Rows[0]["Password"].ToString(), txt_login_username.Text);
                    if (strDecryptedPwd == txt_login_pass.Text)
                    {
                        Session.Timeout = 20;
                        Session["Username"] = txt_login_username.Text;
                        Response.Redirect("Diary.aspx");
                    }
                }
            }

            lblMensaje.Text = "El usuario y/o contraseña no son correctos o no tiene permisos para acceder";
            lblMensaje.Visible = true;
        }
    }
}