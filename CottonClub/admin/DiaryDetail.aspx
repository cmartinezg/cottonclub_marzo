﻿<%@ Page Title="Momentos Bayer | Agenda Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="DiaryDetail.aspx.cs" Inherits="CottonClub.DiaryDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				
				<h3 class="section-header">Agenda Detalle</h3>
				
				<div class="panel panel-default">
					
					<div class="panel-body">
						
						<div class="form-horizontal">
							
                            <div class="form-group">
								<div class="col-sm-10">
									<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
								</div>
							</div>

							<div class="form-group">
								<label for="ddlDetalle" class="col-sm-2 control-label">Tipo</label>
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlDetalle" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
										<asp:ListItem Value="0">Sin detalle</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Con detalle</asp:ListItem>
									</asp:DropDownList>
								</div>
							</div>

							<div class="form-group" id="divNombre" runat="server">
								<label for="txtNombre" class="col-sm-2 control-label">Nombre</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtNombre" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group" id="divApellidos" runat="server">
								<label for="txtApellidos" class="col-sm-2 control-label">Apellidos</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtApellidos" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group" id="divConferencista" runat="server">
								<label for="txtInformacionConferencista" class="col-sm-2 control-label">Descripción</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtInformacionConferencista" runat="server" ReadOnly="True" TextMode="MultiLine" Rows="5"></asp:TextBox>
								</div>
							</div>

							<div class="form-group" id="divFotoConferencista" runat="server">
								<label for="fuFotoConferencista" class="col-sm-2 control-label">Foto</label>
								<div class="col-sm-10">
									<asp:Image ID="imgFotoConferencista" runat="server" Width="50px"/>
									<asp:FileUpload ID="fuFotoConferencista" runat="server" Enabled="False" style="display:inline-block;" />
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div><small>300x300 (preferentemente)</small></div>
								</div>
							</div>
							
							<div class="form-group" id="divAnexa" runat="server">
								<label for="fuImagenAnexa" class="col-sm-2 control-label">Imagen anexa</label>
								<div class="col-sm-10">
									<asp:Image ID="imgImagenAnexa" runat="server" Width="50px"/>
									<asp:FileUpload ID="fuImagenAnexa" runat="server" Enabled="False" style="display:inline-block;"/>
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div><small>En formato horizontal</small></div>
								</div>
							</div>
							
							<div class="form-group" id="divPresentacion" runat="server">
								<label for="fuPresentation" class="col-sm-2 control-label">Presentación</label>
								<div class="col-sm-10">
									<asp:Label ID="lblPresentacion" runat="server"></asp:Label>
									<asp:HyperLink ID="lnkPresentacion" runat="server" Target="_blank" Visible="False">[lnkPresentacion]</asp:HyperLink>
									<br />
									<asp:FileUpload ID="fuPresentation" runat="server" Enabled="False" />
								</div>
								<div class="col-sm-10 col-sm-offset-2">
									<div><small>Formato PDF (Obligatorio)</small></div>
								</div>
							</div>

							<div class="form-group">
								<label for="txtDescripcionConferencia" class="col-sm-2 control-label">Conferencia</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtDescripcionConferencia" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>
							
							<div class="form-group">
								<label for="ddlEvento" class="col-sm-2 control-label">Evento</label>
								<div class="col-sm-10">
									<asp:DropDownList ID="ddlEvento" runat="server" CssClass="form-control"></asp:DropDownList>
								</div>
							</div>
								
                            <!--						
							<div class="form-group">
								<label for="txtFechaConferencia" class="col-sm-2 control-label">Fecha</label>
								<div class="col-sm-10">
									<div class="input-group date" data-provide="datepicker">
										<asp:TextBox class="form-control datepicker" ID="txtFechaConferencia" runat="server" ReadOnly="True"></asp:TextBox>
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</div>
									</div>
								</div>
							</div>
							-->
							<div class="form-group">
								<label for="txtHoraConferenciaInicio" class="col-sm-2 control-label">Hora inicio</label>
								<div class="col-sm-10">
									<div class='input-group date' id='datetimepicker3'>
										<asp:TextBox class="form-control timepicker" ID="txtHoraConferenciaInicio" runat="server" ReadOnly="True" data-format="hh:mm:ss"></asp:TextBox>
										<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="txtHoraConferenciaFin" class="col-sm-2 control-label">Hora término</label>
								<div class="col-sm-10">
									<div class='input-group date' id='datetimepicker4'>
										<asp:TextBox class="form-control timepicker" ID="txtHoraConferenciaFin" runat="server" ReadOnly="True" data-format="hh:mm:ss"></asp:TextBox>
										<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="txtUbicacion" class="col-sm-2 control-label">Ubicación</label>
								<div class="col-sm-10">
									<asp:TextBox class="form-control" ID="txtUbicacion" runat="server" ReadOnly="True"></asp:TextBox>
								</div>
							</div>

                            <div class="form-group">
                                <label for="cbActivo" class="col-sm-2 control-label">Activo</label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="cbActivo" runat="server" />
                                </div>
                            </div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<asp:Button ID="btnEditar" class="btn btn-warning" runat="server" Text="Editar" OnClick="btnEditar_Click" />
									<asp:Button ID="btnAgregar" runat="server" class="btn btn-success" Text="Agregar" OnClick="btnAgregar_Click" />
									<asp:Button ID="btnGuardar" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" OnClick="btnGuardar_Click" />
									<asp:Button ID="btnGuardarActualizacion" class="btn btn-primary"  runat="server" Text="Guardar" Visible="False" />
									<asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click" onclientclick="return confirm('¿Desea eliminar este registro?');"/>
									<asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" Visible="False" OnClick="btnCancelar_Click" />
								</div>
							</div>
													
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$('.datepicker').datetimepicker({format:"YYYY-MM-DD"});
		$('.timepicker').datetimepicker({format:"HH:mm"});
	</script>
</asp:Content>