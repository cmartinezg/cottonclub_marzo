﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace CottonClub
{
    public partial class RankingDetail : System.Web.UI.Page
    {
        int IdAccion = 0;
        int IdEvento = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdEvento = Convert.ToInt32(Request.QueryString["id"]);
                Session["IdEvento"] = IdEvento;
                if (IdAccion == 1)
                {
                    //LimpiaCampos();
                    //btnGuardar.Visible = false;
                    //btnBorrar.Visible = false;
                }
                else
                {
                    //ActivarCampos();
                    //btnCancelar.Visible = true;
                    //btnAgregar.Visible = false;
                    SelectUsuarios(IdEvento);
                }

            }
        }

        protected void SelectUsuarios(int IdEvento)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamIdEvento = new SqlParameter();
            ParamIdEvento.ParameterName = "@IdEvento";
            ParamIdEvento.SqlDbType = SqlDbType.BigInt;
            ParamIdEvento.Value = Session["IdEvento"];
            pArray.Add(ParamIdEvento);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectUsersbyEvent", pArray);
            if (ds.Tables[0] != null)
            {
                gvwRankingDetail.DataSource = ds;
                gvwRankingDetail.DataBind();
            }
        }

    }
}