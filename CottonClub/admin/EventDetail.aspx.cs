﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;
using System.Net;
using System.Net.Mail;

namespace CottonClub
{
    public partial class EventDetail : System.Web.UI.Page
    {
        int IdAccion = 0;
        int IdEvento = 0;
        string Contenido = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("es-MX");
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack)
            {
                Boolean fileOK = false;
                String path = Server.MapPath("files/");
                if (FileUpload1.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        FileUpload1.PostedFile.SaveAs(path
                            + FileUpload1.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }

                //Mapa

                if (fuMapa.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(fuMapa.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        fuMapa.PostedFile.SaveAs(path
                            + fuMapa.FileName);
                        //Label1.Text = "File uploaded!";
                    }
                    catch (Exception ex)
                    {
                        //Label1.Text = "File could not be uploaded.";
                    }
                }
                else
                {
                    //Label1.Text = "Cannot accept files of this type.";
                }
            }
            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdEvento = Convert.ToInt32(Request.QueryString["id"]);
                Session["IdEvento"] = IdEvento;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    ActivarCampos();
                    btnCancelar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectEventos(IdEvento);
                }

            }
        }

        private bool PageIsValid()
        {
            int intContador = 0;
            lblMensaje.Text = "";
            if (txtNombreEvento.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El Nombre del Evento es un dato requerido";
                intContador = intContador + 1;
                //return false;
            }

            if (txtUbicacion1.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Debe agregar una ubicación del evento";
                intContador = intContador + 1;
                //return false;
            }

            if (txtCorreoBienvenida.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Por favor escriba un texto para el correo de Bienvenida";
                intContador = intContador + 1;
                //return false;
            }

            if (txtFechaEvento.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "La Fecha del evento es obligatoria";
                intContador = intContador + 1;
                //return false;
            }

            if (txtHoraVisualizacionCalculadora.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "La Hora del evento es obligatoria";
                intContador = intContador + 1;
                //return false;
            }


            Boolean fileOK = false;
            String path = Server.MapPath("files/");
            //Foto del conferencista
            if (fuMapa.HasFile)
            {
                String fileExtension =
                    System.IO.Path.GetExtension(fuMapa.FileName).ToLower();
                String[] allowedExtensions =
                    {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
            }

            if (fileOK)
            {
                try
                {
                    fuMapa.PostedFile.SaveAs(path
                        + fuMapa.FileName);

                    imgMapa.ImageUrl = "files/" + fuMapa.FileName;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El archivo no pudo ser cargado. Error: (" + ex.Message + ")";
                    intContador = intContador + 1;
                    //return false;
                }
            }
            else
            {
                if (imgMapa.ImageUrl == null)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El tipo de archivo no es válido.";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            fileOK = false;

            if (FileUpload1.HasFile)
            {
                String fileExtension =
                    System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                String[] allowedExtensions =
                    {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
            }

            if (fileOK)
            {
                try
                {
                    FileUpload1.PostedFile.SaveAs(path
                        + FileUpload1.FileName);

                    imgLogo.ImageUrl = "files/" + FileUpload1.FileName;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El archivo no pudo ser cargado. Error: (" + ex.Message + ")";
                    intContador = intContador + 1;
                    //return false;
                }
            }
            else
            {
                if (imgLogo.ImageUrl == null)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El tipo de archivo no es válido.";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            if (intContador > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void LimpiaCampos()
        {
            txtNombreEvento.ReadOnly = false;
            txtUbicacion1.ReadOnly = false;
            txtHoraVisualizacionCalculadora.ReadOnly = false;
            txtCorreoBienvenida.ReadOnly = false;
            txtFechaEvento.ReadOnly = false;
            cbActivo.Enabled = true;
            cbGamificacion.Enabled = true;
            cbCalculadora.Enabled = true;
            cbSurvey.Enabled = true;
            txtNombreEvento.Text = "";
            txtUbicacion1.Text = "";
            txtHoraVisualizacionCalculadora.Text = "";
            txtCorreoBienvenida.Text = "";
            txtFechaEvento.Text = "";
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
            txtGamificationTime.Text = "";
            txtSurveyTime.Text = "";
        }

        protected void SelectEventos(int IdEvento)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdEvento;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificEvents", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombreEvento.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                txtUbicacion1.Text = ds.Tables[0].Rows[0]["Location"].ToString();

                txtCorreoBienvenida.Text = HttpUtility.HtmlDecode(ds.Tables[0].Rows[0]["WelcomeEmail"].ToString());
                txtFechaEvento.Text = ds.Tables[0].Rows[0]["Date"].ToString();
                txtHoraVisualizacionCalculadora.Text = ds.Tables[0].Rows[0]["CalculatorDisplayTime"].ToString();
                if (ds.Tables[0].Rows[0]["Active"].ToString().Equals("1"))
                {
                    cbActivo.Checked = true;
                }
                else
                {
                    cbActivo.Checked = false;
                }
                imgLogo.ImageUrl = null;
                if (ds.Tables[0].Rows[0]["Logo"].ToString() != "")
                {
                    imgLogo.ImageUrl = "files/" + ds.Tables[0].Rows[0]["Logo"].ToString();
                }
                imgMapa.ImageUrl = null;
                if (ds.Tables[0].Rows[0]["Mapa"].ToString() != "")
                {
                    imgMapa.ImageUrl = "files/" + ds.Tables[0].Rows[0]["Mapa"].ToString();
                }
                txtLatitud.Text = ds.Tables[0].Rows[0]["Latitude"].ToString();
                txtLongitud.Text = ds.Tables[0].Rows[0]["Length"].ToString();
                cbCalculadora.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Calculator"]);
                cbGamificacion.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Gamification"]);
                cbSurvey.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["Survey"]);
                txtGamificationTime.Text = ds.Tables[0].Rows[0]["GamificationTime"].ToString();
                txtSurveyTime.Text = ds.Tables[0].Rows[0]["SurveyTime"].ToString();
            }
        }

        protected void AjaxFileUpload1_UploadStart(object sender, AjaxControlToolkit.AjaxFileUploadStartEventArgs e)
        {

        }

        protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs e)
        {

        }

        protected void AjaxFileUpload1_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        {

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                try
                {
                    CottonClub.Data objData = new Data();
                    SqlConnection objCnn = objData.Connection();
                    ArrayList pArray = new ArrayList();
                    SqlParameter ParamName = new SqlParameter();
                    ParamName.ParameterName = "@Name";
                    ParamName.SqlDbType = SqlDbType.NVarChar;
                    ParamName.Value = txtNombreEvento.Text;
                    pArray.Add(ParamName);
                    SqlParameter ParamLocation = new SqlParameter();
                    ParamLocation.ParameterName = "@Location";
                    ParamLocation.SqlDbType = SqlDbType.Text;
                    ParamLocation.Value = txtUbicacion1.Text;
                    pArray.Add(ParamLocation);
                    SqlParameter ParameterWelcomeEmail = new SqlParameter();
                    ParameterWelcomeEmail.ParameterName = "@WelcomeEmail";
                    ParameterWelcomeEmail.SqlDbType = SqlDbType.Text;
                    ParameterWelcomeEmail.Value = txtCorreoBienvenida.Text;
                    pArray.Add(ParameterWelcomeEmail);
                    SqlParameter ParamLogo = new SqlParameter();
                    ParamLogo.ParameterName = "@Logo";
                    ParamLogo.SqlDbType = SqlDbType.NVarChar;
                    ParamLogo.Value = imgLogo.ImageUrl.Replace("files/", "");
                    pArray.Add(ParamLogo);
                    SqlParameter ParamDate = new SqlParameter();
                    string strFecha = txtFechaEvento.Text;
                    ParamDate.ParameterName = "@Date";
                    ParamDate.SqlDbType = SqlDbType.Date;
                    DateTime dtmDate = DateTime.Parse(strFecha);
                    ParamDate.Value = dtmDate;
                    pArray.Add(ParamDate);
                    SqlParameter ParamCalculatorDisplayTime = new SqlParameter();
                    ParamCalculatorDisplayTime.ParameterName = "@CalculatorDisplayTime";
                    ParamCalculatorDisplayTime.SqlDbType = SqlDbType.Time;
                    ParamCalculatorDisplayTime.Value = TimeSpan.Parse(txtHoraVisualizacionCalculadora.Text);
                    pArray.Add(ParamCalculatorDisplayTime);
                    SqlParameter ParamUserModify = new SqlParameter();
                    ParamUserModify.ParameterName = "@UserModify";
                    ParamUserModify.SqlDbType = SqlDbType.NVarChar;
                    ParamUserModify.Value = Session["Username"];
                    pArray.Add(ParamUserModify);
                    SqlParameter ParamAddress = new SqlParameter();
                    ParamAddress.ParameterName = "@Address";
                    ParamAddress.SqlDbType = SqlDbType.NVarChar;
                    ParamAddress.Value = txtUbicacion1.Text;
                    pArray.Add(ParamAddress);
                    SqlParameter ParamMapa = new SqlParameter();
                    ParamMapa.ParameterName = "@Mapa";
                    ParamMapa.SqlDbType = SqlDbType.NVarChar;
                    ParamMapa.Value = imgMapa.ImageUrl.Replace("files/", "");
                    pArray.Add(ParamMapa);
                    SqlParameter ParamLatitude = new SqlParameter();
                    ParamLatitude.ParameterName = "@Latitude";
                    ParamLatitude.SqlDbType = SqlDbType.NVarChar;
                    ParamLatitude.Value = txtLatitud.Text;
                    pArray.Add(ParamLatitude);
                    SqlParameter ParamLength = new SqlParameter();
                    ParamLength.ParameterName = "@Length";
                    ParamLength.SqlDbType = SqlDbType.NVarChar;
                    ParamLength.Value = txtLongitud.Text;
                    pArray.Add(ParamLength);

                    SqlParameter ParamGamification = new SqlParameter();
                    ParamGamification.ParameterName = "@Gamification";
                    ParamGamification.SqlDbType = SqlDbType.Bit;
                    ParamGamification.Value = cbGamificacion.Checked;
                    pArray.Add(ParamGamification);

                    SqlParameter ParamCalculator = new SqlParameter();
                    ParamCalculator.ParameterName = "@Calculator";
                    ParamCalculator.SqlDbType = SqlDbType.Bit;
                    ParamCalculator.Value = cbCalculadora.Checked;
                    pArray.Add(ParamCalculator);

                    SqlParameter ParamSurvey = new SqlParameter();
                    ParamSurvey.ParameterName = "@Survey";
                    ParamSurvey.SqlDbType = SqlDbType.Bit;
                    ParamSurvey.Value = cbSurvey.Checked;
                    pArray.Add(ParamSurvey);

                    SqlParameter ParamGamificationTime = new SqlParameter();
                    ParamGamificationTime.ParameterName = "@GamificationTime";
                    ParamGamificationTime.SqlDbType = SqlDbType.Time;
                    ParamGamificationTime.Value = TimeSpan.Parse(txtGamificationTime.Text);
                    pArray.Add(ParamGamificationTime);

                    SqlParameter ParamSurveyTime = new SqlParameter();
                    ParamSurveyTime.ParameterName = "@SurveyTime";
                    ParamSurveyTime.SqlDbType = SqlDbType.Time;
                    ParamSurveyTime.Value = TimeSpan.Parse(txtSurveyTime.Text);
                    pArray.Add(ParamSurveyTime);

                    objData.ExecuteInsertSP("usp_InsertEvent", pArray);
                    Response.Redirect("Events.aspx?notificacion=1");
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                    lblMensaje.Visible = true;
                }
            }
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            ActivarCampos();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Events.aspx");
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                try
                {
                    CottonClub.Data objData = new Data();
                    SqlConnection objCnn = objData.Connection();
                    ArrayList pArray = new ArrayList();
                    SqlParameter ParamId = new SqlParameter();
                    ParamId.ParameterName = "@Id";
                    ParamId.SqlDbType = SqlDbType.Int;
                    ParamId.Value = Session["IdEvento"];
                    pArray.Add(ParamId);
                    SqlParameter ParamName = new SqlParameter();
                    ParamName.ParameterName = "@Name";
                    ParamName.SqlDbType = SqlDbType.Text;
                    ParamName.Value = txtNombreEvento.Text;
                    pArray.Add(ParamName);
                    SqlParameter ParamLocation = new SqlParameter();
                    ParamLocation.ParameterName = "@Location";
                    ParamLocation.SqlDbType = SqlDbType.Text;
                    ParamLocation.Value = txtUbicacion1.Text;
                    pArray.Add(ParamLocation);
                    SqlParameter ParamWelcomeEmail = new SqlParameter();
                    ParamWelcomeEmail.ParameterName = "@WelcomeEmail";
                    ParamWelcomeEmail.SqlDbType = SqlDbType.Text;
                    ParamWelcomeEmail.Value = txtCorreoBienvenida.Text;
                    pArray.Add(ParamWelcomeEmail);
                    SqlParameter ParamLogo = new SqlParameter();
                    ParamLogo.ParameterName = "@Logo";
                    ParamLogo.SqlDbType = SqlDbType.NVarChar;
                    ParamLogo.Value = imgLogo.ImageUrl.Replace("files/", "");
                    pArray.Add(ParamLogo);
                    SqlParameter ParamDate = new SqlParameter();
                    string strFecha = txtFechaEvento.Text;
                    ParamDate.ParameterName = "@Date";
                    ParamDate.SqlDbType = SqlDbType.Date;
                    DateTime dtmDate = DateTime.Parse(strFecha);
                    ParamDate.Value = dtmDate;
                    pArray.Add(ParamDate);
                    SqlParameter ParamCalculatorDisplayTime = new SqlParameter();
                    ParamCalculatorDisplayTime.ParameterName = "@CalculatorDisplayTime";
                    ParamCalculatorDisplayTime.SqlDbType = SqlDbType.Time;
                    ParamCalculatorDisplayTime.Value = TimeSpan.Parse(txtHoraVisualizacionCalculadora.Text);
                    pArray.Add(ParamCalculatorDisplayTime);
                    SqlParameter ParamActive = new SqlParameter();
                    ParamActive.ParameterName = "@Active";
                    ParamActive.SqlDbType = SqlDbType.Int;
                    int Activo = 0;
                    if (cbActivo.Checked == true)
                    {
                        Activo = 1;
                    }
                    ParamActive.Value = Activo;
                    pArray.Add(ParamActive);
                    SqlParameter ParamAddress = new SqlParameter();
                    ParamAddress.ParameterName = "@Address";
                    ParamAddress.SqlDbType = SqlDbType.NVarChar;
                    ParamAddress.Value = txtUbicacion1.Text;
                    pArray.Add(ParamAddress);
                    SqlParameter ParamMapa = new SqlParameter();
                    ParamMapa.ParameterName = "@Mapa";
                    ParamMapa.SqlDbType = SqlDbType.NVarChar;
                    ParamMapa.Value = imgMapa.ImageUrl.Replace("files/", "");
                    pArray.Add(ParamMapa);
                    SqlParameter ParamLatitude = new SqlParameter();
                    ParamLatitude.ParameterName = "@Latitude";
                    ParamLatitude.SqlDbType = SqlDbType.NVarChar;
                    ParamLatitude.Value = txtLatitud.Text;
                    pArray.Add(ParamLatitude);
                    SqlParameter ParamLength = new SqlParameter();
                    ParamLength.ParameterName = "@Length";
                    ParamLength.SqlDbType = SqlDbType.NVarChar;
                    ParamLength.Value = txtLongitud.Text;
                    pArray.Add(ParamLength);

                    SqlParameter ParamGamification = new SqlParameter();
                    ParamGamification.ParameterName = "@Gamification";
                    ParamGamification.SqlDbType = SqlDbType.Bit;
                    ParamGamification.Value = cbGamificacion.Checked;
                    pArray.Add(ParamGamification);

                    SqlParameter ParamCalculator = new SqlParameter();
                    ParamCalculator.ParameterName = "@Calculator";
                    ParamCalculator.SqlDbType = SqlDbType.Bit;
                    ParamCalculator.Value = cbCalculadora.Checked;
                    pArray.Add(ParamCalculator);

                    SqlParameter ParamSurvey = new SqlParameter();
                    ParamSurvey.ParameterName = "@Survey";
                    ParamSurvey.SqlDbType = SqlDbType.Bit;
                    ParamSurvey.Value = cbSurvey.Checked;
                    pArray.Add(ParamSurvey);

                    SqlParameter ParamGamificationTime = new SqlParameter();
                    ParamGamificationTime.ParameterName = "@GamificationTime";
                    ParamGamificationTime.SqlDbType = SqlDbType.Time;
                    ParamGamificationTime.Value = TimeSpan.Parse(txtGamificationTime.Text);
                    pArray.Add(ParamGamificationTime);

                    SqlParameter ParamSurveyTime = new SqlParameter();
                    ParamSurveyTime.ParameterName = "@SurveyTime";
                    ParamSurveyTime.SqlDbType = SqlDbType.Time;
                    ParamSurveyTime.Value = TimeSpan.Parse(txtSurveyTime.Text);
                    pArray.Add(ParamSurveyTime);

                    objData.ExecuteInsertSP("usp_UpdateEvent", pArray);
                    Response.Redirect("Events.aspx?notificacion=1");
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = ex.Message;
                    lblMensaje.Visible = true;
                }
            }
            /*
            txtNombreEvento.ReadOnly = true;
            txtUbicacion1.ReadOnly = true;
            txtCorreoBienvenida.ReadOnly = true;
            txtFechaEvento.ReadOnly = true;
            txtHoraVisualizacionCalculadora.ReadOnly = true;
            cbActivo.Enabled = false;
            btnGuardar.Visible = false;
            btnCancelar.Visible = false;
            btnEditar.Visible = true;
            SelectEventos(Convert.ToInt32(Session["IdEvento"]));
            */
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdEvento"];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteEvent", pArray);
            Response.Redirect("Events.aspx?notificacion=1");
        }

        protected void ActivarCampos()
        {
            txtNombreEvento.ReadOnly = false;
            txtUbicacion1.ReadOnly = false;
            txtCorreoBienvenida.ReadOnly = false;
            txtFechaEvento.ReadOnly = false;
            txtHoraVisualizacionCalculadora.ReadOnly = false;
            cbActivo.Enabled = true;
            cbGamificacion.Enabled = true;
            cbCalculadora.Enabled = true;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        public string ReemplazarCaracteres(string cadena)
        {
            string iniciales = "";
            string finales = "";
            iniciales = cadena.Replace("<", "&lt;");
            finales = iniciales.Replace(">", "&gt;");
            return finales;
        }

        protected void btnEnvioMail_Click(object sender, EventArgs e)
        {
            /*
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            //La cadena "servidor" es el servidor de correo que enviará tu mensaje
            // Crea el mensaje estableciendo quién lo manda y quién lo recibe
            MailMessage mensaje = new MailMessage(
               "contacto.emprende.negocio@gmail.com",
               "administrador.plataforma3@redaliat.mx",
               "prueba",
               txtCorreoBienvenida.Text);
            //Añade credenciales si el servidor lo requiere.
            NetworkCredential credentials = new NetworkCredential("contacto.emprende.negocio@gmail.com", "www.google.com.mx", "");
            client.Credentials = credentials;
            try
            {
                //client.Send(mensaje);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error en envio de correo: " + ex.Message;
                lblMensaje.Visible = true;
            }
            */
            Response.Redirect("Email.aspx");
        }
    }
}