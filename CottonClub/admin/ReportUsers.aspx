﻿<%@ Page Title="Momentos Bayer | Reporte Usuarios" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="ReportUsers.aspx.cs" Inherits="CottonClub.admin.ReportUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
			    
			    <div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-6">
			    		<h3 class="section-header">Reporte Usuarios</h3>
				    </div>
				    <div class="col-xs-12 col-sm-6 col-md-6 text-right">
			    		<asp:DropDownList ID="ddlEvento" runat="server" class="select-event" AutoPostBack="True"></asp:DropDownList>
				    </div>
			    </div>
			    
		        <div class="panel panel-default">				        
			        
		            <div class="panel-body">
		                  		                  
						<div class="row table-responsive">
							<asp:GridView class="table no-more-tables table-listing" ID="gvwUsuarios" runat="server" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="gvwUsuarios_PageIndexChanging" AutoGenerateColumns="False">
								<AlternatingRowStyle BackColor="White" />
								<Columns>
                                    <asp:BoundField DataField="Nombres" HeaderText="Nombres" />
                                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" />
                                    <asp:BoundField DataField="Correo" HeaderText="Correo" />
                                    <asp:BoundField DataField="PIN" HeaderText="PIN" />
                                    <asp:BoundField DataField="URLRegistro" HeaderText="URLRegistro" />
                                    <asp:BoundField DataField="Puntos" HeaderText="Puntos" />
                                    <asp:BoundField DataField="Acompaniantes" HeaderText="Acompaniantes" />
                                    <asp:BoundField DataField="Actividades" HeaderText="Actividades" />
                                    <asp:BoundField DataField="Calculadoras" HeaderText="Calculadoras" />
                                </Columns>
								<EditRowStyle BackColor="#2461BF" />
								<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
								<RowStyle BackColor="#EFF3FB" />
								<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
								<SortedAscendingCellStyle BackColor="#F5F7FB" />
								<SortedAscendingHeaderStyle BackColor="#6D95E1" />
								<SortedDescendingCellStyle BackColor="#E9EBEF" />
								<SortedDescendingHeaderStyle BackColor="#4870BE" />
							</asp:GridView>
						</div>
		                  
		            </div>
		            
		        </div>
		        
		        <div class="row text-right">
			        <div class="col-xs-12">
			        	<div id="strJson" runat="server" style="display:none"></div>
						<asp:Button ID="btnExportar" runat="server" CssClass="btn btn-success" OnClick="btnExportar_Click" Text="Exportar" />
			        </div>
		        </div>
		        
	    	</div>
		</div>
	</div>
    
</asp:Content>



