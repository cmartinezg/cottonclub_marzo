﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub.admin
{
    public partial class CalcProducts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SelectData();

        }
        protected void SelectData()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamFrase = new SqlParameter();
            ParamFrase.ParameterName = "@frase";
            ParamFrase.SqlDbType = SqlDbType.Text;
            ParamFrase.Value = txtBuscar.Text;
            pArray.Add(ParamFrase);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcProductos", pArray);
            if (ds.Tables[0] != null)
            {
                gvwDiary.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwDiary.DataSource = ds;
                gvwDiary.DataBind();
            }
        }
        protected void gvwDiary_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Label lblId = gvwDiary.Rows[e.NewSelectedIndex].FindControl("lblGvwId") as Label;
            Response.Redirect("CalcProductsDetail.aspx?action=0&id=" + lblId.Text);
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CalcProductsDetail.aspx?action=1");
        }
        protected void gvwDiary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwDiary.PageIndex = e.NewPageIndex;
            SelectData();
        }
        protected void ddlMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectData();
        }
    }
}