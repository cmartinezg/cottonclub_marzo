﻿<%@ Page Title="Momentos Bayer | Regalos Detalle" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="CalcGiftsDetail.aspx.cs" Inherits="CottonClub.admin.CalcGiftsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <h3 class="section-header">Regalo Detalle</h3>

                <div class="panel panel-default">

                    <div class="panel-body">

                        <div class="form-horizontal">

                            <div class="form-group" runat="server">
                                <label for="txtNombre" class="col-sm-2 control-label">Nombre</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtNombre" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" runat="server">
                                <label for="txtCantidad" class="col-sm-2 control-label">Cantidad</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtCantidad" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" runat="server">
                                <label for="txtSacos" class="col-sm-2 control-label">Sacos</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtSacos" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" runat="server">
                                <label for="txtPrecio" class="col-sm-2 control-label">Precio</label>
                                <div class="col-sm-10">
                                    <asp:TextBox class="form-control" ID="txtPrecio" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            				
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <asp:Button ID="btnAgregar" runat="server" class="btn btn-success" Text="Agregar" OnClick="btnAgregar_Click" />
                                    <asp:Button ID="btnGuardar" class="btn btn-primary" runat="server" Text="Guardar" Visible="False" OnClick="btnGuardar_Click" />
                                    <asp:Button ID="btnBorrar" class="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnBorrar_Click" OnClientClick="return confirm('¿Desea eliminar este registro?');" />
                                    <asp:Button ID="btnCancelar" class="btn btn-danger" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
