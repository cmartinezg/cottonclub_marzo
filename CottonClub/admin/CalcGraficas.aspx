﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="CalcGraficas.aspx.cs" Inherits="CottonClub.admin.CalcGraficas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	
	<style>
		.col-centered { float: none;margin: 0 auto;}
		ul li {list-style: none;}
		.box {display: inline-block; width: 10px; height: 10px;}
	</style>
	
	<div class="clearfix"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
			    
			    <div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-6">
			    		<h3 class="section-header">Reporte Calculadora</h3>
				    </div>
				    <div class="col-xs-12 col-sm-6 col-md-6 text-right">
			    		<div class="form-group">
							<asp:DropDownList ID="ddlEvento" style="width:200px;" class="select-event" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvento_SelectedIndexChanged"></asp:DropDownList>
						</div>   
				    </div>
			    </div>
			    
			    <div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-6">
					    http://cottonclub.motti.mx/admin/CalcGraficas.aspx/
                        <input type="text" class="form-control" name="query" value="GraficaDistribuior?eventid=1" id="txt-post" />
                        <input id="bt-post" type="submit" value="Enviar" />
                        <!--<asp:TextBox ID="txtpost" runat="server"  class="form-control" >GraficaDistribuior?eventid=1</asp:TextBox>-->
                        <!--<asp:Button ID="btpost" runat="server" class="btn btn-info" Text="ENVIAR" />-->
					    <div class="result" style="padding:20px;" id="log-post">
						    <pre></pre>
					    </div>
                        <div id="strJson" runat="server"></div>
				    </div>
			    </div>
			  	                		    
			    <div class="row">
				    
				    <div class="col-xs-12">
				    
					    <ul class="nav nav-tabs" role="tablist">
						  	<li class="active"><a href="#tab-distribuitors" aria-controls="tab-charts" role="tab" data-toggle="tab">Distribuidores</a></li>
						  	<li><a href="#tab-packs" aria-controls="tab-charts" role="tab" data-toggle="tab">Paquetes</a></li>
						</ul>
						
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-distribuitors">
								<div class="canvas-holder">
									<div class="col-xs-12">
										<h3 class="title"></h3>
										<h4 class="amount"></h4>
										<div class="row">
											<div class="col-xs-12 col-sm-8">
												<canvas id="charts-distribuitors"></canvas>
											</div>
											<div class="col-xs-12 col-sm-4" id="labels-distribuitors"></div>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="tab-packs">
								<div class="canvas-holder">
									<div class="col-xs-12">
										<h3 class="title"></h3>
										<h4 class="amount"></h4>
										<div class="row">
											<div class="col-xs-12 col-sm-8">
												<canvas id="charts-packs"></canvas>
											</div>
											<div class="col-xs-12 col-sm-4" id="labels-packs"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					
				    </div>
				    
			    </div>
			  
	    	</div>
		</div>
	</div>
	
	
    <script type="text/javascript">
	    
	    var colors = ["#A5BC75",  "#7f7f7f", "#443025", "#9FCABB", "#113a4e", "#113A4E", "#3EA4E8"];
	    var denabled = false;
	    var penabled = false;
	    var cdata    = [];
	    var chart;
	    
	    
        $(document).ready(function() {
            distributor();
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  	var target = $(e.target).attr("href").split("#")[1]
		  	if (target == "tab-packs") {
			  	products();
		  	} else if (target == "tab-distribuitors") {
			  	distributor();
		  	}
		});
        
        function products() {
	        var eventid = $("#ContentPlaceHolder1_ddlEvento").val();
            $.ajax({type: "POST", url: 'CalcGraficas.aspx/GraficaProdcutos?eventid=' + eventid, contentType: "application/json; charset=utf-8", dataType: "json",
                success: function (doc) {
	                penabled = true;
                    charts(doc, "packs", "CantProductos", "Total", "Por No. Paquetes", "SEMILLA + % PRODUCTOS", gotoproducts);
                },
                error: function (error) {
                    alert('Ocurrió un error al cargar la grafica');
                }
            })
        }

        
        function details_products(qty) {
	        var eventid = $("#ContentPlaceHolder1_ddlEvento").val();
            var param   = {};
            param.cantProductos = qty;
            param.eventId 		= $("#ContentPlaceHolder1_ddlEvento").val();
            $.ajax({type: "POST", url: 'CalcGraficas.aspx/GraficaDetalleProdcutos?eventid=' + eventid, data: JSON.stringify(param), contentType: "application/json; charset=utf-8", dataType: "json",
                success: function (doc) {
	                penabled = true;
	                charts(doc, "packs", "Productos", "Total", "<a href='#' onclick='products(); return false;'>↩</a> Combinación " + qty + " Productos", "%");
                },
                error: function (error) {
                    alert('Ocurrió un error al cargar la grafica');
                }
            })
        }

        function distributor() {
	        var eventid = $("#ContentPlaceHolder1_ddlEvento").val();
            $.ajax({type: "POST", url: 'CalcGraficas.aspx/GraficaDistribuior?eventid=' + eventid, contentType: "application/json; charset=utf-8",  dataType: "json",
                success: function (doc) {
	                denabled = true;
	                charts(doc, "distribuitors", "Distribuidor", "Total", "Distribuidores", "DISTRIBUIDOR %", gotodistribuitor);
                },
                error: function (error) {
                    alert('Ocurrió un error al cargar la grafica');
                }
            })
        }	
        
        function details_distribuitor(id) {
	        var eventid = $("#ContentPlaceHolder1_ddlEvento").val();
	        //alert('CalcGraficas.aspx/GraficaProducto?eventid=' + eventid + "&distribuidorid=" + id)
            $.ajax({type: "POST", url: 'CalcGraficas.aspx/GraficaProducto?eventid=' + eventid + "&distribuidorid=" + id, contentType: "application/json; charset=utf-8", dataType: "json",
                success: function (doc) {
	                penabled = true;
	                charts(doc, "distribuitors", "Nombre", "Column1", "<a href='#' onclick='distributor(); return false;'>↩</a> Distribuidor " + id, "%");
                },
                error: function (error) {
                    alert('Ocurrió un error al cargar la grafica');
                }
            })
        }	
		
		function charts(records, name, label, value, title, tooltip, click) {
			
			if (chart != null) {
				chart.destroy();
				chart = null;
				$("#labels-" + name).html("");
			}
			
			var records = JSON.parse(records.d).Table	
			var labels  = [];
			var dataset = [];
			var total   = 0;	
			
			var ul = $("<ul>");		
						
			for (var i in records) {
				var lb = tooltip.replace("%", records[i][label])
				var cl = colors[i%colors.length];
				var li = $("<li>").append( "<div class='box' style='background-color:" + cl + "'></div> " + lb);
				labels.push(lb)
				dataset.push(records[i][value])
				total += records[i][value];
				
				ul.append(li)
			}
			
			total = total.toFixed(2);
			
			$("#labels-" + name).append(ul);
			
			var data = {
			    labels: labels,
			    datasets: [ { label: title, borderWidth: 1, data: dataset, backgroundColor: colors } ]
			};
						
			var options = {
			    tooltips: {
			        enabled: true,
			        mode: 'label',
			        callbacks: {
			            title: function(tooltipItems, data) {
				            var idx = tooltipItems[0].index;
				            return tooltip.replace("%", records[idx][label]);
			            }, 
			            label: function(tooltipItem, data) {
			                var idx = tooltipItem.index;
			                return "$" + commas(records[idx][value])
			            }
			        }
			    },
			    legend: { display: false, position: 'right', fullWidth:true, label : {boxWidth:15} },
				yAxes: [ { ticks: { beginAtZero: true, stepSize: 500000, userCallback:currency } } ] 
			}
			
			var ctx = document.getElementById("charts-" + name)
			
			
			cdata = records;
			chart = new Chart(ctx.getContext("2d"), {
			    type: 'pie',
			    data: data,
			    options: options
			});
			
			if (click != null) {
				ctx.onclick = click;
			}
			
			jQuery("h3.title").html(title);	
			jQuery("h4.amount").html("<strong>TOTAL:</strong> $" + commas(total))		
		}
		
		function gotoproducts(e) {
			var active = chart.getElementsAtEvent(e);
            var index  = active[0]._index;
            var prods  = cdata[index]["CantProductos"];
            if (prods != 0 && prods != undefined) {
            	details_products(prods);
            }
		}
		
		function gotodistribuitor(e) {
			var active = chart.getElementsAtEvent(e);
            var index  = active[0]._index;
            var prods  = cdata[index]["IdDistribuidor"];
            if (prods != 0 && prods != undefined) {
            	details_distribuitor(prods);
            }
		}
		
		function currency(value, index, values) {
			value = value.toString();
			value = value.split(/(?=(?:...)*$)/);
			value = value.join('.');
			return '$' + value;
		}
		
		function commas(x) {
		    var parts = x.toString().split(".");
		    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    return parts.join(".");
		}
		
		
		$("#bt-post").click(function($e){
			$.ajax({type: "POST", url: "CalcGraficas.aspx/" + $("#txt-post").val(), contentType: "application/json; charset=utf-8", dataType: "json",
                success: function (doc) {
	                $("#log-post pre").text( JSON.stringify(doc) )
                },
                error: function (error) {
                    alert('Ocurrió un error al cargar la grafica');
                }
            })
            $e.preventDefault();
            return false;
		})

		function getNameWebService() {
		    var loc = window.location;
		    var lastpart = loc.pathname.substring(loc.pathname.lastIndexOf('/') + 1, loc.length);
		    return lastpart;
		}
	</script>
    
</asp:Content>
