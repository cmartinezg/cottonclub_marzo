﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub
{
    public partial class QRCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try {
                string filePath = "codigoQR/qrcode_" + Session["IdGamificacion"].ToString() + ".png";
                Response.ContentType = "image/png";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
                Response.WriteFile(filePath);
                Response.End();
            }
            catch (Exception) { }
            /*imgCodigoQR.ImageUrl= "codigoQR/qrcode_" + Session["IdGamificacion"].ToString() + ".png";
            imgCodigoQR.Width = 700;
            imgCodigoQR.Height = 700;*/
        }
    }
}