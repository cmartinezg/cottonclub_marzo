﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Net;

namespace CottonClub.admin
{
    public partial class ReportProducts : System.Web.UI.Page
    {
        string json = "";
        string strItemToSelect = "0";

        [STAThreadAttribute]
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                FillEvento();
            }
            SelectProductos();
            gvwProductos.Visible = true;
            SelectSemillas();
            gvwSeeds.Visible = true;
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void SelectProductos()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.BigInt;
            int parametro;
            if (!IsPostBack)
            {
                parametro = Convert.ToInt32(strItemToSelect);
            }
            else
            {
                parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            }
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_ReportProducts", pArray);
            if (ds.Tables[0] != null)
            {
                //gvwUsuarios.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwProductos.DataSource = ds;
                gvwProductos.DataBind();
            }
            //json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
            //strJson.InnerHtml = json;
        }

        protected void SelectSemillas()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamEvento = new SqlParameter();
            ParamEvento.ParameterName = "@IdEvent";
            ParamEvento.SqlDbType = SqlDbType.BigInt;
            int parametro;
            if (!IsPostBack)
            {
                parametro = Convert.ToInt32(strItemToSelect);
            }
            else
            {
                parametro = Convert.ToInt32(ddlEvento.SelectedItem.Value.ToString());
            }
            ParamEvento.Value = parametro;
            pArray.Add(ParamEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_ReportSeeds", pArray);
            if (ds.Tables[0] != null)
            {
                //gvwUsuarios.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwSeeds.DataSource = ds;
                gvwSeeds.DataBind();
            }
            //json = JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
            //strJson.InnerHtml = json;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            ExportToCSV_Productos();
        }

        private void ExportGridToExcel()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            System.Web.UI.Page page = new System.Web.UI.Page();
            HtmlForm form = new HtmlForm();
            gvwProductos.EnableViewState = false;

            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(gvwProductos);
            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Productos_" + DateTime.Now + ".xls");
            //Response.Charset = "UTF-8";
            Response.Charset = "";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        }

        private void ExportGridToExcelSeeds()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            System.Web.UI.Page page = new System.Web.UI.Page();
            HtmlForm form = new HtmlForm();
            gvwSeeds.EnableViewState = false;

            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(gvwSeeds);
            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=Semillas_" + DateTime.Now + ".xls");
            //Response.Charset = "UTF-8";
            Response.Charset = "";
            //Response.ContentEncoding = Encoding.Default;
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Write(sb.ToString());
            Response.End();
        }

        public void ExportToCSV_Semillas()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Semillas_" + DateTime.Now + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            gvwSeeds.AllowPaging = false;
            gvwSeeds.DataBind();
            gvwSeeds.EnableViewState = false;
            for (int index = 0; index < gvwSeeds.Columns.Count; index++)
            {
                sBuilder.Append(WebUtility.HtmlDecode(gvwSeeds.Columns[index].HeaderText) + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < gvwSeeds.Rows.Count; i++)
            {
                for (int k = 0; k < gvwSeeds.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(WebUtility.HtmlDecode(gvwSeeds.Rows[i].Cells[k].Text).Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }

        public void ExportToCSV_Productos()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Productos_" + DateTime.Now + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sBuilder = new System.Text.StringBuilder();
            gvwProductos.AllowPaging = false;
            gvwProductos.DataBind();
            gvwProductos.EnableViewState = false;
            for (int index = 0; index < gvwProductos.Columns.Count; index++)
            {
                sBuilder.Append(WebUtility.HtmlDecode(gvwProductos.Columns[index].HeaderText) + ',');
            }
            sBuilder.Append("\r\n");
            for (int i = 0; i < gvwProductos.Rows.Count; i++)
            {
                for (int k = 0; k < gvwProductos.HeaderRow.Cells.Count; k++)
                {
                    sBuilder.Append(WebUtility.HtmlDecode(gvwProductos.Rows[i].Cells[k].Text).Replace(",", "") + ",");
                }
                sBuilder.Append("\r\n");
            }
            Response.Output.Write(sBuilder.ToString());
            Response.Flush();
            Response.End();
        }

        protected void gvwProductos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwProductos.PageIndex = e.NewPageIndex;
            SelectProductos();
        }

        protected void gvwSeeds_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }
        [STAThreadAttribute]
        protected void btnExportSeeds_Click(object sender, EventArgs e)
        {
            ExportToCSV_Semillas();
        }

        protected void gvwSeeds_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwSeeds.PageIndex = e.NewPageIndex;
            SelectSemillas();
        }
    }
}