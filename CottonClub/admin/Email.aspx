﻿<%@ Page Title="Momentos Bayer | Mailing" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="Email.aspx.cs" Inherits="CottonClub.admin.Email" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				
				<h3 class="section-header">Envío de correo</h3>
				
				<div class="panel panel-default">
					
					<div class="panel-body">
						
						<div class="form-group">
							<div class="col-sm-12">
								<p>El correo se enviara a los siguientes usuarios</p>
							</div>
						</div>
						
						<div class="row table-responsive">
							
							<asp:GridView class="table no-more-tables table-listing" ID="gvwUsers" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None" Width="100%" PageSize="3" ShowFooter="True" ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvwUsers_PageIndexChanging" OnSelectedIndexChanging="gvwUsers_SelectedIndexChanging">
								<AlternatingRowStyle BackColor="White" />
								<Columns>
									<asp:TemplateField Visible="False">
									<ItemTemplate>
										<asp:Label ID="lblGvwId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
									</ItemTemplate>
									</asp:TemplateField>
									<asp:ImageField DataImageUrlField="Photo" HeaderText="Fotografia">
										<ControlStyle Height="40px" Width="40px" />
									</asp:ImageField>
									<asp:BoundField ApplyFormatInEditMode="True" DataField="fullname" HeaderText="Nombre" />
									<asp:BoundField DataField="CorreoElectronico" HeaderText="Correo Electronico" />
								</Columns>
								<EditRowStyle BackColor="#2461BF" />
								<FooterStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="Black" />
								<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
								<PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="paginate" />
								<RowStyle BackColor="#EFF3FB" />
								<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
								<SortedAscendingCellStyle BackColor="#F5F7FB" />
								<SortedAscendingHeaderStyle BackColor="#6D95E1" />
								<SortedDescendingCellStyle BackColor="#E9EBEF" />
								<SortedDescendingHeaderStyle BackColor="#4870BE" />
							</asp:GridView>
							
						</div>
						
						<div class="form-group">
							<div class="col-sm-12 text-right">
								<asp:Button ID="btnEnvioMail" class="btn btn-success" runat="server" Text="Confirmar envio mailing" OnClick="btnEnvioMail_Click" onclientclick="return confirm('Estás a punto de enviar un correo masivo, ¿deseas continuar?');"/>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-12">
								<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>