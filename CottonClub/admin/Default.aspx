﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CottonClub.Default1" %>
<!DOCTYPE HTML>
<html lang="en-GB">
	<head>
		<meta charset="UTF-8" />
		<title>Momentos Bayer</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
		<link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/login.css" rel="stylesheet" />
	</head>
	<body>
		<div class="jumbotron vertical-center">
			<div class="container">
				<div class="col-sm-6 col-sm-offset-3 panel panel-default">
					<div class="panel-body">
						<form id="main" runat="server" method="post" class="text-center">
							<asp:Image ID="imgLogo" runat="server" ImageUrl="~/admin/assets/img/logo_momentos_home.png" Width="400px"  class="img-responsive img-center"/>
							<h1>PLATAFORMA EVENTOS</h1>
							<img src="assets/img/logo_bayer.png" width="75y" class="img-responsive img-center"/>
							<br />
							<div class="form-horizontal">
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox name="txt_login_username" id="txt_login_username" class="form-control input-lg" placeholder="Usuario" runat="server" required autofocus></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<asp:TextBox name="txt_login_pass" id="txt_login_pass" type="password"  class="form-control input-lg" placeholder="Contraseña" runat="server" required></asp:TextBox>
										<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									</div>
								</div>
								<div class="form-group text-center">
									<asp:Button runat="server"  id="login_toggle" Text="Entrar" class="btn btn-primary btn-cons btn-lg" OnClick="login_toggle_Click"/>
								    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="lblMensaje" class="alert alert-danger" runat="server" Text="" Visible="false"></asp:Label>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>