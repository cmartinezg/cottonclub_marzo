﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CottonClub.admin
{
    public partial class CalcGiftsDetail : System.Web.UI.Page
    {
        const string ID_SESION = "IdRegalo";
        protected void Page_Load(object sender, EventArgs e)
        {
            int IdAccion = 0;
            int Id = 0;
            //lblMensaje.Visible = false;
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                Id = Convert.ToInt32(Request.QueryString["id"]);
                Session[ID_SESION] = Id;
                if (IdAccion == 1)
                {
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    btnGuardar.Visible = true;
                    btnAgregar.Visible = false;
                    SelectData(Id);
                }

            }

        }
        protected void LimpiaCampos()
        {
            txtNombre.Text = "";
            txtCantidad.Text = "";
            txtSacos.Text = "";
            txtPrecio.Text = "";
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
        }
        protected void SelectData(int Id)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@Id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = Id;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificRegalo", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombre.Text = ds.Tables[0].Rows[0]["Nombre"].ToString();
                txtCantidad.Text = ds.Tables[0].Rows[0]["Cantidad"].ToString();
                txtSacos.Text = ds.Tables[0].Rows[0]["Sacos"].ToString();
                txtPrecio.Text = ds.Tables[0].Rows[0]["Precio"].ToString();
            }
        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();

                ArrayList pArray = ReadParams();

                objData.ExecuteInsertSP("usp_InsertRegalo", pArray);
                Response.Redirect("CalcGifts.aspx?notificacion=1");
            }
        }
        private ArrayList ReadParams()
        {
            ArrayList pArray = new ArrayList();
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@Nombre";
            param1.SqlDbType = SqlDbType.NVarChar;
            param1.Value = txtNombre.Text;
            pArray.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@Cantidad";
            param2.SqlDbType = SqlDbType.Int;
            param2.Value = int.Parse(txtCantidad.Text);
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@Sacos";
            param3.SqlDbType = SqlDbType.Int;
            param3.Value = txtSacos.Text;
            pArray.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "@Precio";
            param4.SqlDbType = SqlDbType.Decimal;
            param4.Precision = 18;
            param4.Scale = 2;
            param4.Value = Decimal.Parse(txtPrecio.Text);
            pArray.Add(param4);

            return pArray;
        }
        private bool PageIsValid()
        {

            if (txtNombre.Text == "")
            {
                lblMensaje.Text = "El Nombre del regalo es un dato requerido";
                return false;
            }

            if (string.IsNullOrWhiteSpace(txtCantidad.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad";
                return false;
            }
            else
            {
                int i = 0;
                if(!int.TryParse(txtCantidad.Text,out i))
                {
                    lblMensaje.Text = "La cantidad ingresada no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtSacos.Text))
            {
                lblMensaje.Text = "Debe ingresar una cantidad de sacos";
                return false;
            }
            else
            {
                int i = 0;
                if (!int.TryParse(txtSacos.Text, out i))
                {
                    lblMensaje.Text = "La cantidad de sacos ingresada no tiene un formato correcto";
                    return false;
                }
            }

            if (string.IsNullOrWhiteSpace(txtPrecio.Text))
            {
                lblMensaje.Text = "Debe ingresar un precio";
                return false;
            }
            else
            {
                decimal i = 0;
                if (!decimal.TryParse(txtPrecio.Text, out i))
                {
                    lblMensaje.Text = "El precio ingresado no tiene un formato correcto";
                    return false;
                }
            }

            lblMensaje.Visible = true;
            return true;
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CultureInfo culture = new CultureInfo("es-MX");
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = ReadParams();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session[ID_SESION];
                pArray.Add(ParamId);
                
                objData.ExecuteInsertSP("usp_UpdateRegalo", pArray);
                Response.Redirect("CalcGifts.aspx?notificacion=1");
            }
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("CalcGifts.aspx");
        }
        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session[ID_SESION];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteRegalo", pArray);
            Response.Redirect("CalcGifts.aspx?notificacion=1");
        }
    }
}