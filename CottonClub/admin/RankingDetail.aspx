﻿<%@ Page Title="Ranking Detail" Language="C#" MasterPageFile="~/admin/Main.Master" AutoEventWireup="true" CodeBehind="RankingDetail.aspx.cs" Inherits="CottonClub.RankingDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="grid simple ">
                            <div class="grid-body no-border">
                                  <h3><span class="semi-bold">Usuarios<asp:Label ID="lblTitulo" runat="server"></asp:Label>
                                      </span></h3>
                                  <asp:GridView class="table table-bordered no-more-tables" ID="gvwRankingDetail" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                                      <AlternatingRowStyle BackColor="White" />
                                      <Columns>
                                          <asp:TemplateField Visible="False">
                                              <ItemTemplate>
                                                  <asp:Label ID="lblGvwId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:BoundField ApplyFormatInEditMode="True" DataField="FirstName" HeaderText="Nombre" />
                                          <asp:BoundField ApplyFormatInEditMode="True" DataField="LastName" HeaderText="Apellido" />
                                          <asp:CommandField ButtonType="Button" DeleteImageUrl="~/images/icono_detalle_2020.png" EditImageUrl="~/images/icono_detalle_2020.png" InsertImageUrl="~/images/icono_detalle_2020.png" NewImageUrl="~/images/icono_detalle_2020.png" SelectImageUrl="~/images/icono_detalle_2020.png" SelectText="Gamificacion" ShowSelectButton="True" UpdateImageUrl="~/images/icono_detalle_2020.png" >
                                          <ControlStyle CssClass="btn btn-success" />
                                          </asp:CommandField>
                                      </Columns>
                                      <EditRowStyle BackColor="#2461BF" />
                                      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                      <RowStyle BackColor="#EFF3FB" />
                                      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                      <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                      <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                      <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                      <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                  </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>