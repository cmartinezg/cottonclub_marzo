﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace CottonClub
{
    public partial class UserDetail : System.Web.UI.Page
    {
        int IdAccion = 0;
        int IdUsuario = 0;
        string guid = string.Empty;
        string strItemToSelect = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                IdAccion = Convert.ToInt32(Request.QueryString["action"]);
                IdUsuario = Convert.ToInt32(Request.QueryString["id"]);
                FillEvento();
                FillDistribuidor();
                if (IdUsuario != 0)
                {
                    Session["IdUsuario"] = IdUsuario;
                    FillDiary();
                    FillCompanions();
                    FillPDFs();
                    SelectUsuarios(IdUsuario);
                }
                if (IdAccion != 0)
                {
                    Session["IdAccion"] = IdAccion;
                }
                else
                {
                    Session["IdAccion"] = 0;
                }
                if (Convert.ToInt32(Session["IdAccion"]) == 1)
                {
                    Session["IdUsuario"] = 0;
                    LimpiaCampos();
                    btnGuardar.Visible = false;
                    btnBorrar.Visible = false;
                }
                else
                {
                    ActivarCampos();
                    btnCancelar.Visible = true;
                    btnAgregar.Visible = false;
                }
            }
            else
            {
                if (Session["IdAccion"].ToString() != "1")
                {
                    SelectUsuarios(IdUsuario);
                    FillDiary();
                    FillCompanions();
                    FillPDFs();
                }
                  
            }
        }



        protected void FillDistribuidor()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectDistributor", pArray);
            if (ds.Tables[0] != null)
            {
                ddlDistribuidor.DataSource = ds.Tables[0].DefaultView;
                ddlDistribuidor.DataTextField = "Decription";
                ddlDistribuidor.DataValueField = "Id";
                ddlDistribuidor.DataBind();
            }
        }

        protected void FillEvento()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                ddlEvento.DataSource = ds.Tables[0].DefaultView;
                ddlEvento.DataTextField = "Name";
                ddlEvento.DataValueField = "Id";
                ddlEvento.DataBind();

                DataRow[] drActiveItem = ds.Tables[0].Select("Active='SI'");

                if (drActiveItem.Length > 0)
                {
                    string strItemToSelect = drActiveItem[0]["Id"].ToString();
                    ddlEvento.Items.FindByValue(strItemToSelect).Selected = true;
                }
            }
        }

        protected void FillDiary()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamIdUser = new SqlParameter();
            ParamIdUser.ParameterName = "@IdUser";
            ParamIdUser.SqlDbType = SqlDbType.Int;
            ParamIdUser.Value = Session["IdUsuario"];
            pArray.Add(ParamIdUser);
            SqlParameter ParamIdEvento = new SqlParameter();
            ParamIdEvento.ParameterName = "@IdEvento";
            ParamIdEvento.SqlDbType = SqlDbType.Int;
            if (ddlEvento.Items.Count > 0)
            {
                ParamIdEvento.Value = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
            }
            else
            {
                ParamIdEvento.Value = Convert.ToInt32(0);
            }
            pArray.Add(ParamIdEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUserDiary", pArray);
            if (ds.Tables[0] != null)
            {
                gvwUserDiary.DataSource = ds;
                gvwUserDiary.DataBind();
            }
        }


        protected void FillPDFs()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamIdUser = new SqlParameter();
            ParamIdUser.ParameterName = "@IdUsuario";
            ParamIdUser.SqlDbType = SqlDbType.Int;
            ParamIdUser.Value = Session["IdUsuario"];
            pArray.Add(ParamIdUser);
            SqlParameter ParamIdEvento = new SqlParameter();
            ParamIdEvento.ParameterName = "@IdEvento";
            ParamIdEvento.SqlDbType = SqlDbType.Int;
            if (ddlEvento.Items.Count > 0)
            {
                ParamIdEvento.Value = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
            }
            else {
                ParamIdEvento.Value = Convert.ToInt32(0);
            }
            pArray.Add(ParamIdEvento);
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectCotizaciones", pArray);
            if (ds.Tables[0] != null)
            {
                gvwPDFs.DataSource = ds;
                gvwPDFs.DataBind();
            }
        }

        protected void FillCompanions()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamIdUser = new SqlParameter();
            ParamIdUser.ParameterName = "@IdUser";
            ParamIdUser.SqlDbType = SqlDbType.Int;
            ParamIdUser.Value = IdUsuario;
            pArray.Add(ParamIdUser);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUserCompanions", pArray);
            if (ds.Tables[0] != null)
            {
                gvwCompanions.DataSource = ds;
                gvwCompanions.DataBind();
            }
        }

        protected void SelectUsuarios(int IdUsuario)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = Session["IdUsuario"];
            pArray.Add(ParamUsername);

            SqlParameter ParamEvent = new SqlParameter();
            ParamEvent.ParameterName = "@idEvento";
            ParamEvent.SqlDbType = SqlDbType.BigInt;
            if(!IsPostBack)
            {
                ParamEvent.Value = 0;
            }
            else
            {
                ParamEvent.Value = Convert.ToInt32(ddlEvento.SelectedValue.ToString());
            }
            
            pArray.Add(ParamEvent);

            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificUsersEvents", pArray);
            if (ds.Tables[0] != null)
            {
                //asignamos el valor del texto a nivel cliente, porque con la propiedad text no se mostraba...
                //txtContrasenia.Text = ds.Tables[0].Rows[0]["Password"].ToString();
                //txtContraseniaConfirmar.Text = ds.Tables[0].Rows[0]["Password"].ToString();

                txtContrasenia.Attributes.Add("value", Encrypt.DecryptPassword(ds.Tables[0].Rows[0]["Password"].ToString(), ds.Tables[0].Rows[0]["CorreoElectronico"].ToString()));
                txtContraseniaConfirmar.Attributes.Add("value", Encrypt.DecryptPassword(ds.Tables[0].Rows[0]["Password"].ToString(), ds.Tables[0].Rows[0]["CorreoElectronico"].ToString()));

                txtNombres.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                txtApellidos.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                txtEmail.Text = ds.Tables[0].Rows[0]["CorreoElectronico"].ToString();
                if (ds.Tables[0].Rows[0]["Administrador"].ToString() == "1")
                {
                    cbAdministrador.Checked = true;
                }
                else
                {
                    cbAdministrador.Checked = false;
                }
                txtPuntos.Text = ds.Tables[0].Rows[0]["Puntaje"].ToString();
                txtPIN.Text = ds.Tables[0].Rows[0]["PIN"].ToString();
                imgFoto.ImageUrl = ds.Tables[0].Rows[0]["Photo"].ToString();
                if (ds.Tables[0].Rows[0]["Photo"].ToString().Contains("sinimagen.png"))
                    imgFoto.ImageUrl = null;
                txtAcompaniantes.Text= ds.Tables[0].Rows[0]["Companions"].ToString();
                txtTelefono.Text= ds.Tables[0].Rows[0]["Telefono"].ToString();
                txtOcupacion.Text = ds.Tables[0].Rows[0]["Ocupacion"].ToString();

                if (!ds.Tables[0].Rows[0]["Estado"].ToString().Equals(""))
                {
                    string strItemToSelect = ds.Tables[0].Rows[0]["Estado"].ToString();
                    ddlEstado.Items.FindByValue(strItemToSelect).Selected = true;
                }
                txtMunicipio.Text = ds.Tables[0].Rows[0]["Municipio"].ToString();

                string strItemToSelectDistributor = ds.Tables[0].Rows[0]["IdDistribuidor"].ToString();
                ddlDistribuidor.Items.FindByValue(strItemToSelectDistributor).Selected = true;
            }
        }
        protected void LimpiaCampos()
        {
            txtNombres.ReadOnly = false;
            txtContrasenia.ReadOnly = false;
            txtApellidos.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtPuntos.ReadOnly = true;
            txtNombres.Text = "";
            txtContrasenia.Text = "";
            txtApellidos.Text = "";
            txtEmail.Text = "";
            txtPuntos.Text = "";
            cbAdministrador.Checked = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
            txtPuntos.Visible = false;
            txtPIN.Visible = false;
            txtAcompaniantes.Visible = false;
            lblAcompaniantes.Visible = false;
            lblCalculadora.Visible = false;
            lblPinAcceso.Visible = false;
            lblPuntos.Visible = false;
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            ActivarCampos();
        }

        private bool PageIsValid()
        {
            int intContador = 0;
            lblMensaje.Text = "";
            if (txtEmail.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El correo electrónico es un dato obligatorio";
                intContador = intContador + 1;
                //return false;
            }

            if (txtNombres.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El Nombre es un dato obligatorio";
                intContador = intContador + 1;
                //return false;
            }

            if (txtApellidos.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "El campo de Apellidos no puede estar vacío";
                intContador = intContador + 1;
                //return false;
            }

            if (txtContrasenia.Text == "")
            {
                lblMensaje.Text = lblMensaje.Text + ", " + "Debe escribir una contraseña";
                intContador = intContador + 1;
                //return false;
            }

            Boolean fileOK = false;
            String path = Server.MapPath("photos/");
            String fileExtension = string.Empty;
            //Foto del conferencista
            if (fuFotografia.HasFile)
            {
                fileExtension = System.IO.Path.GetExtension(fuFotografia.FileName).ToLower();
                String[] allowedExtensions =
                    {".gif", ".png", ".jpeg", ".jpg",".pdf"};
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
            }

            if (fileOK)
            {
                try
                {
                    //fuFotografia.PostedFile.SaveAs(path+fuFotografia.FileName);
                    guid = Guid.NewGuid().ToString();
                    fuFotografia.PostedFile.SaveAs(path+guid+fileExtension);
                    //imgFoto.ImageUrl = "photos/" + fuFotografia.FileName;
                    imgFoto.ImageUrl = "photos/" + guid + fileExtension;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El archivo no pudo ser cargado. Error: (" + ex.Message + ")";
                    intContador = intContador + 1;
                    //return false;
                }
            }
            else
            {
                if (imgFoto.ImageUrl == null)
                {
                    lblMensaje.Text = lblMensaje.Text + ", " + "El tipo de archivo no es válido.";
                    intContador = intContador + 1;
                    //return false;
                }
            }

            //revisamos si ya existe el usuario...
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdUsuario"];
            pArray.Add(ParamId);

            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@Username";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = txtEmail.Text;
            pArray.Add(ParamUsername);

            DataSet dsData = objData.ExecuteSelectSP("usp_checkifuserexists", pArray);

            if (dsData != null)
                if (dsData.Tables.Count > 0)
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        lblMensaje.Text = lblMensaje.Text + ", " + "La dirección de correo electrónico utilizada ya existe, favor de elegir otra";
                        intContador = intContador + 1;
                        //return false;
                    }
            if (intContador > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamId = new SqlParameter();
                ParamId.ParameterName = "@Id";
                ParamId.SqlDbType = SqlDbType.Int;
                ParamId.Value = Session["IdUsuario"];
                pArray.Add(ParamId);
                SqlParameter ParamUsername = new SqlParameter();
                ParamUsername.ParameterName = "@Username";
                ParamUsername.SqlDbType = SqlDbType.NVarChar;
                ParamUsername.Value = txtEmail.Text;
                pArray.Add(ParamUsername);
                SqlParameter ParamPassword = new SqlParameter();
                ParamPassword.ParameterName = "@Password";
                ParamPassword.SqlDbType = SqlDbType.NVarChar;
                ParamPassword.Value = Encrypt.EncryptPassword(txtContrasenia.Text, txtEmail.Text);
                pArray.Add(ParamPassword);
                SqlParameter ParamFName = new SqlParameter();
                ParamFName.ParameterName = "@FirstName";
                ParamFName.SqlDbType = SqlDbType.NVarChar;
                ParamFName.Value = txtNombres.Text;
                pArray.Add(ParamFName);
                SqlParameter ParamLName = new SqlParameter();
                ParamLName.ParameterName = "@LastName";
                ParamLName.SqlDbType = SqlDbType.NVarChar;
                ParamLName.Value = txtApellidos.Text;
                pArray.Add(ParamLName);
                SqlParameter ParamCorreo = new SqlParameter();
                ParamCorreo.ParameterName = "@CorreoElectronico";
                ParamCorreo.SqlDbType = SqlDbType.NVarChar;
                ParamCorreo.Value = txtEmail.Text;
                pArray.Add(ParamCorreo);
                SqlParameter ParamAdministrador = new SqlParameter();
                ParamAdministrador.ParameterName = "@Administrador";
                ParamAdministrador.SqlDbType = SqlDbType.Int;
                int Administrador = 0;
                if (cbAdministrador.Checked == true)
                {
                    Administrador = 1;
                }
                ParamAdministrador.Value = Administrador;
                pArray.Add(ParamAdministrador);
                SqlParameter ParamPhoto = new SqlParameter();
                ParamPhoto.ParameterName = "@Photo";
                ParamPhoto.SqlDbType = SqlDbType.NVarChar;
                ParamPhoto.Value = imgFoto.ImageUrl.Replace("photos/", "");
                pArray.Add(ParamPhoto);

                SqlParameter ParamTelefono = new SqlParameter();
                ParamTelefono.ParameterName = "@Telefono";
                ParamTelefono.SqlDbType = SqlDbType.NVarChar;
                ParamTelefono.Value = txtTelefono.Text;
                pArray.Add(ParamTelefono);
                SqlParameter ParamOcupacion = new SqlParameter();
                ParamOcupacion.ParameterName = "@Ocupacion";
                ParamOcupacion.SqlDbType = SqlDbType.NVarChar;
                ParamOcupacion.Value = txtOcupacion.Text;
                pArray.Add(ParamOcupacion);
                SqlParameter ParamEstado = new SqlParameter();
                ParamEstado.ParameterName = "@Estado";
                ParamEstado.SqlDbType = SqlDbType.NVarChar;
                ParamEstado.Value = ddlEstado.SelectedValue.ToString();
                pArray.Add(ParamEstado);
                SqlParameter ParamMunicipio = new SqlParameter();
                ParamMunicipio.ParameterName = "@Municipio";
                ParamMunicipio.SqlDbType = SqlDbType.NVarChar;
                ParamMunicipio.Value = txtMunicipio.Text;
                pArray.Add(ParamMunicipio);
                SqlParameter ParamDistribuidor = new SqlParameter();
                ParamDistribuidor.ParameterName = "@Distribuidor";
                ParamDistribuidor.SqlDbType = SqlDbType.Int;
                ParamDistribuidor.Value = Convert.ToInt32(ddlDistribuidor.SelectedValue.ToString());
                pArray.Add(ParamDistribuidor);

                objData.ExecuteInsertSP("usp_UpdateUser", pArray);
                Response.Redirect("Users.aspx?notificacion=1");
                /*
                txtNombres.ReadOnly = true;
                txtContrasenia.ReadOnly = true;
                txtContraseniaConfirmar.ReadOnly = true;
                txtApellidos.ReadOnly = true;
                txtEmail.ReadOnly = true;
                txtPuntos.ReadOnly = true;
                btnGuardar.Visible = false;
                btnCancelar.Visible = false;
                btnEditar.Visible = true;
                SelectUsuarios(Convert.ToInt32(Session["IdUsuario"]));
                */
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("Users.aspx");
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (PageIsValid())
            {
                CottonClub.Data objData = new Data();
                SqlConnection objCnn = objData.Connection();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamUsername = new SqlParameter();
                ParamUsername.ParameterName = "@Username";
                ParamUsername.SqlDbType = SqlDbType.NVarChar;
                ParamUsername.Value = txtEmail.Text;
                pArray.Add(ParamUsername);
                SqlParameter ParamPassword = new SqlParameter();
                ParamPassword.ParameterName = "@Password";
                ParamPassword.SqlDbType = SqlDbType.NVarChar;
                ParamPassword.Value = Encrypt.EncryptPassword(txtContrasenia.Text, txtEmail.Text);
                pArray.Add(ParamPassword);
                SqlParameter ParamFName = new SqlParameter();
                ParamFName.ParameterName = "@FirstName";
                ParamFName.SqlDbType = SqlDbType.NVarChar;
                ParamFName.Value = txtNombres.Text;
                pArray.Add(ParamFName);
                SqlParameter ParamLName = new SqlParameter();
                ParamLName.ParameterName = "@LastName";
                ParamLName.SqlDbType = SqlDbType.NVarChar;
                ParamLName.Value = txtApellidos.Text;
                pArray.Add(ParamLName);
                SqlParameter ParamCorreo = new SqlParameter();
                ParamCorreo.ParameterName = "@CorreoElectronico";
                ParamCorreo.SqlDbType = SqlDbType.NVarChar;
                ParamCorreo.Value = txtEmail.Text;
                pArray.Add(ParamCorreo);
                SqlParameter ParamAdministrador = new SqlParameter();
                ParamAdministrador.ParameterName = "@Administrador";
                ParamAdministrador.SqlDbType = SqlDbType.Int;
                int Administrador = 0;
                if (cbAdministrador.Checked == true)
                {
                    Administrador = 1;
                }
                ParamAdministrador.Value = Administrador;
                pArray.Add(ParamAdministrador);
                SqlParameter ParamPhoto = new SqlParameter();
                ParamPhoto.ParameterName = "@Photo";
                ParamPhoto.SqlDbType = SqlDbType.NVarChar;
                ParamPhoto.Value = imgFoto.ImageUrl.Replace("photos/", "");
                pArray.Add(ParamPhoto);
                SqlParameter ParamTelefono = new SqlParameter();
                ParamTelefono.ParameterName = "@Telefono";
                ParamTelefono.SqlDbType = SqlDbType.NVarChar;
                ParamTelefono.Value = txtTelefono.Text;
                pArray.Add(ParamTelefono);
                SqlParameter ParamOcupacion = new SqlParameter();
                ParamOcupacion.ParameterName = "@Ocupacion";
                ParamOcupacion.SqlDbType = SqlDbType.NVarChar;
                ParamOcupacion.Value = txtOcupacion.Text;
                pArray.Add(ParamOcupacion);
                SqlParameter ParamEstado = new SqlParameter();
                ParamEstado.ParameterName = "@Estado";
                ParamEstado.SqlDbType = SqlDbType.NVarChar;
                ParamEstado.Value = ddlEstado.SelectedValue.ToString();
                pArray.Add(ParamEstado);
                SqlParameter ParamMunicipio = new SqlParameter();
                ParamMunicipio.ParameterName = "@Municipio";
                ParamMunicipio.SqlDbType = SqlDbType.NVarChar;
                ParamMunicipio.Value = txtMunicipio.Text;
                pArray.Add(ParamMunicipio);
                SqlParameter ParamDistribuidor = new SqlParameter();
                ParamDistribuidor.ParameterName = "@Distribuidor";
                ParamDistribuidor.SqlDbType = SqlDbType.Int;
                ParamDistribuidor.Value = Convert.ToInt32(ddlDistribuidor.SelectedValue.ToString());
                pArray.Add(ParamDistribuidor);

                objData.ExecuteInsertSP("usp_InsertUser", pArray);
                Response.Redirect("Users.aspx?notificacion=1");
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Session["IdUsuario"];
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteUser", pArray);
            Response.Redirect("Users.aspx?notificacion=1");
        }

        protected void BorrarCotizacion(int Id)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamId = new SqlParameter();
            ParamId.ParameterName = "@Id";
            ParamId.SqlDbType = SqlDbType.Int;
            ParamId.Value = Id;
            pArray.Add(ParamId);
            objData.ExecuteInsertSP("usp_DeleteCotizaciones", pArray);
        }

        protected void btnGuardarActualizacion_Click(object sender, EventArgs e)
        {

        }

        protected void ActivarCampos()
        {
            txtNombres.ReadOnly = false;
            txtContrasenia.ReadOnly = false;
            txtApellidos.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtPuntos.ReadOnly = false;
            btnGuardar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Visible = false;
        }

        protected void gvwPDFs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwPDFs.PageIndex = e.NewPageIndex;
            FillPDFs();
        }

        protected void gvwPDFs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label lblId = gvwPDFs.Rows[e.RowIndex].FindControl("lblGvwId") as Label;
            BorrarCotizacion(Convert.ToInt32(lblId.Text));
        }

        protected void ddlEvento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}