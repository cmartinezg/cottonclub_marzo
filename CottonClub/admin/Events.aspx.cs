﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Events : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            SelectEventos();
        }

        protected void SelectEventos()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = txtBuscar.Text;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectEvents", pArray);
            if (ds.Tables[0] != null)
            {
                gvwEvents.PageSize = Convert.ToInt32(ddlMostrar.SelectedValue.ToString());
                gvwEvents.DataSource = ds;
                gvwEvents.DataBind();
            }
        }

        protected void gvwEvents_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Label lblId = gvwEvents.Rows[e.NewSelectedIndex].FindControl("lblGvwId") as Label;
            Response.Redirect("EventDetail.aspx?action=0&id=" + lblId.Text);
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("EventDetail.aspx?action=1");
        }

        protected void gvwEvents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvwEvents.PageIndex = e.NewPageIndex;
            SelectEventos();
        }

        protected void ddlMostrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectEventos();
        }
    }
}