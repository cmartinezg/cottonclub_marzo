﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Ranking1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SelectRanking();
        }

        protected void SelectRanking()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@IdEvent";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = 1;
            pArray.Add(ParamUsername);
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_PublicRanking", pArray);
            if (ds.Tables[0] != null)
            {
                gvwRanking.DataSource = ds;
                gvwRanking.DataBind();
            }
        }
    }
}