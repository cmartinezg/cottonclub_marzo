﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace CottonClub
{
    public partial class Register : System.Web.UI.Page
    {
        int Id;
        string strPIN = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            strPIN = Request.QueryString["data"];
            if (!IsPostBack)
            {
                FillDistribuidor();
            }
            SelectUsuariosByPIN(Request.QueryString["data"]);
        }

        protected void FillDistribuidor()
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@parametro";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = "";
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectDistributor", pArray);
            if (ds.Tables[0] != null)
            {
                ddlDistribuidor.DataSource = ds.Tables[0].DefaultView;
                ddlDistribuidor.DataTextField = "Decription";
                ddlDistribuidor.DataValueField = "Id";
                ddlDistribuidor.DataBind();
            }
        }

        protected void SelectUsuariosByPIN(string PIN)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamPIN = new SqlParameter();
            ParamPIN.ParameterName = "@PIN";
            ParamPIN.SqlDbType = SqlDbType.NVarChar;
            ParamPIN.Value = PIN;
            pArray.Add(ParamPIN);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUSerPIN", pArray);
            if (ds.Tables[0] != null)
            {
                Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"].ToString());
                imgLogo.ImageUrl= ds.Tables[0].Rows[0]["Logo"].ToString();
            }
            if(!IsPostBack)
            {
                SelectUsuarios(Id);
            }
            
        }

        protected void SelectUsuarios(int IdUsuario)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@id";
            ParamUsername.SqlDbType = SqlDbType.Int;
            ParamUsername.Value = IdUsuario;
            pArray.Add(ParamUsername);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectSpecificUsers", pArray);
            if (ds.Tables[0] != null)
            {
                txtNombre.Text= ds.Tables[0].Rows[0]["FirstName"].ToString();
                txtApellidos.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                txtCorreo.Text = ds.Tables[0].Rows[0]["CorreoElectronico"].ToString();

                Random r = new Random();
                int rInt = r.Next(0, 10); //for ints
                int range = 10;
                double rDouble = r.NextDouble() * range; //for doubles

                //txtNumeroAcompaniantes.Text = rInt.ToString();
                txtTelefono.Text= ds.Tables[0].Rows[0]["Telefono"].ToString();
                txtOcupacion.Text = ds.Tables[0].Rows[0]["Ocupacion"].ToString();

                if (!ds.Tables[0].Rows[0]["Estado"].ToString().Equals(""))
                {
                    string strItemToSelect = ds.Tables[0].Rows[0]["Estado"].ToString();
                    ddlEstado.Items.FindByValue(strItemToSelect).Selected = true;
                }

                txtMunicipio.Text = ds.Tables[0].Rows[0]["Municipio"].ToString();

                string strItemToSelectDistributor = ds.Tables[0].Rows[0]["IdDistribuidor"].ToString();
                ddlDistribuidor.Items.FindByValue(strItemToSelectDistributor).Selected = true;
            }
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage message = new MailMessage();
                //message.From = new MailAddress("contacto.emprende.negocio@gmail.com");
                message.From = new MailAddress(ConfigurationManager.AppSettings["accountSendMail"].ToString());

                CottonClub.Data objData = new Data();
                DataSet ds = new DataSet();
                ArrayList pArray = new ArrayList();
                SqlParameter ParamPIN = new SqlParameter();
                ParamPIN.ParameterName = "@PIN";
                ParamPIN.SqlDbType = SqlDbType.NVarChar;
                ParamPIN.Value = strPIN;
                pArray.Add(ParamPIN);
                SqlParameter ParamNombre1 = new SqlParameter();
                ParamNombre1.ParameterName = "@Nombre1";
                ParamNombre1.SqlDbType = SqlDbType.NVarChar;
                ParamNombre1.Value = txtCompanion1.Text;
                pArray.Add(ParamNombre1);
                SqlParameter ParamEdad1 = new SqlParameter();
                ParamEdad1.ParameterName = "@Edad1";
                ParamEdad1.SqlDbType = SqlDbType.Int;
                ParamEdad1.Value = ddlEdadCompanion1.SelectedValue;
                pArray.Add(ParamEdad1);
                SqlParameter ParamNombre2 = new SqlParameter();
                ParamNombre2.ParameterName = "@Nombre2";
                ParamNombre2.SqlDbType = SqlDbType.NVarChar;
                ParamNombre2.Value = txtCompanion2.Text;
                pArray.Add(ParamNombre2);
                SqlParameter ParamEdad2 = new SqlParameter();
                ParamEdad2.ParameterName = "@Edad2";
                ParamEdad2.SqlDbType = SqlDbType.Int;
                ParamEdad2.Value = ddlEdadCompanion2.SelectedValue;
                pArray.Add(ParamEdad2);
                SqlParameter ParamNombre3 = new SqlParameter();
                ParamNombre3.ParameterName = "@Nombre3";
                ParamNombre3.SqlDbType = SqlDbType.NVarChar;
                ParamNombre3.Value = txtCompanion3.Text;
                pArray.Add(ParamNombre3);
                SqlParameter ParamEdad3 = new SqlParameter();
                ParamEdad3.ParameterName = "@Edad3";
                ParamEdad3.SqlDbType = SqlDbType.Int;
                ParamEdad3.Value = ddlEdadCompanion3.SelectedValue;
                pArray.Add(ParamEdad3);
                SqlParameter ParamNombre4 = new SqlParameter();
                ParamNombre4.ParameterName = "@Nombre4";
                ParamNombre4.SqlDbType = SqlDbType.NVarChar;
                ParamNombre4.Value = txtCompanion4.Text;
                pArray.Add(ParamNombre4);
                SqlParameter ParamEdad4 = new SqlParameter();
                ParamEdad4.ParameterName = "@Edad4";
                ParamEdad4.SqlDbType = SqlDbType.Int;
                ParamEdad4.Value = ddlEdadCompanion4.SelectedValue;
                pArray.Add(ParamEdad4);

                SqlParameter ParamTelefono = new SqlParameter();
                ParamTelefono.ParameterName = "@Telefono";
                ParamTelefono.SqlDbType = SqlDbType.NVarChar;
                ParamTelefono.Value = txtTelefono.Text;
                pArray.Add(ParamTelefono);
                SqlParameter ParamOcupacion = new SqlParameter();
                ParamOcupacion.ParameterName = "@Ocupacion";
                ParamOcupacion.SqlDbType = SqlDbType.NVarChar;
                ParamOcupacion.Value = txtOcupacion.Text;
                pArray.Add(ParamOcupacion);
                SqlParameter ParamEstado = new SqlParameter();
                ParamEstado.ParameterName = "@Estado";
                ParamEstado.SqlDbType = SqlDbType.NVarChar;
                ParamEstado.Value = ddlEstado.SelectedValue;
                pArray.Add(ParamEstado);
                SqlParameter ParamMunicipio = new SqlParameter();
                ParamMunicipio.ParameterName = "@Municipio";
                ParamMunicipio.SqlDbType = SqlDbType.NVarChar;
                ParamMunicipio.Value = txtMunicipio.Text;
                pArray.Add(ParamMunicipio);
                SqlParameter ParamDistribuidor = new SqlParameter();
                ParamDistribuidor.ParameterName = "@Distribuidor";
                ParamDistribuidor.SqlDbType = SqlDbType.Int;
                ParamDistribuidor.Value = Convert.ToInt32(ddlDistribuidor.SelectedValue.ToString());
                pArray.Add(ParamDistribuidor);

                objData.ExecuteSelectSP("usp_InsertCompanions", pArray);

                pArray.Clear();
                SqlParameter ParamIdEvento = new SqlParameter();
                ParamIdEvento.ParameterName = "@id";
                ParamIdEvento.SqlDbType = SqlDbType.Int;
                ParamIdEvento.Value = 0;
                pArray.Add(ParamIdEvento);

                SmtpClient client = new SmtpClient();
                //client.Host = "relay-hosting.secureserver.net";
                //client.Host = "exsmtp.de.bayer.cnb";
                client.Host = ConfigurationManager.AppSettings["smtpServer"].ToString();
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["serverEmailPort"].ToString());
                ds = objData.ExecuteSelectSP("usp_SelectSpecificEvents", pArray);

                string strBody = "";
                if (ds.Tables[0] != null)
                {
                    message.Subject = "Confirmacion de " + ds.Tables[0].Rows[0]["Name"].ToString();
                    //strBody = ds.Tables[0].Rows[0]["WelcomeEmail"].ToString();
                }

                /*
                strBody = strBody + "<br><br>Nombre: " + txtNombre.Text;
                strBody = strBody + "<br><br>Apellidos: " + txtApellidos.Text;
                strBody = strBody + "<br><br>Correo: " + txtCorreo.Text;
                strBody = strBody + "<br><br>Numero de acompañantes: " + ddlCompanions.SelectedValue.ToString();
                strBody = strBody + "<br><br>PIN: " + strPIN;*/
                string valor = txtCorreo.Text;
                //string valor = "cemg7@hotmail.com";
                message.To.Add(new MailAddress(valor));
                        message.IsBodyHtml = true;
                //message.Body = "<html><body>"  + strBody + "</html></body>"; //+ "<br><br><a href='http://www.momentos.bayer.mx/Register.aspx?data=" + fila["PIN"] + "'>Da clic aqui</a>";

                //message.Body = "<html><head> < title > Confirmación </ title >    < meta charset = \"UTF-8\" /> </ head >< body >     < div style = \"font-family: Helvetica; text-align: center; width: 600px;\" >          < div >           < img src = \"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/mail_invitacion_1.jpg\" alt = \"Cotton Club\" />                  </ div >                  < div style = \"text-transform: uppercase; padding: 5px; text-decoration: none; font-size: 14px; color: #444; padding-top: 20px;\" >                  Descarga las aplicaciones de iOs y Android e ingresa tu              </ div >                    < div style = \"text-transform: uppercase; padding: 5px; text-decoration: none; font-size: 30px; color: #444;\" >                   TU PIN DE ACCESO < strong style = \"color: #430B07; text-decoration: underline;\" >" + strPIN + " </ strong >                        </ div >                        < table style = \"text-align: center; width: 100%; margin-top: 50px; \" >                              < tbody >                                  < tr >                                      < td >< a href = \"https://itunes.apple.com/us/app/momentos-bayer/id1209905341?ls=1&mt=8\" >< img src = \"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/store_ios.jpg\" alt = \"Cotton Club\" /></ a ></ td >                                                        < td >< a href = \"https://play.google.com/store/apps/details?id=mx.app.momentos\" >< img src = \"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/store_android.jpg\" alt = \"Cotton Club\" /></ a ></ td >                                                                      </ tr >                                                                  </ tbody >                                                              </ table >                                                          </ div >                                                      </ body >                           </ html >";
                message.Body = "<html><head><title>Confirmación</title><meta charset=\"UTF-8\" /></head><body><div style=\"font-family: Helvetica; text-align: center; width: 600px;\"><div><img src=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/mail_invitacion_1.jpg\" alt =\"Cotton Club\"/></div><div style=\"text-transform: uppercase; padding: 5px; text-decoration: none; font-size: 14px; color: #444; padding-top: 20px;\">Descarga las aplicaciones de iOs y Android e ingresa tu</div><div style=\"text-transform: uppercase; padding: 5px; text-decoration: none; font-size: 30px; color: #444;\">TU PIN DE ACCESO <strong style=\"color: #430B07; text-decoration: underline;\">" + strPIN + "</strong></div><table style=\"text-align: center; width: 100%; margin-top: 50px;\"><tbody><tr><td><a href=\"http://apps.tangamampilia.net/cottonclub/ios.php\"><img src=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/store_ios.jpg\" alt=\"Cotton Club\"/></a></td><td><a href=\"http://apps.tangamampilia.net/cottonclub/android.php\"><img src=\"" + ConfigurationManager.AppSettings["serverBaseURL"].ToString() + "admin/assets/img/store_android.jpg\" alt =\"Cotton Club\"/></a></td></tr></tbody></table></div></body> </html>"; 
                
                    client.Send(message);
                message.To.Clear();
            }
            catch (Exception ex)
            {
                
            }


            Response.Redirect("Thanks.aspx?data="+ strPIN);
        }
    }
}