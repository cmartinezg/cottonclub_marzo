﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace CottonClub
{
    public partial class Thanks : System.Web.UI.Page
    {
        int Id;
        string strPIN = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            strPIN = Request.QueryString["data"];
            SelectUsuariosByPIN(Request.QueryString["data"]);
        }

        protected void SelectUsuariosByPIN(string PIN)
        {
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamPIN = new SqlParameter();
            ParamPIN.ParameterName = "@PIN";
            ParamPIN.SqlDbType = SqlDbType.NVarChar;
            ParamPIN.Value = PIN;
            pArray.Add(ParamPIN);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            ds = objData.ExecuteSelectSP("usp_SelectUSerPIN", pArray);
            if (ds.Tables[0] != null)
            {
                Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"].ToString());
                imgLogo.ImageUrl = ds.Tables[0].Rows[0]["Logo"].ToString();
            }

        }
    }
}