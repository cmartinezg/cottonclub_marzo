﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ranking.aspx.cs" Inherits="CottonClub.Ranking1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    	    <link href="admin/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
		<link href="admin/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
		<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	    
	    <script src="admin/assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script> 

    <title></title>
    <style type="text/css">
.table-bordered{border-color:#e8edf1 !important}.table-bordered{border:1px solid #ddd}.table{width:100%;margin-bottom:20px}.table{border-collapse:collapse!important}table{max-width:100%;background-color:transparent}table{border-spacing:0;border-collapse:collapse}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}*{color:#000!important;text-shadow:none!important;background:transparent!important;box-shadow:none!important}.no-more-tables table,.no-more-tables thead,.no-more-tables tbody,.no-more-tables th,.no-more-tables td,.no-more-tables tr{display:block}.table > thead > tr > th,.table > tbody > tr > th,.table > tfoot > tr > th,.table > thead > tr > td,.table > tbody > tr > td,.table > tfoot > tr > td{padding:12px}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px solid #ddd}.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td{padding:8px;line-height:1.42857143;vertical-align:top;border-top:1px solid #ddd}.table-bordered th{background-color:#ecf0f2;border:0 !important}.table th{border-top:1px solid #e8edf1;padding:10px 12px;font-size:12px;text-transform:uppercase}.table th{font-size:14px}.table-bordered th,.table-bordered td{border:1px solid #ddd!important}.table td,.table th{background-color:#fff!important}th{text-align:left}th{text-align:left}td,th{padding:0}.table-bordered td:first-child{border-radius:0px}.table td:first-child{-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px}.no-more-tables.table-bordered td{border-left:1px solid #eee;border-bottom:1px solid #eee}.table > tbody > tr > td,.table > tfoot > tr > td{border-top:1px solid #e8edf1}.table-bordered td{border:1px solid #e8edf1 !important}.table td{border-top:1px solid #e8edf1;vertical-align:top;padding:10px 12px !important;color:#576475;font-size:13px}.no-more-tables td{border:none;border-bottom:1px solid #eee;position:relative;white-space:normal;text-align:left}.table td{font-size:14px}a,a:focus,a:hover,a:active{outline:0;text-decoration:none}a{text-shadow:none !important;color:#0d638f;transition:color 0.1s linear 0s, background-color 0.1s linear 0s, opacity 0.1s linear 0s !important}a{color:#428bca;text-decoration:none}a,a:visited{text-decoration:underline}a{background:transparent}
        .auto-style1 {
            color: White;
            font-weight: bold;
            background-color: #507CD1;
        }
        .auto-style2 {
            background-color: #EFF3FB;
        }
        .auto-style3 {
            background-color: White;
        }
        .auto-style4 {
            color: White;
            background-color: #2461BF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
                                  <asp:GridView class="table table-bordered no-more-tables" ID="gvwRanking" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
                                      <AlternatingRowStyle BackColor="White" />
                                      <Columns>
                                          <asp:BoundField DataField="RankPos" HeaderText="Posición" />
                                          <asp:BoundField DataField="Fullname" HeaderText="Nombre" />
                                          <asp:ImageField DataImageUrlField="Photo">
                                          </asp:ImageField>
                                          <asp:BoundField DataField="Score" HeaderText="Puntos" />
                                      </Columns>
                                      <EditRowStyle BackColor="#2461BF" />
                                      <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                      <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                      <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                      <RowStyle BackColor="#EFF3FB" />
                                      <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                      <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                      <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                      <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                      <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                  </asp:GridView>
    
    </div>
    </form>
</body>
</html>
