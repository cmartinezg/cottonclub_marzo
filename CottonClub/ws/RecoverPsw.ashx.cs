﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub.ws
{
    /// <summary>
    /// Summary description for RecoverPsw
    /// </summary>
    public class RecoverPsw : IHttpHandler
    {
        NameValueCollection frm = null;
        string username = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            LeerCampos();
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            if (username == "")
            {
                SqlParameter ParamUsername = new SqlParameter();
                ParamUsername.ParameterName = "@parametro";
                ParamUsername.SqlDbType = SqlDbType.NVarChar;
                ParamUsername.Value = "";
                pArray.Add(ParamUsername);
                dsResult = objData.ExecuteSelectSP("usp_wsRecoverPassword", pArray);
            }
            else
            {
                SqlParameter ParamUsername = new SqlParameter();
                ParamUsername.ParameterName = "@parametro";
                ParamUsername.SqlDbType = SqlDbType.NVarChar;
                ParamUsername.Value = username;
                pArray.Add(ParamUsername);
                dsResult = objData.ExecuteSelectSP("usp_wsRecoverPassword", pArray);
            }
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows.Count > 0)
                {
                    if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                    {
                        dsResult.Tables[0].TableName = "data";
                    }
                    else
                    {
                        dsResult.Tables[0].TableName = "error_message";
                    }
                }
                else
                {
                    dsResult.Tables[0].TableName = "error_message";
                }
            }
            string json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
            if (dsResult.Tables[0] != null)
            {
                const string quote = "\"";
                if (dsResult.Tables[0].Rows.Count > 0)
                {
                    if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                    {
                        if (dsResult.Tables[0].Rows.Count == 1)
                        {
                            json = json.Replace("[", "");
                            json = json.Replace("]", "");
                        }
                        json = json.Remove(json.Length - 1);
                        json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "recoverPassword" + quote + " }";
                    }
                    else
                    {
                        //json = json.Remove(json.Length -1);
                        json = "{" + quote + "error_message" + quote + ": [" + quote + "no se encontraron coincidencias" + quote + "]";
                        json = json + "," + quote +
                                "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "recoverPassword" + quote + "}";
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "no se encontraron coincidencias" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "recoverPassword" + quote + "}";
                }
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            username = frm["username"];
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}