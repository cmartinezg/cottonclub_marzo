﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.IO;
using System.Web.Script.Serialization;
using System.Globalization;

namespace CottonClub
{
    /// <summary>
    /// Summary description for UploadPhoto
    /// </summary>
    public class UploadPhoto : IHttpHandler
    {
        private readonly JavaScriptSerializer js;
        NameValueCollection frm = null;
        string ValidToken = string.Empty;
        string FileName = string.Empty;
        string FileNameGUID = string.Empty;
        const string quote = "\"";
        string guid = string.Empty;
        private string StorageRoot
        {
            get { return Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/userFiles/")); } //Path should! always end with '/'
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();
            String path = System.Web.HttpContext.Current.Server.MapPath("../admin/photos/");
            string savedFileName = "";
            String fileExtension = string.Empty;
            foreach (string file in context.Request.Files)
            {
                HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
                if (hpf.ContentLength == 0)
                    continue;
                FileName=hpf.FileName;
                fileExtension = System.IO.Path.GetExtension(FileName).ToLower();
                guid = Guid.NewGuid().ToString();
                //savedFileName = path + Path.GetFileName(hpf.FileName);
                savedFileName = path + Path.GetFileName(guid + fileExtension);
                FileNameGUID = guid + fileExtension;
                hpf.SaveAs(savedFileName);
            }

            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {

                    DataSet dsResult=null;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();
                    if (savedFileName != "")
                    {
                        SqlParameter ParamToken = new SqlParameter();
                        ParamToken.ParameterName = "@token";
                        ParamToken.SqlDbType = SqlDbType.NVarChar;
                        ParamToken.Value = ValidToken;
                        pArray.Add(ParamToken);
                        SqlParameter ParamPhoto = new SqlParameter();
                        ParamPhoto.ParameterName = "@Photo";
                        ParamPhoto.SqlDbType = SqlDbType.NVarChar;
                        //ParamPhoto.Value = FileName;
                        ParamPhoto.Value = FileNameGUID;
                        pArray.Add(ParamPhoto);
                        dsResult = objData.ExecuteSelectSP("usp_wsUploadPhoto", pArray);
                    }
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            dsResult.Tables[0].TableName = "data";
                        }
                        else
                        {
                            dsResult.Tables[0].TableName = "error_message";
                        }
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            if (dsResult.Tables[0].Rows.Count == 1)
                            {
                                json = json.Replace("[", "");
                                json = json.Replace("]", "");
                            }
                            json = json.Remove(json.Length - 1);
                            json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "uploadphoto" + quote + " }";
                        }
                        else
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "datos invalidos" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "uploadphoto" + quote + "}";
                        }
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "uploadphoto" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "uploadphoto" + quote + "}";
            }
            context.Response.Write(json);
        }


        private void LeerCampos()
        {
            //id = frm["id"];
            ValidToken = frm["token"];
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}