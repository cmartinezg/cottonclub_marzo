﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using System.Configuration;

namespace CottonClub.ws
{
    /// <summary>
    /// Summary description for Cotizacion
    /// </summary>
    public class Cotizacion : IHttpHandler
    {
        #region CONSTANTES COTIZACION
        const string HEADER_LOGO_1 = "[!LOGO1]";
        const string HEADER_LOGO_2 = "[!LOGO2]";
        const string HEADER_COMPRADOR = "[!COMPRADOR]";


        const string BODY_TABLA_1 = "[!TABLASEMILLAS]";
        const string BODY_TABLA_2 = "[!TABLAAGROQUIMICOS]";
        const string BODY_TABLA_3 = "[!TABLAREGALOS]";

        const string PARAM_AHORRIODIABBC = "[!AHORRODIABBC]";
        const string PARAM_AHORROPACASDIABBC = "[!AHORROPACASDIABBC]";
        const string PARAM_CANTCOMBOS = "[!CANTCOMBOS]";
        const string PARAM_PCTSEMILLAS = "[!%SEMILLAS]";
        const string PARAM_TOTALSEMILLAS = "[!TOTALSEMILLAS]";
        const string PARAM_CANTPRODUCTOS = "[!CANTPRODUCTOS]";
        const string PARAM_PCTAGROQUIMICOS = "[!%AGROQUIMICOS]";
        const string PARAM_TOTALAGROQUIMICOS = "[!TOTALAGROQUIMICOS]";
        const string PARAM_AHORROADICIONAL = "[!AHORROADICIONAL]";
        const string PARAM_AHORROENPACASADICIONAL = "[!AHORROENPACASADICIONAL]";
        const string PARAM_AHORROFINAL = "[!AHORROFINAL]";
        const string PARAM_AHORROPACASFINAL = "[!AHORROPACASFINAL]";

        const string BODY_TABLA_DESCUENTOS = "[!TABLADESCUENTOS]";

        const string FOOTER_FIRMA = "[!FIRMA]";
        const string FOOTER_COMPRADOR = "[!COMPRADOR]";
        #endregion
        #region CONSTANTES CONTRATO
        const string CONTRATO_CLIENTE = "[!CLIENTE]";
        const string CONTRATO_FIRMA = "[!FIRMA]";

        const string FOOTER_FIRMA_DISTRIBUIDOR = "[!DISTRIBUIDOR]";
        const string FOOTER_DISTRIBUIDOR = "[!NOMBRE_DISTRIBUIDOR]";
        #endregion
        NameValueCollection frm = null;
        string ValidToken = string.Empty;
        string jsonData = string.Empty;
        const string quote = "\"";
        string guid = string.Empty;
        decimal total = 0;
        dynamic data;
        int idCotizacion = 0;
        string strIdDistribuidor = "";
        string strDescripcionDistribuidor = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();

            try
            {
                if (ValidToken != null)
                {
                    int ResultToken = ValidaToken(ValidToken);
                    if (ResultToken != 0)
                    {
                        if (context.Request.Files.Count > 0)
                        {
                            List<string> lstArchivos = new List<string>();
                            data = JsonConvert.DeserializeObject(jsonData);
                           
                            string mailComprador = data.summary.email;
                            decimal.TryParse((string)data.summary.offer, out total);
                            guid = Guid.NewGuid().ToString();

                            DataSet ds;
                            CottonClub.Data objData = new Data();
                            ArrayList pArray = new ArrayList();
                            pArray.Clear();
                            SqlParameter ParamToken = new SqlParameter();
                            ParamToken.ParameterName = "@token";
                            ParamToken.SqlDbType = SqlDbType.NVarChar;
                            ParamToken.Value = ValidToken;
                            pArray.Add(ParamToken);
                            ds = objData.ExecuteSelectSP("usp_wsRegresaDistribuidor", pArray);

                            if (ds.Tables[0] != null)
                            {
                                strIdDistribuidor= ds.Tables[0].Rows[0]["Id"].ToString();
                                strDescripcionDistribuidor = ds.Tables[0].Rows[0]["Decription"].ToString();
                            }

                            idCotizacion = SaveData();
                            string fileFirma = HttpContext.Current.Server.MapPath("cotizaciones/" + guid + "_firma.png");
                            HttpPostedFile hpf = context.Request.Files[0] as HttpPostedFile;
                            hpf.SaveAs(fileFirma);

                            string html = CreateHTMLCotizacion(data, context, guid);

                            byte[] pdfBytes = PdfSharpConvert(html);
                            string filePath = HttpContext.Current.Server.MapPath("cotizaciones/" + guid + "_cotizacion.pdf");
                            File.WriteAllBytes(filePath, pdfBytes); // Requires System.IO
                            lstArchivos.Add(filePath);

                            html = CreateHTMLContrato(data, context, guid);

                            pdfBytes = PdfSharpConvert(html);
                            filePath = HttpContext.Current.Server.MapPath("cotizaciones/" + guid + "_contrato.pdf");
                            File.WriteAllBytes(filePath, pdfBytes); // Requires System.IO
                            lstArchivos.Add(filePath);

                            File.Delete(fileFirma);

                            SendMail(mailComprador, "Cotización", "Envío de cotización", lstArchivos);

                            json = "{ " + quote + "data" + quote + ": { }" + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "cotizacion" + quote + " }";
                        }
                        else
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "No se ha enviado ninguna firma" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "004" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "cotizacion" + quote + "}";
                        }
                    }
                    else
                    {
                        //json = json.Remove(json.Length -1);
                        json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                        json = json + "," + quote +
                                "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "cotizacion" + quote + "}";
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "cotizacion" + quote + "}";
                }
            }
            catch (Exception ex)
            {
                json = "{" + quote + "error_message" + quote + ": [" + quote + ex.Message + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "003" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "cotizacion" + quote + "}";
            }
            context.Response.Write(json);
        }

        private int SaveData()
        {
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@token";
            param1.SqlDbType = SqlDbType.NVarChar;
            param1.Value = ValidToken;
            pArray.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@cotizacion";
            param2.SqlDbType = SqlDbType.NVarChar;
            param2.Value = guid + ".pdf";
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@total";
            param3.SqlDbType = SqlDbType.Decimal;
            param3.Value = total;
            pArray.Add(param3);

            SqlParameter pvNewId = new SqlParameter();
            pvNewId.ParameterName = "@id";
            pvNewId.DbType = DbType.Int32;
            pvNewId.Direction = ParameterDirection.Output;
            pArray.Add(pvNewId);

            objData.ExecuteInsertSP("usp_InsertCotizacion", pArray);
            idCotizacion = (int)pvNewId.Value;
            return idCotizacion;

        }

        private void SaveDetail(int idCotizacion, int idObjeto, string tipo, int packs, decimal toffer)
        {
            string sp = "";
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();

            ArrayList pArray = new ArrayList();
            SqlParameter param1 = new SqlParameter();
            param1.ParameterName = "@idCotizacion";
            param1.SqlDbType = SqlDbType.Int;
            param1.Value = idCotizacion;
            pArray.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.SqlDbType = SqlDbType.Int;
            param2.Value = idObjeto;

            if (tipo == "productos")
            {
                sp = "usp_InsertCotizacionProducto";
                param2.ParameterName = "@idProducto";

            }
            else if (tipo == "semillas")
            {
                sp = "usp_InsertCotizacionSemilla";
                param2.ParameterName = "@idSemilla";
            }
            else
                return;
            
            pArray.Add(param2);

            SqlParameter param3 = new SqlParameter();
            param3.ParameterName = "@cantidad";
            param3.SqlDbType = SqlDbType.Int;
            param3.Value = packs;
            pArray.Add(param3);

            SqlParameter param4 = new SqlParameter();
            param4.ParameterName = "@total";
            param4.SqlDbType = SqlDbType.Decimal;
            param4.Value = toffer;
            pArray.Add(param4);

            objData.ExecuteInsertSP(sp, pArray);

        }

        private string CreateHTMLContrato(dynamic data, HttpContext context, string guid)
        {
            StringBuilder html = new StringBuilder();

            string template = HttpContext.Current.Server.MapPath("../admin/assets/contrato.html");

            html.Append(System.IO.File.ReadAllText(template));

            string url = context.Request.Url.Authority;
            if (context.Request.ServerVariables["HTTPS"] == "on") { url = "https://" + url; }
            else { url = "http://" + url; }

            html.Replace(CONTRATO_CLIENTE, (string)data.summary.name);
            string firma = url + "/ws/cotizaciones/" + guid + "_firma.png";
            html.Replace(CONTRATO_FIRMA, firma);

            string firmaDistribuidor = url + "/admin/assets/firms/distribuidor_" + strIdDistribuidor + ".jpg";
            html.Replace(FOOTER_FIRMA_DISTRIBUIDOR, firmaDistribuidor);
            html.Replace(FOOTER_DISTRIBUIDOR, strDescripcionDistribuidor);

            return html.ToString();
        }
        private string CreateHTMLCotizacion(dynamic data, HttpContext context, string guid)
        {
            StringBuilder html = new StringBuilder();

            string template = HttpContext.Current.Server.MapPath("../admin/assets/cotizacion.html");

            string url = context.Request.Url.Authority;
            if (context.Request.ServerVariables["HTTPS"] == "on") { url = "https://" + url; }
            else { url = "http://" + url; }

            html.Append(System.IO.File.ReadAllText(template));
            string logo1 = url + "/admin/assets/img/bayer.png";
            html.Replace(HEADER_LOGO_1, logo1);
            html.Replace(HEADER_COMPRADOR, (string)data.summary.name);
            string logo2 = url + "/admin/assets/img/fibermax.png";
            html.Replace(HEADER_LOGO_2, logo2);

            decimal valorBCC = 7300;

            decimal savingVariedad = 0;
            decimal totalOfferVariedad = 0;
            html.Replace(BODY_TABLA_1, CreateTableData(data.varieties, out savingVariedad, out totalOfferVariedad, "semillas"));
            decimal savingProductos = 0;
            decimal totalOfferProductos = 0;
            html.Replace(BODY_TABLA_2, CreateTableData(data.products, out savingProductos, out totalOfferProductos, "productos"));
            decimal savingRegalos = 0;
            decimal totalOfferRegalos = 0;
            html.Replace(BODY_TABLA_3, CreateTableData(data.presents, out savingRegalos, out totalOfferRegalos, "regalos"));
            if (hayRegalos)
            {
                html.Replace("<!--", "");
                html.Replace("-->", "");
            }

            int cantProductos = 0;
            int.TryParse((string)data.summary.products, out cantProductos);
            int cantCombos = 0;
            int.TryParse((string)data.summary.combos, out cantCombos);
            decimal pctSemilla = 0;
            decimal.TryParse((string)data.summary.percent_seed, out pctSemilla);
            decimal pctAgroquimico = 0;
            decimal.TryParse((string)data.summary.percent_product, out pctAgroquimico);
            decimal ahorroDiaBCC = savingVariedad + savingProductos + savingRegalos;
            decimal ahorroPacasDiaBCC = ahorroDiaBCC / valorBCC;
            decimal totalSemillas = totalOfferVariedad * (pctSemilla / 100);
            decimal totalAgroquimicos = totalOfferProductos * (pctAgroquimico / 100);
            decimal ahorroAdicional = totalSemillas + totalAgroquimicos;
            decimal ahorroEnPacasAdicional = ahorroAdicional / valorBCC;
            decimal ahorroFinal = ahorroAdicional + ahorroDiaBCC;
            decimal ahorroPacasFinal = ahorroFinal / valorBCC;

            html.Replace(PARAM_AHORRIODIABBC, ahorroDiaBCC.ToString("C"));
            html.Replace(PARAM_AHORROPACASDIABBC, ahorroPacasDiaBCC.ToString("C"));

            html.Replace(PARAM_CANTCOMBOS, cantCombos.ToString());
            html.Replace(PARAM_PCTSEMILLAS, pctSemilla.ToString() + "%");
            html.Replace(PARAM_TOTALSEMILLAS, totalSemillas.ToString("C"));

            html.Replace(PARAM_CANTPRODUCTOS, cantProductos.ToString());
            html.Replace(PARAM_PCTAGROQUIMICOS, pctAgroquimico.ToString() + " %");
            html.Replace(PARAM_TOTALAGROQUIMICOS, totalAgroquimicos.ToString("C"));

            html.Replace(PARAM_AHORROADICIONAL, ahorroAdicional.ToString("C"));
            html.Replace(PARAM_AHORROENPACASADICIONAL, ahorroEnPacasAdicional.ToString("C"));

            html.Replace(PARAM_AHORROFINAL, ahorroFinal.ToString("C"));
            html.Replace(PARAM_AHORROPACASFINAL, ahorroPacasFinal.ToString("C"));

            //html.Replace(BODY_TABLA_DESCUENTOS, CreateTableDataRegalos());


            string firma = url + "/ws/cotizaciones/" + guid + "_firma.png";

            html.Replace(FOOTER_FIRMA, firma);
            html.Replace(FOOTER_COMPRADOR, (string)data.name);

            return html.ToString();
        }

        bool hayRegalos = false;
        private string CreateTableData(dynamic array, out decimal totalSaving, out decimal totalOffer, string tipo)
        {
            StringBuilder html = new StringBuilder();

            totalSaving = 0;
            totalOffer = 0;
            decimal totalPrice = 0;
            //decimal price = 0;
            decimal num = 0;
            foreach (dynamic obj in array)
            {
                if (tipo == "regalos")
                    hayRegalos = true;
                int id = 0;
                int.TryParse((string)obj.id, out id);

                SaveDetail(idCotizacion, id, tipo, (int)obj.packs, (decimal)obj.toffer);

                html.Append("<tr>");
                html.Append("<td>" + obj.name + "</td>");
                html.Append("<td>" + obj.packs + "</td>");
                decimal.TryParse((string)obj.tprice, out num);
                html.Append("<td>" + num.ToString("C") + "</td>");
                decimal.TryParse((string)obj.toffer, out num);
                html.Append("<td>" + num.ToString("C") + "</td>");
                //decimal.TryParse((string)obj.saving, out num);

                decimal price = 0;
                decimal offer = 0;
                decimal.TryParse((string)obj.tprice, out price);
                decimal.TryParse((string)obj.toffer, out offer);
                decimal saving = price - offer;

                html.Append("<td>" + saving.ToString("C") + "</td>");
                html.Append("</tr>");

                //decimal saving = 0;
                //decimal.TryParse((string)obj.saving, out saving);
                totalSaving += saving;

                decimal toffer = 0;
                decimal.TryParse((string)obj.toffer, out toffer);
                totalOffer += toffer;


                decimal tprice = 0;
                decimal.TryParse((string)obj.tprice, out tprice);
                totalPrice += tprice;
            }

            html.Append("<tr>");
            html.Append("<td colspan='5'></td>");
            html.Append("<td>AHORRO</td>");
            html.Append("<td>" + totalSaving.ToString("C") + "</td>");
            //html.Append("<td>" + (totalOffer- totalPrice) + "</td>");
            html.Append("</tr>");

            return html.ToString();
        }

        private string CreateTableDataRegalos()
        {
            StringBuilder html = new StringBuilder();

            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();
            ArrayList pArray = new ArrayList();
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            objCnn.Open();
            ds = objData.ExecuteSelectSP("usp_SelectCalcPorcentajes", pArray);
            objCnn.Close();
            if (ds.Tables[0] != null)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    html.Append("<tr>");
                    html.Append("<td>" + row["Cantidad"].ToString() + "</td>");
                    html.Append("<td>" + row["Paquetes"].ToString() + "</td>");
                    html.Append("<td>" + row["PorcentajeSemilla"].ToString() + "%</td>");
                    html.Append("<td>" + row["Porcentaje"].ToString() + "%</td>");
                    html.Append("</tr>");

                }
            }

            return html.ToString();
        }

        private void SendMail(string recipient, string subject, string body, List<string> attchments)
        {

            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["accountSendMail"].ToString());
            SmtpClient client = new SmtpClient();
            //client.Host = "exsmtp.de.bayer.cnb";
            client.Host = ConfigurationManager.AppSettings["smtpServer"].ToString();
            //client.Port = 25;
            client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["serverEmailPort"].ToString());

            message.Subject = "Cotización";

            message.To.Add(new MailAddress(recipient));
            message.CC.Add(new MailAddress("momentos@bayer.com"));
            message.Subject = subject;
            //message.IsBodyHtml = true;
            foreach (string attachmentFilename in attchments)
                message.Attachments.Add(new Attachment(attachmentFilename));
            message.Body = body;
            client.Send(message);

        }

        public static Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var config = new PdfGenerateConfig();
                config.PageOrientation = PageOrientation.Portrait;
                config.PageSize = PageSize.A4;
                PdfSharp.Pdf.PdfDocument pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, config);

                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }

        private void LeerCampos()
        {
            ValidToken = frm["token"];
            jsonData = frm["data"];

            jsonData = jsonData.Replace("\"presents\" : null", "\"presents\" : []");
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}