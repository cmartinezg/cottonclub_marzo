﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub.ws
{
    /// <summary>
    /// Summary description for Answers
    /// </summary>
    public class Answers : IHttpHandler
    {
        NameValueCollection frm = null;
        string guid = string.Empty;
        string ValidToken = string.Empty;
        int respuesta = 0;
        const string quote = "\"";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();

            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {

                    DataSet dsResult;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();
                    SqlParameter ParamToken = new SqlParameter();
                    ParamToken.ParameterName = "@token";
                    ParamToken.SqlDbType = SqlDbType.NVarChar;
                    ParamToken.Value = ValidToken;
                    pArray.Add(ParamToken);
                    SqlParameter ParamRespuesta = new SqlParameter();
                    ParamRespuesta.ParameterName = "@IdRespuesta";
                    ParamRespuesta.SqlDbType = SqlDbType.Int;
                    ParamRespuesta.Value = (respuesta+1);
                    pArray.Add(ParamRespuesta);
                    dsResult = objData.ExecuteSelectSP("usp_wsInsertAnswers", pArray);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            dsResult.Tables[0].TableName = "data";
                        }
                        else
                        {
                            dsResult.Tables[0].TableName = "error_message";
                        }
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            if (dsResult.Tables[0].Rows.Count == 1)
                            {
                                json = json.Replace("[", "");
                                json = json.Replace("]", "");
                            }
                            json = json.Remove(json.Length - 1);
                            json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "result" + quote + ": " + quote + "respondio correctamente" + quote + ",service" + quote + ": " + quote + "answers" + quote + " }";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "2")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "Ya ha respondido a esta pregunta" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "101")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "data" + quote + ": [" + quote + "La respuesta fue correcta" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "intentos_restantes" + quote + ": " + quote + "0" + quote + ",  " + quote + "success" + quote + ": " + quote + "true" + quote + ",  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "102")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "La respuesta fue incorrecta" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "102" + quote + ",  " + quote + "intentos_restantes" + quote + ": " + quote + "1" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "103")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "La respuesta fue incorrecta" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "103" + quote + ",  " + quote + "intentos_restantes" + quote + ": " + quote + "0" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
                        }
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : 002,  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "answers" + quote + "}";
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            guid = frm["guid"];
            respuesta = Convert.ToInt32(frm["respuesta"]);
            ValidToken = frm["token"];
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}