﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub.ws
{
    /// <summary>
    /// Descripción breve de InfoSurvey
    /// </summary>
    public class InfoSurvey : IHttpHandler
    {
        NameValueCollection frm = null;
        string ValidToken = string.Empty;
        int respuesta1 = 0;
        int respuesta2 = 0;
        int respuesta3 = 0;
        int respuesta4 = 0;
        int respuesta5 = 0;
        int respuesta6 = 0;
        string comentarios = string.Empty;
        const string quote = "\"";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();

            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {

                    DataSet dsResult;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();
                    SqlParameter ParamRespuesta1 = new SqlParameter();
                    ParamRespuesta1.ParameterName = "@Respuesta1";
                    ParamRespuesta1.SqlDbType = SqlDbType.Int;
                    ParamRespuesta1.Value = respuesta1;
                    pArray.Add(ParamRespuesta1);
                    SqlParameter ParamRespuesta2 = new SqlParameter();
                    ParamRespuesta2.ParameterName = "@Respuesta2";
                    ParamRespuesta2.SqlDbType = SqlDbType.Int;
                    ParamRespuesta2.Value = respuesta2;
                    pArray.Add(ParamRespuesta2);
                    SqlParameter ParamRespuesta3 = new SqlParameter();
                    ParamRespuesta3.ParameterName = "@Respuesta3";
                    ParamRespuesta3.SqlDbType = SqlDbType.Int;
                    ParamRespuesta3.Value = respuesta3;
                    pArray.Add(ParamRespuesta3);
                    SqlParameter ParamRespuesta4 = new SqlParameter();
                    ParamRespuesta4.ParameterName = "@Respuesta4";
                    ParamRespuesta4.SqlDbType = SqlDbType.Int;
                    ParamRespuesta4.Value = respuesta4;
                    pArray.Add(ParamRespuesta4);
                    SqlParameter ParamRespuesta5 = new SqlParameter();
                    ParamRespuesta5.ParameterName = "@Respuesta5";
                    ParamRespuesta5.SqlDbType = SqlDbType.Int;
                    ParamRespuesta5.Value = respuesta5;
                    pArray.Add(ParamRespuesta5);
                    SqlParameter ParamRespuesta6 = new SqlParameter();
                    ParamRespuesta6.ParameterName = "@Respuesta6";
                    ParamRespuesta6.SqlDbType = SqlDbType.BigInt;
                    ParamRespuesta6.Value = respuesta6;
                    pArray.Add(ParamRespuesta6);
                    SqlParameter ParamComentarios = new SqlParameter();
                    ParamComentarios.ParameterName = "@Comentarios";
                    ParamComentarios.SqlDbType = SqlDbType.Text;
                    ParamComentarios.Value = comentarios;
                    pArray.Add(ParamComentarios);
                    SqlParameter ParamToken = new SqlParameter();
                    ParamToken.ParameterName = "@token";
                    ParamToken.SqlDbType = SqlDbType.NVarChar;
                    ParamToken.Value = ValidToken;
                    pArray.Add(ParamToken);
                    dsResult = objData.ExecuteSelectSP("usp_wsInsertaRespuesta", pArray);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            dsResult.Tables[0].TableName = "data";
                        }
                        else
                        {
                            dsResult.Tables[0].TableName = "error_message";
                        }
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            if (dsResult.Tables[0].Rows.Count == 1)
                            {
                                json = json.Replace("[", "");
                                json = json.Replace("]", "");
                            }
                            json = json.Remove(json.Length - 1);
                            json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "survey" + quote + " }";
                        }
                        else
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "datos invalidos" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "survey" + quote + "}";
                        }
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "survey" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "survey" + quote + "}";
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            respuesta1 = Convert.ToInt32(frm["respuesta1"]);
            respuesta2 = Convert.ToInt32(frm["respuesta2"]);
            respuesta3 = Convert.ToInt32(frm["respuesta3"]);
            respuesta4 = Convert.ToInt32(frm["respuesta4"]);
            respuesta5 = Convert.ToInt32(frm["respuesta5"]);
            respuesta6 = Convert.ToInt32(frm["respuesta6"]);
            comentarios = frm["comentarios"];
            ValidToken = frm["token"];
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}