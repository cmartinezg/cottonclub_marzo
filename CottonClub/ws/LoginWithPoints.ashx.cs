﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub.ws
{
    /// <summary>
    /// Descripción breve de LoginWithPoints
    /// </summary>
    public class LoginWithPoints : IHttpHandler
    {
        NameValueCollection frm = null;
        string pin = string.Empty;
        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            LeerCampos();
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();


            ArrayList pArray = new ArrayList();
            SqlParameter ParamPin = new SqlParameter();
            ParamPin.ParameterName = "@pin";
            ParamPin.SqlDbType = SqlDbType.NVarChar;
            ParamPin.Value = pin;
            pArray.Add(ParamPin);
            dsResult = objData.ExecuteSelectSP("usp_wsLoginWithPoints", pArray);
            string json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
            if (dsResult.Tables[0] != null)
            {
                const string quote = "\"";
                if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                {
                    if (dsResult.Tables[0].Rows.Count == 1)
                    {
                        json = json.Replace("[", "");
                        json = json.Replace("]", "");
                    }
                    json = json.Remove(json.Length - 1);
                    json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "LoginWithPoints" + quote + " }";
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "usuario y contraseña invalidos" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "LoginWithPoints" + quote + "}";
                }
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            pin = frm["pin"];
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}