﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub
{
    /// <summary>
    /// Summary description for Gamification1
    /// </summary>
    public class Gamification1 : IHttpHandler
    {
        NameValueCollection frm = null;
        string guid = string.Empty;
        string ValidToken = string.Empty;
        const string quote = "\"";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();
            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {
                    DataSet dsResult;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();
                    SqlParameter ParamGUID= new SqlParameter();
                    ParamGUID.ParameterName = "@parametro";
                    ParamGUID.SqlDbType = SqlDbType.NVarChar;
                    ParamGUID.Value = guid;
                    pArray.Add(ParamGUID);
                    SqlParameter ParamToken = new SqlParameter();
                    ParamToken.ParameterName = "@token";
                    ParamToken.SqlDbType = SqlDbType.NVarChar;
                    ParamToken.Value = ValidToken;
                    pArray.Add(ParamToken);
                    dsResult = objData.ExecuteSelectSP("usp_wsSelectGamification", pArray);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            dsResult.Tables[0].TableName = "data";
                        }
                        else
                        {
                            dsResult.Tables[0].TableName = "error_message";
                        }
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
                    if (dsResult.Tables[0] != null)
                    {
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            if (dsResult.Tables[0].Rows.Count == 1)
                            {
                                json = json.Replace("[", "");
                                json = json.Replace("]", "");
                            }
                            json = json.Remove(json.Length - 1);
                            json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "gamification" + quote + " }";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "1")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "datos invalidos" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "003" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "gamification" + quote + "}";
                        }
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "2")
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "¡Lo sentimos! Ya contentaste esta pregunta" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "004" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "gamification" + quote + "}";
                        }
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "gamification" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "gamification" + quote + "}";
            }
            context.Response.Write(json);
        }
        private void LeerCampos()
        {
            guid = frm["guid"];
            ValidToken = frm["token"];
        }
                private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}