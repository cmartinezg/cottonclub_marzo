﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub
{
    /// <summary>
    /// Summary description for Diary1
    /// </summary>
    public class Diary1 : IHttpHandler
    {
        NameValueCollection frm = null;
        string id = string.Empty;
        string ValidToken = string.Empty;
        string detail = string.Empty;
        const string quote = "\"";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();
            
            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {

                    DataSet dsResult;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();
                    if (id == "")
                    {
                        SqlParameter ParamUsername = new SqlParameter();
                        ParamUsername.ParameterName = "@parametro";
                        ParamUsername.SqlDbType = SqlDbType.Int;
                        ParamUsername.Value = 0;
                        pArray.Add(ParamUsername);
                        SqlParameter ParamDetail = new SqlParameter();
                        ParamDetail.ParameterName = "@detail";
                        ParamDetail.SqlDbType = SqlDbType.Int;
                        ParamDetail.Value = detail;
                        pArray.Add(ParamDetail);
                        dsResult = objData.ExecuteSelectSP("usp_wsSelectDiary", pArray);
                    }
                    else
                    {
                        SqlParameter ParamUsername = new SqlParameter();
                        ParamUsername.ParameterName = "@parametro";
                        ParamUsername.SqlDbType = SqlDbType.Int;
                        ParamUsername.Value = Convert.ToInt32(id);
                        pArray.Add(ParamUsername);
                        SqlParameter ParamDetail = new SqlParameter();
                        ParamDetail.ParameterName = "@detail";
                        ParamDetail.SqlDbType = SqlDbType.Int;
                        ParamDetail.Value = detail;
                        pArray.Add(ParamDetail);
                        dsResult = objData.ExecuteSelectSP("usp_wsSelectDiary", pArray);
                    }
                    if (dsResult.Tables[0] != null)
                    {
                        if(dsResult.Tables[0].Rows.Count>0)
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                            dsResult.Tables[0].TableName = "data";
                        }
                        else
                        {
                            dsResult.Tables[0].TableName = "error_message";
                        }
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
                    if (dsResult.Tables[0] != null)
                    {
                        if(dsResult.Tables[0].Rows.Count>0)
                        if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                        {
                                /*
                            if(dsResult.Tables[0].Rows.Count==1)
                            {
                                json = json.Replace("[", "");
                                json = json.Replace("]", "");
                            }
                            */
                            json = json.Remove(json.Length - 1);
                            json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "diary" + quote + " }";
                        }
                        else
                        {
                            //json = json.Remove(json.Length -1);
                            json = "{" + quote + "error_message" + quote + ": [" + quote + "datos invalidos" + quote + "]";
                            json = json + "," + quote +
                                    "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "diary" + quote + "}";
                        }
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "diary" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "diary" + quote + "}";
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            id = frm["id"];
            ValidToken = frm["token"];
            detail = frm["detail"];
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
                SqlParameter ParamToken = new SqlParameter();
                ParamToken.ParameterName = "@token";
                ParamToken.SqlDbType = SqlDbType.NVarChar;
                ParamToken.Value = token;
                pArray.Add(ParamToken);
                dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}