﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CottonClub.ws
{
    /// <summary>
    /// Summary description for Calculadora
    /// </summary>
    public class Calculadora : IHttpHandler
    {
        NameValueCollection frm = null;
        string ValidToken = string.Empty;
        const string quote = "\"";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            string json = "";
            LeerCampos();

            if (ValidToken != null)
            {
                int ResultToken = ValidaToken(ValidToken);
                if (ResultToken != 0)
                {
                    DataSet dsResult;
                    CottonClub.Data objData = new Data();
                    ArrayList pArray = new ArrayList();

                    dsResult = objData.ExecuteSelectSP("usp_ws_CalcCatalogos", pArray);

                    if(dsResult.Tables.Count > 0)
                    {
                        dsResult.Tables[0].TableName = "Incidencias";
                        dsResult.Tables[1].TableName = "IncidenciasProductos";
                        dsResult.Tables[2].TableName = "Productos";
                        dsResult.Tables[3].TableName = "ProductosPorcentaje";
                        dsResult.Tables[4].TableName = "Regalos";
                        dsResult.Tables[5].TableName = "Variedades";
                    }
                    json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.None);
                    int contador = 0;
                    if (dsResult.Tables[0] != null)
                    {
                        foreach (DataTable dt in dsResult.Tables)
                        {
                            if (dsResult.Tables[contador].Rows[0]["vacio"].ToString() == "0")
                            {
                                /*
                                    if (dt.Rows.Count == 1)
                                        {
                                            json = json.Replace("[", "");
                                            json = json.Replace("]", "");
                                        }
                                        */
                            }
                            if (dsResult.Tables[contador].Rows[0]["vacio"].ToString() == "1")
                            {
                                json = json.Replace("{\"vacio\":1}", "");
                            }
                            contador = contador + 1;
                        }
                        json = json.Remove(json.Length - 1);
                        json = "{ " + quote + "data" + quote + ": {" + json.Substring(1, json.Length - 1);
                        json += "}";
                        json = json + "," + quote + "success" + quote + ": " + quote + "true" + quote + "," + quote + "service" + quote + ": " + quote + "calculadora" + quote + " }";
                    }
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "el token ha caducado" + quote + "]";
                    json = json + "," + quote +
                            "error_code" + quote + " : " + quote + "001" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "calculadora" + quote + "}";
                }
            }
            else
            {
                //json = json.Remove(json.Length -1);
                json = "{" + quote + "error_message" + quote + ": [" + quote + "no se ha enviado token para validar" + quote + "]";
                json = json + "," + quote +
                        "error_code" + quote + " : " + quote + "002" + quote + ",  " + quote + "success" + quote + ": false,  " + quote + "service" + quote + ": " + quote + "calculadora" + quote + "}";
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            ValidToken = frm["token"];
        }

        private int ValidaToken(string token)
        {
            int Result = 0;
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            ArrayList pArray = new ArrayList();
            SqlParameter ParamToken = new SqlParameter();
            ParamToken.ParameterName = "@token";
            ParamToken.SqlDbType = SqlDbType.NVarChar;
            ParamToken.Value = token;
            pArray.Add(ParamToken);
            dsResult = objData.ExecuteSelectSP("usp_wsValidToken", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["Valido"].ToString() == "1")
                {
                    Result = 1;
                }
            }
            return Convert.ToInt32(Result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}