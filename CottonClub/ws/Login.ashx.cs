﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace CottonClub
{
    /// <summary>
    /// Summary description for Login
    /// </summary>
    public class Login : IHttpHandler
    {
        NameValueCollection frm = null;
        string username = string.Empty;
        string password = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            
            context.Response.ContentType = "application/json";
            frm = context.Request.Form;
            LeerCampos();
            DataSet dsResult;
            CottonClub.Data objData = new Data();
            SqlConnection objCnn = objData.Connection();


            ArrayList pArray = new ArrayList();
            SqlParameter ParamPasswordAux = new SqlParameter();
            ParamPasswordAux.ParameterName = "@username";
            ParamPasswordAux.SqlDbType = SqlDbType.NVarChar;
            ParamPasswordAux.Value = username;
            pArray.Add(ParamPasswordAux);
            dsResult = objData.ExecuteSelectSP("usp_wsSelectEscalarUserPassword", pArray);
            string strPasswordAux = dsResult.Tables[0].Rows[0]["Password"].ToString();
            string strDecryptedPwd = Encrypt.DecryptPassword(dsResult.Tables[0].Rows[0]["Password"].ToString(), username);
            if (strDecryptedPwd == password)
            {
                password = "true";
            }
            dsResult.Clear();
            pArray.Clear();
            SqlParameter ParamUsername = new SqlParameter();
            ParamUsername.ParameterName = "@username";
            ParamUsername.SqlDbType = SqlDbType.NVarChar;
            ParamUsername.Value = username;
            pArray.Add(ParamUsername);
            SqlParameter ParamPassword = new SqlParameter();
            ParamPassword.ParameterName = "@password";
            ParamPassword.SqlDbType = SqlDbType.NVarChar;
            ParamPassword.Value = password;
            pArray.Add(ParamPassword);
            dsResult = objData.ExecuteSelectSP("usp_wsSelectEscalarUser", pArray);
            if (dsResult.Tables[0] != null)
            {
                if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                {
                    dsResult.Tables[0].TableName = "data";
                }
                else {
                    dsResult.Tables[0].TableName = "error_message";
                }
            }
                    
            string json = JsonConvert.SerializeObject(dsResult, Newtonsoft.Json.Formatting.Indented);
            if (dsResult.Tables[0] != null)
            {
                const string quote = "\"";
                if (dsResult.Tables[0].Rows[0]["error_message"].ToString() == "0")
                {
                    if (dsResult.Tables[0].Rows.Count == 1)
                    {
                        json = json.Replace("[", "");
                        json = json.Replace("]", "");
                    }
                    json = json.Remove(json.Length -1);
                    json=json +"," + quote + "success" + quote +": " + quote + "true" + quote +"," + quote + "service" + quote +": " + quote + "Login" + quote +" }";
                }
                else
                {
                    //json = json.Remove(json.Length -1);
                    json = "{" + quote + "error_message" + quote + ": [" + quote + "usuario y contraseña invalidos" + quote + "]";
                    json = json +","+ quote +
                            "error_code" + quote + " : " + quote + "000" + quote + ",  " + quote + "success" + quote +": false,  " + quote + "service" + quote +": " + quote + "login" + quote +"}";
            }
            }
            context.Response.Write(json);
        }

        private void LeerCampos()
        {
            username = frm["txt_login_username"];
            password = frm["txt_login_pass"];
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}